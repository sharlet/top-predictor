<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);
/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);
/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');
/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code
define('ADS_IMG_PATH','upload/advertisement');
define('ADS_IMG_DELAY','1000');
define('ADVERTISEMENT_IMAGE', 'upload/advertisement/');
define('NEWS_IMAGE', 'upload/news/');
define('NEWS_WIDTH', '320');
define('NEWS_HEIGHT', '180');
define('AD_IMAGE_DIR', 'upload/advertisement/');
switch(ENVIRONMENT)
{
    case 'development':
        define( 'ROOT_PATH'         ,   $_SERVER['DOCUMENT_ROOT'].'/fantasy_dfs/' );
        define( 'PATH_URL'          ,   'http://'.$_SERVER['SERVER_NAME'].'/fantasy_dfs/' );
        /** FACEBOOK ID **/
        define( 'FACEBOOK_ID'       ,  '' );  
        /** GOOGLE+ ID **/
        define( 'GOOGLE_CLIENT_ID'      ,   '' );
        define( 'GOOGLE_API_KEY'        ,   '' );
        define('IMAGE_SERVER','local');
        
        define('IMAGE_PATH',        'http://'.$_SERVER['SERVER_NAME'].'/fantasy_dfs/');
        define('ROOT_PATH_UPLOADS', $_SERVER['DOCUMENT_ROOT'] . "/fantasy_dfs/");

        define('NODE_ADDR', 'http://localhost:3200');
        define('RECAPTCHA_PUBLIC_KEY', '6LcS2iUUAAAAAA1RPEK6xDIlfqzMOKs3Ni7V5Ubv');
        define('RECAPTCHA_SECRETE_KEY','6LcS2iUUAAAAAI0wFpd1m7fsmUoIOjMp51AibDJg');
        break;
    case 'testing':
    case 'staging':
        define( 'ROOT_PATH'         ,   $_SERVER['DOCUMENT_ROOT'].'/fantasy_dfs/' );
        define( 'PATH_URL'          ,   'http://'.$_SERVER['SERVER_NAME'].'/fantasy_dfs/' );
        define('IMAGE_SERVER','local');
        define('IMAGE_PATH',        'http://'.$_SERVER['SERVER_NAME'].'/fantasy_dfs/');
        define('ROOT_PATH_UPLOADS', $_SERVER['DOCUMENT_ROOT'] . "/fantasy_dfs/");
        define('RECAPTCHA_PUBLIC_KEY', '6LcS2iUUAAAAAA1RPEK6xDIlfqzMOKs3Ni7V5Ubv');
        define('RECAPTCHA_SECRETE_KEY','6LcS2iUUAAAAAI0wFpd1m7fsmUoIOjMp51AibDJg');
        define('FACEBOOK_ID'       ,  '' );  // FACEBOOK ID
        define('NODE_ADDR', '');
        define('GOOGLE_CLIENT_ID'      ,   '33799717353-qkmfjpluv1adu0ctd448m9ilcn95lccn.apps.googleusercontent.com' );
        define('GOOGLE_API_KEY'        ,   'AIzaSyAVwQjmaxv7tE37OvsjcC5x4RefY3uawwE' );
        
    break;       
    case 'production':
        define( 'ROOT_PATH'           ,   $_SERVER['DOCUMENT_ROOT'].'/' );
        define( 'PATH_URL'          ,   'https://'.$_SERVER['SERVER_NAME'].'/' );
         // FACEBOOK ID
        define( 'FACEBOOK_ID'       ,  '' ); 
         // GOOGLE+ ID
        define( 'GOOGLE_CLIENT_ID'      ,   '' );
        define( 'GOOGLE_API_KEY'        ,   '' );
        define('IMAGE_SERVER','local');
        define('IMAGE_PATH',        'https://'.$_SERVER['SERVER_NAME'].'/');
        define('ROOT_PATH_UPLOADS', $_SERVER['DOCUMENT_ROOT'] . "/");
        define('NODE_ADDR', '');  // NEED TO CHANGE WRT LIVE
        define('RECAPTCHA_PUBLIC_KEY', '');
        define('RECAPTCHA_SECRETE_KEY','' );
    break;
}
/* TABLE NAMES  28-Dec-15 6:18:07 PM */
define( 'ACTIVE_LOGIN', 'active_login' );
define( 'ADMIN', 'admin' );
define( 'ADMIN_ACTIVE_LOGIN', 'admin_active_login' );
define( 'ADMIN_ROLES', 'admin_roles' );
define( 'ADMIN_ROLES_RIGHTS', 'admin_roles_rights' );
define( 'ADS_MANAGEMENT', 'ad_management');
define( 'ADS_POSITION', 'ad_position');
define( 'BACKGROUND_PROCESS', 'background_process' );
define( 'CONTEST', 'contest' );
define( 'CONTEST_INVITE' , 'contest_invite');
define( 'CONTEST_OPT_MATCHES', 'contest_opt_matches' );
define( 'CONTEST_USER_LINEUP', 'contest_user_lineup' );
define( 'CONTEST_USER_PRIME', 'contest_user_prime' );
define( 'FEED_MATCH_STATISTICS', 'feed_match_statistics' );
define( 'FEED_PLAYER_NEWS', 'feed_player_news' );
define( 'FEED_PLAYER_STATISTICS', 'feed_player_statistics' );
define( 'FEED_SCORES_COMPUTATION', 'feed_scores_computation' );
define( 'LEAGUE', 'league' );
define( 'MASTER_CONTEST_ROSTER', 'master_contest_roster' );
define( 'MASTER_CONTEST_SCORING', 'master_contest_scoring' );
define( 'MASTER_COUNTRY', 'master_country' );
define( 'MASTER_DATA_ENTRY', 'master_data_entry' );
define( 'MASTER_PRIZE_PAYOUT', 'master_prize_payout' );
define( 'MASTER_SALARY_CAP', 'master_salary_cap' );
define( 'MASTER_SPORTS', 'master_sports' );
define( 'MASTER_STATE', 'master_state' );
define( 'MASTER_TBL_CONSTANT', 'master_tbl_constant' );
define( 'NOTIFICATION', 'notification' );
define( 'NOTIFICATION_TYPE', 'notification_type' );
define( 'PAYMENT_DEPOSIT_TRANSACTION', 'payment_deposit_transaction' );
define( 'PAYMENT_HISTORY_TRANSACTION', 'payment_history_transaction' );
define( 'PAYMENT_WITHDRAW_TRANSACTION', 'payment_withdraw_transaction' );
define( 'PLAYER', 'player' );
define( 'PLAYERS_RANK', 'players_rank');
define( 'PLAYER_DETAILS', 'player_details' );
define('PLAYER_NEWS'                            ,'player_news');
define('PLAYER_NEWS_DETAIL'                     ,'player_news_detail');
define( 'SEASON', 'season' );
define( 'SEASON_WEEK', 'season_week' );
define( 'TEAM', 'team' );
define( 'TEAM_DETAILS', 'team_details' );
define("VIDEOS" ,"videos");
define( 'USER', 'user' );
define( 'USER_FILTER', 'user_filter' );

/* TABLE NAMES  */

define("TRADE_TYPE"                             ,"FIRST,SECOND,THIRD");
define( 'IS_LOCAL_TIME' , TRUE );
define( 'BACK_YEAR', '0 month' );
define( 'PROJECT_NAME_FORMATED',             'Fantasy DFS' );
define( 'PROJECT_NAME_UNFORMATED',           'Fantasy DFS' );
define( 'PROJECT_NAME_SUPPORT' ,             'Fantasy DFS' );
define( 'PROJECT_NAME_WITHOUT_SPACE' ,       'FantasyDFS' );
/*Email info & SMTP */
define( 'SMTP_HOST'         ,   '' );
define( 'SMTP_USER'         ,   '' );
define( 'SMTP_PASS'         ,   '' );
define( 'SMTP_PORT'         ,   '' );
define( 'PROTOCOL'          ,   'smtp' );
define( 'SMTP_CRYPTO'       ,   'ssl' );
define( 'SMTP_FROM'         ,   'admin@fantasydfs.com');
define( 'FROM_ADMIN_EMAIL'  ,   'admin@fantasydfs.com' );


define( 'BCC_EMAIL'         ,   '' );
define( 'FROM_EMAIL_NAME'   ,   'Fantasy DFS' );
define( 'FROM_EMAIL_TITLE'  ,   'Fantasy DFS' );
define( 'NO_REPLY_EMAIL'    ,   'admin@fantasydfs.com' );
define( 'SUPPORT_EMAIL'     ,   'admin@fantasydfs.com' );
define( 'FEEBACK_EMAIL'     ,   'admin@fantasydfs.com' );
define( 'MAIL_TO_SUPPORT'   ,   'admin@fantasydfs.com' );
define( 'USER_TYPE' ,   'USER' );
define( 'ADMIN_TYPE' ,  'ADMIN' );
define("AUTH_KEY"       ,"session_key");
define("REMEMBER_ME_KEY","remember_me_key");
define("REMEMBER_ME_EXPIRY" ,35580000); // 1 year
define('PROFILE', 'upload/');
define('PROFILE_THUMB','upload/logo/');
define('TEAM_LOGO_THUMB','upload/team_logo/custom/');
define('PLAYER_SALARIES','upload/player_salaries/');
define('DEBIT', '1');
define('CREDIT', '2');
define('DEPOSIT', 'DEPOSIT');
define('TRANSACTION_HISTORY_DESCRIPTION_ENTRY_FEE', 'Entry fee for');
define('TRANSACTION_HISTORY_DESCRIPTION_DEPOSIT', 'Payment deposit');
define('TRANSACTION_HISTORY_DESCRIPTION_WITHDRAW', 'Payment withdraw');
define('USER_GAME_ACCESS_TYPE', 'PRIVATE');
// Login sessin key for logged in user
define("CURRENCY_CODE"  ,"$");
define("DEFAULT_SITE_RAKE"  , 10);
// define Lobby data page limit per page
define("LOBBY_RECORD_LIMIT" , 20);
define("LEADERBOARD_RECORD_LIMIT"   ,20);
define("MESSAGE_LIMIT"      , 10);
define('MYSQL_DATE_FORMAT'      , '%b %d, %Y %h:%i %p');
define('MYSQL_DATE_FORMAT_2'        , '%b %d, %Y');
define('PHP_DATE_FORMAT'        , 'M d, Y');
define('JS_DATE_FORMAT'     , 'M dd, yy');
define('PHP_DATE_TIME_FORMAT'       , 'M d, Y h :m A');
define("ROSTER_LIMIT"           , 30);
define('UPLOAD_DIR'             , 'upload/');
define('ROSTER_DIR'             , 'upload/rosters/');
define('FILE_UPLOAD_DIR'        , 'upload/xls_file');
define('ACTIVE'                 , '1');
define('UNCAPPED_GAME_SIZE'     , '-1');
define("GAMES_LIMIT"            , 30);
define("MAX_ROSTER_SIZE"        , 20);
define("MIN_ROSTER_SIZE"        , 9);
define("RECENT_TRANSACTION_LIMIT", 10);
define("ENTRY_FEE_VARY_BY", 50);
/*----------------PayPal Credintial--------------------*/
// sandbox or live
define("PAYPAL_MODE","sandbox");
define("CLIENT_ID","");
define("CLIENT_SECRET","");
/*-------------Admin Email----------------- */
// Change mail id to client mail id.
define("ADMIN_MAILID","admin@fantasydfs.com");
// OffSeason date will be calculated from Last date of 17th week ( Season week 17th close date) 06-Sep-16 2:51:24 PM
define('OFF_SEASON_START_DATE', ' +1 Day ');
define('OFF_SEASON_END_DATE', '+2 Month');
define('APP_CURRENT_YEAR', 2017);
define('MIN_WITHDRAW_AMOUNT', 100);