<?php 

/* Display List for positions */
$lang["master_positions"]["QB"]		= array('id'=>"QB"	,'display_text'=>'Quater Back') ;
$lang["master_positions"]["RB"]		= array('id'=>"RB"	,'display_text'=>'Running Back');
$lang["master_positions"]["WR"]		= array('id'=>"WR"	,'display_text'=>'Wide Receiver');
$lang["master_positions"]["TE"]		= array('id'=>"TE"	,'display_text'=>'Tight End');
$lang["master_positions"]["K"]		= array('id'=>"K"	,'display_text'=>'Kicker');
$lang["master_positions"]["DEF"]	= array('id'=>"DEF"	,'display_text'=>'Defense');
$lang["master_positions"]["BENCH"]	= array('id'=>"BN"	,'display_text'=>'Bench');
$lang["master_positions"]["FLEX"]	= array('id'=>"WR/TE",'display_text'=>'Flex', 'te' => "TE");

/* Master Constants */
$lang["master_constants"]["waiver_type"]["WAIVER_BUDGET"]			= "Waiver Budget";
$lang["master_constants"]["waiver_type"]["CONTINUOUS"]				= "Waiver Priority";
$lang["master_constants"]["waiver_mode"]["STANDARD"]				= "Standard";
$lang["master_constants"]["waiver_mode"]["CONTINUOUS"]				= "Continuous";
$lang["master_constants"]["trade_review"]["LEAGUE_VOTE"]			= "League Vote";
$lang["master_constants"]["trade_review"]["COMMISH_VOTE"]			= "Commish to Vote";
$lang["master_constants"]["scoring_type"]["ROTISSERIE"]				= "Rotisserie";
$lang["master_constants"]["scoring_type"]["H2H"]					= "H2H";
$lang["master_constants"]["scoring_type"]["POINTS"]					= "Points";
$lang["master_constants"]["roster_changes"]["TOMORROW"]				= "Tomorrow";
$lang["master_constants"]["roster_changes"]["WEEKLY"]				= "Weekly";
$lang["master_constants"]["post_draft_player"]["FREE_AGENT"]		= "Free Agent";
$lang["master_constants"]["post_draft_player"]["WAIVER"]			= "Waiver";
$lang["master_constants"]["H2H_division"]["1"]						= "Off";
$lang["master_constants"]["H2H_division"]["2"]						= "2 Division";
$lang["master_constants"]["game_type"]["STANDARD"]					= "Standard";
$lang["master_constants"]["game_type"]["KEEPER"]					= "Keeper";
$lang["master_constants"]["game_type"]["DYNASTY"]					= "Dynasty";
$lang["master_constants"]["game_invite_permission"]["COMMISH_ONLY"]	= "By Commish Only";
$lang["master_constants"]["game_invite_permission"]["ALL"]			= "By All Managers";
$lang["master_constants"]["game_draft"]["SNAKE_DRAFT"]				= "Snake Draft";
$lang["master_constants"]["game_draft"]["MANUAL_DRAFT"]				= "Offline";
$lang["master_constants"]["game_access_type"]["PUBLIC"]				= "Public";
$lang["master_constants"]["game_access_type"]["PRIVATE"]			= "Private";

/* Master Constants End */
/* Master scoring  26-Dec-16 4:24:16 PM 
	Only "points_allowed"  key will in Feed_player_statictics table instead of  0_points_allowed , 1_6_points_allowed etc
 */
$defense = array(
				"0_points_allowed"				=>'Points Allowed: 0',
				"1_6_points_allowed"			=>'Points Allowed: 1-6',
				"7_13_points_allowed"			=>'Points Allowed: 7-13',
				"14_20_points_allowed"			=>'Points Allowed: 14-20',
				"21_27_points_allowed"			=>'Points Allowed: 21-27',
				"28_34_points_allowed"			=>'Points Allowed: 28-34',
				"35_plus_points_allowed"		=>'Points Allowed: 35+',
				"sacks"							=> "Sack ",
				"interceptions"					=> "Interception Made",
				"fumbles_recovered"				=> "Fumble Recovery",
				"defensive_touchdown"			=> "Defensive Touchdown",
				"safety"						=>'Safety',				
				"blocked_kick"					=> "Blocked Kick",				
			);


$returning = array(
					"kick_return_touchdowns"	=> "Kick Returning Touchdown",
					"punt_returning_touchdown"	=> "Punt Returning Touchdown",
					"pat_returning_touchdown"	=> "PAT Returning Touchdown"
				);

$kicking = array(				
				"field_goal_0_29_yards"		=> "Field Goal 0-29 Yards",
				"field_goal_30_39_yards"	=> "Field Goal 30-39 Yards",
				"field_goal_40_49_yards"	=> "Field Goal 40-49 Yards",
				"field_goal_50_plus_yards"	=> "Field Goal 50+ Yards",				
				"made"						=> "PAT Made"
			);


$passing = array(				
				"yards"			=> "Passing Yards (25) ",
				"touchdowns"	=> "Passing Touchdown",
				"interceptions"	=> "Interception Thrown"
			);

$receiving	= array(
				"touchdowns"	=>  "Receiving Touchdown",
				"yards"			=>  "Receiving Yards (10)"
				);
$rushing = array(			
				"touchdowns"	=>  "Rushing Touchdown",
				"yards"			=>  "Rushing Yards (10)"				
			);
$other = array(
				"two_point_conversion"				=> "2 Point Conversion",	
				"fumble_lost"						=> "Fumble Lost",	
			);
$lang['scoring'] = array(
					"PASSING"			=> $passing,
					"RUSHING"			=> $rushing,
					"RECEIVING"			=> $receiving,
					"RETURNING"			=> $returning,
					"KICKING"			=> $kicking,
					"DEFENSE"			=> $defense,
					"OTHER"				=> $other,
			);

$lang['scoring_category'] = array(
					"PASSING"	=> 'PASSING',
					"RUSHING"	=> 'RUSHING',
					"RECEIVING"	=> 'RECEIVING',
					"RETURNING"	=> "RETURNING",
					"KICKING"	=> 'KICKING',
					"DEFENSE"	=> 'DEFENSE',
					"OTHER"	    => 'OTHERS',
			);
//------------------ Scoring Abbrivations---------------------
$defense_abbr = array(
				["points_allowed"			=>'PA'],
				["sacks"					=>"SACK"],
				["interceptions"			=>"INT"],
				["fumbles_recovered"		=>"FUM REC"],
				["defensive_touchdown"		=>"TD"],
				["safety"					=>"SAF"],	
				["blocked_kick"				=>"BLK KICK"],				
			);


$returning_abbr = array(
					["kick_return_touchdowns"	=> "KICK RET TD"],
					["punt_returning_touchdown"	=> "PUNT RET TD"],
					["pat_returning_touchdown"	=> "PAT RET TD"]
				);

$kicking_abbr = array(				
				["made"						=> "PAT"],
				["field_goal_0_29_yards"	=> "FG:0-29"],
				["field_goal_30_39_yards"	=> "FG:30-39"],
				["field_goal_40_49_yards"	=> "FG:40-49"],
				["field_goal_50_plus_yards"	=> "FG:50+"],				
				);


$passing_abbr = array(				
				["touchdowns"		=> "PAS TD"],
				["yards"			=> "PAS YDS"],
				["interceptions"	=> "INT"]
			);

$receiving_abbr	= array(
				["touchdowns"	=>  "REC TD"],
				["yards"		=>  "REC YDS"]
				);
$rushing_abbr = array(			
				["touchdowns"	=>  "RUS TD"],
				["yards"		=>  "RUS YDS"]				
			);
$other_abbr = array(
				["two_point_conversion"	=> "2PT"],	
				["fumble_lost"			=> "FUM LOST"],	
			);
$lang['scoring_abbr'] = array(
					"PASSING"			=> $passing_abbr,
					"RUSHING"			=> $rushing_abbr,
					"RECEIVING"			=> $receiving_abbr,
					"RETURNING"			=> $returning_abbr,
					"KICKING"			=> $kicking_abbr,
					"DEFENSE"			=> $defense_abbr,
					"OTHER"				=> $other_abbr,
			);

$lang['scoring_types'] = array(
						"QB" => array(
							["PASSING"		=>"PASSING"],
							["RUSHING"		=>"RUSHING"],
							["RECEIVING"	=>"RECEIVING"],
							// ["OTHERS"		=>"OTHERS"],
							),
						"WR" => array(
							["RECEIVING"	=>"RECEIVING"],
							["RUSHING"		=>"RUSHING"],
							// ["OTHERS"		=>"OTHERS"],
							),						
						"RB" => array(
							["RUSHING"		=>"RUSHING"],
							["RECEIVING"	=>"RECEIVING"],
							// ["OTHERS"		=>"OTHERS"],
							),						
						"TE" => array(
							["RECEIVING"	=>"RECEIVING"],
							["RUSHING"		=>"RUSHING"],
							// ["OTHERS"		=>"OTHERS"],
							),					
						"K" => array(
							["KICKING"	=>"KICKING"],						
							),
						"DEF" => array(
							["DEFENSE"	=>"DEFENSE"],						
							),									
	);



/*------------------- START multi language for player card ------------------------------*/

$PASSING= array(	
		['touchdowns'		=>	'TD'],
		['yards'			=>	'YARDS'] , 
		['interceptions'	=>	'INT'],
		//['FP'				=>	'FP'],		
);

$RECEIVING = array(
		['touchdowns'	=>	'TD'], 
		['yards'		=>	'YARDS'],
		//['FP'			=>	'FP'],	
);

$RUSHING = array(
		['touchdowns'	=>	'TD'], 
		['yards'		=>	'YARDS'],
		//['FP'			=>	'FP'],
);
								
$KICKING = array(
		['made'						=>	'PAT'], 
		['field_goal_0_29_yards'	=>	'FG:0-29'],
		['field_goal_30_39_yards'	=>	'FG:30-39'],
		['field_goal_40_49_yards'	=>	'FG:40-49'],
		['field_goal_50_plus_yards'	=>	'FG:50+'],
		//['FP'						=>	'FP'],
);

$DEFENSE = array(									 
		['point_allowed'		=>	'PA'], 
		['sacks'				=>	'SACK'],
		['interceptions'		=>	'INT'],
		['fumbles_recovered'	=>	'FUM REC'], 
		['defensive_touchdown'	=>	'TD'], 
		['safety'				=>	'SAF'], 
		['blocked_kick'			=>	'BLK KICK'], 
		//['FP'					=>	'FP'],
);

$RETURNING = array(
		["kick_return_touchdowns"	=> "KICK RET TD"],
		["punt_returning_touchdown"	=> "PUNT RET TD"],
		["pat_returning_touchdown"	=> "PAT RET TD"] 
		//['FP'						=>	'FP'],
);
																																			
				
$OTHER = array(	
		['two_point_conversion'	=>	'2PT'],
		['fumble_lost'			=>	'FUM LOST'],
		//['FP'					=>	'FP'],
);

$lang['scoring_card'] = array(
		"PASSING"			=> $PASSING,
		"RUSHING"			=> $RUSHING,
		"RECEIVING"			=> $RECEIVING,
		"RETURNING"			=> $RETURNING,
		"KICKING"			=> $KICKING,
		"DEFENSE"			=> $DEFENSE,
		"OTHER"				=> $OTHER,
);




$lang['stats_head'] = array(
					'QB' => array(
								['week'							=> 'WEEK'],
								['OPP'							=> 'OPP'],
								['PASSING_touchdowns'			=> 'PASSING <br> TD'],
								['PASSING_yards'				=> 'PASSING <br> YARDS'],
								['PASSING_interceptions'		=> 'PASSING <br> INT'],
								['RUSHING_touchdowns'			=> 'RUSHING <br> TD'],
								['RUSHING_yards'				=> 'RUSHING <br> YARDS'],
								['OTHER_two_point_conversion'	=> '2PT <br> CONV'],
								['OTHER_fumble_lost'			=> 'FUMBLE <br> LOST'],
								['FP'							=> 'FANTASY <br> POINTS']
					                        
					),
					'RB' => array(
								['week'							=> 'WEEK'],
								['OPP'							=> 'OPP'],
								['RECEIVING_touchdowns'			=> 'RECEIVING <br> TD'],
								['RECEIVING_yards'				=> 'RECEIVING <br> YARDS'],
								['RUSHING_touchdowns'			=> 'RUSHING <br> TD'],
								['RUSHING_yards'				=> 'RUSHING <br> YARDS'],
								['OTHER_two_point_conversion'	=> '2PT <br> CONV'],
								['OTHER_fumble_lost'			=> 'FUMBLE <br> LOST'],
								['FP'							=> 'FANTASY <br> POINTS']
					                        
					),
					'WR' => array(
								['week'							=> 'WEEK'],
								['OPP'							=> 'OPP'],
								['RECEIVING_touchdowns'			=> 'RECEIVING <br> TD'],
								['RECEIVING_yards'				=> 'RECEIVING <br> YARDS'],
								['RUSHING_touchdowns'			=> 'RUSHING <br> TD'],
								['RUSHING_yards'				=> 'RUSHING <br> YARDS'],
								['OTHER_two_point_conversion'	=> '2PT <br> CONV'],
								['OTHER_fumble_lost'			=> 'FUMBLE <br> LOST'],
								['FP'							=> 'FANTASY <br> POINTS']
					                        
					),
					'TE' => array(
								['week'							=> 'WEEK'],
								['OPP'							=> 'OPP'],
								['RECEIVING_touchdowns'			=> 'RECEIVING <br> TD'],
								['RECEIVING_yards'				=> 'RECEIVING <br> YARDS'],
								['RUSHING_touchdowns'			=> 'RUSHING <br> TD'],
								['RUSHING_yards'				=> 'RUSHING <br> YARDS'],
								['OTHER_two_point_conversion'	=> '2PT <br> CONV'],
								['OTHER_fumble_lost'			=> 'FUMBLE <br> LOST'],
								['FP'							=> 'FANTASY <br> POINTS']
					                        
					),
					'K' => array(
								['week'								=> 'WEEK'],
								['OPP'								=> 'OPP'],
								['KICKING_made'						=> ' PAT'],
								['KICKING_field_goal_0_29_yards'	=> ' FG:0-29'],
								['KICKING_field_goal_30_39_yards'	=> ' FG:30-39'],
								['KICKING_field_goal_40_49_yards'	=> ' FG:40-49'],
								['KICKING_field_goal_50_plus_yards'	=> ' FG:50+'],
								['FP'								=> 'FANTASY <br> POINTS']
					                        
					),
					'DEF' => array(
								['week'							=> 'WEEK'],
								['OPP'							=> 'OPP'],
								['DEFENSE_point_allowed'		=> ' PA'],
								['DEFENSE_sacks'				=> ' SACK'],
								['DEFENSE_interceptions'		=> ' INT'],
								['DEFENSE_fumbles_recovered'	=> ' FUM REC'],
								['DEFENSE_defensive_touchdown'	=> ' TD'],
								['DEFENSE_safety'				=> ' SAF'],
								['DEFENSE_blocked_kick'			=> ' BLK KICK'],
								['FP'							=> 'FANTASY <br> POINTS']
					                        
					)

	);

$lang['season_stats_head'] = array(

				'QB' => array(	
								'PASSING' => array(
													"yards"			=> "YARDS",
													"touchdowns"	=> "TD",
													"interceptions"	=> "INT"
												),
												
							),

				'RB'=>	array(
								'RUSHING' => array(
									'touchdowns' 			=> 'TD',
									'yards'					=> 'YARDS'
												),

								'OTHER' => array(
									'fumble_lost'			=> 'FL'
												),
							),
								
				'WR'=> 	array(
								'RECEIVING' => array(
									'touchdowns' 			=> 'TD',
									'yards'					=> 'YARDS'
													),
												
								'OTHER' => array(
									'fumble_lost'			=> 'FL'
												),
							),

				'TE'=> 	array(
								'RECEIVING' => array(	
									'touchdowns' 			=> 'TD',
									'yards'					=> 'YARDS'
													),
												
								'OTHER' => array(	
									'fumble_lost'				=> 'FL'
												),
							),
								
				'K'=> 	array(
								'KICKING' =>	array(				
									"field_goal_0_29_yards"		=> "FG",
									"field_goal_30_39_yards"	=> "FG",
									"field_goal_40_49_yards"	=> "FG",
									"field_goal_50_plus_yards"	=> "FG",				
									"made"						=> "PAT"
													)

							),

				'DEF'=> 	array(
								'DEFENSE'	=>  array(
									"0_points_allowed"				=>'PA',
									"1_6_points_allowed"			=>'PA',
									"7_13_points_allowed"			=>'PA',
									"14_20_points_allowed"			=>'PA',
									"21_27_points_allowed"			=>'PA',
									"28_34_points_allowed"			=>'PA',
									"35_plus_points_allowed"		=>'PA',
									"interceptions"					=> "INT",
									"fumbles_recovered"				=> "FUM REC",
									
													)
								)

			);



/*------------------- START multi language for player card ------------------------------*/
