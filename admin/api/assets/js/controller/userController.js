'use strict';

angular.module("vfantasy").controller('UserCtrl', ['$scope', '$timeout', '$rootScope', '$stateParams','settings', 'dataSavingHttp', 'showServerError', '$location','$state', function($scope, $timeout, $rootScope, $stateParams,settings, dataSavingHttp, showServerError, $location,$state){

	$rootScope.settings.layout.pageBodyFullWidth = true;
	
	$scope.userList		= [];
	$scope.isLoading	= false;
	
	$scope.userObj					= {};
	$scope.userObj.user_unique_id	= [];
	$scope.userObj.salary			= {};
	$scope.userObj.action			= "";
	$scope.user_unique_id			= {};

	$scope.transParam				= {};
	$scope.transParam.items_perpage	= 10;
	$scope.transParam.total_items	= 0;
	$scope.transParam.current_page	= 1;
	$scope.transParam.sort_order	= 'DESC';
	$scope.transParam.sort_field	= 'created_date';
	$scope.istransLoading			= false;
	$scope.userTransactions			= [];

	$scope.gameParam				= {};
	$scope.gameParam.items_perpage	= 10;
	$scope.gameParam.total_items	= 0;
	$scope.gameParam.current_page	= 1;
	$scope.gameParam.sort_order		= 'DESC';
	$scope.gameParam.sort_field		= 'season_scheduled_date';
	$scope.gameList					= [];
	$scope.isgameLoading			= false;

	$scope.userBalance = {};
	$scope.userBalance.transaction_type = "CREDIT"

	$scope.country_list = [];
	$scope.keyword;

	$scope.min_balance_value     = 0;
	$scope.max_balance_value     = 0;
	$scope.user_id 				 = "";

	/* Start User History Section */

	$scope.initObject = function(filter) {
		
		$scope.userParam				= {}
		$scope.userParam.items_perpage = 10;
		$scope.userParam.total_items   = 0;
		$scope.userParam.current_page  = 1;
		$scope.userParam.sort_order    = 'DESC';
		$scope.userParam.sort_field    = 'added_date';
		$scope.userParam.frombalance;
		$scope.userParam.tobalance;
		$scope.userParam.country       = "";
		$scope.userParam.keyword       = "";
		$scope.max_balance             = 100;
		$scope.min_balance             = 0;
		if(filter=='clear')
		{
			$scope.getFilterData();
			$location.search($scope.userParam);
		}
		else
		{
			var query_data = $location.search();
			$scope.userParam = angular.extend($scope.userParam, query_data);
		}
		$timeout(function(){
			angular.element("#country").select2("val", $scope.userParam.country);
		}, 1000);
		$scope.getUserList();
	};

	$scope.getFilterData = function() {
		dataSavingHttp({
			url: site_url+"user/get_filter_data",
			data: {}
		}).success(function (response) {
			$scope.country_list				= response.data.country;
			$scope.max_balance_value		= response.data.max_value;
			$scope.min_balance_value		= response.data.min_value;

			$scope.userParam.tobalance		= response.data.max_value;
			$scope.userParam.frombalance	= response.data.min_value;
		});
	};

	$scope.getUserList = function() {
		if($scope.isLoading) return;
		$scope.isLoading = true;
		$location.search($scope.userParam);

		if(Number($scope.userParam.frombalance) > Number($scope.userParam.tobalance))
		{
			$rootScope.alert_error	= $rootScope.lang.BALANCE_GREATER_FROM_TO;
			return false;
		}
		dataSavingHttp({
			url: site_url+"users",
			data: $scope.userParam,
		}).success(function (response) {
			$scope.userList            = response.data.result;
			$scope.userParam.total_items = response.data.total;
			deselectUser();
			$scope.isLoading = false;
		}).error(function (error) {
			$scope.isLoading = false;
		});
	};

	$scope.sortUesrList = function(sort_field) {
		if($scope.isLoading) return;
		var sort_order = 'DESC';
		if(sort_field==$scope.userParam.sort_field){
			sort_order = ($scope.userParam.sort_order=='DESC')?'ASC':'DESC';
		}
		$scope.userParam.sort_field  = sort_field;
		$scope.userParam.sort_order  = sort_order;
		$scope.userParam.total_items  = 0;
		$scope.userParam.current_page = 1;
		$scope.getUserList();
		deselectUser();
	};

	$scope.searchByEmailOrName = function() {
		var textLen = $scope.userParam.keyword.length;
		if(textLen<2 && textLen!==0)
		{
			return;
		}

		if($scope.isLoading)
		{
			$scope.userParam.keyword = $scope.keyword;
			return;
		}
		$scope.keyword = $scope.userParam.keyword;
		$scope.getUserList();
	};

	$scope.filterBalance = function(event, ui) {
		$scope.userParam.frombalance	= ui.values[0];
		$scope.userParam.tobalance		= ui.values[1];
		$scope.getUserList();
	};

	$scope.getUserDetail = function(user_id,index) {
		if($scope.isLoading) return;
		$scope.isLoading = true;
		dataSavingHttp({
			url: site_url+"get_user_detail",
			data: {'user_unique_id':user_id},
		}).success(function (response) {
			angular.element("#add_balance_modal").modal('show');
			$scope.userDetail		= response.data;
			$scope.userDetail.index	= index;
			$scope.isLoading		= false;
		}).error(function (error) {
			$scope.isLoading = false;
		});
	};

	$scope.addUserBalance = function(index) {
		$rootScope.current_loader = '.btn-primary';
		if($scope.isLoading) return;
		$scope.isLoading = true;
		$scope.userBalance.user_unique_id = $scope.userDetail.user_unique_id;
		dataSavingHttp({
			url: site_url+"user/add_user_balance",
			data: $scope.userBalance,
		}).success(function (response) {
			$scope.userList[$scope.userDetail.index].balance		= response.data.balance;
			$rootScope.alert_success			= response.message;
			$scope.userBalance					= {};
			$scope.userBalance.transaction_type	= "CREDIT";

			angular.element("#add_balance_modal").modal('hide');
			$scope.isLoading	= false;

			// update range filter and url params
			$scope.getFilterData();
			$timeout(function() {
				$location.search($scope.userParam);
			}, 1000);
			
		}).error(function (error) {
			$rootScope.alert_error	= error.message;
			$scope.isLoading = false;
		});
	};

	$scope.banUser = function() {
		$rootScope.current_loader = '.btn-primary';
		if($scope.isLoading) return;
		$scope.isLoading = true;
		dataSavingHttp({
			url: site_url+"user/change_user_status",
			data: {'user_unique_id':$scope.userObj.user_unique_id,'reason':$scope.userObj.reason,'status': 3,'user_id': $scope.user_id},
		}).success(function (response) {
			$rootScope.alert_success	= response.message;
			$scope.userList[$scope.userObj.index].status = 3;
			$scope.userObj.reason = "";
			$('#confirm_modal').modal('hide');
			angular.element("#ban_user_modal").modal('hide');
			$scope.isLoading	= false;
		}).error(function (error) {
			$rootScope.alert_success	= error.message;
			$('#confirm_modal').modal('hide');
			$scope.isLoading = false;
		});
	};
	/* End User History Section */

	/* Start User Detail Section */
	$scope.getUserDetailByID = function() {
		if($scope.isLoading) return;
		$scope.isLoading = true;
		dataSavingHttp({
			url: site_url+"get_user_detail",
			data: {'user_unique_id':$stateParams.user_id},
		}).success(function (response) {
			$scope.userDetail	= response.data;
			$scope.userTransactionHistory();
			$scope.getGameList();
			$scope.isLoading	= false;
		}).error(function (error) {
			$scope.isLoading = false;
		});	
	};

	$scope.userTransactionHistory = function() {
		if($scope.istransLoading) return;
		$scope.istransLoading = true;
		$scope.transParam.user_id = $scope.userDetail.user_id;
		dataSavingHttp({
			url: site_url+"user/user_transaction_history",
			data: $scope.transParam,
		}).success(function (response) {
			$scope.userTransactions    = response.data.result;
			$scope.transParam.total_items = response.data.total;
			$scope.istransLoading = false;
		}).error(function (error) {
			$scope.istransLoading = false;
		});
	};

	$scope.sortUserTransaction = function(sort_field) {
		if($scope.istransLoading) return;
		var sort_order = 'DESC';
		if(sort_field==$scope.transParam.sort_field){
			sort_order = ($scope.transParam.sort_order=='DESC')?'ASC':'DESC';
		}
		$scope.transParam.sort_field  = sort_field;
		$scope.transParam.sort_order  = sort_order;
		$scope.transParam.total_items  = 0;
		$scope.transParam.current_page = 1;
		$scope.userTransactionHistory();
	};

	$scope.getGameList = function() {	
		
		if($scope.isgameLoading) return;
		$scope.isgameLoading = true;
		$scope.gameParam.user_id = $scope.userDetail.user_id;
		dataSavingHttp({
			url: site_url+"user/user_game_history/",
			data: $scope.gameParam,
		}).success(function (response) {
			$scope.gameList            = response.data.result;
			$scope.gameParam.total_items = response.data.total;
			$scope.isgameLoading = false;
		}).error(function (error) {
			$scope.isgameLoading = false;
		});
	};

	$scope.sortGameList = function(sort_field) {
		if($scope.isgameLoading) return;
		var sort_order = 'DESC';
		if(sort_field==$scope.gameParam.sort_field){
			sort_order = ($scope.gameParam.sort_order=='DESC')?'ASC':'DESC';
		}
		$scope.gameParam.sort_field  = sort_field;
		$scope.gameParam.sort_order  = sort_order;
		$scope.gameParam.total_items  = 0;
		$scope.gameParam.current_page = 1;
		$scope.getGamelist();
	};

	$scope.goToUserList = function()
	{
		$state.go('user');
	}

	/* Start Extra Function */
	$scope.toggleUser = function($event){
		$scope.userObj.user_unique_id = [];
		$scope.user_unique_id	= $scope.user_unique_id = Array.apply(null, Array($scope.userList.length)).map(function() { return $event.target.checked; });
		if($event.target.checked){
			for (var i = 0; i < $scope.userList.length; i++) {
				$scope.userObj.user_unique_id.push($scope.userList[i].user_unique_id);
			};
		}
		$rootScope.updateUi();
	};

	var deselectUser = function() {
		$scope.user_unique_id = {};
		$scope.userObj.user_unique_id = [];
		$scope.userObj.selectall = false;
		$rootScope.updateUi();
	};

	$scope.selectUser = function($event) {

		var user_unique_id = $event.target.value;
		var index = $scope.userObj.user_unique_id.indexOf(user_unique_id);
		
		($event.target.checked)?$scope.userObj.user_unique_id.push(user_unique_id):$scope.userObj.user_unique_id.splice(index, 1);

		if($scope.userObj.user_unique_id.length==$scope.userList.length)
		{
			$scope.userObj.selectall = true;
		}
		else
		{
			$scope.userObj.selectall = false;
		}
		$rootScope.updateUi();
	};

	$scope.showBanUserModel = function(user_id,index) {
		$scope.userObj.reason = "";
		$scope.userObj.user_unique_id = user_id;
		$scope.userObj.index = index;
		$scope.user_id = $scope.userList[index].user_id;
		angular.element("#ban_user_modal").modal('show');
	};

	$scope.openBlockConfirm = function()
	{
		console.log($scope.user_id);
		$('#confirm_modal').modal('show');
	}

	$scope.showActivateUserConfirm = function(user_id,index)
	{
		$scope.userObj.reason			= "";
		$scope.userObj.user_unique_id	= user_id;
		$scope.userObj.index			= index;
		$scope.user_id					= $scope.userList[index].user_id;
		$('#activate_user_confirm_modal').modal('show');	
	}

	$scope.activateUser = function()
	{
		$rootScope.current_loader = '.btn-primary';
		if($scope.isLoading) return;
		$scope.isLoading = true;

		dataSavingHttp({
			url: site_url+"user/change_user_status",
			data: {'user_unique_id':$scope.userObj.user_unique_id,'status': 1,'user_id': $scope.user_id},
		}).success(function (response) 
		{
			$rootScope.alert_success						= response.message;
			$scope.userList[$scope.userObj.index].status	= 1;
			$scope.userObj.reason							= "";
			$('#activate_user_confirm_modal').modal('hide');
			$scope.isLoading	= false;
		}).error(function (error) {
			$rootScope.alert_success	= error.message;
			$('#activate_user_confirm_modal').modal('hide');
			$scope.isLoading = false;
		});

	}
	/* End Extra Function*/	
}]);