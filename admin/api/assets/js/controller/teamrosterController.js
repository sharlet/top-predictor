'use strict';

angular.module("vfantasy").controller('TeamrosterCtrl', ['$scope', '$timeout', '$state', '$rootScope', 'settings', 'dataSavingHttp', '$location', function($scope, $timeout, $state, $rootScope, settings, dataSavingHttp, $location){

	$rootScope.settings.layout.pageBodyFullWidth = true;

	$scope.player_unique_id		= {};
	$scope.teamrosterList			= [];
	$scope.isLoading			= false;
	$scope.leagues = [];

	$scope.getAllLeague = function() {
		dataSavingHttp({
			url: site_url+"teamroster/get_all_league"
		}).success(function (response) {
			if(response.status)
			{
				var query_data = $location.search();
				$scope.leagues = response.data;
				$scope.teamrosterParam.league_id = (query_data.league_id)?query_data.league_id:response.data[0].league_id;
				$timeout(function(){
					angular.element("#league_id").select2("val", $scope.teamrosterParam.league_id);
				});
				$scope.teamrosterParam = angular.extend($scope.teamrosterParam, query_data);
				$location.search($scope.teamrosterParam);
				$scope.getTeamList();
			}
		});
	};
	$scope.filterResult = function() {
		$scope.teamrosterParam.total_items		= 0;
		$scope.teamrosterParam.current_page		= 1;
		$scope.teamrosterParam.sort_order		= "ASC";
		$scope.teamrosterParam.sort_field		= "team_name";
		$timeout(function(){
			angular.element("#league_id").select2("val", $scope.teamrosterParam.league_id);
		});
		$scope.getTeamList();
	};

	$scope.getTeamList = function() {
		if($scope.isLoading) return;
		$scope.isLoading = true;

		$location.search($scope.teamrosterParam);
		dataSavingHttp({
			url: site_url+"teamroster/get_all_teamrosters",
			data: $scope.teamrosterParam,
		}).success(function (response) {
			$scope.teamrosterList				= response.data.result;
			$scope.teamrosterParam.total_items	= response.data.total;
			$scope.isLoading					= false;
		}).error(function (error) {
			$scope.isLoading = false;
		});
	};

	$scope.sortTeamrosterList = function(sort_field) {
		if($scope.isLoading) return;
		var sort_order = 'DESC';
		if(sort_field==$scope.teamrosterParam.sort_field) {
			sort_order = ($scope.teamrosterParam.sort_order=='DESC')?'ASC':'DESC';
		}
		$scope.teamrosterParam.sort_field	= sort_field;
		$scope.teamrosterParam.sort_order	= sort_order;
		$scope.teamrosterParam.total_items	= 0;
		$scope.teamrosterParam.current_page	= 1;
		$scope.getTeamList();
	};

	$scope.initObject = function() {
		$scope.teamrosterParam					= {};
		$scope.teamrosterParam.items_perpage	= 10;
		$scope.teamrosterParam.total_items		= 0;
		$scope.teamrosterParam.current_page		= 1;
		$scope.teamrosterParam.sort_order		= "ASC";
		$scope.teamrosterParam.sort_field		= "team_name";
		$scope.teamrosterParam.team_id			= "";
		$scope.teamrosterParam.league_id		= "";
		$scope.teamrosterParam.lang 			= "en";
		$timeout(function(){
			angular.element("#league_id").select2("val", $scope.teamrosterParam.league_id);
		});		
	};
	$scope.clearFilter = function() {
		$scope.initObject();
		angular.element("#league_id").select2("val", $scope.leagues[0].league_id);
		$scope.teamrosterParam.league_id = $scope.leagues[0].league_id;
		$scope.getTeamList();
	};
	$scope.initObject();

	$scope.add_team_data = {};
    $scope.showAddTeamPopup = function() {
            $scope.add_team_data = {};

            $scope.isLoading = true;
            angular.element("#add_statistics_modal").modal('show');
            $scope.isLoading = false;
    		//console.log($scope.edit_team_data);
    };

    $scope.AddTeam = function()
    {
    	$rootScope.current_loader = '.btn-success';		
		$scope.isLoading = true;	
		console.log($scope.add_team_data);	
		$scope.add_team_data.league_id = $scope.teamrosterParam.league_id;
		$scope.add_team_data.team_abbr = $scope.add_team_data.team_abbr_lable_en;
		console.log($scope.add_team_data);	
		dataSavingHttp({
			url: site_url+"stats_feed/create_team_stats",
			data: $scope.add_team_data,
		}).success(function (response) {
			$scope.isLoading = false;
			
			$rootScope.alert_success = response.message;
                        
                        // Reste statistics modal
                        $scope.add_team_data= {};                        
                        angular.element("#add_statistics_modal").modal('hide');
						//$location.path('/contest');
                        $timeout(function(){
                                $state.reload();
                        }, 500);                       
		}).error(function(error) {
			$rootScope.alert_error = error.message;
		});
    }

    $scope.doBlur = function($element){
    		angular.element("#"+$element).trigger('blur');
	}



	$scope.edit_team_data = {};
    $scope.showEditTeamPopup = function(team_data) {
            $scope.edit_team_data = {};

            $scope.isLoading = true;
    		
				var reqData = {
		            'league_id'         : $scope.teamrosterParam.league_id,
		            'team_abbr'    		: team_data['team_abbr'],
		        };
				dataSavingHttp({
					url: site_url+"teamroster/get_team_data",
					data: reqData,
				}).success(function (response) {
					$scope.isLoading = false;
					$scope.edit_team_data = response.data;
					angular.element("#edit_statistics_modal").modal('show');
					$rootScope.updateUi();
		        }).error(function(error) {
	                
				});
			//console.log($scope.edit_team_data);
    };

    $scope.UpdateTeam = function() {
            
		$rootScope.current_loader = '.btn-success';		
		$scope.isLoading = true;	
		$scope.edit_team_data.league_id = $scope.teamrosterParam.league_id;	
		dataSavingHttp({
			url: site_url+"stats_feed/edit_team_stats",
			data: $scope.edit_team_data,
		}).success(function (response) {
			$scope.isLoading = false;
			
			$rootScope.alert_success = response.message;
                        
                        // Reste statistics modal
                        $scope.edit_team_data= {};                        
                        angular.element("#edit_statistics_modal").modal('hide');
						//$location.path('/contest');
                        $timeout(function(){
                                $state.reload();
                        }, 500);                       
		}).error(function(error) {
			$rootScope.alert_error = error.message;
		});
	};



}]);