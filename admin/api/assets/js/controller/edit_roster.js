angular.module("vfantasy").controller('editRosterCtrl', ['$scope', '$stateParams', '$location', '$state','$timeout','$rootScope', 'settings', 'dataSavingHttp', 'showServerError','$filter', '$compile', function ($scope, $stateParams, $location, $state, $timeout, $rootScope, settings, dataSavingHttp, showServerError,$filter, $compile) {
	$scope.game_unique_id				= $stateParams.game_unique_id;	
	var rosterLoading                 = false;
	$scope.rosterParam					= {};
	$scope.rosterParam.offset			= 0;
	$scope.rosterParam.position			= 'all';
	$scope.rosterParam.search_text		= '';
	$scope.rosterParam.order_by			= 'rank_number';
	$scope.rosterParam.order_sequence	= 'ASC';
	$scope.rosterParam.game_unique_id	= $stateParams.game_unique_id;
	$scope.draftedPlrObj              = {};
	$scope.myLineupPlayer             = {};
	$scope.teamLineupPlayer           = {};		
	$scope.myLineupDetails            = []; 		
	$scope.activeTab                  = 'all';		
	$scope.selectedTeamLineupId = "";	


 	$scope.getDraftingDetail = function()
 	{ 	
 		var post =  {
						url		: site_url+"CommishTool/edit_roster_detail",
						data	: { game_unique_id:$scope.game_unique_id }
					};
 		dataSavingHttp(post).success(function (response)
		{
			console.log(response,"edit roster details");
			$scope.gameDetail         = response.data.game_detail;						
			$scope.tabPosition        = response.data.tab_position;			
			//$scope.loginLinupMasterID = response.data.login_lineup_id;			

			$rootScope.playerImgInfo = response.data.player_img_path_info;
			$scope.teamList          = response.data.team_list;
			$scope.getUserTeamRoster($scope.teamList[0]);
			$timeout(function(){
				$('#active_'+$scope.teamList[0].lineup_master_id).click();	
			})
			
			$scope.selectedPlayer    = (typeof response.data.selected_players_id != "undefined") ? response.data.selected_players_id : [];			
			$scope.positionList		= response.data.position_list;
			$scope.myLineupPlayer	= response.data.lineup_detail;

		}).error(function (error)
		{
			$rootScope.alert_error = error.Message;
			$state.go('commish-tool', {'game_unique_id' : $scope.game_unique_id});
			return;
		});	
 	};

	

 	// function to get list of roster by team change
 	$scope.getUserTeamRoster = function(team)
	{
		$scope.selectedTeamLineupId = team.lineup_master_id;
		var post =  {
						url		: site_url+"CommishTool/get_team_roster_lineup",
						data	: {game_unique_id:$stateParams.game_unique_id,lineup_master_id:team.lineup_master_id}
					};
		dataSavingHttp(post).success(function (response)
		{
			$scope.myLineupDetails = response.data.lineup_detail;			
			$scope.teamDetail = response.data.team_detail;			
		}).error(function (error)
		{});	 
	};

 	var is_req_send = false;
	$scope.addPlayer = function(addPlayerObj)
 	{ 
 		$('.loader').show();
 		var draftedPlrObj = {
									game_unique_id			:$scope.game_unique_id,									
									player_unique_id		:addPlayerObj.player_unique_id,
									lineup_master_id		:$scope.selectedTeamLineupId,
									// teamLineupMasterId		:$scope.teamLineupMasterId,
									position				:addPlayerObj.position									
								};
		if (is_req_send) {
			return;
		}						
 		is_req_send = true;
 		angular.element('.tooltip-inner').html('').parent().hide();

 		var post =  {
						url		: site_url+"CommishTool/add_player",
						data	: draftedPlrObj
					};
 		dataSavingHttp(post).success(function (response)
		{
			console.log(response);
			reqSent = false;
			$('.loader').hide();
			is_req_send = false;						
			$scope.myLineupDetails   = response.data.lineup_detail;	
			$scope.rosterParam.offset  = 0;
			$scope.getPlayerRoster();		
			$('#Player_Card_Popup').modal('hide');
		}).error(function (error)
		{
			console.log(error);
			$('.loader').hide();
			is_req_send = false;
			angular.element('.clock-loader').hide();
			$rootScope.alert_error = error.message;
		});		
 	};

 	$scope.getDraftingDetail();
 
 	$scope.getPlayerRoster = function()
	{
		if(!$scope.playerRosterIsLoadMore&&$scope.rosterParam.offset!=0)return;
		if(rosterLoading)return;
		rosterLoading = true;

		$('.loader').show();
		// $scope.rosterParam.order_by = 'rank_number';
		$scope.rosterParam.show_drafted_player = true;
		// console.log(" Player roster param ", $scope.rosterParam);
		var post =  {
						url		: site_url+"CommishTool/get_all_roster_list",
						data	: $scope.rosterParam
					};
		dataSavingHttp(post).success(function (response)
		{
			$('.loader').hide();
			$scope.playerRosterIsLoadMore = response.data.is_more_list;
			if($scope.rosterParam.offset == 0 )
			{
				$scope.playerList = [];
				$scope.playerList = response.data.player_list;
			}
			else
			{
				$scope.playerList = $scope.playerList.concat(response.data.player_list);
			}
			$scope.rosterParam.offset = $scope.playerList.length;
			rosterLoading = false;
		}).error(function (error)
		{
			rosterLoading = false;
		});	
	};

	

	$scope.orderByRoster = function(order_by)
	{
		if($scope.rosterParam.order_by == order_by)
		{
			$scope.rosterParam.order_sequence = ($scope.rosterParam.order_sequence =='ASC')?'DESC':'ASC';
		}

		$scope.rosterParam.order_by = order_by;
		$scope.rosterParam.offset  = 0;
		$scope.getPlayerRoster();	
	}


 	$scope.searchbyposition = function(tab)
	{
		if(tab)
		{
			$scope.activeTab            = tab.position;
			$scope.rosterParam.position = tab.allowed_positions;
		}
		else
		{
			$scope.activeTab = 'all';
			$scope.rosterParam.position = 'all';
		}
		$scope.rosterParam.offset = 0;
		$scope.getPlayerRoster();
	};

	$scope.searchByName = function()
	{
		setTimeout(function(){
			var t = $.trim($scope.rosterParam.search_text);
			if(t.length <= 2 && t.length != 0)
			{
				return false;
			}
			else
			{
				$scope.rosterParam.search_text = t ;
				$scope.rosterParam.offset = 0;
			}
			$scope.getPlayerRoster();
		}, 500);		
	};


	$scope.downloadReports = function ()
	{
		var PostData = {game_unique_id: $stateParams.game_unique_id,lineup_master_id: $scope.lineUpMasterID};
		var post =  {
						url		: site_url+"team_draft/download_player_reports",
						data	: PostData
					};
		dataSavingHttp(post).success(function (response)
		{
			var anchor = angular.element('<a/>');
			anchor.css({display: 'none'}); 
			angular.element(document.body).append(anchor);
			anchor.attr({
				href: 'data:attachment/csv;charset=utf-8,' + encodeURI(response),
				target: '_blank',
				download: 'Game_Report' + $stateParams.game_unique_id + '.csv'
			})[0].click();
			anchor.remove();
		},function (error)
		{});	
	};
	
	$scope.removeTeamRoster = function(player)
	{		
		var PostData = {lineup_id: player[player.display_position].lineup_id,lineup_master_id: $scope.selectedTeamLineupId,game_unique_id: $stateParams.game_unique_id,player_unique_id: player[player.display_position].player_unique_id};
		$scope.myLineupDetails = {};

		var post =  {
						url		: site_url+"CommishTool/remove_team_roster",
						data	: PostData
					};
		dataSavingHttp(post).success(function (response)
		{
			$scope.myLineupDetails = response.data.lineup_detail;
			$scope.rosterParam.offset  = 0;
			$scope.getPlayerRoster();
		}).error(function (error)
		{});	
	};
	$scope.goToContest = function()
	{
		$state.go('contest');
	}
	$scope.goToLineup = function()
	{
		$state.go('lineup-detail', {'game_unique_id' : $scope.game_unique_id});
		return;
	}

	$scope.goToCommishnerTool = function()
	{
		$state.go('commish-tool', {'game_unique_id' : $scope.game_unique_id});
		return;
	}

 	$scope.initThirdPartFun = function()
 	{
		svg4everybody();
        $('.queue-block > ul').width($('.queue-block > ul > li').length * $('.queue-block > ul > li').width());
        $('select').select2({
            minimumResultsForSearch: -1
        });
        $(document).on('click', '[data-toggle="buttons-checkbox"]', function() {
            $(this).button('toggle');
        });

        $("#sliderTab").owlCarousel({
            itemsDesktop: [1025, 3]
        });
        $('.tabHeader-sm a[data-toggle="tab"]').on('show.bs.tab', function(e) {
            $('.tabContent').removeClass('active');
            $(e.target).parents('.nav-tabs').find('a[data-toggle="tab"]').removeClass('active');
            $(e.target).addClass('active');
        });
        $('.mobile-tabs li a[data-toggle="tab"]').on('show.bs.tab', function(e) {

            $('#mobiChat[aria-expanded="true"]').collapse('hide');
            $('.mobileContent').removeClass('active');
            $(e.target).parents('.mobile-tabs').find('a[data-toggle="tab"]').removeClass('active');
            $(e.target).addClass('active');
        });
        $(document).on('click', '.convertDropdown>a', function(event) {
            event.preventDefault();
            $(this).next('.nav-tabs').toggleClass('active');
        });
        $(document).on('click', function(event) {
            if (!$(event.target).is('.convertDropdown, .convertDropdown *')) {
                $('.convertDropdown').find('.nav-tabs.active').removeClass('active');
            }
        });
 	};
 	$timeout(function(){
 		$scope.initThirdPartFun();
 		$scope.getPlayerRoster();
 	},500); 

}]);