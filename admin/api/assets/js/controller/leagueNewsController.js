angular.module("vfantasy").controller('leagueNewsCtrl', leagueNewsCtrl);
leagueNewsCtrl.$inject = ['$scope', '$stateParams', '$location', '$state','$timeout','$rootScope', 'settings', 'dataSavingHttp', 'showServerError','$filter'];

function leagueNewsCtrl($scope, $stateParams, $location, $state, $timeout, $rootScope, settings, dataSavingHttp, showServerError,$filter)
{

	var LN = this;
 
	LN.leagueNewsObj = {};
	LN.leagueNewsObj.news_image					= "";
	LN.leagueNewsObj.player_unique_id			= "";
	LN.leagueNewsObj.news_desc					= "";
	LN.leagueNewsObj.news_short_desc			= "";
	LN.leagueNewsObj.spanish_news_desc			= "";
	LN.leagueNewsObj.spanish_news_short_desc	= "";
	LN.leagueNewsObj.spanish_news_title			= "";
	LN.leagueNewsObj.news_title					= "";
	LN.leagueNewsObj.league_type				= "";
	
	LN.is_show_figure = false;
	LN.display_file_name = false;
	LN.isEditMode = false;
	/* Function declaration */
	LN.get_master_leagues = get_master_leagues;
	LN.create_league_news = create_league_news;
	LN.intializeImageLoad = intializeImageLoad;
	LN.getLeagueNewsDetail = getLeagueNewsDetail;

	$scope.Leagues = [];
	$scope.newsList = [];
	$scope.leaguesParam = {};
	$scope.teams			= [];
	$scope.rosters = {};
	$scope.players = [];

	function get_master_leagues()
	{
		var post = { url : site_url+"league_news/get_all_league" };
 		dataSavingHttp(post).success(function (response)
		{
			LN.leagueType = response.data;
				if($state.current.name == 'edit_news' && $stateParams.news_id != undefined && $stateParams.news_id != '') {
					LN.getLeagueNewsDetail();
					

					LN.isEditMode = true;
				}

		}).error(function (error)
		{
			LN.leagueType = [];
		});
	}

	$scope.getAllLeague = function() {
		dataSavingHttp({
			url: site_url+"league_news/get_all_league"
		}).success(function (response) {
			if(response.status)
			{
				$scope.Leagues = response.data;
			}
		});
	};

	$scope.getAllTeam = function(reset_param) {
		if(!LN.leagueNewsObj.league_type) return;

		if(!reset_param)
		{
			LN.leagueNewsObj.team_abbr = '';
			//$location.search('team_abbr', null);
			//$location.search('position', null);
		}

		dataSavingHttp({
			url: site_url+"league_news/get_all_team",
			data: {league_id:LN.leagueNewsObj.league_type}
		}).success(function (response) {
			if(response.status)
			{
				$scope.teams = response.data;
				$timeout(function(){
					angular.element("#team_abbr").select2("val", LN.leagueNewsObj.team_abbr);
				});
				//$scope.getAllPosition();
			}
		});
	};

	$scope.getAllRoster = function(reset_param) {
		if(!LN.leagueNewsObj.league_type) return;

		if(!reset_param)
		{
			LN.leagueNewsObj.team_abbr = '';
			
			//$location.search('team_abbr', null);
			//$location.search('position', null);
		}

		dataSavingHttp({
			url: site_url+"league_news/get_all_roster",
			data: {league_id:LN.leagueNewsObj.league_type}
		}).success(function (response) {
			if(response.status)
			{
				$scope.rosters = response.data;
				$scope.getRoster(false);
			}
		});
	};

	$scope.getRoster = function(reset_param) {
		//console.log(LN.leagueNewsObj.team_abbr);
		if($scope.rosters[LN.leagueNewsObj.team_abbr])
		{
			$scope.players = $scope.rosters[LN.leagueNewsObj.team_abbr];
			if(LN.leagueNewsObj.player_unique_id && !reset_param)
			{
				$timeout(function(){
					angular.element("#players").select2("val", LN.leagueNewsObj.player_unique_id);
				});
			}
			else
			{
				$timeout(function(){
					LN.leagueNewsObj.player_unique_id = '';
					angular.element("#players").select2("val",'');
				});
			}
		}
		else
		{
			$scope.players = [];
			LN.leagueNewsObj.player_unique_id = '';
			angular.element("#players").select2("val",'');
		}
	};



	function create_league_news()
	{
		

			tinymce.triggerSave();
		LN.leagueNewsObj.spanish_news_desc	= $( "#spanish_news_desc" ).val();
		LN.leagueNewsObj.news_desc	= $( "#news_desc" ).val();

		if(LN.leagueNewsObj.spanish_news_desc == "")
		{
			angular.element('#spanish_news_desc_error').empty().append($rootScope.lang['spanish_news_desc_required']);
			angular.element('#spanish_news_desc_error').show();
			angular.element('#spanish_news_desc_error').removeClass('hide');
			return false;
		}

		if(LN.leagueNewsObj.news_desc == "")
		{
			angular.element('#news_desc_error').empty().append($rootScope.lang['spanish_news_desc_required']);
			angular.element('#news_desc_error').show();
			angular.element('#news_desc_error').removeClass('hide');
			return false;
		}

		

		if(LN.leagueNewsObj.news_image == "" && LN.leagueNewsObj.page_type == 2)
		{
			angular.element('#news_image_error').empty().append($rootScope.lang['news_image_required']);
			angular.element('#news_image_error').show();
			angular.element('#news_image_error').removeClass('hide');
			return false;
		}

		var post = { url : site_url+"League_news/create_league_news" , data : LN.leagueNewsObj};
 		dataSavingHttp(post).success(function (response)
		{
			
			angular.element('#league_type').select2('val','');
			LN.leagueNewsObj = {};	
			LN.is_show_figure = false;
			$rootScope.alert_success = response.message;
			if(LN.isEditMode){
				$state.go('news_list');
			}
		}).error(function (error)
		{
			if(error.hasOwnProperty('error'))
			{
				showServerError(error.error);
			}
			else
			{
				$rootScope.alert_error = error.Message;
			}
		});
	}

	function intializeImageLoad(){
		angular.element('#myInput').remove();
		var btnupload = $('#news_image');
		new AjaxUpload(btnupload, {
			action: site_url + 'League_news/upload_custom_logo',
			name: 'news_image',
			responseType: 'json',
			data: {field_name: 'news_image'},
			onSubmit: function(file, ext) {	

				if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
					$rootScope.alert_error = "Invalid image extension.";					
					$scope.$apply();
					return false;
				}
				var fileUpload = document.getElementById("myInput");

		        if (typeof (fileUpload.files) != "undefined") 
		        {
		            var size = parseFloat(fileUpload.files[0].size / 1024 / 1024).toFixed(2);
		            if(size > 4)
		            {
		            	$rootScope.alert_error = "Please upload image of size less than 1000X1024 and less than 4MB."
		            }
		        }
		        else 
		        {
		            $rootScope.alert_error = "This browser does not support HTML5.";
		        }
		        //angular.element('#image-loader').removeClass('hidden');
			},
			onComplete: function(file, response) {
				var output = response.data;				
				/*if (response.ResponseCode == 200)
				{															
					angular.element(".news_image").attr('src', output.image);
					angular.element('#news_image_text').text(output.file_name);
					//LN.display_file_name = true;
					LN.leagueNewsObj.news_image = output.file_name;
					LN.is_show_figure = true;

					angular.element('#news_image_error').empty();
					angular.element('#news_image_error').hide();

					$scope.$apply();
				}
				else
				{
					$rootScope.alert_error = response.Error;
					$scope.$apply();
				}*/

				if (response.status == true)
				{													
					angular.element(".news_image").attr('src', response.data);
					angular.element('#news_image_text').text(response.file_name);
					//LN.display_file_name = true;
					LN.leagueNewsObj.news_image = response.file_name;
					LN.is_show_figure = true;

					angular.element('#news_image_error').empty();
					angular.element('#news_image_error').hide();

					$scope.$apply();
				}
				else
				{
					$rootScope.alert_error = response.message;
					$scope.$apply();
				}
			}
		});
	}


	

	$scope.getAllNews = function()
	{
		$location.search($scope.newsParam);
		dataSavingHttp({
			url: site_url+"league_news/get_all_league_news",
			data:$scope.newsParam,
		}).success(function (response) {
			if(response.status)
			{
				$scope.newsList = response.data.list;
				$scope.newsParam.total_items	= response.data.total;
				$scope.isLoading				= false;
			}
		});
	}

	$scope.filterResult = function() 
	{
		$scope.newsParam.items_perpage		= 10;
		$scope.newsParam.total_items		= 0;
		$scope.newsParam.current_page		= 1;
		$scope.newsParam.sort_order			= "DESC";
		$scope.newsParam.sort_field			= "created_date";
		$timeout(function(){
			angular.element("#team_id").select2("val", $scope.newsParam.league_id);
		});
		$scope.getAllNews();
	};

	$scope.initObject = function() 
	{
		$scope.newsParam					= {};
		$scope.newsParam.items_perpage		= 10;
		$scope.newsParam.total_items		= 0;
		$scope.newsParam.current_page		= 1;
		$scope.newsParam.sort_order			= "DESC";
		$scope.newsParam.sort_field			= "created_date";
		$scope.newsParam.league_id			= "";
		$scope.newsParam.language_id		= "en";
	};

	$scope.sortNewsList = function(sort_field) {
		if($scope.isLoading) return;
		var sort_order = 'DESC';
		if(sort_field==$scope.newsParam.sort_field){
			sort_order = ($scope.newsParam.sort_order=='DESC')?'ASC':'DESC';
		}
		$scope.newsParam.sort_field	= sort_field;
		$scope.newsParam.sort_order	= sort_order;
		$scope.newsParam.total_items	= 0;
		$scope.newsParam.current_page	= 1;
		$scope.getAllNews();
	};

	$scope.newDetails = {};
	$scope.get_new_details = function()
	{
		//if($stateParams.news_id=''){ $state.go('news_list'); };
		dataSavingHttp({
			url: site_url+"league_news/get_news_details",
			data:{news_id:$stateParams.news_id},
		}).success(function (response) {
			if(response.status)
			{
				$scope.newDetails = response.data;
			}
		});
	}

	//function for get news detail for edit section
	function getLeagueNewsDetail() {
		dataSavingHttp({
			url: site_url+"league_news/get_news_details",
			data:{news_id:$stateParams.news_id},
		}).success(function (response) {
			LN.leagueNewsObj.news_id 				 = $stateParams.news_id;
			LN.leagueNewsObj.league_type 			 = response.data.league_id;
			LN.leagueNewsObj.page_type 				 = response.data.page_type;
			LN.leagueNewsObj.news_title  			 = response.data.en_title;
			LN.leagueNewsObj.news_short_desc 		 = response.data.en_short_desc;
			LN.leagueNewsObj.spanish_news_title  	 = response.data.sp_title;
			LN.leagueNewsObj.spanish_news_short_desc = response.data.sp_short_desc;
			LN.leagueNewsObj.news_desc 	 			 = response.data.en_description;
			LN.leagueNewsObj.spanish_news_desc 	 	 = response.data.sp_description;
			LN.leagueNewsObj.spanish_news_desc 	 	 = response.data.sp_description;
			LN.leagueNewsObj.player_unique_id		 = response.data.player_unique_id;	
			LN.leagueNewsObj.team_abbr				 = response.data.team_id;
			$scope.getAllRoster(true);	
			$scope.getAllTeam(true);
			

			if(response.data.news_image != ''){
				angular.element(".news_image").attr('src',response.data.image);
				angular.element('#news_image_text').text(response.data.file_name);

				LN.leagueNewsObj.news_image = response.data.file_name;
				LN.is_show_figure = true;
				angular.element('#news_image_error').empty();
				angular.element('#news_image_error').hide();
			}
			$timeout(function(){
				angular.element('#league_type').select2('val',LN.leagueNewsObj.league_type);
				angular.element('#page_type').select2('val',LN.leagueNewsObj.page_type);
			},1000);
		}).error(function (error)
		{
			if(error.hasOwnProperty('message'))
			{
				$rootScope.alert_error = error.message;
			}
			else
			{
				showServerError(error);
			}
		});
	}

	//for manage league news sequence section
	$scope.newsSequenceParam = {};
	$scope.newsSequence = [];
	$scope.newsSequenceList = [];
	$scope.getAllLeagueForSequence = function(){
		dataSavingHttp({
			url: site_url+"league_news/get_all_league"
		}).success(function (response) {
			if(response.status)
			{
				$scope.Leagues = response.data;
				$scope.newsSequenceParam.league_id = $scope.Leagues[0].league_id;
				$timeout(function(){
					angular.element("#league_id").select2("val", $scope.newsSequenceParam.league_id);
				}); 

				$scope.getAllNewsForSequence();
			}
		});
	}

	$scope.getAllNewsForSequence = function(){
		if($scope.newsSequenceParam.league_id == undefined || $scope.newsSequenceParam.league_id == ''){
			return false;
		}
		dataSavingHttp({
			url: site_url+"league_news/get_all_league_news_for_sequence",
			data:$scope.newsSequenceParam,
		}).success(function (response) {
			if(response.status)
			{
				$scope.newsSequenceList = response.data.list;
				//$scope.newsParam.total_items	= response.data.total;
				$scope.isLoading				= false;
			}
		});
	}

	$scope.changeLeagueForSequence = function(){
		$scope.newsSequenceList = [];
		$scope.getAllNewsForSequence();
	}

	$scope.dragStart = function(e, ui)
    {
        ui.item.data('start', ui.item.index());
    }

	$scope.dragEnd = function(e, ui)
	{		
		var start = ui.item.data('start'),
            end   = ui.item.index();
        
        $scope.newsSequenceList.splice(end, 0, $scope.newsSequenceList.splice(start, 1)[0]);
        $scope.$apply();

    	var intialVal = 1;  
	  	//$scope.newsSequenceList.sequence = $scope.sequence;
	   
	  	// update player queue order on server	  
	  	//$scope.updateNewsSequence();	
    }

    $scope.updateNewsSequence = function(){
    	
    	if($scope.newsSequenceList.length == 0 || $scope.newsSequenceParam.league_id == undefined || $scope.newsSequenceParam.league_id == '') {
    		return false;
    	} 

		var updatedSequence = [];
		angular.forEach($scope.newsSequenceList, function(value, key) {
		  this.push(value.player_news_id);
		}, updatedSequence);

    	dataSavingHttp({
			url: site_url+"league_news/update_news_sequence",
			data:{league_id:$scope.newsSequenceParam.league_id,updated_sequence:updatedSequence},
		}).success(function (response) {
			if(response.hasOwnProperty('message'))
			{
				$rootScope.alert_success = response.message;	
			}
		}).error(function (error)
		{
			if(error.hasOwnProperty('message'))
			{
				$rootScope.alert_error = error.message;
			}
			else
			{
				showServerError(error);
			}
		});;
	}

	$scope.changeNewsStatus = function(news_id,news_status,data_index){
		var update_param ={status:news_status,news_id:news_id};
		dataSavingHttp({
			url: site_url+"league_news/update_news_status",
			data: update_param,
		}).success(function (response) {
			$rootScope.alert_success	= response.message;
			$scope.newsList[data_index].status = news_status;
			
		}).error(function(error){
			showServerError(error);
		});
	}

	$scope.doBlur = function($element){
		//console.log('id is '+$element);
		angular.element("#"+$element).trigger("blur");
	};

}
