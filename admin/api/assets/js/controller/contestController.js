'use strict';
angular.module("vfantasy").controller('ContestCtrl', ['$scope', '$stateParams', '$location', '$state','$timeout','$rootScope', 'settings', 'dataSavingHttp', 'showServerError','$filter', function($scope, $stateParams, $location, $state, $timeout, $rootScope, settings, dataSavingHttp, showServerError,$filter){

  $rootScope.settings.layout.pageBodyFullWidth = true;

  // Create Contest
  $scope.leagues = []
  $scope.weekMatches = []
  $scope.salaryCap = []
  $scope.contestParam = {
    optMatches     : [],
    leagueId       : '',
    contestName    : '',
    entryFee       : 0,
    prizePool      : 0,
    siteRake       : 10,
    size           : 0,
    prizePayoutId  : '',
    isFeaturedGame : false,
    salaryCap      : 100,
    customRewards : ''
  }

  $scope.entryFee = { min: 0 , max: 5000 }
  $scope.size     = { min: 0 , max: 0 }

  $scope.prizePayoutStructure = []
  $scope.prizeComposition = []

  $scope.prizePool = '';

   // Contest list
  $scope.loadingResult                 = false;

  $scope.contest_id = 0;
  $scope.contestListParam = {
    orderByField : 'created_date',
    sortOrder    : "DESC",
    itemsPerPage : 10,
    totalItems   : 0,
    currentPage  : 1
  };

  $scope.editObj = {
    showEditBtn  : false,
    editContestName : {}
  };

  $scope.deleteContestId = "";

  $scope.contestStatus = []

  $scope.getAllContestData = function() {

    let requestData = {
        url  : site_url+"contest/get_all_contest_data",
        data : {},
    }
    dataSavingHttp( requestData ).success( function( response ) {

      $scope.leagues = response.data.leagues;
      $scope.prizePayoutStructure = response.data.prize_pool
      $scope.salaryCap = response.data.salary_cap


      if( response.data.hasOwnProperty('master_data_entry') ) {
        angular.forEach( response.data.master_data_entry, function( v,k ) {
          if( v.data_desc == 'Entry_Fee' ) {
            $scope.entryFee.min = parseInt(v.admin_lower_limit);
            $scope.entryFee.max = parseInt(v.admin_upper_limit);
          } else if( v.data_desc == 'Size' ) {
            $scope.size.min = parseInt(v.user_lower_limit);
            $scope.size.max = parseInt(v.user_upper_limit);
          }
        });
      }
    });
  };


  $scope.OnLeagueChange = function() {
    $scope.getSeasonWeek();
  }

  $scope.getSeasonWeek = function() {
    const requestData = {
                         url: site_url+"contest/get_season_week",
                         data: { league_id : $scope.contestParam.leagueId },
                      }

    dataSavingHttp( requestData ).success(function (response) {
       $scope.master_season_week = response.data.season_week;
    });
  }

  $scope.getWeekMatch = function() {
    const requestData = {
      url: site_url + 'contest/get_week_matches',
      data: {week: $scope.contestParam.week, league_id: $scope.contestParam.leagueId }
    }
    dataSavingHttp( requestData ).success(function (response) {
      $scope.weekMatches = response.data.week_matches;
    });
  }

  $scope.optAllMatches = function() {
    if($scope.weekMatches.length == $scope.contestParam.optMatches.length) {
      $scope.contestParam.optMatches = []
    } else {
      let allSeasonId = []
      angular.forEach($scope.weekMatches, function(value, key) { allSeasonId.push(value.season_id) });
      $scope.contestParam.optMatches = allSeasonId
    }
  }

  $scope.toggleOptMatch = function(season_id){
    if ($scope.contestParam.optMatches.indexOf(season_id) < 0) {
      $scope.contestParam.optMatches.push(season_id);

    } else {
      var index = $scope.contestParam.optMatches.indexOf(season_id);
      $scope.contestParam.optMatches.splice(index, 1);
    }
  }

  $scope.calculateAbsolutePrizePool = function() {
    if($scope.contestParam.size == 0  || $scope.contestParam.entryFee == 0 ) {
      $scope.contestParam.prizePool = 0
    } else  {
      let prizePool = Number($scope.contestParam.size) * Number($scope.contestParam.entryFee)
      let siteRakeAmout = prizePool *   ( $scope.contestParam.siteRake / 100 )
      $scope.contestParam.prizePool  =  prizePool - siteRakeAmout
    }
    $scope.calculatePrizeDistributionAmount()
  }

  $scope.calculatePrizeDistributionAmount = function() {
    let composition = '';
    $scope.prizeComposition = []
    angular.forEach($scope.prizePayoutStructure, function(value) {
      if(value.master_prize_payout_id === $scope.contestParam.prizePayoutId) {
        composition = JSON.parse(value.composition)
      }
    })

    if(composition.length > 0) {

      composition.forEach(function(val, index) {
        computeAmountOnRank(val)
      });
    }
  }

  let computeAmountOnRank = function(separation) {
    if(isNaN(separation.max_place)){
      let  diviser = separation.max_place.split('/')[1]
      switch( parseInt(diviser) ) {
        case 2 :
          $scope.prizeComposition.push( `Top 50 % will get $${$scope.contestParam.prizePool}` )
        break;
      }

    } else {
      let rankMsg = 'Top '
      rankMsg += (separation.max_place == separation.min_place)? separation.max_place : separation.min_place +' - ' + separation.max_place
      let amount = $scope.contestParam.prizePool * ( separation.value / 100)
      amount = amount.toFixed(2)
      $scope.prizeComposition.push( `${rankMsg} will get ${amount}` )
    }
  }



  $scope.createContest = function() {

    console.log('Submit hit ');
    const type = 'error';
    let msg = '';
    if($scope.contestParam.leagueId == '' ) {
      msg = 'Please select league ';
      showMessage(type, msg);
      angular.element('#leagueId').focus();
      return false
    }

    if($scope.contestParam.contestName == '') {
      msg = 'Please enter contest name ';
      showMessage(type, msg);
      angular.element('#contestName').focus();
      return false
    }

    if($scope.contestParam.week == '') {
      msg = 'Please select week ';
      showMessage(type, msg);
      angular.element('#contestWeek').focus();
      return false
    }

    if($scope.contestParam.optMatches.length < 1 ) {
      msg = 'Please select at least two matches ';
      showMessage(type, msg);
      angular.element('#contestWeek').focus();
      return false
    }

    if($scope.contestParam.entryFee < 0  || $scope.contestParam.entryFee == '' ) {
      msg = 'Please enter valid entry fees ';
      showMessage(type, msg);
      angular.element('#entryFee').focus();
      return false;
    }

    if($scope.entryFee.min > $scope.contestParam.entryFee || $scope.entryFee.max < $scope.contestParam.entryFee ) {
      msg = `Please enter entry fees ranges ${$scope.entryFee.min}  to ${$scope.entryFee.max}  ` ;
      showMessage(type, msg);
      angular.element('#entryFee').focus();
      return false;
    }


    if($scope.contestParam.size < 0 ||  $scope.contestParam.size == '') {
      msg = 'Please enter valid contest size ';
      showMessage(type, msg);
      angular.element('#size').focus();
      return false
    }

    if($scope.contestParam.prizePool > 0 &&  $scope.contestParam.prizePayoutId == '' ) {
      msg = 'Please select prize payout ';
      showMessage(type, msg);
      angular.element('#prizePayout').focus();
      return false
    }

    $rootScope.current_loader = '.btn-success';

    let requestData = {
                url  : site_url+"contest/new_contest",
                data : $scope.contestParam,
              }
    dataSavingHttp(requestData).success(function (response) {
      showMessage('success', response.message);
      $timeout(function(){ $state.go('contest'); },100);

    }).error(function (error) {
      if(error.hasOwnProperty('error')) {
        showServerError(error);
      } else {
        $rootScope.alert_error = error.message;
      }
    });

  }

  $scope.editContest = function() {
    console.log('form submit', $scope.contestParam);



      let requestData = {
                url  : site_url+"contest/edit_contest",
                data : $scope.contestParam,
              }
    dataSavingHttp(requestData).success(function (response) {
      showMessage('success', response.message);
      $timeout(function(){ $state.go('contest'); },100);

    }).error(function (error) {
      if(error.hasOwnProperty('error')) {
        showServerError(error);
      } else {
        $rootScope.alert_error = error.message;
      }
    });


    return false;
  }


  $rootScope.updateUi();

    // get all Contest Filters
    $scope.getContestFilters = function()
    {
        $scope.leagues      = [];
        $scope.contestStatus  = [];
        dataSavingHttp({
            url: site_url+"contest/get_contest_filters"
        }).success(function (response) {
            $scope.leagues      = response.data.leagues;
            $scope.contestStatus       = response.data.contest_status;
        }).error(function (error) {
            if(error.hasOwnProperty('error'))
            {
                showServerError(error);
            }
        });
    }
    $scope.filterContestList = function()
    {
        $scope.contestListParam.totalItems    = 0;
        $scope.contestListParam.current_page   = 1;
        $scope.getContestList();
    }
    $scope.clearContestFilter = function()
    {
        $scope.contestListParam.league_id      = "";
        $scope.contestListParam.game_type      = "";
        $scope.contestListParam.scoring_type   = "";

        $scope.contestListParam.status         = "";
        $scope.contestListParam.totalItems = 0;
        $scope.contestListParam.currentPage   = 1;
        $timeout(function() {
            angular.element("#league_id").select2("val", "");
            angular.element("#game_type").select2("val", "");
            angular.element("#scoring_type").select2("val", "");
            angular.element("#game_draft").select2("val", "");
            angular.element("#game_status").select2("val", "");
        });
        $scope.getGames();
    }
    // get all Contest Filters
    $scope.getContestList = function()
    {
        if ($scope.loadingResult) { return; }
        $scope.loadingResult = true;

        const requestData = { url: site_url+"contest/contest_list", data: $scope.contestListParam }

        dataSavingHttp(requestData).success(function (response) {
            if(response.data.contest.length > 0)
            {
                $scope.contestList                 = response.data.contest;
                $scope.contestListParam.totalItems    = response.data.total;
                $scope.loadingResult            = false;
            }
            else
            {
                $scope.contestList = [];
                $scope.loadingResult = false;
            }
        }).error(function (error) {
            if(error.hasOwnProperty('error'))
            {
                showServerError(error);
                $scope.loadingResult = false;
            }
        });
    }
    // sorted by field
    $scope.sortGameList = function(field)
    {
        var sortOrder = 'DESC';
        if($scope.contestListParam.orderByField == field){
            sortOrder = ($scope.contestListParam.sortOrder=='DESC')?'ASC':'DESC';
        }
        $scope.contestListParam.totalItems    = 0;
        $scope.contestListParam.currentPage   = 1;
        $scope.contestListParam.orderByField = field;
        $scope.contestListParam.sortOrder = sortOrder;
        $scope.getContestList();
    }
    $scope.goToContest = function()
    {
        $state.go('contest');
    }




    $scope.getTeamPlayers = function(lineup_id)
    {
        if(lineup_id)
        {
            if($scope.LineupTeams[lineup_id][0].player_unique_id !== "")
            {
                $scope.LineupDetail = $scope.LineupTeams[lineup_id];
            }
            else
            {
                $scope.LineupDetail = [];
            }
        }
        else
        {
            $scope.LineupDetail = [];
        }
    }
    $scope.goToLineup = function()
    {
        $state.go('lineup-detail', {'game_unique_id' : $scope.game_id});
        return;
    }

    $scope.updateLeagueName = function(index)
    {
        var contest_id = $scope.editObj.showEditBtn;
        $scope.editObj.showEditBtn = false;
        dataSavingHttp({
            url: site_url+"contest/update_contest_name",
            data: {'contest_id' : contest_id, "contest_name" : $scope.editObj.edit_game_name[index]},
        }).success(function (response){
            $scope.contestList[index].contest_name = $scope.editObj.edit_game_name[index];
            $rootScope.alert_success = response.message;
        }).error(function(error){
            $rootScope.alert_error = error.message;
        });
    }
    $scope.openConfirmModal = function(gameObj) {
      $scope.delete_contest_id = gameObj.contest_id;
      $('#confirm_modal').modal('show');
    }

    $scope.deleteLeague = function() {

        dataSavingHttp({
            url: site_url+"contest/delete_league",
            data: {contest_id : $scope.delete_contest_id},
        }).success(function (response){
            var itemObj = $filter('filter')($scope.contestList, {'contest_id' : $scope.delete_contest_id})[0];
            var index   = $scope.contestList.indexOf(itemObj);
            $scope.contestList.splice(index,1);
            $scope.delete_contest_id = "";
            $('#confirm_modal').modal('hide');
            $rootScope.alert_success = response.message;
        }).error(function(error){
            $rootScope.alert_error = error.message;
            $scope.delete_contest_id = "";
            $('#confirm_modal').modal('hide');
        });
    }

    // get game unique id from state param
    if($stateParams.game_unique_id)
    {
        $scope.game_id = $stateParams.game_unique_id;
        dataSavingHttp({
            url: site_url+"contest/get_contest_detail",
            data: {contest_uid:$scope.game_id},
        }).success(function (response){
            $scope.contestDetail   = response.data.contest_detail;

            $scope.contestParam.contestName = $scope.contestDetail.contest_name;
            $scope.contestParam.size        = $scope.contestDetail.size;
            $scope.contestParam.siteRake    = $scope.contestDetail.site_rake;
            $scope.contestParam.entryFee    = $scope.contestDetail.entry_fee;
            $scope.contestParam.contest_id = $scope.contestDetail.contest_id;

        });
    }
    else
    {
        $scope.getContestFilters();
    }

}]);
