'use strict';

angular.module("vfantasy").controller('SeasonCtrl', ['$scope', '$timeout', '$state', '$rootScope', 'settings', 'dataSavingHttp', function($scope, $timeout, $state, $rootScope, settings, dataSavingHttp){

    $rootScope.settings.layout.pageBodyFullWidth = true;
    $scope.seasonStatsParam                 = {};
    $scope.seasonStatsParam.items_perpage   = 10;
    $scope.seasonStatsParam.total_items     = 0;
    $scope.seasonStatsParam.current_page    = 1;
    // $scope.seasonStatsParam.sort_order       = "DESC";
    // $scope.seasonStatsParam.sort_field       = "scheduled_date";
    $scope.seasonStatsParam.fromdate        = "";
    $scope.seasonStatsParam.todate          = "";
    $scope.seasonStatsParam.league_id       = 1;

    $scope.sortSeasonStatsList = function(sort_field) {

        if($scope.isLoading) return;
        var sort_order = 'DESC';

        if(sort_field==$scope.seasonStatsParam.sort_field){
            sort_order = ($scope.seasonStatsParam.sort_order=='DESC')?'ASC':'DESC';
        }
        $scope.seasonStatsParam.sort_field  = sort_field;
        $scope.seasonStatsParam.sort_order  = sort_order;
        $scope.seasonStatsParam.total_items  = 0;
        $scope.seasonStatsParam.current_page = 1;
        $scope.getSeasonStatsList();
    };

    /*******************************************
     ********* CUSTOM SCORE FEED START *********
     *******************************************/
    $scope.rules = [];
        
    $scope.filterStatsResult = function() {
        $scope.seasonStatsParam.items_perpage   = 10;
        $scope.seasonStatsParam.total_items = 0;
        $scope.seasonStatsParam.current_page    = 1;
        $scope.seasonStatsParam.sort_order  = "ASC";
        $scope.seasonStatsParam.sort_field  = "scoring_category_name"
                
        $timeout(function(){
            angular.element("#filter_team_id").select2("val", $scope.seasonStatsParam.team_id);
            angular.element("#filter_player_id").select2("val", $scope.seasonStatsParam.player_unique_id);
            angular.element("#filter_position").select2("val", $scope.seasonStatsParam.position);
        });
        $scope.getSeasonStatisticsFeedList();
    };

    $scope.initStatsFilterObject = function() {
        $scope.seasonStatsParam         = {};
        $scope.seasonStatsParam.items_perpage   = 10;
        $scope.seasonStatsParam.total_items = 0;
        $scope.seasonStatsParam.current_page    = 1;
        $scope.seasonStatsParam.sort_order  = "ASC";
        $scope.seasonStatsParam.sort_field  = "scoring_category_name";
        $scope.seasonStatsParam.team_id         = "";
        $scope.seasonStatsParam.player_unique_id= "";
        $scope.seasonStatsParam.position        = "";

        $timeout(function(){
            angular.element("#filter_team_id").select2("val", "");
            angular.element("#filter_player_id").select2("val", "");
            angular.element("#filter_position").select2("val", "");
        });
    };

    $scope.clearStatsFilter = function () {
        $scope.initStatsFilterObject();
                
        $scope.getSeasonStatisticsFeedList();
    };
    /* Start Season Listing Functions */
    $scope.getSeasonStatisticsFeedList = function() {
        if($scope.isLoading) return;
        $scope.isLoading = true;
                
                $scope.seasonStatsParam.sort_order      = "ASC";
                $scope.seasonStatsParam.sort_field      = "";
                
        $scope.seasonStatsParam.season_game_unique_id = $state.params.game_unique_id;
        $scope.seasonStatsParam.league_id = $state.params.league_id;
                
        dataSavingHttp({
            url: site_url+"Projected_stats_feed/get_season_statisctics_feed",
            data: $scope.seasonStatsParam,
        }).success(function (response) {
            $scope.season_stats         = response.data.result;
            $scope.summaries            = response.data.summary;
            $scope.seasonStatsParam.total_items = response.data.total;
            $scope.isLoading            = false;
        }).error(function (error) {
            $scope.isLoading = false;
        });
    };

        $scope.edit_stats_data = {};
        $scope.showEditStatisticsPopup = function(stats_data) {
            $scope.edit_stats_data = {};

            angular.element("#edit_statistics_modal").modal('show');
            $rootScope.updateUi();
            var edit_data = stats_data;
            $scope.edit_stats_data = edit_data;

            $timeout(function(){
                    angular.element("#team_id").select2("val", $scope.edit_stats_data.team_id);
                    angular.element("#player_id").select2("val", $scope.edit_stats_data.player_unique_id);
                    angular.element("#scoring_category").select2("val", $scope.edit_stats_data.scoring_type);
                    angular.element("#scoring_rule").select2("val", $scope.edit_stats_data.stats_key);
            }, 100);               
                
                //console.log($scope.edit_stats_data);
        };
        
        $scope.showAddStatisticsPopup = function() {
            
                // Reste all select box values
                angular.forEach($scope.statistics_data.statistics, function(value, key) {

                        if(value ) {
                                angular.element("#team_id_"+key).select2("val", "");
                                angular.element("#player_id_"+key).select2("val", "");
                                angular.element("#scoring_category_"+key).select2("val", "");
                                angular.element("#scoring_rule_"+key).select2("val", "");
                        }
                });
                
                $scope.statistics_data.statistics = [];
                
                $scope.AddStatisticRow();
                
        angular.element("#add_statistics_modal").modal('show');
        $rootScope.updateUi();
                //$scope.getAddStatisticsFormData();
    };
        
        $scope.getAddStatisticsFormData = function(){
                
                //if($scope.isLoading) return;
                
        //$scope.isLoading = true;
                var ReqParam = {
                    season_game_unique_id   : $state.params.game_unique_id,
                    league_id               : $state.params.league_id
                };
                
        dataSavingHttp({
            url: site_url+"Projected_stats_feed/get_add_score_stats_form_data",
            data: ReqParam,
        }).success(function (response) {
            $scope.players      = response.data.player;
            $scope.scoring_category = response.data.scoring_category;
            $scope.scoring_rule     = response.data.scoring_rule;
            $scope.teams        = response.data.team;
            $scope.positions        = response.data.position;
            //$scope.isLoading  = false;
        }).error(function (error) {
            //$scope.isLoading = false;
        });
        };
        
        $scope.SaveStatistics = function(RequestFrom) {
                
                var reqData = {
                    season_game_unique_id   : $state.params.game_unique_id,
                    league_id               : $state.params.league_id,
                    team_id                 : [],
                    player_unique_id        : [],
                    scoring_type            : [],
                    stats_key               : [],
                    stats_value             : [],
                    position                : [],
                    minute                  : [],
                    row                     : []
                };
                angular.forEach($scope.statistics_data.statistics, function(value, key) {
                        //console.log(value);
                        angular.forEach(value, function(val, k) {
                            if(k == 'team_id'){
                                reqData.team_id.push(val);
                            }
                            else if(k == 'player_unique_id') {
                                reqData.player_unique_id.push(val);
                            }
                            else if(k == 'position') {
                                reqData.position.push(val);
                            }
                            else if(k == 'scoring_type') {
                                reqData.scoring_type.push(val);
                            }
                            else if(k == 'stats_key'){
                                reqData.stats_key.push(val);
                            }
                            else if(k == 'stats_value'){
                                reqData.stats_value.push(val);
                            }
                            else if(k == 'minute'){
                                reqData.minute.push(val);
                            }
                            else if(k == 'row'){
                                reqData.row.push(val);
                            }
                        });
                });
                //return false;
                
        $rootScope.current_loader = '.btn-success';
        
        $scope.isLoading = true;
        
        dataSavingHttp({
            url: site_url+"Projected_stats_feed/create_score_stats",
            data: reqData,
        }).success(function (response) {
            $scope.isLoading = false;            
            if(RequestFrom != 'AddRow') {
                
                    $rootScope.alert_success = response.message;
                    angular.element("#add_statistics_modal").modal('hide');
                   
                   //$location.path('/contest');
                    $timeout(function(){
                            $state.reload();
                    }, 500);
            }
        }).error(function(error) {
            console.log('RequestFrom -error:',RequestFrom);                    
            if(RequestFrom != 'AddRow') {
                console.log('show error');
            $rootScope.alert_error = error.message;
            }
        });
    };
        
        $scope.UpdateStatistics = function() {
            
        $rootScope.current_loader = '.btn-success';
        
        $scope.isLoading = true;
        
        dataSavingHttp({
            url: site_url+"Projected_stats_feed/edit_score_stats",
            data: $scope.edit_stats_data,
        }).success(function (response) {
            $scope.isLoading = false;
            
            $rootScope.alert_success = response.message;
                        
                        // Reste statistics modal
                        $scope.edit_stats_data= {};
                        
                        angular.element("#edit_statistics_modal").modal('hide');
            //$location.path('/contest');
                        $timeout(function(){
                                $state.reload();
                        }, 500);
                        
        }).error(function(error) {
            $rootScope.alert_error = error.message;
        });
    };
        
        $scope.showConfirmPopUp = function(stats_data) {
                $scope.delete_stats_data = stats_data;
        angular.element("#delete_confirm_model").modal('show');
    };
        
        $scope.delete_stats_data= {};
        
        $scope.deleteStatistics = function () {
                $rootScope.current_loader = '.btn-success';
        
        $scope.isLoading = true;
        
        dataSavingHttp({
            url: site_url+"Projected_stats_feed/delete_score_stats",
            data: $scope.delete_stats_data,
        }).success(function (response) {
            $scope.isLoading = false;
            
            $rootScope.alert_success = response.message;
                        
                        angular.element("#delete_confirm_model").modal('hide');
                        
                        // Reste statistics modal
                        $scope.delete_stats_data= {};
                        
                        $timeout(function(){
                                $state.reload();
                        }, 500);
                        
        }).error(function(error) {
            $rootScope.alert_error = error.message;
        });
        };
        
        $scope.RemoveStatisticRow = function (index) {
            
            if ($scope.statistics_data.statistics.length > 1) {

                $scope.statistics_data.statistics[index] = [];                
                $timeout(function () {
                    
                        $scope.statistics_data.statistics.splice(index, 1);
                        
                        // reste remaing row's select box values
                        angular.forEach($scope.statistics_data.statistics, function(value, key) {
                            
                if(value ) {
                    angular.element("#team_id_"+key).select2("val", value.team_id);
                                        angular.element("#player_id_"+key).select2("val", value.player_unique_id);
                                        angular.element("#scoring_category_"+key).select2("val", value.scoring_type);
                                        angular.element("#scoring_rule_"+key).select2("val", value.stats_key);
                }
            });
                        
                }, 100);
                
            }
        };

        $scope.AddStatisticRow = function () {
//            if ($scope.statistics_data.statistics.length <= 4) { // max row limit 5 if lenght <= 4 
                if($scope.statistics_data.statistics.length > 0){
                    $scope.SaveStatistics('AddRow');
                }
                
                $scope.statistics_data.statistics.push({
                    team_id             : '',
                    player_unique_id    : '',
                    scoring_type        : '',
                    stats_key           : '',
                    stats_value         : '',
                    position            : '',
                    minute              : '',
                    row                 : ''
                });
//            } else {
//                $rootScope.alert_error = 'Max 5 rows allowed at one time';
//            }
        };
        
        $scope.onChangeTeam = function(index){
            
                $scope.statistics_data.statistics[index].player_unique_id = '';
                    
                angular.element("#player_id_"+index).select2("val", '');
        }
        $scope.onChangeScoringCategory = function(index,category){
            
                $scope.rules[index]  = $scope.scoring_rule[category]; 
                console.log(category,$scope.rules);
                $scope.statistics_data.statistics[index].stats_key = '';
                    
                angular.element("#scoring_rule_"+index).select2("val", '');
        }
        
        $scope.empty_statistic = {
            team_id             : '',
            player_unique_id    : '',
            scoring_type        : '',
            stats_key           : '',
            stats_value         : '',
            position            : '',
            minute              : '',
            row                 : ''
        };
        $scope.statistics_data = {
                season_game_unique_id   : $state.params.game_unique_id,
                league_id               : $state.params.league_id,
                statistics              : []
        }
        //$scope.statistics_data.statistics = [];
        $scope.statistics_data.statistics.push($scope.empty_statistic);
        
        
        $scope.downloadStatsTemplate = function(){
                var url = "Projected_stats_feed/export_score_stats_template/"+$state.params.game_unique_id+'/'+$state.params.league_id+"";
        window.open(site_url+url);
        }
        
        $scope.saveUploadedStatsData = function(data){
                //console.log('data: ',data.data.file_name);
            
                $scope.isLoading = true;
                
                var reqData = {
                    season_game_unique_id   : $state.params.game_unique_id,
                    league_id               : $state.params.league_id,
                    file_name               : data.data.file_name
                };
        
        dataSavingHttp({
            url: site_url+"Projected_stats_feed/save_uploaded_score_stats_data",
            data: reqData,
        }).success(function (response) {
            $scope.isLoading = false;
            
            $rootScope.alert_success = response.message;
                        
            //$location.path('/contest');
                        $timeout(function(){
                                $state.reload();
                        }, 500);
        }).error(function(error) {
            $rootScope.alert_error = error.message;
        });
            
        }

$scope.selectPosition= function(index)
{
    var position = angular.element('#player_id_'+index+' option:selected').attr('data-val');
    $scope.statistics_data.statistics[index]['position'] = position;
    // console.log($scope.statistics_data.statistics);
}        
        /*****************************
         *** Custom SCORE Feed END ***
         *****************************/
    /* End Withrawal Listing Functions */
}]);