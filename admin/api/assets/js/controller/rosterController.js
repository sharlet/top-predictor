'use strict';

angular.module("vfantasy").controller('RosterCtrl', ['$scope', '$stateParams','$timeout', '$state', '$rootScope', 'settings', 'dataSavingHttp', '$location', function($scope,$stateParams, $timeout, $state, $rootScope, settings, dataSavingHttp, $location){

	$rootScope.settings.layout.pageBodyFullWidth = true;

	$scope.tempRosterList 				= {};
	$scope.rosterObj					= {};
	$scope.rosterObj.player_unique_id	= [];
	$scope.rosterObj.salary				= {};
	$scope.rosterObj.action				= "";
	$scope.rosterObj.selectall			= false;
	$scope.rosterObj.league_id			= "1";
	$scope.rosterObj.lang 				= "en";
	$scope.rosterObj.roster_type		= "csv";

	$scope.player_unique_id		= {};
	$scope.teamList				= [];
	$scope.rosterList			= [];
	$scope.isLoading			= false;

	$scope.team_postition	= [];
	$scope.teams			= [];
	$scope.salary			= {};

	$scope.player_name;

	$scope.roster_limit = 50;

	$scope.getAllLeague = function() {
		dataSavingHttp({
			url: site_url+"roster/get_all_league"
		}).success(function (response) {
			if(response.status)
			{
				var query_data = $location.search();
				$scope.current_page = 0;
				$scope.leagues = response.data;
				$scope.rosterParam.league_id = (query_data.league_id)?query_data.league_id:response.data[0].league_id;
				$timeout(function(){
					angular.element("#league_id").select2("val", $scope.rosterParam.league_id);
				});
				$scope.rosterParam = angular.extend($scope.rosterParam, query_data);
				$location.search($scope.rosterParam);

				$scope.getAllTeam(true);
				$scope.initRosterFormData();
			}
		});
	};

	$scope.getAllTeam = function(reset_param) {
		if(!$scope.rosterParam.league_id) return;

		if(!reset_param)
		{
			$scope.rosterParam.team_abbr = '';
			$scope.rosterParam.position = '';
			$location.search('team_abbr', null);
			$location.search('position', null);
		}

		dataSavingHttp({
			url: site_url+"roster/get_all_team",
			data: {league_id:$scope.rosterParam.league_id}
		}).success(function (response) {
			if(response.status)
			{
				$scope.teams = response.data;
				$timeout(function(){
					angular.element("#team_abbr").select2("val", $scope.rosterParam.team_abbr);
				});
				$scope.getAllPosition();
			}
		});
	};

	$scope.getAllPosition = function() {
		if(!$scope.rosterParam.league_id) return;
		dataSavingHttp({
			url: site_url+"roster/get_all_position",
			data: {league_id:$scope.rosterParam.league_id}
		}).success(function (response) {
			if(response.status)
			{
				$scope.team_postition =	response.data;

				$timeout(function(){
					angular.element("#position").select2("val", $scope.rosterParam.position);
				});
				$scope.getPlayerList();
			}
		});
	};

	$scope.getPlayerList = function() {
		if($scope.isLoading) return;
		$scope.isLoading = true;

		$location.search($scope.rosterParam);
		$scope.rosterObj.selectall = false;

		dataSavingHttp({
			url: site_url+"roster/get_all_roster",
			data: $scope.rosterParam,
		}).success(function (response) {
			$scope.rosterObj.player_unique_id = [];

			$scope.player_unique_id = Array.apply(null, Array(response.data.result)).map(function() { return false });

			$scope.rosterList				= response.data.result;
			$scope.rosterParam.total_items	= response.data.total;
			$scope.isLoading				= false;

		}).error(function (error) {
			$scope.isLoading = false;
		});
	};

	$scope.sortRosterList = function(sort_field) {
		if($scope.isLoading) return;
		var sort_order = 'DESC';
		if(sort_field==$scope.rosterParam.sort_field){
			sort_order = ($scope.rosterParam.sort_order=='DESC')?'ASC':'DESC';
		}
		$scope.rosterParam.sort_field	= sort_field;
		$scope.rosterParam.sort_order	= sort_order;
		$scope.rosterParam.total_items	= 0;
		$scope.rosterParam.current_page	= 1;
		$scope.getPlayerList();
	};

	$scope.filterResult = function() {
		$scope.rosterParam.items_perpage	= 10;
		$scope.rosterParam.total_items		= 0;
		$scope.rosterParam.current_page		= 1;
		$scope.rosterParam.sort_order		= "ASC";
		$scope.rosterParam.sort_field		= "full_name";
		$timeout(function(){
			angular.element("#team_abbr").select2("val", $scope.rosterParam.team_abbr);
		});
		$timeout(function(){
			angular.element("#position").select2("val", $scope.rosterParam.position);
		});
		$scope.getPlayerList();
	};

	$scope.updateRoster = function() {		
		$rootScope.current_loader = '.btn-primary';
		$scope.rosterObj.league_id = $scope.rosterParam.league_id;

		dataSavingHttp({
			url: site_url+"roster/update_roster",
			data: $scope.rosterObj
		}).success(function (response) {
			$rootScope.alert_success = response.message;
			angular.forEach($scope.player_unique_id, function(value, key) {
			  	if(value && $scope.rosterObj.action != "") {
			  		$scope.rosterList[key].player_status = $scope.rosterObj.action;
			  	}
			});
			deselectPlayer();
		}).error(function(error) {
			$rootScope.alert_error = error.message;
			deselectPlayer();
		});
	};

	$scope.changePlayerStatus = function(player_unique_id,status,index) {
		dataSavingHttp({
			url: site_url+"roster/change_player_status",
			data: {player_unique_id:player_unique_id,player_status:status},
		}).success(function (response) {
			if(response.status) {
				$rootScope.alert_success = response.message;
				$scope.rosterList[index].player_status = status;
			} 
		}).error(function(error){
			$rootScope.alert_error = error.message;	
		});
	};

	$scope.playerRelease = function() {
		$rootScope.current_loader = '.btn-success';
		if(!confirm("Are you sure you release all player."))
		{
			return false;
		}
		dataSavingHttp({
			url: site_url+"roster/release_player",
			data: {league_id:$scope.rosterParam.league_id},
		}).success(function (response) {
			if(response.status)	{
				$rootScope.alert_success = response.message;
			}
		}).error(function(error) {
			$rootScope.alert_error = error.message;
		});
	};

	$scope.togglePlayer = function($event){
		$scope.rosterObj.player_unique_id=[];
		$scope.player_unique_id	= $scope.player_unique_id = Array.apply(null, Array($scope.rosterList.length)).map(function() { return $event.target.checked; });
		if($event.target.checked){
			for (var i = 0; i < $scope.rosterList.length; i++) {
				$scope.rosterObj.player_unique_id.push($scope.rosterList[i].player_unique_id);
			};
		}
		$rootScope.updateUi();
	};

	 var deselectPlayer = function() {
		$scope.player_unique_id = {};
		$scope.rosterObj.player_unique_id = [];
		$scope.rosterObj.selectall = false;
		$rootScope.updateUi();
	}

	$scope.selectPlayer = function($event) {

		var player_unique_id = $event.target.value;
		var index = $scope.rosterObj.player_unique_id.indexOf(player_unique_id);
		
		($event.target.checked)?$scope.rosterObj.player_unique_id.push(player_unique_id):$scope.rosterObj.player_unique_id.splice(index, 1);

		if($scope.rosterObj.player_unique_id.length==$scope.rosterList.length)
		{
			$scope.rosterObj.selectall = true;
		}
		else
		{
			$scope.rosterObj.selectall = false;
		}
		$rootScope.updateUi();
	};

	$scope.$on('repeatFinished', function(){
		angular.element('#league_id').select2("val", $scope.rosterParam.league_id);
		$rootScope.updateUi();
	});

	$scope.initObject = function() {
		$scope.rosterParam					= {};
		$scope.rosterParam.items_perpage	= 10;
		$scope.rosterParam.total_items		= 0;
		$scope.rosterParam.current_page		= 1;
		$scope.rosterParam.sort_order		= "ASC";
		$scope.rosterParam.sort_field		= "full_name";
		$scope.rosterParam.team_abbr		= "";
		$scope.rosterParam.role_id			= "";
		$scope.rosterParam.league_id		= "";
		$scope.rosterParam.lang				= "en";
		$scope.roster_export_type			= "csv";
		$timeout(function(){
			angular.element("#league_id").select2("val", $scope.rosterParam.league_id);
			angular.element("#team_abbr").select2("val", "");
			angular.element("#position").select2("val", "");
		});
	};

	$scope.clearFilter = function () {
		$scope.initObject();
		angular.element("#league_id").select2("val", $scope.leagues[0].league_id);
		$scope.rosterParam.league_id = $scope.leagues[0].league_id;
		$scope.getPlayerList();
	};

	$scope.searchByName = function() {
		var textLen = $scope.rosterParam.player_name.length;
		if(textLen<2 && textLen!==0)
		{
			return;
		}

		/*if($scope.isLoading)
		{
			$scope.rosterParam.player_name = $scope.player_name;
			return;
		} */
		$scope.player_name = $scope.rosterParam.player_name;
		$scope.getPlayerList();
	};

	$scope.showExportPopup = function() {
		angular.element("#export_roster_modal").modal('show');
		$rootScope.updateUi();
	};

	$scope.getFileRosterDetail = function(response) {
		var raw_name = response.data.file_name;
		$state.go('import_player', {file_name:raw_name});
	}



	$scope.intilizeRosterList = function() {
		dataSavingHttp({
			url: site_url+"upload/roster/"+$state.params.file_name+".json",
			method:"GET",
		}).success(function (response) {
			$scope.tempRosterList = response;
		}).error(function(error) {
			$scope.tempRosterList = [];
			$rootScope.alert_error = error.message;
		});
	};

	$scope.tempRoster = {};

	$scope.load_more_roster = function(){
		if($scope.tempRosterList.length<$scope.roster_limit)return;
		$scope.roster_limit += 50;
	};

	$scope.importRoster = function() {
		$rootScope.current_loader = '.btn-success';
		dataSavingHttp({
			url: site_url+"roster/import_roster",
			data:$scope.tempRosterList,
		}).success(function (response) {
			$rootScope.alert_success = response.message;
			$state.go("roster");
		}).error(function(error) {
			$rootScope.alert_error = error.message;
		});
	};

	$scope.closeModel = function() {
		angular.element("#export_roster_modal").modal('hide');
		var url = "roster/export_roster/"+$scope.roster_export_type+'/'+$scope.rosterParam.league_id+"";		
		window.open(site_url+url);
	};

	$scope.edit_roster_data = {};
    $scope.showEditRosterPopup = function(stats_data) {

    		$scope.isLoading = true;
    		if(stats_data['team_abbreviation'] && stats_data['position'] && stats_data['player_unique_id'])
			{
				var reqData = {
		            'league_id'           : $scope.rosterParam.league_id,
		            'player_unique_id'    : stats_data['player_unique_id'],
		            'position'			: stats_data['position']
	        	};
				dataSavingHttp({
					url: site_url+"stats_feed/get_roster_name_by_team_and_pos",
					data: reqData,
				}).success(function (response) {
					$scope.isLoading = false;
					$scope.edit_roster_data = response.data;
					$scope.imgPath = response.img_path;
					angular.element("#edit_roster_modal").modal('show');
					$rootScope.updateUi();
		            //var edit_data = stats_data;
		            //$scope.edit_roster_data = stats_data;
            
		            $timeout(function(){
		                    angular.element("#edit_team_abbr").select2("val", $scope.edit_roster_data['team_abbr']);
		                    angular.element("#edit_position").select2("val", $scope.edit_roster_data['position']);
		            }, 100);
		                
		        }).error(function(error) {
	                
				});
			}

	};

    $scope.UpdateRosterStats = function() {
            
		$rootScope.current_loader = '.btn-success';
		
		$scope.isLoading = true;
                $scope.edit_roster_data.league_id = $scope.rosterParam.league_id;
                $scope.edit_roster_data.full_name_sp = $scope.edit_roster_data.full_name_en;
		
		dataSavingHttp({
			url: site_url+"stats_feed/edit_roster_stats",
			data: $scope.edit_roster_data,
		}).success(function (response) {
			$scope.isLoading = false;
			
			$rootScope.alert_success = response.message;
                        
                        // Reste statistics modal
                        $scope.edit_roster_data= {};
                        
                        angular.element("#edit_roster_modal").modal('hide');
			//$location.path('/contest');
                        $timeout(function(){
                                $state.reload();
                        }, 500);
                        
		}).error(function(error) {
			$rootScope.alert_error = error.message;
		});
	};

    $scope.initRosterFormData = function (){
                
               $scope.roster_data = {
                        league_id               : $scope.rosterParam.league_id,
                        roster                 : []
                }
                
				$scope.roster_data.roster['team_abbr']		= '';
				$scope.roster_data.roster['position']		= '';
				$scope.roster_data.roster['full_name_en']	= '';
				$scope.roster_data.roster['full_name_sp']	= '';
				$scope.roster_data.roster['salary']			= '';
				$scope.roster_data.roster['injury_status']	= '';
				$scope.roster_data.roster['img_url']		= '';
				$scope.roster_data.roster['bye_week']		= '';
				$scope.roster_data.roster['jersey_number']	= '';
				$scope.roster_data.roster['projection_rank']	= '';
        };

    $scope.AddRosterRow = function () {

			$scope.roster_data.roster['team_abbr']		= '';
			$scope.roster_data.roster['position']		= '';
			$scope.roster_data.roster['full_name_en']	= '';
			$scope.roster_data.roster['full_name_sp']	= '';
			$scope.roster_data.roster['salary']			= '';
			$scope.roster_data.roster['injury_status']	= '';
			$scope.roster_data.roster['img_url']		= '';
			$scope.roster_data.roster['bye_week']		= '';
			$scope.roster_data.roster['jersey_number']	= '';
			$scope.roster_data.roster['projection_rank']	= '';
            
    };

    $scope.showAddRosterPopup = function() {
            
                // Reste all select box values
                angular.element("#team_abbr").select2("val", "");
                angular.element("#position").select2("val", "");
                
                $scope.roster_data.roster = [];
                
                $scope.AddRosterRow();
                
				angular.element("#add_roster_modal").modal('show');
				$rootScope.updateUi();
                //$scope.getAddStatisticsFormData();
	};

	$scope.addedRosterList = [];
	$scope.getRostersByTeamAndPos = function()
	{
		if($scope.roster_data.roster['team_abbr'] && $scope.roster_data.roster['position'])
		{
			var reqData = {
            league_id           : $scope.rosterParam.league_id,
            team_abbr             : $scope.roster_data.roster['team_abbr'],
            position            : $scope.roster_data.roster['position'],
        };
			dataSavingHttp({
				url: site_url+"stats_feed/get_roster_name_by_team_and_pos",
				data: reqData,
			}).success(function (response) {
				$scope.isLoading = false;
				$scope.addedRosterList = response.data;
	                
	        }).error(function(error) {
                
			});
		}

		return false;
	}

	$scope.SaveRoster = function(RequestFrom) {
        var reqData = {
			league_id		: $scope.rosterParam.league_id,
			team_abbr		: $scope.roster_data.roster['team_abbr'],

			full_name_en	: $scope.roster_data.roster['full_name_en'],
			full_name_sp	: $scope.roster_data.roster['full_name_en'],
			
			position		: $scope.roster_data.roster['position'],
			salary			: $scope.roster_data.roster['salary'],
			injury_status	: $scope.roster_data.roster['injury_status'],
			img_url			: $scope.roster_data.roster['img_url'],
			jersey_number	: $scope.roster_data.roster['jersey_number'],
			bye_week		: $scope.roster_data.roster['bye_week'],
			projection_rank		: $scope.roster_data.roster['projection_rank'],
            
        };
        
        $rootScope.current_loader = '.btn-success';

		$scope.isLoading = true;

		dataSavingHttp({
			url: site_url+"stats_feed/create_roster_stats",
			data: reqData,
		}).success(function (response) {
			$scope.isLoading = false;
			if(RequestFrom != 'AddRow') {
                $rootScope.alert_success = response.message;
                angular.element("#add_roster_modal").modal('hide');
               
                $timeout(function(){
                        $state.reload();
                }, 500);
            }
		}).error(function(error) {
			console.log();
            if(RequestFrom != 'AddRow') {
                    $rootScope.alert_error = error.message;
            }
		});
	};

$scope.getRoserUploadedImg = function(response)
	{
			$scope.roster_img =  response.data.image_name;
			$scope.roster_data.roster.img_url =  response.data.roster_image_name;
			$scope.$apply();
			console.log(response.data.image_name);
	}

$scope.UpdateRosterImg = function(response)
	{
			// $scope.roster_img =  response.data.image_name;
			$scope.edit_roster_data.img_url =  response.data.roster_image_name;
			$scope.$apply();
			console.log(response.data.image_name);
	}

}]);