'use strict';

angular.module("vfantasy").controller('ContestDraftingCtrl', ['$scope', '$stateParams', '$location', '$state','$timeout','$rootScope', 'settings', 'dataSavingHttp', 'showServerError', function($scope, $stateParams, $location, $state, $timeout, $rootScope, settings, dataSavingHttp, showServerError){
	
	$rootScope.settings.layout.pageBodyFullWidth = true;
	console.log('contest drafting controller ');

	if($stateParams.game_unique_id)
	{
		$scope.game_id = $stateParams.game_unique_id;
		dataSavingHttp({
			url: site_url+"contestdrafting/get_contest_detail",
			data: {game_unique_id:$scope.game_id},
		}).success(function (response){
			console.log(response,"GAME details");
			$scope.gameDetail = response.data.game_details;
	
		});
	}else{
		$scope.getContestFilters();	
	}

	$scope.startDrafting = function()
	{
		dataSavingHttp({
			url		: site_url+"contestdrafting/start_drafting",
			data	: {game_unique_id:$stateParams.game_unique_id},
		}).success(function (response){
			console.log(response,"GAME start drafting ");
			$scope.gameDetail = response.data.game_details;	
		});
	}



}]);