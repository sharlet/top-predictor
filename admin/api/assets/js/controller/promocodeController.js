'use strict';

angular.module("vfantasy").controller('PromocodeCtrl', ['$scope', '$timeout', '$filter', '$state', '$location', '$rootScope', 'settings', 'dataSavingHttp', 'showServerError', function($scope, $timeout, $filter, $state, $location, $rootScope, settings, dataSavingHttp, showServerError){

	$rootScope.settings.layout.pageBodyFullWidth = true;

	$scope.countryList				= {};
	$scope.stateList				= {};
	$scope.salesPersonList			= {};
	$scope.promoCodeList			= {};
	$scope.userList					= {};
	$scope.commissionPayoutList		= {};
	$scope.promoCodeDetailList		= {};
	$scope.salesPersonDetail		= [];
	$scope.salesPersonEarningList	= [];
	$scope.salerPersonEarning = {};
	$scope.is_panel = 0;
	$scope.salerPersonEarning.amount = 0;

	$scope.salesPersonParam						= {};
	$scope.salesPersonParam.address				= "";
	$scope.salesPersonParam.zip_code			= "";
	$scope.salesPersonParam.city				= "";
	$scope.salesPersonParam.master_country_id	= "";
	$scope.salesPersonParam.master_state_id		= "";

	$scope.isLoading = false;
	$scope.personsParam					= {};
	$scope.personsParam.items_perpage	= 10;
	$scope.personsParam.total_items		= 0;
	$scope.personsParam.current_page	= 1;
	$scope.personsParam.sort_order		= "ASC";
	$scope.personsParam.sort_field		= "first_name";


	$scope.isPromoCodeLoading = false;
	$scope.promoCodeParam				= {};
	$scope.promoCodeParam.items_perpage	= 10;
	$scope.promoCodeParam.total_items	= 0;
	$scope.promoCodeParam.current_page	= 1;
	$scope.promoCodeParam.sort_order	= "DESC";
	$scope.promoCodeParam.sort_field	= "expiry_date";

	$scope.newPromoCodeParam			= {};
	$scope.newPromoCodeParam.user_type	= 0;
	$scope.newPromoCodeParam.sales_person = "";
	$scope.search_key = "";

	$scope.isPromoCodePayoutDetailLoading = false;
	$scope.promoCodeDetailParam					= {};
	$scope.promoCodeDetailParam.items_perpage	= 10;
	$scope.promoCodeDetailParam.total_items		= 0;
	$scope.promoCodeDetailParam.current_page		= 1;
	$scope.promoCodeDetailParam.sort_order		= "DESC";
	$scope.promoCodeDetailParam.sort_field		= "PCE.added_date";

	$scope.isSalesPersonDetailLoading = false;
	$scope.salesPersonDetailParam				= {};
	$scope.salesPersonDetailParam.items_perpage	= 10;
	$scope.salesPersonDetailParam.total_items	= 0;
	$scope.salesPersonDetailParam.current_page	= 1;
	$scope.salesPersonDetailParam.sort_order		= "DESC";
	$scope.salesPersonDetailParam.sort_field		= "PCE.added_date";

	$scope.isSalesPersonEarningLoading = false;
	$scope.salesPersonEarningParam				= {};
	$scope.salesPersonEarningParam.items_perpage	= 10;
	$scope.salesPersonEarningParam.total_items	= 0;
	$scope.salesPersonEarningParam.current_page	= 1;
	$scope.salesPersonEarningParam.sort_order		= "DESC";
	$scope.salesPersonEarningParam.sort_field		= "added_date";

	$scope.getAllCountry = function() {
		dataSavingHttp({
			url: site_url+"promo_code/get_all_country",
			data: {},
		}).success(function (response) {
			$scope.countryList = response.data;
			$rootScope.updateUi()
		});
	};

	$scope.getAllState = function() {
		if($scope.salesPersonParam.master_country_id=="")
			return;
		$timeout(function(){
			// $scope.salesPersonParam.master_state_id ="";
			angular.element("#master_state_id").select2("val", "");
		});
		dataSavingHttp({
			url: site_url+"promo_code/get_all_state",
			data: {master_country_id:$scope.salesPersonParam.master_country_id},
		}).success(function (response) {
			$scope.stateList = response.data;
			$rootScope.updateUi()
		});
	};

	$scope.setDob = function (date) {
		$scope.salesPersonParam.dob = date;
	};

	/*-----------------------Sales Persons----------------------------*/
	$scope.newSalesPerson = function() {
		$rootScope.current_loader = '.btn-success';

		dataSavingHttp({
			url: site_url+"promo_code/new_sales_person",
			data: $scope.salesPersonParam,
		}).success(function (response) {
			$rootScope.alert_success = response.message;
			$timeout(function(){
				$location.path('/sales_person');
			},1000);			
		}).error(function(error){
			if(error.hasOwnProperty("message"))
				$rootScope.alert_error = error.message;	
			else
				showServerError(error);
		});
	};

	$scope.getSalesPerson = function() {
		if($scope.isLoading) return;
		$scope.isLoading = true;

		dataSavingHttp({
			url: site_url+"promo_code/get_sales_person",
			data: $scope.personsParam,
		}).success(function (response) {
			$scope.salesPersonList			= response.data.result;
			$scope.personsParam.total_items	= response.data.total;
			$scope.isLoading				= false;
		}).error(function(error){
			showServerError(error);
		});
	};

	$scope.sortSalesPersonList = function(sort_field) {
		if($scope.isLoading) return;
		var sort_order = 'DESC';
		if(sort_field==$scope.personsParam.sort_field){
			sort_order = ($scope.personsParam.sort_order=='DESC')?'ASC':'DESC';
		}
		$scope.personsParam.sort_field		= sort_field;
		$scope.personsParam.sort_order		= sort_order;
		$scope.personsParam.total_items		= 0;
		$scope.personsParam.current_page	= 1;
		$scope.getSalesPerson();
	};
	/*------------------------------------------------------------------*/

	/*-----------------------------Promo Code---------------------------*/

	$scope.newPromoCodeParam.promo_code = $filter('uppercase')(random_string(7));

	$scope.newPromoCode = function() {
		$rootScope.current_loader = '.btn-success';

		$scope.newPromoCodeParam.sales_person = angular.element("#sales_person").val();
		dataSavingHttp({
			url: site_url+"promo_code/new_promo_code",
			data: $scope.newPromoCodeParam,
		}).success(function (response) {
			$rootScope.alert_success = response.message;
			$timeout(function(){
				$location.path('/promo_code');
			},1000);
		}).error(function(error){
			if(error.hasOwnProperty("message"))
				$rootScope.alert_error = error.message;	
			else
				showServerError(error);
		});
	};

	$scope.getPromoCodes = function() {
		if($scope.isLoading) return;
		$scope.isLoading = true;

		dataSavingHttp({
			url: site_url+"promo_code/get_promo_codes",
			data: $scope.promoCodeParam,
		}).success(function (response) {
			$scope.promoCodeList				= response.data.result;
			$scope.promoCodeParam.total_items	= response.data.total;
			$scope.isLoading					= false;
		}).error(function(error){
			showServerError(error);
		});
	};

	$scope.sortPromoCodesList = function(sort_field) {
		if($scope.isLoading) return;
		var sort_order = 'DESC';
		if(sort_field==$scope.promoCodeParam.sort_field){
			sort_order = ($scope.promoCodeParam.sort_order=='DESC')?'ASC':'DESC';
		}
		$scope.promoCodeParam.sort_field	= sort_field;
		$scope.promoCodeParam.sort_order	= sort_order;
		$scope.promoCodeParam.total_items	= 0;
		$scope.promoCodeParam.current_page	= 1;
		$scope.getPromoCodes();
	};
	/*------------------------------------------------------------------*/

	/*-----------------------------Promo Code DETAIL---------------------------*/

	$scope.getPromoCodeDetail = function() {
		if($scope.isPromoCodeLoading) return;
		$scope.isPromoCodeLoading = true;

		$scope.promoCodeDetailParam.promo_code = $state.params.promo_code;
		dataSavingHttp({
			url: site_url+"promo_code/get_promo_code_detail",
			data: $scope.promoCodeDetailParam,
		}).success(function (response) {
			$scope.promoCodeDetailList				= response.data.result;
			$scope.promoCodeDetailParam.total_items	= response.data.total;
			$scope.salesPersonDetail				= response.data.sales_person_detail;
			$scope.is_panel = Object.keys($scope.salesPersonDetail).length;
			$scope.isPromoCodeLoading				= false;
		}).error(function(error){
			showServerError(error);
		});
	};

	$scope.sortPromoCodeDetailList = function(sort_field) {
		if($scope.isPromoCodeLoading) return;
		var sort_order = 'DESC';
		if(sort_field==$scope.promoCodeDetailParam.sort_field){
			sort_order = ($scope.promoCodeDetailParam.sort_order=='DESC')?'ASC':'DESC';
		}
		$scope.promoCodeDetailParam.sort_field		= sort_field;
		$scope.promoCodeDetailParam.sort_order		= sort_order;
		$scope.promoCodeDetailParam.total_items		= 0;
		$scope.promoCodeDetailParam.current_page	= 1;
		$scope.getPromoCodeDetail();
	};
	/*------------------------------------------------------------------*/

	/*------------------------Edit Sales Persons--------------------------*/

	$scope.getSalesPersonDetail = function() {
		if($scope.isLoading) return;
		$scope.isLoading = true;

		dataSavingHttp({
			url: site_url+"promo_code/get_sales_person_detail",
			data: {sales_person_id : $state.params.sales_person_id},
		}).success(function (response) {
			$scope.salesPersonParam	= response.data;
			$scope.getAllState();
			$timeout(function(){
				angular.element("#master_country_id").select2("val", response.data.master_country_id);

				angular.element("#master_state_id").select2("val", response.data.master_state_id);
			},1000);
			$rootScope.updateUi();
			$scope.isLoading		= false;
		}).error(function(error){
			showServerError(error);
		});
	};

	$scope.updateSalesPersonDetail = function() {
		if($scope.isLoading) return;
		$scope.isLoading = true;
		$rootScope.current_loader = '.btn-success';

		$scope.salesPersonParam.sales_person_unique_id = $state.params.sales_person_id;
		dataSavingHttp({
			url: site_url+"promo_code/update_sales_person_detail",
			data: $scope.salesPersonParam,
		}).success(function (response) {
			$scope.isLoading = false;
			$rootScope.alert_success = response.message;
			$timeout(function(){
				$location.path('/sales_person');
			},1000);
		}).error(function(error){
			$scope.isLoading = false;
			if(error.hasOwnProperty("message"))
				$rootScope.alert_error = error.message;	
			else
				showServerError(error);
		});
	};

	/*--------------------------------------------------------------------*/

	/*-----------------------------SALES PERSON PAYOUT---------------------------*/

	$scope.getSalesPersonPayout = function() {
		if($scope.isSalesPersonDetailParamLoading) return;
		$scope.isSalesPersonDetailParamLoading = true;
		$scope.salesPersonDetailParam.sales_person_unique_id = $state.params.sales_person_id;

		dataSavingHttp({
			url: site_url+"promo_code/get_sales_person_payout",
			data: $scope.salesPersonDetailParam,
		}).success(function (response) {
			$scope.salesPersonDetailList				= response.data.result;
			$scope.salesPersonDetailParam.total_items	= response.data.total;
			$scope.salesPersonDetail					= response.data.sales_person_detail;
			$scope.is_panel = Object.keys($scope.salesPersonDetail).length;
			if($scope.is_panel>0)
				$scope.getSalesPersonEarningHistory($scope.salesPersonDetail.sales_person_id);

			$scope.isSalesPersonDetailParamLoading		= false;
		}).error(function(error){
			showServerError(error);
		});
	};

	$scope.sortSalesPersonPayoutList = function(sort_field) {
		if($scope.isSalesPersonDetailParamLoading) return;
		var sort_order = 'DESC';
		if(sort_field==$scope.salesPersonDetailParam.sort_field){
			sort_order = ($scope.salesPersonDetailParam.sort_order=='DESC')?'ASC':'DESC';
		}
		$scope.salesPersonDetailParam.sort_field		= sort_field;
		$scope.salesPersonDetailParam.sort_order		= sort_order;
		$scope.salesPersonDetailParam.total_items	= 0;
		$scope.salesPersonDetailParam.current_page	= 1;
		$scope.getSalesPersonPayout();
	};
	/*------------------------------------------------------------------*/
	/*----------------------------Manage Seles Person Earning-----------*/
	$scope.getEarningDetail = function(sales_person_id) {
		if($scope.isLoading) return;
		$scope.isLoading = true;
		dataSavingHttp({
			url: site_url+"promo_code/get_sales_person_earning",
			data: {'sales_person_id':sales_person_id},
		}).success(function (response) {
			angular.element("#earning_detail_modal").modal('show');
			$scope.salerPersonEarning	= response.data;
			$scope.isLoading			= false;
		}).error(function (error) {
			$scope.isLoading = false;
		});
	};
	$scope.updateSalesPersonEarning = function(sales_person_id) {
		if($scope.isLoading) return;
		$scope.isLoading = true;
		$scope.salerPersonEarning.sales_person_id = sales_person_id;
		dataSavingHttp({
			url: site_url+"promo_code/update_sales_person_earning",
			data: $scope.salerPersonEarning,
		}).success(function (response) {
			angular.element("#earning_detail_modal").modal('hide');
			$rootScope.alert_success	= response.message;
			$scope.getSalesPersonEarningHistory(sales_person_id);
			$scope.isLoading			= false;
		}).error(function (error) {
			$scope.isLoading = false;
		});
	};
	/*--------------------------------------------------------------------------*/

	/*-----------------------------SALES Earning History---------------------------*/

	$scope.getSalesPersonEarningHistory = function(sales_person_id) {
		if($scope.isSalesPersonEarningLoading) return;
		$scope.isSalesPersonEarningLoading = true;
		if(sales_person_id=="")
			return;
		$scope.salesPersonEarningParam.sales_person_id = sales_person_id;
		dataSavingHttp({
			url: site_url+"promo_code/get_sales_person_earning_history",
			data:$scope.salesPersonEarningParam,
		}).success(function (response) {
			$scope.salesPersonEarningList				= response.data.result;
			$scope.salesPersonEarningParam.total_items	= response.data.total;
			$scope.isSalesPersonEarningLoading			= false;
		}).error(function(error){
			showServerError(error);
		});
	};

	$scope.sortSalesPersonEarningList = function(sort_field) {
		if($scope.isSalesPersonEarningLoading) return;
		var sort_order = 'DESC';
		if(sort_field==$scope.salesPersonEarningParam.sort_field){
			sort_order = ($scope.salesPersonEarningParam.sort_order=='DESC')?'ASC':'DESC';
		}
		$scope.salesPersonEarningParam.sort_field	= sort_field;
		$scope.salesPersonEarningParam.sort_order	= sort_order;
		$scope.salesPersonEarningParam.total_items	= 0;
		$scope.salesPersonEarningParam.current_page	= 1;
		$scope.getSalesPersonEarningHistory($scope.salesPersonEarningParam.sales_person_id);
	};
	/*------------------------------------------------------------------*/
	$rootScope.updateUi();
}]);