angular.module("vfantasy").controller('waiverPriorityCtrl', ['$rootScope', '$scope', '$timeout','$filter','$stateParams', '$state','dataSavingHttp', 'showServerError', function ($rootScope, $scope, $timeout,$filter,$stateParams, $state, dataSavingHttp, showServerError) {
	var sortableEle;
	if(!$stateParams.game_unique_id)
	{
		$rootScope.alert_error = MATCHNOFOUND;
		$state.go('contest');
		return;
	}

	$scope.game_unique_id		= $stateParams.game_unique_id;
	$scope.priority_list			= [];
	$scope.updated_draft_list	= [];
	$scope.isCommisnor = "";
    $scope.getWaiverPriorityList = function()
    {
    	var post =  {
						url		: site_url + "CommishTool/get_waiver_priority_list",
						data	: { game_unique_id : $scope.game_unique_id }
					};
 		dataSavingHttp(post).success(function (response)
		{
			$scope.priority_list	= response.data.priority_list;				
			$scope.isCommisnor		= response.data.is_commish;	
			$scope.gameDetail		= response.data.game_detail;	
		 	$scope.initDrag();
		}).error(function (error)
		{
			$rootScope.alert_error = error.message;
			$state.go('contest');
			return;	
		});	
    }

    $scope.dragStart = function(e, ui)
    {
        ui.item.data('start', ui.item.index());
    }

	$scope.dragEnd = function(e, ui)
	{
		var start = ui.item.data('start'),
            end   = ui.item.index();
        
        $scope.priority_list.splice(end, 0, $scope.priority_list.splice(start, 1)[0]);
        $scope.$apply();

    	var intialVal = 1;  
	  	$('#sortable li .num_span').each(function(){
	    	$(this).html(intialVal);
	    	intialVal++;
	  	});
    }
    $scope.initDrag = function(e, ui)
	{
	    // initialize sortable list
	    sortableEle = $('#sortable').sortable({
			start	: $scope.dragStart,
			update	: $scope.dragEnd
	    });
	    console.log("HELLO DEAR");
	}
    $scope.updateDraftOrder = function()
    {
    	angular.forEach($scope.priority_list, function(value, key) 
    	{
			$scope.updated_draft_list.push({ lineup_master_id:  value.lineup_master_id, current_priority_place: parseInt(key) + 1 });
		});
		var post =  {
						url		: site_url + "CommishTool/update_waiver_priority_list_order",
						data	: { game_unique_id : $scope.game_unique_id, update_list : $scope.updated_draft_list }
					};
 		dataSavingHttp(post).success(function (response)
		{
			$scope.updated_draft_list	= [];
			$rootScope.alert_success	= response.message;
		}).error(function (error)
		{
			$rootScope.alert_error = error.message;
		});
    }

    $scope.goToContest = function()
	{
		$state.go('contest');
	}
	$scope.goToLineup = function()
	{
		$state.go('lineup-detail', {'game_unique_id' : $scope.game_unique_id});
		return;
	}

	$scope.goToCommishnerTool = function()
	{
		$state.go('commish-tool', {'game_unique_id' : $scope.game_unique_id});
		return;
	}
	
    $scope.getWaiverPriorityList();
}]);