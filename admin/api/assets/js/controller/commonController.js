'use strict';

angular.module("vfantasy").controller('LogoutCtrl', ['$scope', '$rootScope', '$state', 'settings', 'dataSavingHttp', 'showServerError', function($scope, $rootScope, $state, settings, dataSavingHttp, showServerError){
	
	$scope.language_prefer = 'english';
	$scope.logout = function() {
		dataSavingHttp({
			url: site_url+"logout",
			data: {},
		}).success(function (response) {
			sessionStorage.clear();
			$rootScope.is_logged_in = false;
			$state.go('root');
		}).error(function (error) {
			showServerError(error);
		});
	};

}]);

angular.module("vfantasy").controller('CommonCtrl', ['$scope', '$rootScope', '$state', 'settings', 'dataSavingHttp', function($scope, $rootScope, $state, settings, dataSavingHttp){
	$scope.getLanguage = function() {
		dataSavingHttp({
			url: site_url+"get_language_list",
			data: {},
		}).success(function (response) {
			$rootScope.language_list = response.data.language_list;
			$rootScope.site_language = response.data.site_language;
		});
	};

	$scope.changeLanguage = function (lang) {
		$('.page-spinner-bar').removeClass('hide').show();
		$rootScope.site_language = lang;
		var site_lang = $state.current.data.lang;
		dataSavingHttp({
			url: "update_site_language",
			data: {language:lang},
		}).success(function (response) {
			$rootScope.site_language = response.data.language;
			site_lang.forEach( function(element, index) {
				var src = "utility/javascript_var/"+element;
				var newScript = document.createElement('script');
				newScript.async=1;
				newScript.src=src;
				newScript.onload = function () {
					$rootScope.lang = SERVER_GLOBAL;
					$scope.$apply($rootScope.lang);
				};
				var load_before = document.getElementById("ng_load_plugins_before");
				load_before.insertBefore(newScript, load_before.childNodes[0]);
			});
			$('.page-spinner-bar').addClass('hide');
		}).error(function () {
			$('.page-spinner-bar').addClass('hide');
		});
	};

	$scope.logout = function() {
		dataSavingHttp({
			url: site_url+"logout",
			data: {},
		}).success(function (response) {
			sessionStorage.clear();
			$rootScope.is_logged_in = false;
			$state.go('root');
		}).error(function (error) {
			showServerError(error);
		});
	};

	$scope.redirectUrl = function($url){
		window.location.href  = site_url+$url; 
	}
}]);