angular.module("vfantasy").controller('editLeagueSettingsCtrl', editLeagueSettingsCtrl);
editLeagueSettingsCtrl.$inject = ['$scope', '$stateParams', '$location', '$state','$timeout','$rootScope', 'settings', 'dataSavingHttp', 'showServerError','$filter'];

function editLeagueSettingsCtrl($scope, $stateParams, $location, $state, $timeout, $rootScope, settings, dataSavingHttp, showServerError,$filter)
{
	var els = this;
	if(!$stateParams.game_unique_id)
	{
		$rootScope.alert_error = GAME_NOT_FOUND;
		$state.go('contest');
		return;
	}
	els.game_unique_id	= $stateParams.game_unique_id;
	els.contestParam	= {};
	els.valid_time 		= true;
	
	// function declaration for get game details
	els.get_game_details		= get_game_details;
	els.updateTimePicker		= updateTimePicker;
	els.updateLeague 			= updateLeague;
	els.goToContest				= goToContest;
	els.goToLineup				= goToLineup;
	els.goToCommishnerTool		= goToCommishnerTool;
	els.setDate 				= setDate;

	function get_game_details()
	{
		return dataSavingHttp({
				url: site_url+"CommishTool/get_league_settings",
				data: {"game_unique_id" : els.game_unique_id},
			}).success(function (response) {
				console.log(response);
				els.gameDetail				= response.data.game_details;
				els.season_week				= response.data.season_week;
				els.max_roster_size         = response.data.max_roster_size;

				els.draft_settings 			= response.data.draft_dates;
				// set current date
				var common_date				= response.data.season_start_date.replace(/\-/g,'/');
				var start_date				= new Date(common_date);
				els.season_start_date		= $.datepicker.formatDate('yy-mm-dd', start_date);
				
				// set default draft date
				var game_draft_date			= els.gameDetail.game_draft_date.replace(/\-/g,'/');
				var draft_date				= new Date(game_draft_date);
				els.contestParam.game_date	= $.datepicker.formatDate('yy-mm-dd', draft_date);

				els.contestParam.gametime = els.gameDetail.game_draft_time;
			}).error(function (error) {
				if(error.hasOwnProperty('error'))
				{
					showServerError(error);
					$state.go('contest');
					return;
				}
				else
				{
					$rootScope.alert_error = error.message;
					$state.go('contest');
					return;
				}
			});
	}

	function toTimestamp(strDate)
	{
	   var datum = Date.parse(strDate);
	   return datum/1000;
	}

	function updateTimePicker()
	{
		console.log(els.contestParam);
		return dataSavingHttp({
			url: site_url+"contest/validate_past_time",
			data: {'game_date': els.contestParam.game_date, 'game_time': els.contestParam.gametime},
		}).success(function (response){
			$('#gametime_error').text('').addClass('hide');
			els.valid_time = true;
			//set_trade_deadline_date();
		}).error(function(error){
			showServerError(error);
			els.valid_time = false;
		});
	}

	function updateLeague()
	{
		if(els.valid_time)
		{
			els.contestParam.game_unique_id = els.game_unique_id;
			return dataSavingHttp({
				url: site_url+"CommishTool/update_league_settings",
				data: els.contestParam,
			}).success(function (response){
				$rootScope.alert_success = response.message;
				$state.go('commish-tool',{game_unique_id : els.game_unique_id});
				return;
			}).error(function(error){
				if(error.hasOwnProperty('error'))
				{
					showServerError(error);
				}
				else
				{
					$rootScope.alert_error = error.message;
				}
			});
		}
		
	}

	function goToContest()
	{
		$state.go('contest');
		return;
	}

	function goToLineup()
	{
		$state.go('lineup-detail', {'game_unique_id' : els.game_unique_id});
		return;
	}

	function goToCommishnerTool()
	{
		$state.go('commish-tool', {'game_unique_id' : els.game_unique_id});
		return;
	}

	function setDate(date,field)
	{
		els.contestParam[field] = date;

		if(field == "game_date")
		{
			var currentDate = $.datepicker.formatDate('yy-mm-dd', new Date());
            if(date == currentDate)
            {
            	var coeff = 1000 * 60 * 15;
				var date = new Date();  //or use any other date
				var rounded = new Date(Math.ceil(date.getTime() / coeff) * coeff);
				var cur_time = rounded.toLocaleTimeString().replace(/([\d]+:[\d]{2})(:[\d]{2})(.*)/, "$1$3");
            	$('#gametime').timepicker('option', 'minTime',cur_time);
            	$('#gametime_error').text('').addClass('hide');
            	$('#gametime').val('');
            	$scope.$apply();
            }
            else
            {
            	$('#gametime').timepicker('option', 'minTime','00:00 AM');
            	$('#gametime_error').text('').addClass('hide');
            	$('#gametime').val('');
            	$scope.$apply();
            }
		}
	}
}