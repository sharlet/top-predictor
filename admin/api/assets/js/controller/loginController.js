'use strict';

angular.module("vfantasy").controller('LoginCtrl', ['$scope', '$rootScope', '$state', 'settings', 'dataSavingHttp', 'showServerError', function($scope, $rootScope, $state, settings, dataSavingHttp, showServerError){
	$scope.formData				= {};
	$scope.formData.email		= '';
	$scope.formData.password	= '';
	$rootScope.is_logged_in = false;

	$scope.doLogin = function() {
		dataSavingHttp({
			url: site_url+"login",
			data: {email:$scope.formData.email, password:window.md5($scope.formData.password)},
		}).success(function (response) {
			if(response.status)
			{
				sessionStorage.setItem(AUTH_KEY, response.Data[AUTH_KEY]);
				$rootScope.settings.layout.pageBodyFullWidth = true;
				$rootScope.is_logged_in = true;
				$state.go('user');
			}
		}).error(function (error) {
			showServerError(error);
		});
	};

	var key = sessionStorage.getItem(AUTH_KEY);

	if(key!=null)
	{
		$rootScope.is_logged_in = true;
		$rootScope.settings.layout.pageBodyFullWidth = true;
		$state.go('user');
	}
	$rootScope.settings.layout.pageBodyFullWidth = false;
}]);