'use strict';

angular.module("vfantasy").controller('TransactionCtrl', ['$scope', '$timeout', '$state', '$rootScope', 'settings', 'dataSavingHttp', '$location', function($scope, $timeout, $state, $rootScope, settings, dataSavingHttp, $location){

	$rootScope.settings.layout.pageBodyFullWidth = true;

	$scope.transactionList			= [];
	$scope.isLoading = false;

	/* Start Transaction Listing Functions */	
	$scope.getTransactionList = function() {
		if($scope.isLoading) return;
		$scope.isLoading = true;
			if($scope.transactionParam.fromdate > $scope.transactionParam.todate) {
		$scope.transactionParam.todate = $scope.transactionParam.fromdate; 
	}

		$location.search($scope.transactionParam);
		dataSavingHttp({
			url: site_url+"payment_transaction/get_all_transaction",
			data: $scope.transactionParam,
		}).success(function (response) {
			$scope.transactionList = response.data.result;
			$scope.transactionParam.total_items = response.data.total;
			$scope.isLoading = false;
		}).error(function (error) {
			$scope.isLoading = false;
		});
	};

	$scope.sortTransactionList = function(sort_field) {

		if($scope.isLoading) return;
		var sort_order = 'DESC';

		if(sort_field==$scope.transactionParam.sort_field){
			sort_order = ($scope.transactionParam.sort_order=='DESC')?'ASC':'DESC';
		}
		$scope.transactionParam.sort_field  = sort_field;
		$scope.transactionParam.sort_order  = sort_order;
		$scope.transactionParam.total_items  = 0;
		$scope.transactionParam.current_page = 1;
		$scope.getTransactionList();
	};

	$scope.initObject = function() {
		$scope.transactionParam					= {};
		$scope.transactionParam.items_perpage	= 10;
		$scope.transactionParam.total_items		= 0;
		$scope.transactionParam.current_page	= 1;
		$scope.transactionParam.payment_type	= "";
		$scope.transactionParam.fromdate		= "";
		$scope.transactionParam.todate			= "";
		$scope.transactionParam.sort_order		= 'DESC';
		$scope.transactionParam.sort_field		= 'created_date';
		var query_data = $location.search();
		$scope.transactionParam = angular.extend($scope.transactionParam, query_data);
		$timeout(function(){
			angular.element("#payment_type").select2("val", $scope.transactionParam.payment_type);
		});		
	};
	/* End Transaction Listing Functions */
	$scope.initObject();

	$scope.clearFilter = function () {
		$scope.transactionParam					= {};
		$scope.transactionParam.items_perpage	= 10;
		$scope.transactionParam.total_items		= 0;
		$scope.transactionParam.current_page	= 1;
		$scope.transactionParam.payment_type	= "";
		$scope.transactionParam.fromdate		= "";
		$scope.transactionParam.todate			= "";
		$scope.transactionParam.sort_order		= 'DESC';
		$scope.transactionParam.sort_field		= 'created_date';
		$timeout(function(){
			angular.element("#payment_type").select2("val", $scope.transactionParam.payment_type);
		});
		$scope.getTransactionList();
	};
}]);