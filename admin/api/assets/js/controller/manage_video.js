'use strict';
angular.module("vfantasy").controller('manageVideoCtrl', manageVideoCtrl);
manageVideoCtrl.$inject = ['$scope', '$stateParams', '$location', '$state','$timeout','$rootScope', 'settings', 'dataSavingHttp', 'showServerError','$filter'];

function manageVideoCtrl($scope, $stateParams, $location, $state, $timeout, $rootScope, settings, dataSavingHttp, showServerError,$filter)
{
	var mvc	= this;

	mvc.videoParam				= {};
	mvc.videoParam.league_id	= "";
	mvc.videoParam.offset		= 0;
	mvc.videoParam.sort_field	= "last_updated";
	mvc.videoParam.sort_order	= "DESC";
	mvc.videoParam.items_perpage= 10;
	mvc.isLoading				= false;
	mvc.video_list 				= [];
	mvc.video_types 			= ['LOBBY','REPLAYS','HOME'];
	mvc.video_orders 			= [1,2,3];
	mvc.get_filters				= get_filters;
	mvc.get_video_list			= get_video_list;
	mvc.sortRosterList			= sortRosterList;
	mvc.get_league_video_list	= get_league_video_list;
	mvc.show_add_popup			= show_add_popup;
	mvc.add_video 				= add_video;
	mvc.remove_vedio_confirm    = remove_vedio_confirm;
	mvc.delete_video 			= delete_video;
	mvc.edit_vedio_popup		= edit_vedio_popup;
	mvc.edit_video              = edit_video;
	function get_filters()
	{
		return dataSavingHttp({
			url: site_url+"common/get_master_filters",
			data: {},
		}).success(function (response)
		{
			mvc.leagues = response.data.leagues;
			mvc.videoParam.league_id = mvc.leagues[0].league_id;
			$timeout(function() {
				angular.element("#league_id").select2("val", mvc.videoParam.league_id);
			});

			// call get_video_list()
			mvc.get_video_list();
		}).error(function(error){
			showServerError(error);
		});
	}
	get_filters();

	function get_video_list()
	{
		if(mvc.isLoading) return;

		mvc.isLoading = true;
		return dataSavingHttp({
			url: site_url+"common/get_video_list",
			data: mvc.videoParam,
		}).success(function (response)
		{
			mvc.video_list				= response.data.result;
			mvc.videoParam.total_items	= response.data.total;
			mvc.isLoading = false;
		}).error(function(error){
			mvc.isLoading = false;
			showServerError(error);
		});	
	}

	function sortRosterList(sort_field) 
	{
		if(mvc.isLoading) return;
		var sort_order = 'DESC';
		if(sort_field == mvc.videoParam.sort_field){
			sort_order = (mvc.videoParam.sort_order=='DESC') ? 'ASC' : 'DESC';
		}
		mvc.videoParam.sort_field	= sort_field;
		mvc.videoParam.sort_order	= sort_order;
		mvc.videoParam.total_items	= 0;
		mvc.videoParam.current_page	= 1;
		// call get_video_list()
		mvc.get_video_list();
	};

	function get_league_video_list()
	{
		mvc.videoParam.total_items	= 0;
		mvc.videoParam.current_page	= 1;
		mvc.videoParam.offset		= 0;
		mvc.videoParam.sort_field	= "last_updated";
		mvc.videoParam.sort_order	= "DESC";
		// call get_video_list()
		mvc.get_video_list();
	}

	function show_add_popup()
	{
		mvc.add_video_case 				= true;
		mvc.edit_video_case 			= false;
		mvc.add_video_param				= {};
		mvc.add_video_param.video_type	= "LOBBY";
		mvc.add_video_param.status		= '1';
		mvc.add_video_param.video_order =  1;
		mvc.add_video_param.league_id   = mvc.leagues[0].league_id;
		angular.element("#video_type").select2("val", mvc.add_video_param.video_type);
		angular.element("#add_league_id").select2("val", mvc.add_video_param.league_id);
		angular.element("#video_order").select2("val", mvc.add_video_param.video_order);
        angular.element("#add_video_modal").modal('show');
	}

	function add_video()
	{
		return dataSavingHttp({
			url: site_url+"common/add_video",
			data: mvc.add_video_param,
		}).success(function (response)
		{
			mvc.add_video_param = {};                        
            angular.element("#add_video_modal").modal('hide');
            $rootScope.alert_success = response.message;
            $timeout(function(){
                $state.reload();
            }, 500);  
		}).error(function(error){
			if(error.hasOwnProperty('error'))
			{
				showServerError(error);
			}
			else
			{
				$rootScope.alert_error = error.message;
			}
		});
	}

	function edit_vedio_popup(video)
	{
		mvc.edit_video_case					= true;
		mvc.add_video_case					= false;
		mvc.edit_video_param				= {};
		mvc.edit_video_param.video_id		= video.video_id;
		mvc.edit_video_param.video_type		= video.video_type;
		mvc.edit_video_param.status			= video.status;
		mvc.edit_video_param.league_id		= video.league_id;
		mvc.edit_video_param.video_title_en	= video.video_title_en;
		mvc.edit_video_param.video_title_sp	= video.video_title_sp;
		mvc.edit_video_param.video_desc_en	= video.video_desc_en;
		mvc.edit_video_param.video_desc_sp	= video.video_desc_sp;
		mvc.edit_video_param.video_url		= video.video_url;
		mvc.edit_video_param.video_order    = (video.video_type == "LOBBY") ? video.video_order : 1;
		
		$timeout(function() { 
			angular.element("#video_type").select2("val", mvc.edit_video_param.video_type);
			angular.element("#edit_league_id").select2("val", mvc.edit_video_param.league_id);
			angular.element("#video_order").select2("val", mvc.edit_video_param.video_order);
			angular.element("#edit_video_modal").modal('show'); 
		});
	}

	function edit_video()
	{
		if(mvc.edit_video_param.video_type == "LOBBY" && mvc.edit_video_param.video_order == 0)
		{
			$("#video_order_error").html('Please select video order.').show();
			return false;
		}
		$("#video_order_error").html('').hide();
		return dataSavingHttp({
			url: site_url+"common/update_video",
			data: mvc.edit_video_param,
		}).success(function (response)
		{
            angular.element("#edit_video_modal").modal('hide');
            $rootScope.alert_success = response.message;
            $timeout(function(){
                $state.reload();
            }, 500);  
		}).error(function(error){
			$rootScope.alert_error = error.message;
		});
	}

	function remove_vedio_confirm(video)
	{
		mvc.delet_video				= {};
		mvc.delet_video.video_id	= video.video_id;
		mvc.delet_video.league_id	= video.league_id;
		
		$('#delete_video_confirm_modal').modal('show');	
	}

	function delete_video()
	{
		return dataSavingHttp({
			url: site_url+"common/delete_video",
			data: mvc.delet_video,
		}).success(function (response)
		{
			mvc.delet_video = {};                        
            angular.element("#delete_video_confirm_modal").modal('hide');
            $rootScope.alert_success = response.message;
            $timeout(function(){
                $state.reload();
            }, 500);  
		}).error(function(error){
			$rootScope.alert_error = error.message;
		});
	}
}