'use strict';

angular.module("vfantasy").controller('SettingCtrl', ['$scope', '$timeout', '$state', '$rootScope', 'settings', 'dataSavingHttp', function($scope, $timeout, $state, $rootScope, settings, dataSavingHttp){

	$rootScope.settings.layout.pageBodyFullWidth = true;
	$scope.settingObj = {};
	$scope.changePassword = function() {
		$rootScope.current_loader = '.btn-success';

		$scope.settingObj.old_password = window.md5($scope.settingObj.old_password)
		$scope.settingObj.new_password = window.md5($scope.settingObj.new_password)
		$scope.settingObj.confirm_password = window.md5($scope.settingObj.confirm_password)

		dataSavingHttp({
			url: site_url+"setting/change_password",
			data: $scope.settingObj,
		}).success(function (response) {
			$rootScope.alert_success = response.message;
			$scope.settingObj = {};
		}).error(function(error){
			$rootScope.alert_error = error.message;
			$scope.settingObj = {};
		});
	};

	$scope.changeDateTime = function() {
		$rootScope.current_loader = '.btn-success';

		dataSavingHttp({
			url: site_url+"setting/change_date_time",
			data: $scope.settingObj,
		}).success(function (response) {
			$rootScope.alert_success = response.message;
			$timeout(function(){
				window.location.href = window.location.href;
			},1000)
		}).error(function(error){
			$rootScope.alert_error = error.message;
			$scope.settingObj = {};
		});
	};
}]);