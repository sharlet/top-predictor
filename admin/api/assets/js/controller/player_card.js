angular.module("vfantasy").controller('playerCardCtrl', ['$rootScope', '$scope', '$timeout', '$stateParams', '$state', '$compile','dataSavingHttp', 'showServerError', function ($rootScope, $scope, $timeout, $stateParams, $state, $compile, dataSavingHttp, showServerError){
	var pcc				= this;
	pcc.playerDetail	= {};
	pcc.playerCardData	= {} ; 
	pcc.game_unique_id	= $stateParams.game_unique_id;
	
 	pcc.playerDetailPopup = function(player_id)
 	{

 		post_data={
 					'player_id':player_id,
 					'game_unique_id':pcc.game_unique_id
 		};

 		var post =  {
						url		: site_url+"CommishTool/edit_roster_detail",
						data	: post_data
					};
 		dataSavingHttp(post).success(function (response)
		{
			pcc.playerDetail						= response.data.player_info;
			pcc.nextGame							= response.data.next_game;
			pcc.playerCardData.playerGameLogHeads	= response.data.game_log.gameLogHeads;
			pcc.playerCardData.playerGameLogValues	= response.data.game_log.gameLogValues;	 					
			pcc.playerCardData.summary				= response.data.summary;	
			pcc.playerCardData.playerMatchUp		= response.data.matchup;		

			if (!$scope.$$phase) {
				$scope.$apply();
			}
			
			$timeout(function() {
			$('#Player_Card_Popup').modal('show') ;	
			angular.element('.class_'+player_id).show();
			}); 
		}).error(function (error)
		{
			
		});
	};

 	pcc.closePlayerDetail = function()
 	{
 		$('#Player_Card_Popup').modal('hide');
 	}

}]);