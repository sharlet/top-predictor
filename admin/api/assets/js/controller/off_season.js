'use strict';

angular.module("vfantasy").controller('offSeasonSettingCtrl', offSeasonSettingCtrl);
offSeasonSettingCtrl.$inject = ['$scope', '$stateParams', '$location', '$state','$timeout','$rootScope', 'settings', 'dataSavingHttp', 'showServerError','$filter'];

function offSeasonSettingCtrl($scope, $stateParams, $location, $state, $timeout, $rootScope, settings, dataSavingHttp, showServerError,$filter)
{
	var os								= this;
	os.contestParam						= {};
	os.contestParam.action				= '';
	os.draftParam 						= {};
	// set function declarations
	os.get_games						= get_games;
	os.sendNotifications				= sendNotifications;
	os.setDate							= setDate;
	os.OffSeasonChange 					= OffSeasonChange;
	os.OnYearChange 					= OnYearChange;
	os.saveDraftDate					= saveDraftDate;
	os.get_master_leagues 				= get_master_leagues;

	function get_games()
	{
		return dataSavingHttp({
			url: site_url+"contest/get_all_paid_games",
			data: {},
		}).success(function (response)
		{
			os.gameList = {};
			if(!angular.isUndefined(response.data.games))
			{
				os.gameList = response.data.games;
			}
			os.off_season						= response.data.off_season;
			os.contestParam.off_season_end_date	= os.off_season.off_season_end_date;
			os.contestParam.season_year			= os.off_season.season_year;
			os.contestParam.is_current_year		= os.off_season.is_current_year == '1' ? true : false;
			$timeout(function() {
				angular.element('#off_season_year').select2('val',os.contestParam.season_year);
			}, 10);

			os.gameListLength = Object.keys(os.gameList).length;
		}).error(function(error){
			showServerError(error);
		});
	}

	function sendNotifications()
	{
		return dataSavingHttp({
			url: site_url+"contest/update_off_season",
			data: os.contestParam,
		}).success(function (response)
		{
			$rootScope.alert_success = response.message;

		}).error(function(error){
			if(error.hasOwnProperty('error'))
			{
				showServerError(error);
			}
			else
			{
				$rootScope.alert_error = error.message;
			}
		});	
	}

	function setDate(date, field)
	{
		if(field == "off_season_end_date")
		{
			os.contestParam[field] = date;
		}
		else
		{
			os.draftParam[field] = date;
			if(field == "min_draft_date")
			{
				var min_date = os.draftParam.min_draft_date;
				os.draftParam.min_draft_date = false;
				$timeout(function() {
					os.draftParam.min_draft_date = min_date;
					var m_date = new Date(min_date);
					var next = new Date(m_date.getTime() + 24*60*60*1000);
					var final = $.datepicker.formatDate('yy-mm-dd', next);
					os.draftParam.max_draft_date = final;
				}, 10);
			}
		}
		angular.element('#'+field+"_error").addClass('hide');
	}

	function OffSeasonChange()
	{
		var year = os.contestParam.season_year;
		os.contestParam.season_year = false;

		// get data for selected year	
		return dataSavingHttp({
			url: site_url+"contest/get_off_season_date_by_year",
			data: {'season_year': year}
		}).success(function (response)
		{
			os.contestParam.is_current_year = false;
			os.contestParam.off_season_end_date = "";
			if(response.data != null)
			{
				os.contestParam.is_current_year = (response.data.is_current_year == '1') ? true : false;
				os.contestParam.off_season_end_date = response.data.off_season_end_date;
			}
			$timeout(function() {
				os.contestParam.season_year = year;
			}, 10);
		}).error(function(error){
			$timeout(function() {
				os.contestParam.season_year = year;
				os.contestParam.is_current_year = false;
			}, 10);
			if(error.hasOwnProperty('error'))
			{
				showServerError(error);
			}
			else
			{
				$rootScope.alert_error = error.message;
			}
		});	
	}

	function OnYearChange()
	{
		var year = os.draftParam.season_year;
		os.draftParam.season_year = false;	
		$timeout(function() {
			os.draftParam.season_year = year;
		}, 10);
	}

	function saveDraftDate()
	{
		return dataSavingHttp({
			url: site_url+"contest/update_master_draft_date",
			data: os.draftParam,
		}).success(function (response)
		{
			$rootScope.alert_success = response.message;

		}).error(function(error){
			if(error.hasOwnProperty('error'))
			{
				showServerError(error);
			}
			else
			{
				$rootScope.alert_error = error.message;
			}
		});	
	}

	function get_master_leagues()
	{
		var post = { url : site_url+"league_news/get_all_league" };
 		dataSavingHttp(post).success(function (response)
		{
			os.leagues = response.data;
		}).error(function (error)
		{
			os.leagues = [];
		});
	}
	os.get_master_leagues();
}	
