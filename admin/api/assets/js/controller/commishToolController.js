'use strict';

angular.module("vfantasy").controller('CommishToolCtrl', ['$scope', '$stateParams', '$location', '$state','$timeout','$rootScope', 'settings', 'dataSavingHttp', 'showServerError','$filter', function($scope, $stateParams, $location, $state, $timeout, $rootScope, settings, dataSavingHttp, showServerError,$filter){
	
	$rootScope.settings.layout.pageBodyFullWidth = true;

	$scope.game_unique_id = $stateParams.game_unique_id;

	$scope.getContestDetail = function()
	{
		$scope.game_id = $stateParams.game_unique_id;
		var post = {
						url		: site_url+"CommishTool/get_contest_detail",
						data	: {	game_unique_id	:$scope.game_unique_id	}
					};

		dataSavingHttp(post).success(function (response){
			console.log(response,"GAME details");
			$scope.gameDetail = response.data.game_details;
	
		});
	}

	$scope.goToContest = function()
	{
		$state.go('contest');
	}
	$scope.goToLineup = function()
	{
		$state.go('lineup-detail', {'game_unique_id' : $scope.game_unique_id});
		return;
	}

	$scope.goToCommishnerTool = function()
	{
		$state.go('commish-tool', {'game_unique_id' : $scope.game_unique_id});
		return;
	}

	/* Acquisition Budget  */
	$scope.acqBudget =  "";
	$scope.gameBudget =  "";
	$scope.getGameAcquisitionBudget = function()
	{
		var post = {
						url		: site_url+"CommishTool/get_contest_detail",
						data	: {	game_unique_id	:$scope.game_unique_id	}
					};

		dataSavingHttp(post).success(function (response){

			$scope.gameBudget = (response.data.game_details.waiver_budget == null || typeof response.data.game_details.waiver_budget == "undefined") ? '0.00' : response.data.game_details.waiver_budget;
			$scope.gameDetail = response.data.game_details;	

		});		
	}
	$scope.updateAcqBudget = function()
	{
		if (!$scope.acqBudget) {return false;}

		var post = {
						url		: site_url+"CommishTool/update_acquisition_budget",
						data	: {	game_unique_id	:$scope.game_unique_id , "acq_budget" :$scope.acqBudget	}
					};

		dataSavingHttp(post).success(function(response){

			$scope.acqBudget= "";
			$scope.getGameAcquisitionBudget();
			$rootScope.alert_success = 'Acquisition budget updated sucessfully';

		});		
	}

	/* Acquisition Budget  */

	/* Lock  Teams  */

	$scope.getGameTeam = function()
	{
		var post = {
						url		: site_url+"CommishTool/get_game_teams",
						data	: {	game_unique_id	:$scope.game_unique_id	}
					};

		dataSavingHttp(post).success(function (response)
		{
			$scope.teamList   = response.data.team_list;
			$scope.gameDetail = response.data.game_details;
			$timeout(function(){
				angular.element('input[type=checkbox]').each(function(){
					if(angular.element(this).val() ==0)
					{
					  angular.element(this).prop('checked',true);
					}
				});	
			});
		});		
		
	}

	$scope.updateTeamSetting = function(indx,actionKey){
		$scope.toolSettingObj = {};
		var lineupid = '';
		var isChecked = angular.element('.'+actionKey+'_'+$scope.teamList[indx].lineup_master_id).is(':checked');		
		$scope.teamList[indx][actionKey] = (isChecked) ? "0" : "1";
	};

	$scope.updateTeamLock = function()
	{
		var post = {
						url		: site_url+"CommishTool/update_teams_lock_setting",
						data	: {	'game_unique_id'	:$scope.game_unique_id, 'lock_details':$scope.teamList	}
					};

		dataSavingHttp(post).success(function (response)
		{
			$rootScope.alert_success = response.message;
		},function (error)
		{
			
		});			
	}


}]);
