'use strict';

angular.module("vfantasy").controller('WithdrawalCtrl', ['$scope', '$timeout', '$state', '$rootScope', 'settings', 'dataSavingHttp', '$location', function($scope, $timeout, $state, $rootScope, settings, dataSavingHttp, $location){

	$rootScope.settings.layout.pageBodyFullWidth = true;

	$scope.withdrawalObj							= {};
	$scope.withdrawalObj.action						= "";
	$scope.withdrawalObj.selectall					= false;
	$scope.withdrawalObj.withdraw_transaction_id	= [];
	// $scope.withdrawalObj.description				= "";

	$scope.withdraw_transaction_id	= [];
	$scope.withdrawalList			= [];
	$scope.isLoading				= false;
	/* Start Withrawal Listing Functions */
	$scope.getFilterData = function() {
		dataSavingHttp({
			url: site_url+"withdrawal/get_filter_data",
			data: $scope.withdrawalObj
		}).success(function (response) {
			
			$scope.withdrawalType = response.data.withdrawal_type;
			$scope.withdrawalStatus = response.data.withdrawal_status;			
		});
	}
	$scope.getWithdrawalList = function() {
		if($scope.isLoading) return;
		$scope.isLoading = true;
		$location.search($scope.withdrawalParam);
		// $scope.withdrawalObj.selectall = false;
		$rootScope.updateUi();

		dataSavingHttp({
			url: site_url+"withdrawal/get_all_withdrawal_request",
			data: $scope.withdrawalParam,
		}).success(function (response) {
			$scope.withdrawalList = response.data.result;
			$scope.withdrawalParam.totalItems = response.data.total;
			$scope.isLoading = false;
		}).error(function (error) {
			$scope.isLoading = false;
		});
	};

	$scope.sortWithdrawalList = function(sort_field) {

		if($scope.isLoading) return;
		var sort_order = 'DESC';

		if(sort_field==$scope.withdrawalParam.sort_field){
			sort_order = ($scope.withdrawalParam.sort_order=='DESC')?'ASC':'DESC';
		}
		$scope.withdrawalParam.sort_field	= sort_field;
		$scope.withdrawalParam.sort_order	= sort_order;
		$scope.withdrawalParam.total_items	= 0;
		$scope.withdrawalParam.current_page	= 1;
		$scope.getWithdrawalList();
	};

	$scope.updateWithdrawalRequest = function() {
		$rootScope.current_loader = '.btn-primary';
		
		if($scope.withdrawalObj.action == "")
		{
			$rootScope.alert_warning = $rootScope.lang.SELECT_ACTION;
			return false;
		}
		if($scope.withdrawalObj.withdraw_transaction_id.length==0)
		{
			$rootScope.alert_warning = $rootScope.lang.SELECT_WITHDRAWAL;
			return false;
		}
			
		dataSavingHttp({
			url: site_url+"withdrawal/update_withdrawal_request",
			data: $scope.withdrawalObj
		}).success(function (response) {
			$rootScope.alert_success = response.message;
			angular.forEach($scope.withdraw_transaction_id, function(value, key) {
				if(value)
				{
					$scope.withdrawalList[key].status = $scope.withdrawalObj.action;
				}
			});
			// angular.element("#all_withdrawal_status_model").modal('hide');
			// $scope.withdrawalObj.description = "";
			$scope.deselectWithdrawal();
		}).error(function(error) {
			$rootScope.alert_error = error.message;
			$scope.deselectWithdrawal();
		});
	};

	$scope.changeWithdrawalRequest = function() {
		dataSavingHttp({
			url: site_url+"withdrawal/change_withdrawal_status",
			data: $scope.withdrawalObj,
		}).success(function (response) {
			// console.log($scope.withdrawalObj.index);return false;
			if(response.status)
			{
				$rootScope.alert_success = response.message;
				// angular.element("#withdrawal_status_model").modal('hide');
				$scope.withdrawalList[$scope.withdrawalObj.index].status = $scope.withdrawalObj.status;
				
			} 
		}).error(function(error){
			$rootScope.alert_error = error.message;
		});
	};
	/* End Withrawal Listing Functions */

	/* Start Withrawal Extra Functions */
	$scope.toggleWithdrawal = function($event){
		$scope.withdrawalObj.withdraw_transaction_id=[];
		$scope.withdraw_transaction_id	= $scope.withdraw_transaction_id = Array.apply(null, Array($scope.withdrawalList.length)).map(function() { return $event.target.checked; });
		if($event.target.checked){
			for (var i = 0; i < $scope.withdrawalList.length; i++) {
				console.log($scope.withdrawalObj.withdraw_transaction_id);
				$scope.withdrawalObj.withdraw_transaction_id.push($scope.withdrawalList[i].payment_withdraw_transaction_id);
			};
		}
		$rootScope.updateUi();
	};

	$scope.deselectWithdrawal = function() {
		$scope.withdraw_transaction_id = {};
		$scope.withdrawalObj.withdraw_transaction_id = [];
		$scope.withdrawalObj.selectall = false;
		$rootScope.updateUi();
	}
	$scope.selectWithdrawal = function($event) {

		var withdraw_transaction_id = $event.target.value;
		var index = $scope.withdrawalObj.withdraw_transaction_id.indexOf(withdraw_transaction_id);
		
		($event.target.checked)?$scope.withdrawalObj.withdraw_transaction_id.push(withdraw_transaction_id):$scope.withdrawalObj.withdraw_transaction_id.splice(index, 1);

		if($scope.withdrawalObj.withdraw_transaction_id.length==$scope.withdrawalList.length)
		{
			$scope.withdrawalObj.selectall = true;
		}
		else
		{
			$scope.withdrawalObj.selectall = false;
		}
		$rootScope.updateUi();
	};

	$scope.initObject = function() {
		$scope.withdrawalParam					= {};
		$scope.withdrawalParam.items_perpage	= 10;
		$scope.withdrawalParam.total_items		= 0;
		$scope.withdrawalParam.type 			= "";
		$scope.withdrawalParam.status 			= "";
		$scope.withdrawalParam.current_page		= 1;
		$scope.withdrawalParam.sort_order		= 'DESC';
		$scope.withdrawalParam.sort_field		= 'created_date';
		var query_data = $location.search();
		$scope.withdrawalParam = angular.extend($scope.withdrawalParam, query_data);
		$timeout(function(){
			angular.element("#withdrawal_type").select2("val", $scope.withdrawalParam.type);
			angular.element("#withdrawal_status").select2("val", $scope.withdrawalParam.status);
		},500);
		$scope.getWithdrawalList();
	}

	$scope.clearFilter = function () {
		$scope.withdrawalParam					= {};
		$scope.withdrawalParam.items_perpage	= 10;
		$scope.withdrawalParam.total_items		= 0;
		$scope.withdrawalParam.type 			= "";
		$scope.withdrawalParam.status 			= "";
		$scope.withdrawalParam.current_page		= 1;
		$scope.withdrawalParam.sort_order		= 'DESC';
		$scope.withdrawalParam.sort_field		= 'created_date';		
		$timeout(function(){
			angular.element("#withdrawal_type").select2("val", $scope.withdrawalParam.type);
			angular.element("#withdrawal_status").select2("val", $scope.withdrawalParam.status);
		});
		$scope.getWithdrawalList();
	};
	$scope.showPopUp = function(withdrawal_id,status,index) {
		// angular.element("#withdrawal_status_model").modal('show');
		$scope.withdrawalObj.payment_withdraw_transaction_id	= withdrawal_id;
		$scope.withdrawalObj.status								= status;
		$scope.withdrawalObj.index								= index;
		$scope.changeWithdrawalRequest();
	};
	$scope.showPopUpAllWithdrawal = function() {
		// angular.element("#all_withdrawal_status_model").modal('show');
		$scope.updateWithdrawalRequest();
	};
	/* End Withrawal Extra Functions */
}]);