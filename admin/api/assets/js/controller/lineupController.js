'use strict';

angular.module("vfantasy").controller('LineupCtrl', ['$scope', '$stateParams', '$location', '$state','$timeout','$rootScope', 'settings', 'dataSavingHttp', 'showServerError','$filter', function($scope, $stateParams, $location, $state, $timeout, $rootScope, settings, dataSavingHttp, showServerError,$filter){
	$scope.LineupTeams	= [];
	$scope.LineupDetail	= [];

	$scope.getTeamPlayers = function(lineup_id)
	{
		if(lineup_id)
		{
			if($scope.LineupTeams[lineup_id][0].player_unique_id !== "" && $scope.LineupTeams[lineup_id][0].player_unique_id != null)
			{
				$scope.LineupDetail = $scope.LineupTeams[lineup_id];
			}
			else
			{
				$scope.LineupDetail = [];		
			}
		}
		else
		{
			$scope.LineupDetail = [];
		}
	}

	$scope.goToContestDetails = function()
	{
		$state.go('contest_detail', {'game_unique_id' : $scope.game_id});
		return;
	}

	if($stateParams.game_unique_id)
	{
		$scope.game_id = $stateParams.game_unique_id;
		dataSavingHttp({
			url: site_url+"contest/get_user_lineup_detail",
			data: {game_unique_id:$scope.game_id},
		}).success(function (response)
		{
			
			$scope.LineupTeams = response.data;
			
			
		});
	}
	else
	{
		$rootScope.alert_error = "Invalid game";
		$state.go('contest');
		return;
	}
}]);