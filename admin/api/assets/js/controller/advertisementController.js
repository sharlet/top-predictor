'use strict';

angular.module("vfantasy").controller('AdvertisementCtrl', ['$scope', '$timeout', '$filter', '$state', '$location', '$rootScope', 'settings', 'dataSavingHttp', 'showServerError', function($scope, $timeout, $filter, $state, $location, $rootScope, settings, dataSavingHttp, showServerError){

	$rootScope.settings.layout.pageBodyFullWidth = true;

	$scope.adsList					= {};
	
	$scope.adsObj					= {};
	$scope.adsObj.name				= "";
	$scope.adsObj.target_url		= "";
	$scope.adsObj.position_type		= "";
	$scope.adsObj.ad_image			= "";
	$scope.adsPositionList 			= {};


	$scope.initAdvertisement = function(){
		$scope.isLoading = false;
		$scope.adsParam					= {};
		$scope.adsParam.items_perpage	= 10;
		$scope.adsParam.total_items		= 0;
		$scope.adsParam.current_page	= 1;
		$scope.adsParam.sort_order		= "ASC";
		$scope.adsParam.sort_field		= "ADM.name";
		$scope.adsObj.is_preview = 0;
        $scope.adsObj.is_remove  = 0;
        $scope.adsObj.uploadbtn  = 1;
        $scope.adsObj.image_name = '';
        $scope.adsObj.size_tip = '';
		dataSavingHttp({
			url: site_url+"advertisements/get_positions",
			data: {},
		}).success(function (response) {
			$scope.adsPositionList 			= response.data;
		}).error(function(error){
			if(error.hasOwnProperty("message"))
				$rootScope.alert_error = error.message;	
			else
				showServerError(error);
		});


		$scope.getAdvertisements();
		if($state.current.name == 'new_advertisement'){
			$scope.intializeImageLoad();
		}
	}

	$scope.intializeImageLoad = function ()
    {

        var btnupload = $('#upload_btn');
    	
      
        new AjaxUpload(btnupload, {
            action: site_url + 'advertisements/do_upload/',
            name: 'userfile',
            responseType: 'json',
            type: "POST", 
            data: {field_name: 'userfile'},
            onSubmit: function (file, ext)
            {
                var post_type = $scope.adsObj.position_type;
                if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext)))
                {
                	$rootScope.alert_error = $rootScope.lang.invalid_img_type;
					$scope.$apply();
					return false;
                }
                
                if(post_type == '')
                {
                	$rootScope.alert_error = $rootScope.lang.position_error;
                    $scope.$apply();
                    return false;
                }
               
                this.setData({'post_type': post_type});
                $scope.$apply();
				$rootScope.$apply();           
            },
            onComplete: function (file, response)
            {  
                if(response.status)
                {
                	
                	$scope.adsObj.ads_image = response.data;
                	$scope.adsObj.image_name = response.file_name;
                	$scope.adsObj.is_preview = 1;
			        $scope.adsObj.is_remove  = 1;
			        $scope.adsObj.uploadbtn  = 0;
                    $scope.$apply();
					$rootScope.$apply(); 
                }
                else
                {
                	$rootScope.alert_error = response.message;
                    $scope.adsObj.is_preview = 0;
			        $scope.adsObj.is_remove  = 0;
			        $scope.adsObj.uploadbtn  = 1;
                  	 $scope.$apply();
					$rootScope.$apply(); 
                }
            }
        });
    };
    
   
    $scope.removeImage = function ()
    {
       
        var PostData = {image_name :$scope.adsObj.image_name};
       	dataSavingHttp({
			url: site_url+"advertisements/remove_image",
			data: PostData,
		}).success(function (response) {
			$scope.adsObj.is_preview = 0;
            $scope.adsObj.is_remove  = 0;
            $scope.adsObj.uploadbtn  = 1;
		}).error(function(error){
			if(error.hasOwnProperty("message"))
				$rootScope.alert_error = error.message;	
			else
				showServerError(error);
		});


   
    };


	//for new ad creation
	$scope.newAdvertisement = function() {
		
		
		dataSavingHttp({
			url: site_url+"advertisements/create_advertisement",
			data: $scope.adsObj,
		}).success(function (response) {
			$rootScope.alert_success = response.message;
			$timeout(function(){
				$location.path('/advertisement');
			},1000);
		}).error(function(error){
			if(error.hasOwnProperty("message"))
				$rootScope.alert_error = error.message;	
			else
				showServerError(error);
		});
	};

	$scope.getAdvertisements = function() {
		if($scope.isLoading) return;
		$scope.isLoading = true;

		dataSavingHttp({
			url: site_url+"advertisements/get_advertisement",
			data: $scope.adsParam,
		}).success(function (response) {
			$scope.adsList				= response.data.result;
			$scope.adsParam.total_items	= response.data.total;
			$scope.isLoading					= false;
		}).error(function(error){
			showServerError(error);
		});
	};

	$scope.sortAdvertisementList = function(sort_field) {
		if($scope.isLoading) return;
		var sort_order = 'DESC';
		if(sort_field==$scope.adsParam.sort_field){
			sort_order = ($scope.adsParam.sort_order=='DESC')?'ASC':'DESC';
		}
		$scope.adsParam.sort_field	= sort_field;
		$scope.adsParam.sort_order	= sort_order;
		$scope.adsParam.total_items	= 0;
		$scope.adsParam.current_page	= 1;
		$scope.getAdvertisements();
	};

	$scope.doBlur = function($element){
		//console.log('id is '+$element);
		angular.element("#"+$element).trigger("blur");
	};

	$scope.update_status = function(ad_id,ad_status,data_index){
		var update_param ={status:ad_status,ad_management_id:ad_id};
		dataSavingHttp({
			url: site_url+"advertisements/update_status",
			data: update_param,
		}).success(function (response) {
			$rootScope.alert_success	= response.message;
			$scope.adsList[data_index].status = ad_status;
			
		}).error(function(error){
			showServerError(error);
		});

	}

	$scope.getPositionInfo = function(){
		var sel_position = $scope.adsObj.position_type;
		console.log("sel position : "+sel_position);
		if(sel_position != ''){
			angular.forEach($scope.adsPositionList, function(value, key) {
			  	if(value.ad_position_id == sel_position){
			  		$scope.adsObj.size_tip = '('+value.width+'x'+value.height+')';
			  	}
			});
		} else {
			$scope.adsObj.size_tip = '';
		}
	}

$scope.deleteAdv = function(ad_id,data_index)
{
	var update_param ={ad_management_id:ad_id};
	dataSavingHttp({
		url: site_url+"advertisements/delete_advertisement",
		data: update_param,
	}).success(function (response) {
		$rootScope.alert_success	= response.message;
		// delete $scope.adsList[data_index];
		$scope.adsList.splice(data_index, 1);
		
	}).error(function(error){
		showServerError(error);
	});
}

	$rootScope.updateUi();
}]);