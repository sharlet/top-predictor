'use strict';

angular.module("vfantasy").controller('SeasonCtrl', ['$scope', '$timeout', '$state', '$rootScope', 'settings', 'dataSavingHttp', function($scope, $timeout, $state, $rootScope, settings, dataSavingHttp){

	$rootScope.settings.layout.pageBodyFullWidth = true;

	$scope.seasonStatsParam					= {};
	$scope.seasonStatsParam.items_perpage			= 10;
	$scope.seasonStatsParam.total_items				= 0;
	$scope.seasonStatsParam.current_page			= 1;
	// $scope.seasonStatsParam.sort_order			= "DESC";
	// $scope.seasonStatsParam.sort_field			= "scheduled_date";
	


	$scope.seasonStatsParam.fromdate				= "";
	$scope.seasonStatsParam.todate					= "";
	$scope.seasonStatsParam.league_id				= 1;

	$scope.sesasonObj							= {};
	$scope.sesasonObj.action					= "";
	$scope.sesasonObj.selectall					= false;
	$scope.sesasonObj.withdraw_transaction_id	= [];

	$scope.withdraw_transaction_id	= []
	$scope.teams					= [];
	$scope.leagues					= [];
	$scope.seasonList				= [];
	$scope.summaries				= "";

	$scope.seasonWeek =		{};
	$scope.seasonFormLeagueId 		='';
	$scope.isLoading = false;

	$scope.current_season_game_unique_id	= $state.params.game_unique_id;
	$scope.current_league_id				= $state.params.league_id;

	/* Start Season Listing Functions */
	$scope.getSeasonList = function() {
		if($scope.isLoading) return;
		$scope.isLoading = true;

		$rootScope.updateUi();

		dataSavingHttp({
			url: site_url+"season/get_all_season_schedule",
			data: $scope.seasonParam,
		}).success(function (response) {
			$scope.seasonList = response.data.result;
			$scope.seasonParam.total_items = response.data.total;
			$scope.isLoading = false;
		}).error(function (error) {
			$scope.isLoading = false;
		});
	};

	$scope.sortSeasonList = function(sort_field) {

		if($scope.isLoading) return;
		var sort_order = 'DESC';

		if(sort_field==$scope.seasonParam.sort_field){
			sort_order = ($scope.seasonParam.sort_order=='DESC')?'ASC':'DESC';
		}
		$scope.seasonParam.sort_field  = sort_field;
		$scope.seasonParam.sort_order  = sort_order;
		$scope.seasonParam.total_items  = 0;
		$scope.seasonParam.current_page = 1;
		$scope.getSeasonList();
	};

	$scope.getAllLeague = function() {
		dataSavingHttp({
			url: site_url+"season/get_all_league"
		}).success(function (response) {
			if(response.status)
			{	
				$scope.current_page = 0;
				$scope.leagues = response.data;
				$scope.seasonParam.league_id = response.data[0].league_id;
				$timeout(function(){
					angular.element("#league_id").select2("val", response.data[0].league_id);
				},0)
				$scope.getAllTeam();
				$rootScope.updateUi();
			}
		}).error(function(error){

		});
	};

	$scope.getAllTeam = function() {
		if(!$scope.seasonParam.league_id) return;
		dataSavingHttp({
			url: site_url+"season/get_all_team",
			data: {league_id:$scope.seasonParam.league_id}
		}).success(function (response) {
			if(response.status)
			{
				$scope.teams = response.data;
				$scope.seasonParam.team_id = response.data[0].team_id;
				$timeout(function(){
					angular.element("#team_id").select2("val", "");
				},0)
				$scope.getAllYear();
				$scope.getAllWeek();
			 	// Used in Custome Season Feed Form
                $scope.initSeasonFormData();
			}
		}).error(function(error){

		});
	};

	$scope.getAllWeek = function() {
		if(!$scope.seasonParam.league_id) return;
		dataSavingHttp({
			url: site_url+"season/get_all_week",
			data: {league_id:$scope.seasonParam.league_id}
		}).success(function (response) {
			if(response.status)
			{
				$scope.weeks = response.data;
				$scope.seasonParam.week = response.data[0].week;
				$timeout(function(){
					angular.element("#week").select2("val", "");
				},0)
				$scope.getSeasonList();
			}
		}).error(function(error){

		});
	};

// get Season Year
	$scope.getAllYear = function() {
		if(!$scope.seasonParam.league_id) return;
		dataSavingHttp({
			url: site_url+"season/get_all_year",
			data: {league_id:$scope.seasonParam.league_id}
		}).success(function (response) {
			if(response.status)
			{
				$scope.years = response.data.years;
				$scope.current_year = response.data.current_year;
				// $timeout(function(){
				// 	angular.element("#year").select2("val", response.data.current_year);
				// },0)
				// $scope.getSeasonList();
			}
		}).error(function(error){

		});
	};

	$scope.initObject = function() {
		$scope.seasonParam					= {};
		$scope.seasonParam.items_perpage	= 10;
		$scope.seasonParam.total_items		= 0;
		$scope.seasonParam.current_page		= 1;
		$scope.seasonParam.sort_order		= "DESC";
		$scope.seasonParam.sort_field		= "season_scheduled_date";
		$scope.seasonParam.fromdate			= "";
		$scope.seasonParam.todate			= "";
		$scope.seasonParam.league_id		= 1;
		$scope.getAllLeague();
		$timeout(function(){
			angular.element("#league_id").select2("val", $scope.seasonParam.league_id);
			angular.element("#team_id").select2("val", "");
			angular.element("#week").select2("val", "");
		});
	};

	$scope.clearFilter = function () {
		$scope.initObject();		
		angular.element("#league_id").select2("val", $scope.leagues[0].league_id);
		$scope.seasonParam.league_id = $scope.leagues[0].league_id;
		$scope.getPlayerList();
	};

	/* Start Season Listing Functions */
	$scope.getSeasonStatsList = function() {
		if($scope.isLoading) return;
		$scope.isLoading = true;
		$scope.seasonStatsParam.game_unique_id = $state.params.game_unique_id;
		$scope.seasonStatsParam.league_id = $state.params.league_id;

		dataSavingHttp({
			url: site_url+"season/get_season_stats",
			data: $scope.seasonStatsParam,
		}).success(function (response) {
			$scope.season_stats					= response.data.result;
			$scope.fields						= response.data.fields;
			$scope.summaries					= response.data.summary;
			$scope.seasonStatsParam.total_items	= response.data.total;
			$scope.isLoading					= false;
		}).error(function (error) {
			$scope.isLoading = false;
		});
	};
	
	$scope.sortSeasonStatsList = function(sort_field) {

		if($scope.isLoading) return;
		var sort_order = 'DESC';

		if(sort_field==$scope.seasonStatsParam.sort_field){
			sort_order = ($scope.seasonStatsParam.sort_order=='DESC')?'ASC':'DESC';
		}
		$scope.seasonStatsParam.sort_field  = sort_field;
		$scope.seasonStatsParam.sort_order  = sort_order;
		$scope.seasonStatsParam.total_items  = 0;
		$scope.seasonStatsParam.current_page = 1;
		$scope.getSeasonStatsList();
	};

/*	$scope.getAddStatisticsFormData = function(){
                
        //if($scope.isLoading) return;
                
		//$scope.isLoading = true;
        var ReqParam = {
            season_game_unique_id   : $state.params.game_unique_id,
            league_id               : $state.params.league_id
        };
                
		dataSavingHttp({
			url: site_url+"stats_feed/get_add_score_stats_form_data",
			data: ReqParam,
		}).success(function (response) {
			$scope.players		= response.data.player;
			$scope.scoring_category	= response.data.scoring_category;
			$scope.scoring_rule     = response.data.scoring_rule;
			$scope.teams    	= response.data.team;
			$scope.positions    	= response.data.position;
			//$scope.isLoading	= false;
		}).error(function (error) {
			//$scope.isLoading = false;
		});
    };*/

    /*******************************************
         ******** CUSTOM SEASON FEED START *********
         *******************************************/
        
    $scope.initSeasonFormData = function (){
        $scope.season_type = ['REG','PST','PRE'];
        
        $scope.empty_season = {
                home_id         : '',
                away_id         : '',
                type            : '',
                scheduled_date  : '',
                row             : ''
        };
            
        $scope.season_data = {
                league_id               : $scope.seasonParam.league_id,
                seasons                 : []
        }
            
        $scope.season_data.seasons.push($scope.empty_season);
        
        // $scope.getAddSeasonFormData();
    };

    $scope.onChangeHomeTeam = function(index){
        if($scope.season_data.seasons[index].home_id == $scope.season_data.seasons[index].away_id){
                $scope.season_data.seasons[index].away_id = '';
                angular.element("#ateam_id_"+index).select2("val", '');
        }
    };

    $scope.AddSeasonRow = function () {

        if($scope.season_data.seasons.length > 0){
        //    $scope.SaveSeason('AddRow');
        }
        
        $scope.season_data.seasons.push({
                home_id         : '',
                away_id         : '',
                type            : '',
                scheduled_date  : '',
                row             : '',
                year 			: ''
        });
        console.log($scope.season_data.seasons);
    };

    $scope.RemoveSeasonRow = function (index) {
            
        if ($scope.season_data.seasons.length > 1) {

            $scope.season_data.seasons[index] = [];
            
            $timeout(function () {
                
                $scope.season_data.seasons.splice(index, 1);
                
                // reste remaing row's select box values
                angular.forEach($scope.season_data.seasons, function(value, key) {
                    if(value ) {
				  		angular.element("#hteam_id_"+key).select2("val", value.home_id);
		                angular.element("#ateam_id_"+key).select2("val", value.away_id);
		                angular.element("#year_"+key).select2("val", value.year);
				  	}
				});
                    
            }, 100);
            
        }
    };

    $scope.getAddSeasonFormData = function(){
                
        var ReqParam = {
            league_id               : $scope.seasonFormLeagueId
        };
                
		dataSavingHttp({
			url: site_url+"stats_feed/get_add_season_form_data",
			data: ReqParam,
		}).success(function (response) {
			$scope.formleague = true;
			$scope.home_teams    	= response.data.team;
			$scope.away_teams    	= response.data.team;
			angular.element(".home_teams").select2("val", "");
			angular.element(".away_teams").select2("val", "");
			
			
		}).error(function (error) {
			
		});
    };

    $scope.showAddSeasonPopup = function() {
            $scope.formleague = false;
        // Reste all select box values
        angular.forEach($scope.season_data.seasons, function(value, key) {
                if(value ) {
                        angular.element("#hteam_id_"+key).select2("val", "");
                        angular.element("#ateam_id_"+key).select2("val", "");
                        angular.element("#year_"+key).select2("val","");
                }
        });
        
        $scope.season_data.seasons = [];
        
        $scope.AddSeasonRow();
                
		angular.element("#add_season_modal").modal('show');
		$rootScope.updateUi();
                //$scope.getAddStatisticsFormData();
	};

    $scope.SaveSeason = function(RequestFrom) {
                
        var reqData = {
            league_id               : $scope.seasonFormLeagueId,
            home_id                 : [],
            away_id                 : [],
            type                    : [],
            scheduled_date          : [],
            row                     : [],
            year 					:[],
        };
        angular.forEach($scope.season_data.seasons, function(value, key) {
                        
	        angular.forEach(value, function(val, k) {
	                    if(k == 'home_id') {
	                        reqData.home_id.push(val);
	                    }
	                    else if(k == 'away_id') {
	                        reqData.away_id.push(val);
	                    }
	                    else if(k == 'type') {
	                        reqData.type.push(val);
	                    }
	                    else if(k == 'scheduled_date'){
	                        reqData.scheduled_date.push(val);
	                    }
	                    else if(k == 'row'){
	                        reqData.row.push(val);
	                    }
	                    else if(k == 'year'){
	                        reqData.year.push(val);
	                    }	                    
	                });
        });
                
		$rootScope.current_loader = '.btn-success';
		
		$scope.isLoading = true;
		
		dataSavingHttp({
			url: site_url+"stats_feed/create_season_stats",
			data: reqData,
		}).success(function (response) {
			$scope.isLoading = false;
			
            if(RequestFrom != 'AddRow') {
                
                $rootScope.alert_success = response.message;
                angular.element("#add_season_modal").modal('hide');
               
                $timeout(function(){
                        $state.reload();
                }, 500);
            }
		}).error(function(error) {
                    
            if(RequestFrom != 'AddRow') {
                //$rootScope.alert_error = error.message;
            }
		});
	};

    $scope.showSeasonDelConfirmPopUp = function(stats_data) {
                $scope.delete_season_data = stats_data;
		angular.element("#delete_confirm_model").modal('show');
	};

    $scope.delete_season_data= {};
    $scope.deleteSeason = function () {
        $rootScope.current_loader = '.btn-success';
		
		$scope.isLoading = true;
		
		dataSavingHttp({
			url: site_url+"stats_feed/delete_season_stats",
			data: $scope.delete_season_data,
		}).success(function (response) {
			$scope.isLoading = false;
			
			$rootScope.alert_success = response.message;
                        
            angular.element("#delete_confirm_model").modal('hide');
                        
            // Reste statistics modal
            $scope.delete_season_data= {};
                        
            $timeout(function(){
                    $state.reload();
            }, 500);
                        
		}).error(function(error) {
			$scope.isLoading = false;
			$rootScope.alert_error = error.message;
			 angular.element("#delete_confirm_model").modal('hide');
		});
    };

    $scope.downloadSeasonTemplate = function(){
        var url = "stats_feed/export_season_template/"+$scope.seasonParam.league_id+"";
		window.open(site_url+url);
    }
        
    $scope.saveUploadedSeasonData = function(data){
        //console.log('data: ',data.data.file_name);
            
        $scope.isLoading = true;
                
        var reqData = {
            league_id               : $scope.seasonParam.league_id,
            file_name               : data.data.file_name
        };
		
		dataSavingHttp({
			url: site_url+"stats_feed/save_uploaded_season_data",
			data: reqData,
		}).success(function (response) {
			$scope.isLoading = false;
			
			$rootScope.alert_success = response.message;
                        
			//$location.path('/contest');
                        $timeout(function(){
                                $state.reload();
                        }, 500);
		}).error(function(error) {
			$rootScope.alert_error = error.message;
		});            
    }
        
    /******************************************
     ********* CUSTOM SEASON FEED END *********
     ******************************************/
    
    
    /*******************************************
     ********* CUSTOM SCORE FEED START *********
     *******************************************/
	$scope.rules = [];
        
    $scope.filterStatsResult = function() {
		$scope.seasonStatsParam.items_perpage	= 10;
		$scope.seasonStatsParam.total_items	= 0;
		$scope.seasonStatsParam.current_page	= 1;
		$scope.seasonStatsParam.sort_order	= "ASC";
		$scope.seasonStatsParam.sort_field	= "scoring_category_name"
                
		$timeout(function(){
			angular.element("#filter_team_id").select2("val", $scope.seasonStatsParam.team_id);
			angular.element("#filter_player_id").select2("val", $scope.seasonStatsParam.player_unique_id);
			angular.element("#filter_position").select2("val", $scope.seasonStatsParam.position);
		});
		$scope.getSeasonStatisticsFeedList();
	};

	$scope.initStatsFilterObject = function() {
		$scope.seasonStatsParam			= {};
		$scope.seasonStatsParam.items_perpage	= 10;
		$scope.seasonStatsParam.total_items	= 0;
		$scope.seasonStatsParam.current_page	= 1;
		$scope.seasonStatsParam.sort_order	= "ASC";
		$scope.seasonStatsParam.sort_field	= "scoring_category_name";
		$scope.seasonStatsParam.team_id         = "";
		$scope.seasonStatsParam.player_unique_id= "";
		$scope.seasonStatsParam.position        = "";

		$timeout(function(){
			angular.element("#filter_team_id").select2("val", "");
			angular.element("#filter_player_id").select2("val", "");
			angular.element("#filter_position").select2("val", "");
		});
	};

    $scope.clearStatsFilter = function () {
		$scope.initStatsFilterObject();
                
		$scope.getSeasonStatisticsFeedList();
	};
    /* Start Season Listing Functions */
	$scope.getSeasonStatisticsFeedList = function() {
		if($scope.isLoading) return;
		$scope.isLoading = true;
                
                $scope.seasonStatsParam.sort_order		= "ASC";
                $scope.seasonStatsParam.sort_field		= "";
                
		$scope.seasonStatsParam.season_game_unique_id = $state.params.game_unique_id;
		$scope.seasonStatsParam.league_id = $state.params.league_id;
                
		dataSavingHttp({
			url: site_url+"stats_feed/get_season_statisctics_feed",
			data: $scope.seasonStatsParam,
		}).success(function (response) {
			$scope.season_stats			= response.data.result;
			$scope.summaries			= response.data.summary;
			$scope.seasonStatsParam.total_items	= response.data.total;
			$scope.isLoading			= false;
		}).error(function (error) {
			$scope.isLoading = false;
		});
	};

        $scope.edit_stats_data = {};
        $scope.showEditStatisticsPopup = function(stats_data) {
			$scope.edit_stats_data = {};

			angular.element("#edit_statistics_modal").modal('show');
			$rootScope.updateUi();
			var edit_data = stats_data;
			$scope.edit_stats_data = edit_data;

			$timeout(function(){
			        angular.element("#team_id").select2("val", $scope.edit_stats_data.team_id);
			        angular.element("#player_id").select2("val", $scope.edit_stats_data.player_unique_id);
			        angular.element("#scoring_category").select2("val", $scope.edit_stats_data.scoring_type);
			        angular.element("#scoring_rule").select2("val", $scope.edit_stats_data.stats_key);
			}, 100);               
                
                //console.log($scope.edit_stats_data);
        };
        
        $scope.showAddStatisticsPopup = function() {
            
                // Reste all select box values
                angular.forEach($scope.statistics_data.statistics, function(value, key) {

                        if(value ) {
                                angular.element("#team_id_"+key).select2("val", "");
                                angular.element("#player_id_"+key).select2("val", "");
                                angular.element("#scoring_category_"+key).select2("val", "");
                                angular.element("#scoring_rule_"+key).select2("val", "");
                        }
                });
                
                $scope.statistics_data.statistics = [];
                
                $scope.AddStatisticRow();
                
		angular.element("#add_statistics_modal").modal('show');
		$rootScope.updateUi();
                //$scope.getAddStatisticsFormData();
	};
        
        $scope.getAddStatisticsFormData = function(){
                
                //if($scope.isLoading) return;
                
		//$scope.isLoading = true;
                var ReqParam = {
                    season_game_unique_id   : $state.params.game_unique_id,
                    league_id               : $state.params.league_id
                };
                
		dataSavingHttp({
			url: site_url+"stats_feed/get_add_score_stats_form_data",
			data: ReqParam,
		}).success(function (response) {
			$scope.players		= response.data.player;
			$scope.scoring_category	= response.data.scoring_category;
			$scope.scoring_rule     = response.data.scoring_rule;
			$scope.teams    	= response.data.team;
			$scope.positions    	= response.data.position;
			//$scope.isLoading	= false;
		}).error(function (error) {
			//$scope.isLoading = false;
		});
        };
        
        $scope.SaveStatistics = function(RequestFrom) {
                
                var reqData = {
                    season_game_unique_id   : $state.params.game_unique_id,
                    league_id               : $state.params.league_id,
                    team_id                 : [],
                    player_unique_id        : [],
                    scoring_type 			: [],
                    stats_key               : [],
                    stats_value             : [],
                    position 				: [],
                    minute                  : [],
                    row                     : []
                };
                angular.forEach($scope.statistics_data.statistics, function(value, key) {
                        //console.log(value);
                        angular.forEach(value, function(val, k) {
                            if(k == 'team_id'){
                                reqData.team_id.push(val);
                            }
                            else if(k == 'player_unique_id') {
                                reqData.player_unique_id.push(val);
                            }
                            else if(k == 'position') {
                                reqData.position.push(val);
                            }
                            else if(k == 'scoring_type') {
                                reqData.scoring_type.push(val);
                            }
                            else if(k == 'stats_key'){
                                reqData.stats_key.push(val);
                            }
                            else if(k == 'stats_value'){
                                reqData.stats_value.push(val);
                            }
                            else if(k == 'minute'){
                                reqData.minute.push(val);
                            }
                            else if(k == 'row'){
                                reqData.row.push(val);
                            }
                        });
                });
                //return false;
                
		$rootScope.current_loader = '.btn-success';
		
		$scope.isLoading = true;
		
		dataSavingHttp({
			url: site_url+"stats_feed/create_score_stats",
			data: reqData,
		}).success(function (response) {
			$scope.isLoading = false;
			
                        if(RequestFrom != 'AddRow') {
                            
                                $rootScope.alert_success = response.message;
                                angular.element("#add_statistics_modal").modal('hide');
                               
                               //$location.path('/contest');
                                $timeout(function(){
                                        $state.reload();
                                }, 500);
                        }
		}).error(function(error) {
                    console.log('RequestFrom -error:',RequestFrom);
                    
                    if(RequestFrom != 'AddRow') {
                        console.log('show error');
						$rootScope.alert_error = error.message;
                    }
		});
	};
        
        $scope.UpdateStatistics = function() {
            
		$rootScope.current_loader = '.btn-success';
		
		$scope.isLoading = true;
		
		dataSavingHttp({
			url: site_url+"stats_feed/edit_score_stats",
			data: $scope.edit_stats_data,
		}).success(function (response) {
			$scope.isLoading = false;
			
			$rootScope.alert_success = response.message;
                        
                        // Reste statistics modal
                        $scope.edit_stats_data= {};
                        
                        angular.element("#edit_statistics_modal").modal('hide');
			//$location.path('/contest');
                        $timeout(function(){
                                $state.reload();
                        }, 500);
                        
		}).error(function(error) {
			$rootScope.alert_error = error.message;
		});
	};
        
        $scope.showConfirmPopUp = function(stats_data) {
                $scope.delete_stats_data = stats_data;
		angular.element("#delete_confirm_model").modal('show');
	};
        
        $scope.delete_stats_data= {};
        
        $scope.deleteStatistics = function () {
                $rootScope.current_loader = '.btn-success';
		
		$scope.isLoading = true;
		
		dataSavingHttp({
			url: site_url+"stats_feed/delete_score_stats",
			data: $scope.delete_stats_data,
		}).success(function (response) {
			$scope.isLoading = false;
			
			$rootScope.alert_success = response.message;
                        
                        angular.element("#delete_confirm_model").modal('hide');
                        
                        // Reste statistics modal
                        $scope.delete_stats_data= {};
                        
                        $timeout(function(){
                                $state.reload();
                        }, 500);
                        
		}).error(function(error) {
			$rootScope.alert_error = error.message;
		});
        };
        
        $scope.RemoveStatisticRow = function (index) {
            
            if ($scope.statistics_data.statistics.length > 1) {

                $scope.statistics_data.statistics[index] = [];                
                $timeout(function () {
                    
                        $scope.statistics_data.statistics.splice(index, 1);
                        
                        // reste remaing row's select box values
                        angular.forEach($scope.statistics_data.statistics, function(value, key) {
                            
			  	if(value ) {
			  		angular.element("#team_id_"+key).select2("val", value.team_id);
                                        angular.element("#player_id_"+key).select2("val", value.player_unique_id);
                                        angular.element("#scoring_category_"+key).select2("val", value.scoring_type);
                                        angular.element("#scoring_rule_"+key).select2("val", value.stats_key);
			  	}
			});
                        
                }, 100);
                
            }
        };

        $scope.AddStatisticRow = function () {
//            if ($scope.statistics_data.statistics.length <= 4) { // max row limit 5 if lenght <= 4 
                if($scope.statistics_data.statistics.length > 0){
                    $scope.SaveStatistics('AddRow');
                }
                
                $scope.statistics_data.statistics.push({
					team_id				: '',
					player_unique_id	: '',
					scoring_type		: '',
					stats_key			: '',
					stats_value			: '',
					position 			: '',
					minute				: '',
					row					: ''
                });
//            } else {
//                $rootScope.alert_error = 'Max 5 rows allowed at one time';
//            }
        };
        
        $scope.onChangeTeam = function(index){
            
                $scope.statistics_data.statistics[index].player_unique_id = '';
                    
                angular.element("#player_id_"+index).select2("val", '');
        }
        $scope.onChangeScoringCategory = function(index,category){
            
           	 	$scope.rules[index]  = $scope.scoring_rule[category]; 
           	 	console.log(category,$scope.rules);
                $scope.statistics_data.statistics[index].stats_key = '';
                    
                angular.element("#scoring_rule_"+index).select2("val", '');
        }
        
        $scope.empty_statistic = {
			team_id				: '',
			player_unique_id	: '',
			scoring_type		: '',
			stats_key			: '',
			stats_value			: '',
			position			: '',
			minute				: '',
			row					: ''
        };
        $scope.statistics_data = {
                season_game_unique_id   : $state.params.game_unique_id,
                league_id               : $state.params.league_id,
                statistics              : []
        }
        //$scope.statistics_data.statistics = [];
        $scope.statistics_data.statistics.push($scope.empty_statistic);
        
        
        $scope.downloadStatsTemplate = function(){
                var url = "stats_feed/export_score_stats_template/"+$state.params.game_unique_id+'/'+$state.params.league_id+"";
		window.open(site_url+url);
        }
        
        $scope.saveUploadedStatsData = function(data){
                //console.log('data: ',data.data.file_name);
            
                $scope.isLoading = true;
                
                var reqData = {
                    season_game_unique_id   : $state.params.game_unique_id,
                    league_id               : $state.params.league_id,
                    file_name               : data.data.file_name
                };
		
		dataSavingHttp({
			url: site_url+"stats_feed/save_uploaded_score_stats_data",
			data: reqData,
		}).success(function (response) {
			$scope.isLoading = false;
			
			$rootScope.alert_success = response.message;
                        
			//$location.path('/contest');
                        $timeout(function(){
                                $state.reload();
                        }, 500);
		}).error(function(error) {
			$rootScope.alert_error = error.message;
		});
            
        }

$scope.selectPosition= function(index)
{
	var position = angular.element('#player_id_'+index+' option:selected').attr('data-val');
	$scope.statistics_data.statistics[index]['position'] = position;
	// console.log($scope.statistics_data.statistics);
}        
        /*****************************
         *** Custom SCORE Feed END ***
         *****************************/
	/* End Withrawal Listing Functions */

// ************** Create and Update Season Week ***************

$scope.create_update_week = function(){
	console.log($scope.seasonWeek);
	dataSavingHttp({
		url: site_url+"stats_feed/create_update_season_week",
		data: $scope.seasonWeek,
	}).success(function (response) {
		$rootScope.alert_success = response.message;
		$scope.getSeasonList();
	}).error(function(error) {
		$rootScope.alert_error = error.message;
	});
}

}]);