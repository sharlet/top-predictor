/*Form Validations*/
vfantasy.directive('loginForm', ['showFormError', 'hideFormError','$rootScope', function (showFormError, hideFormError, $rootScope) {
	return {
		restrict: 'A',
		scope: {
			submitHandle:'&'
		},
		link: function postLink($scope, iElement, iAttrs) {
			var validationRulesLogin = {email:{required:true,email:true},password:{required:true}};
			var validationLoginMessage = {
											email:{required:$rootScope.lang.EMAIL_REQUIRED, email:$rootScope.lang.EMAIL_NOT_VALID},
											password:{required:$rootScope.lang.PASS_REQUIRED}
			};

			iElement.validate({
				errorElement: "span",
				rules:validationRulesLogin,
				messages:validationLoginMessage,
				errorPlacement: function (error, element){
					showFormError(error, element);
				},
				submitHandler: function(form) {
					$scope.submitHandle();
				},
				success: function(error, element) {
					hideFormError(error, element);
				}
			});
		}
	};
}]);

vfantasy.directive('changepasswordForm', ['showFormError', 'hideFormError','$rootScope', function (showFormError, hideFormError, $rootScope) {
	return {
		restrict: 'A',
		scope: {
			submitHandle:'&'
		},
		link: function postLink($scope, iElement, iAttrs) {
			var validationRulesChangePass = {
									old_password: {
										required: true
									},
									new_password: {
										required: true,
										minlength: 6
									},
									confirm_password: {
										required: true,
										minlength: 6,
										equalTo: "#new_password"
									}
								};

			var validationChangePassMessage = {
							old_password:{required:$rootScope.lang.OLD_PASS_REQUIRED},
							new_password:{required:$rootScope.lang.NEW_PASS_REQUIRED, minlength:$rootScope.lang.PASS_MIN_LENGTH},
							confirm_password:{required:$rootScope.lang.CONFIRM_PASS_REQUIRED, minlength:$rootScope.lang.PASS_MIN_LENGTH, minlength:$rootScope.lang.PASS_NOT_MATCH}
			};

			iElement.validate({
				errorElement: "span",
				rules:validationRulesChangePass,
				messages:validationChangePassMessage,
				errorPlacement: function (error, element){
					showFormError(error, element);
				},
				submitHandler: function(form) {
					$scope.submitHandle();
				},
				success: function(error, element) {
					hideFormError(error, element);
				}
			});
		}
	};
}]);

vfantasy.directive('addbalanceForm', ['showFormError', 'hideFormError', function (showFormError, hideFormError) {
	return {
		restrict: 'A',
		scope: {
			submitHandle:'&'
		},
		link: function postLink($scope, iElement, iAttrs) {
			var validationRulesBalance = {amount:{required:true,maxlength: 7},description:{required:true,maxlength: 255},transaction_type:{required:true}};
			iElement.validate({
				errorElement: "span",
				rules:validationRulesBalance,
				errorPlacement: function (error, element){
					showFormError(error, element);
				},
				submitHandler: function(form) {
					$scope.submitHandle();
				},
				success: function(error, element) {
					hideFormError(error, element);
				}
			});
		}
	};
}]);

vfantasy.directive('updatesalespersonForm', ['showFormError', 'hideFormError', function (showFormError, hideFormError) {
	return {
		restrict: 'A',
		scope: {
			submitHandle:'&'
		},
		link: function postLink($scope, iElement, iAttrs) {
			var validationRulesBalance = {amount:{required:true,maxlength: 7},comment:{required:true,maxlength: 255}};
			iElement.validate({
				errorElement: "span",
				rules:validationRulesBalance,
				errorPlacement: function (error, element){
					showFormError(error, element);
				},
				submitHandler: function(form) {
					$scope.submitHandle();
				},
				success: function(error, element) {
					hideFormError(error, element);
				}
			});
		}
	};
}]);

vfantasy.directive('banuserForm', ['showFormError', 'hideFormError', function (showFormError, hideFormError) {
	return {
		restrict: 'A',
		scope: {
			submitHandle:'&'
		},
		link: function postLink($scope, iElement, iAttrs) {
			var validationRulesBalance = {reason:{required:true,maxlength: 255}};
			iElement.validate({
				errorElement: "span",
				rules:validationRulesBalance,
				errorPlacement: function (error, element){
					showFormError(error, element);
				},
				submitHandler: function(form) {
					$scope.submitHandle();
				},
				success: function(error, element) {
					hideFormError(error, element);
				}
			});
		}
	};
}]);

vfantasy.directive('salespersonForm', ['showFormError', 'hideFormError', function (showFormError, hideFormError) {
	return {
		restrict: 'A',
		scope: {
			submitHandle:'&'
		},
		link: function postLink($scope, iElement, iAttrs) {
			var validationRulesBalance = {
				first_name		:{required:true},
				last_name		:{required:true},
				email			:{required:true,email: true},
				paypal_email	:{required:true,email: true},
				dob				:{required:true},
			};
			iElement.validate({
				errorElement: "span",
				rules:validationRulesBalance,
				errorPlacement: function (error, element){
					showFormError(error, element);
				},
				submitHandler: function(form) {
					$scope.submitHandle();
				},
				success: function(error, element) {
					hideFormError(error, element);
				}
			});
		}
	};
}]);

vfantasy.directive('contestForm', ['showFormError', 'hideFormError','$rootScope', function (showFormError, hideFormError,$rootScope) {
	return {
		restrict: 'A',
		scope: {
			submitHandle:'&'
		},
		link: function postLink($scope, iElement, iAttrs) {
			var validationRulesContest = { 
				league_id			: "required",
				league_name	     	: {required: true, nospacialchar:false},
				league_type	     	: "required",
				invite_permission	: "required",
				draft			 	: "required",
				game_date	     	: "required",
				gametime		 	: "required",
				//entry_fee		 	: {required: true},
				teams			 	: {required: true},
				site_rake		 	: {required: true}
			};
			var validationCreategameMessage    = {
				league_id 			:{required: $rootScope.lang.LEAGUE_REQUIRED},
				league_name			:{required: $rootScope.lang.LEAGUE_NAME_REQUIRED, nospacialchar: $rootScope.lang.SPECIAL_CHAR_ERROR},
				league_type			:{required: $rootScope.lang.LEAGUE_TYPE_REQUIRED},
				teams				:{required:$rootScope.lang.TEAM_SIZE_REQUIRED},
				invite_permission	:{required:$rootScope.lang.INVITE_PERMISSION_REQUIRED},
				draft				:{required:$rootScope.lang.DRAFT_REQUIRED},
				game_date			:{required:$rootScope.lang.DRAFT_DATE_REQUIRED},
				gametime			:{required:$rootScope.lang.DRAFT_TIME_REQUIRED},
				//entry_fee			:{required:$rootScope.lang.ENTRY_FEE_REQUIRED}          
			};

			iElement.validate({
				errorElement: "span",
				rules:validationRulesContest,
				messages:validationCreategameMessage,
				errorPlacement: function (error, element){
					showFormError(error, element);
				},
				submitHandler: function(form) {
					$scope.submitHandle();
				},
				success: function(error, element) {
					hideFormError(error, element);
				}
			});
		}
	};
}]);

vfantasy.directive('teamForm', ['showFormError', 'hideFormError','$rootScope', function (showFormError, hideFormError,$rootScope) {
	return {
		restrict: 'A',
		scope: {
			submitHandle:'&'
		},
		link: function postLink($scope, iElement, iAttrs) {
			var validationRulesContest = {
				add_league_id		: "required",
				team_name_en	    : {required: true,maxlength:100},
				team_name_sp	    : {required: true,maxlength:100 },
				team_abbr	    	: {required: true, maxlength:100,nospacialchar:true },
				team_abbr_lable_sp	: {required: true,maxlength:100 },
				team_abbr_lable_en	: {required: true,maxlength:100 },
				bye_week			: {maxlength:3 },
				
			};
			var validationCreategameMessage    = {
				add_league_id 			:{required: $rootScope.lang.LEAGUE_REQUIRED},
				team_name_en			:{required: $rootScope.lang.TEAM_NAME_REQUIRED, nospacialchar: $rootScope.lang.SPECIAL_CHAR_ERROR},
				team_name_sp			:{required: $rootScope.lang.TEAM_NAME_REQUIRED},
				team_abbr				:{required: $rootScope.lang.TEAM_ABBR_REQUIRED},
				team_abbr_lable_sp		:{required: $rootScope.lang.TEAM_ABBR_LABLE_REQUIRED},
				team_abbr_lable_en		:{required: $rootScope.lang.TEAM_ABBR_LABLE_REQUIRED},
				bye_week				: { },
			};

			iElement.validate({
				errorElement: "span",
				rules:validationRulesContest,
				messages:validationCreategameMessage,
				errorPlacement: function (error, element){
					showFormError(error, element);
				},
				submitHandler: function(form) {
					console.log('hi');
					$scope.submitHandle();
				},
				success: function(error, element) {
					hideFormError(error, element);
				}
			});
		}
	};
}]);
vfantasy.directive('addvideoForm', ['showFormError', 'hideFormError','$rootScope', function (showFormError, hideFormError,$rootScope) {
	return {
		restrict: 'A',
		scope: {
			submitHandle:'&'
		},
		link: function postLink($scope, iElement, iAttrs) {
			var validationRulesContest = {
				video_type			: "required",
				video_title_en	    : {required: true},
				video_title_sp	    : {required: true },
				video_url	    	: {required: true},
				video_desc_en	    : {required: true},
				video_desc_sp	    : {required: true},
			};
			var validationCreategameMessage    = {
				video_type		:{required: $rootScope.lang.TYPE_REQUIRED},
				video_title_en	:{required: $rootScope.lang.TITLE_EN_REQUIRED},
				video_title_sp	:{required: $rootScope.lang.TITLE_SP_REQUIRED},
				video_url		:{required: $rootScope.lang.VIDEO_URL_REQUIRED},
				video_desc_en	:{required: $rootScope.lang.VIDEO_DESC_EN_REQUIRED},
				video_desc_sp	:{required: $rootScope.lang.VIDEO_DESC_SP_REQUIRED}
			};

			iElement.validate({
				errorElement: "span",
				rules:validationRulesContest,
				messages:validationCreategameMessage,
				errorPlacement: function (error, element){
					showFormError(error, element);
				},
				submitHandler: function(form) {
					$scope.submitHandle();
				},
				success: function(error, element) {
					hideFormError(error, element);
				}
			});
		}
	};
}]);

vfantasy.directive('promocodeForm', ['showFormError', 'hideFormError', function (showFormError, hideFormError) {
	return {
		restrict: 'A',
		scope: {
			submitHandle:'&'
		},
		link: function postLink($scope, iElement, iAttrs) {
			var validationRulesContest = {				
				promo_code			: {required: true,minlength: 7},
				sales_person		: {required: true},
				discount			: {required: true,maxlength: 3},
				benefit_cap			: {required: true,maxlength: 3},
				commission_sales	: {required: true,maxlength: 3},
				start_date			: "required",
				expiry_date			: "required",
			};
			iElement.validate({
				errorElement: "span",
				rules:validationRulesContest,
				errorPlacement: function (error, element){
					showFormError(error, element);
				},
				submitHandler: function(form) {
					$scope.submitHandle();
				},
				success: function(error, element) {
					hideFormError(error, element);
				}
			});
		}
	};
}]);
/*Form Validations*/

// Route State Load Spinner(used on page or content load)
vfantasy.directive('ngSpinnerBar', ['$rootScope', '$state',	function($rootScope, $state) {
		return {
			link: function(scope, element, attrs) {
				// by defult hide the spinner bar
				element.addClass('hide'); // hide spinner bar by default

				// display the spinner bar whenever the route changes(the content part started loading)
				$rootScope.$on('$stateChangeStart', function() {
					$('.body-loader').addClass('page-on-load');
					element.removeClass('hide'); // show spinner bar
				});

				// hide the spinner bar on rounte change success(after the content loaded)
				$rootScope.$on('$stateChangeSuccess', function() {
					$rootScope.routename = $rootScope.$state.current.data.routename;
					element.addClass('hide'); // hide spinner bar
					$('.body-loader').removeClass('page-on-load'); // remove page loading indicator
					// auto scorll to page top

					// Language insert in scope from server.
					$rootScope.lang = SERVER_GLOBAL;
				});

				// handle errors
				$rootScope.$on('$stateNotFound', function() {
					element.addClass('hide'); // hide spinner bar
				});

				// handle errors
				$rootScope.$on('$stateChangeError', function() {
					element.addClass('hide'); // hide spinner bar
				});
			}
		};
	}
])

vfantasy.directive('input', [function(){
	return {
		restrict: 'E',
		priority:-1000,
		link: function (scope, iElement, iAttrs) {
		}
	};
}]);

vfantasy.directive('uixBxslider', function($timeout){
	return {
		restrict: 'A',
		link: function($scope, iElm, iAttrs) {
			$scope.$on('repeatFinished', function(){
				if(typeof iElm.destroySlider!='undefined')iElm.destroySlider();
				iElm.bxSlider($scope.$eval('{' + iAttrs.uixBxslider + '}'));
			});
			if(iAttrs.windowWidth&&angular.element(window).width()<=iAttrs.windowWidth){
				iElm.bxSlider($scope.$eval('{' + iAttrs.uixBxslider + '}'));
			}
		}
	};
});

vfantasy.directive('notifyWhenRepeatFinished', ['$timeout',
	function ($timeout) {
		return {
			restrict: 'A',
			link: function($scope, iElm, iAttrs){
				if ($scope.$last === true) {
					$timeout(function(){
						$scope.$emit('repeatFinished');
					});
				}
			}
		};
	}
]);

vfantasy.directive('datePicker', function(){
	return {
		restrict: 'A',
		scope : {
			beforeShowday : '&',
			onSelect : '&'
		},
		link: function($scope, iElm, iAttrs) {
			var options = {
				changeMonth:true,
				changeYear:true,
				yearRange: '1945:'+(new Date).getFullYear(),
				autoSize:true,
				dateFormat:"yy-mm-dd",
				
				onSelect : function(date,field){
					if(typeof $scope.onSelect==='function'){
						$scope.onSelect({date:date,field:field});
					}
				}
			};

			if(typeof $scope.beforeShowday==='function'&&iAttrs.beforeShowday!==undefined)
			{
				options.beforeShowDay = function(date) {
					return $scope.beforeShowday({date:date});
				};
			}

			iAttrs.$observe('datePicker', function(){
				var newoptions = $scope.$eval('{'+iAttrs.datePicker+'}');
				var d = new Date();
				var current_year = iAttrs.currentyear;

				var max_year = newoptions.maxYear;
				var minDate = new Date(newoptions.minDate);
				var maxDate = new Date(newoptions.maxDate);

				minDate = minDate.getFullYear();
				maxDate = maxDate.getFullYear();
				if(max_year)
				{
					newoptions.yearRange =  minDate + ':' + max_year;	
				}
				if(minDate == current_year && maxDate == current_year)
				{
					newoptions.changeYear = false;
				}
				var extendedoption = angular.extend(options, newoptions);
				iElm.datepicker(extendedoption);
			});
		}
	};
});

vfantasy.directive('datePickerRange', function(){
	return {
		restrict: 'A',
		link: function($scope, iElm, iAttrs) {
			var options = {
				changeMonth:true,
				changeYear:true,
				autoSize:true,
				dateFormat:"yy-mm-dd",
				onClose :function(date) {
					if(iAttrs.datePickerRange!="")
					{
						angular.element( "."+iAttrs.datePickerRange ).datepicker( "option", "minDate", date );						
					}
				}
			};
			iElm.datepicker(options);
		}
	};
});

vfantasy.directive('selectTwo', function($timeout){
	return {
		restrict: 'A',
		link: function($scope, iElm, iAttrs) {
			iAttrs.$observe('placeholder',function(){
				if(iAttrs.selectTwo)
				{
					iElm.select2($scope.$eval('{'+iAttrs.selectTwo+'}'));
				}
				else
				{
					iElm.select2();
				}
			});
		}
	};
});

vfantasy.directive('ajaxSelect', ['$compile', '$timeout', 'dataSavingHttp', function($compile, $timeout, dataSavingHttp){
	return {
		restrict: 'A',
		link: function($scope, iElm, iAttrs) {
			$scope.$watch(function() {
				iElm.select2({
					minimumInputLength: iAttrs.minSearch,				
					ajax: {
						url: site_url+iAttrs.postUrl,
						dataType: 'json',
						delay: 250,
						type: "POST",
						data: function (params) {
							console.log(params);
							return {
								search_key: params, // search term
							};
						},
						results: function (response) {
							return {
								results: response.data
							};
						}
					}
				});
			});
		}
	};
}]);

vfantasy.directive('uniform', function($timeout){
	return{
		restrict: 'A',
		link: function($scope, iElm, iAttrs) {
			if(iAttrs.uniform)
			{
				iElm.uniform($scope.$eval('{'+iAttrs.uniform+'}'));
			}
			else
			{
				iElm.uniform();
			}
		}
	};
});

vfantasy.directive('resetForm', function($timeout){
	return {
		restrict: 'A',
		link: function($scope, iElm, iAttrs) {
			iElm.bind("click",function(e){
				if(typeof angular.element(iAttrs.resetForm)[0] != undefined && angular.element(iAttrs.resetForm)[0].reset != undefined ){
					angular.element(iAttrs.resetForm)[0].reset();
				}
				angular.element(iAttrs.resetForm +' .myerror').hide().html('');
				angular.forEach(angular.element(iAttrs.resetForm+' select'), function(v,k){
					angular.element(v).select2("val","");
				});
				if(iAttrs.resetForm=='#frmsignup')
				{
					angular.element('#signup_country').select2("val", user_country);
				}
				if(iAttrs.resetForm=='#frmwithdraw')
				{
					angular.element('#frmlivecheck .myerror').hide().html('');
				}
			});
		}
	};
});

vfantasy.directive("whenScrolled", function(){
	return{
		restrict: 'A',
		link: function($scope, elem, attrs){
			elem.bind("scroll", function(e){
				raw = elem[0];
				if($scope.loading===true){
					e.preventDefault();
					e.stopPropagation();
				}
				if(raw.scrollTop+raw.offsetHeight+5 >= raw.scrollHeight){
					$scope.$apply(attrs.whenScrolled);
				}
			});
		}
	};
});

vfantasy.directive("myerror", function(){
	return{
		restrict: 'C',
		link: function($scope, elem, attrs){
			elem.bind("click", function(e){
				elem.hide('fast',function(){elem.html('');});
			});
		}
	};
});

vfantasy.directive('myRepeatFinish', function($timeout) {
	return function($scope, element, attrs) {
		if ($scope.$last === true) {
			$timeout(function(){
				$scope.$emit('ngRepeatFinished');
			});
		}
	};
});

// Route State Load Spinner(used on page or content load)
vfantasy.directive('ngSpinnerBar', ['$rootScope',
	function($rootScope) {
		return {
			link: function($scope, element, attrs) {
				// by defult hide the spinner bar
				element.addClass('hide').hide(); // hide spinner bar by default

				// display the spinner bar whenever the route changes(the content part started loading)
				$rootScope.$on('$stateChangeStart', function() {
					element.removeClass('hide').show(); // show spinner bar
				});

				// hide the spinner bar on rounte change success(after the content loaded)
				$rootScope.$on('$stateChangeSuccess', function() {
					element.addClass('hide').hide(); // hide spinner bar

					// auto scorll to page top
					setTimeout(function () {
						angular.element('html,body').animate({scrollTop:0}, 'slow'); // scroll to the top on content load
					}, 1000);
				});

				// handle errors
				$rootScope.$on('$stateNotFound', function() {
					element.addClass('hide').hide(); // hide spinner bar
				});

				// handle errors
				$rootScope.$on('$stateChangeError', function() {
					element.addClass('hide').hide(); // hide spinner bar
				});
			}
		};
	}
]);

vfantasy.directive('toggleClass', function() {
	return {
		restrict: 'A',
		link: function($scope, element, attrs) {
			element.bind('click', function() {
				element.toggleClass(attrs.toggleClass);
			});
		}
	};
});

vfantasy.directive('arraySum', [ function() {

	var array_sum = function(array){
		//   example 1: array_sum([4, 9, 182.6]);
		//   example 2: total = []; index = 0.1; for (y=0; y < 12; y++){total[y] = y + index;}
		//   example 2: array_sum(total);
		//   returns 2: 67.2

		var key, sum = 0;

		if (array && typeof array === 'object' && array.change_key_case) { // Duck-type check for our own array()-created PHPJS_Array
			return array.sum.apply(array, Array.prototype.slice.call(arguments, 0));
		}

		// input sanitation
		if (typeof array !== 'object') {
			return null;
		}

		for (key in array) {
			if (!isNaN(parseFloat(array[key]))) {
				sum += parseFloat(array[key]);
			}
		}
		return numberWithCommas(sum);
	}

	var numberWithCommas = function(x){
		return x.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	var prizeFormat = function (item) {
		if(item || item == 0){
			if(typeof item === "string"){
				var n = item.split('.');
				if(n[1]!=undefined&&n[1]==0){
					item = n[0];
				}
			}
			formattedsalary = item.toString().replace( /(^\d{1,3}|\d{3})(?=(?:\d{3})+(?:$|\.))/g , '$1,' );
			// Universal Currency code
			return currency_code+formattedsalary;
		}
		return item;
	};

	return {
		restrict: 'A',
		link: function ($scope, iElement, iAttrs) {
			iAttrs.$observe('arraySum',function(){
				var arr = $scope.$eval(iAttrs.arraySum);
				iElement.empty().append(prizeFormat(array_sum(arr)));
			});
		}
	};
}]);

vfantasy.directive('numbersOnly', [function(){
	// Runs during compile
	return {
		restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
		link: function($scope, iElm, iAttrs, controller) {
			iElm.keypress(function(event){
				if ((event.which!=46||iElm.val().indexOf('.')!=-1) && (event.which<48||event.which>57) && event.which!=8 && event.which!=0)
				{
					event.preventDefault();
				}
			});
		}
	};
}]);

vfantasy.directive('intigerOnly', [function(){
	return {
		restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
		require: 'ngModel',
		link: function($scope, iElm, iAttrs, controller) {
			controller.$parsers.push(function (inputValue) {
				// this next if is necessary for when using ng-required on your input. 
				// In such cases, when a letter is typed first, this parser will be called
				// again, and the 2nd time, the value will be undefined
				if (inputValue == undefined) return '' 
				var transformedInput = inputValue.replace(/[^0-9]/g, ''); 
				if (transformedInput!=inputValue) {
					controller.$setViewValue(transformedInput);
					controller.$render();
				}
				return transformedInput;
			});
		}
	};
}]);

vfantasy.directive('expand', [function(){
	// Runs during compile
	return {
		restrict: 'C', // E = Element, A = Attribute, C = Class, M = Comment
		link: function($scope, iElm, iAttrs, controller) {
			/* # Interface Related Plugins
			================================================== */
			//===== Collapsible navigation =====//
			iElm.collapsible({
				defaultOpen: 'new_contest',
				cssOpen: 'level-opened',
				cssClose: 'level-closed',
				speed: 150
			});
		}
	};
}]);

vfantasy.directive('subMenu', [function(){
	// Runs during compile
	return {
		restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
		link: function($scope, iElm, iAttrs, controller) {
			iAttrs.$observe('subMenu',function(){
				if(iAttrs.subMenu==='true'){
					angular.element('.expand').next('ul').stop(true, true).slideUp('slow', function(){
						iElm.stop(true, true).slideDown('slow');
					});
				}
				else{
					angular.element('.expand').next('ul').stop(true, true).slideUp('slow');
				}
			});
		}
	};
}]);

vfantasy.directive('showMessage', ['$rootScope',function($rootScope){
	return {
		restrict: 'A',
		link: function($scope, element, attribute){
			attribute.$observe('showMessage',function(){				
				if($rootScope.alert_success!="")
				{
					$.jGrowl($rootScope.alert_success, {position: 'center',theme: 'growl-success', header: 'Success!',life: 5000 });
					$rootScope.alert_success =''
				}
				else if($rootScope.alert_error!="")
				{
					$.jGrowl($rootScope.alert_error, {position: 'center',theme: 'growl-error', header: 'Error!',life: 5000 });
					$rootScope.alert_error   ='';
				}
				else if($rootScope.alert_warning !="")
				{
					$.jGrowl($rootScope.alert_warning, {position: 'center',theme: 'growl-warning', header: 'Warning!',life: 5000 });
					$rootScope.alert_warning ='';
				}
			});
		}
	};
}]);

vfantasy.directive('rangeSlider', function(){	
	return {
		restrict: 'A',
		scope : {
			callback : '&'
		},
		link: function($scope, iElm, iAttrs){
			iAttrs.$observe('rangeSlider', function(){
				var option = $scope.$eval('{'+iAttrs.rangeSlider+'}');
				if(option.max_value===undefined||!option.max_value)option.max_value=parseInt(option.max);
				if(option.min_value===undefined||!option.min_value)option.min_value=parseInt(option.min);
				iElm.slider({
					range: true,
					min: parseInt(option.min),
					max: parseInt(option.max),
					values: [option.min_value, option.max_value],
					stop: function(event, ui) {
						$scope.callback({event:event, ui:ui});						
					}
				});
			});			
		}
	};
});

vfantasy.directive('toolTip', ['$compile',function($compile){	
	return {
		restrict: 'A',
		link: function(scope, iElm, iAttrs){
			iAttrs.$observe('title',function(){
				iElm.tooltip( "destroy" )
				iElm.tooltip();
			});
		}
	};
}]);

/*vfantasy.directive('uploadFile',['$rootScope', function($rootScope){	
	return {
		restrict: 'A',
		scope : {
			callback : '&'
		},
		link: function($scope, iElm, iAttrs){
			iAttrs.$observe('uploadFile', function(){				
				var option = $scope.$eval('{'+iAttrs.uploadFile+'}');
				var uploader = new plupload.Uploader({
					  browse_button: iAttrs.id, // this can be an id of a DOM element or the DOM element itself
					  url: site_url+option.post_url,
					  max_file_size : '2mb',
					  filters : [
					        {title : "Image files", extensions : "jpg,gif,png"},
					  ],
					    multipart_params :$scope.$eval('{'+iAttrs.addData+'}')
					});

					uploader.init();

					uploader.bind('FilesAdded', function(up, files) {
							console.log(files);

						if(iAttrs.imgSize && files[0].size > iAttrs.imgSize){
							angular.element('#'+iAttrs.id+'_error').html("Image size should be less than 2 MB!");
							angular.element('#'+iAttrs.id+'_error').removeClass('hide');
							// up.files = null;
							// $rootScope.alert_error = "Image size should be less than 2 MB!";$rootScope
							uploader.removeFile(files[0].id);
							return false;
						}
							angular.element('#'+iAttrs.id+'_error').html("");
							angular.element('#'+iAttrs.id+'_error').addClass('hide');
						uploader.start();

					});

					uploader.bind('FileUploaded', function (up, file, res) {
						var res1 = res.response.replace('"{', '{').replace('}"', '}');
						var objResponse = JSON.parse(res1);						
						$scope.callback({data:objResponse});
					});

					uploader.bind('Error', function (up, err) {
					var response = JSON.parse(err.response);
					$.jGrowl(response.message, {position: 'center',theme: 'growl-error', header: 'Error!',life: 5000});
				});
			});
		}
	};
}]);*/

vfantasy.directive('uploadFile', function($rootScope){
	return {
		restrict: 'A',
		scope : {
			callback : '&'
		},
		link: function($scope, iElm, iAttrs){
			iAttrs.$observe('uploadFile', function(){
				var option = $scope.$eval('{'+iAttrs.uploadFile+'}');
				var uploader = new plupload.Uploader({
					runtimes : 'gears,html5,flash,browserplus,silverlight,html4',
					browse_button: iAttrs.id, // this can be an id of a DOM element or the DOM element itself
					url: site_url+option.post_url,
					max_file_size : '2mb',
					filters : [
					        {title : "Image files", extensions : "jpg,gif,png"},
					 ],
				});
				uploader.init();

				uploader.bind('FilesAdded', function(up, files) {
					uploader.start();
				});

				uploader.bind('FileUploaded', function (up, file, res) {
					var res1 = res.response.replace('"{', '{').replace('}"', '}');
					var objResponse = JSON.parse(res1);
					$scope.callback({data:objResponse});
				});
				uploader.bind('Error', function (up, err) {
					console.log(err);
					if(err.response){
						var response = JSON.parse(err.response);
						$.jGrowl(response.message, {position: 'center',theme: 'growl-error', header: 'Error!',life: 5000});
					}else{

						var errMsg = err.message;
						if(err.message == "File size error."){
							errMsg = 'Image size should not grater then 2MB.'
						}
						$.jGrowl(errMsg, {position: 'center',theme: 'growl-error', header: 'Error!',life: 5000});
					}
				});
				
			});
		}
	};
});

vfantasy.directive('basicDatatable', ['$timeout',
	function ($timeout) {
		return {
			restrict: 'A',
			link: function($scope, iElm, iAttrs){
				if ($scope.$last === true) {
					angular.element('.table-responsive').removeClass('load');
						angular.element('.basic-datatable').dataTable({
							"bJQueryUI": false,
							"bAutoWidth": false,
							"sPaginationType": "full_numbers",
							"sDom": '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
							"oLanguage": {
								"sSearch": "<span>Filter:</span> _INPUT_",
								"sLengthMenu": "<span>Show entries:</span> _MENU_",
								"oPaginate": { "sFirst": "First", "sLast": "Last", "sNext": ">", "sPrevious": "<" }
							}
						});
				}
			}
		};
	}
]);
// directive for timePicker
vfantasy.directive('initTimePicker', ['$timeout',function($timeout){  
	return {  
		restrict:'EA',
		link: function($scope, iElm, iAttrs) {
			$timeout(function() {
				var coeff = 1000 * 60 * 15;
				var date = new Date();  //or use any other date
				var rounded = new Date(Math.ceil(date.getTime() / coeff) * coeff);
				var cur_time = rounded.toLocaleTimeString().replace(/([\d]+:[\d]{2})(:[\d]{2})(.*)/, "$1$3");
				var options = {
					scrollDefaultNow: true,
					timeFormat		: 'h:i A',
					minTime			: cur_time,
					step			: 15, 
					disableTextInput: true
				};

				iAttrs.$observe('initTimePicker', function()
				{
					var newoptions = $scope.$eval('{'+iAttrs.initTimePicker+'}');
					var extendedoption = angular.extend(options, newoptions);
					iElm.timepicker(extendedoption);
				});
				// open datepicker on click of icon
				var component = iElm.siblings('[data-toggle="timepicker"]');
	            if (component.length) 
	            {
	                component.on('click', function () 
	                {
	                    iElm.trigger('focus');
	                });
	            }
			}, 1000);
		}
	};
}]);

vfantasy.directive('leaguesettingsForm', ['showFormError', 'hideFormError', '$rootScope', function (showFormError, hideFormError, $rootScope) {
	return {
		restrict: 'A',
		scope: {
			submitLeaguesettings:'&'
		},
		link: function postLink(scope, iElement, iAttrs) {      
			scope.validationRulesCreategame = {
				game_date			 :{required:true},
				gametime			 :{required:true},
				keeper_date_picker   :{required:true},
				trade_end_date_picker: {required:true},
			};

			scope.validationCreategameMessage    = { 
				game_date			 :{required:$rootScope.lang.DRAFT_DATE_REQUIRED},
				gametime			 :{required:$rootScope.lang.DRAFT_TIME_REQUIRED},
				keeper_date_picker	 :{required:$rootScope.lang.KEEPER_DATE_REQUIRED},
				trade_end_date_picker:{required:$rootScope.lang.TRADE_DATE_REQUIRED},
			};

			iElement.validate({
				errorElement: "span",
				rules:scope.validationRulesCreategame,
				messages:scope.validationCreategameMessage,
				errorPlacement: function (error, element){
					showFormError(error, element);
				},
				submitHandler: function(form) {         
					scope.submitLeaguesettings();
				},
				success: function(error, element) {
					hideFormError(error, element);
				}
			});
		}
	};
}]);
vfantasy.directive('offseasonForm', ['showFormError', 'hideFormError', '$rootScope', function (showFormError, hideFormError, $rootScope) {
	return {
		restrict: 'A',
		scope: {
			submitOffseason:'&'
		},
		link: function postLink(scope, iElement, iAttrs) {      
			scope.validationRulesCreategame = {
				off_season_end_date			 :{required:true},
				off_season_year			 	:{required:true}
			};

			scope.validationCreategameMessage    = { 
				off_season_end_date			 :{required: $rootScope.lang.OFF_SEASON_END_DATE_REQUIRED},
				off_season_year			 	:{required: $rootScope.lang.SEASON_YEAR_REQUIRED}
			};

			iElement.validate({
				errorElement: "span",
				rules:scope.validationRulesCreategame,
				messages:scope.validationCreategameMessage,
				errorPlacement: function (error, element){
					showFormError(error, element);
				},
				submitHandler: function(form) {         
					scope.submitOffseason();
				},
				success: function(error, element) {
					hideFormError(error, element);
				}
			});
		}
	};
}]);
vfantasy.directive('draftdateForm', ['showFormError', 'hideFormError', '$rootScope', function (showFormError, hideFormError, $rootScope) {
	return {
		restrict: 'A',
		scope: {
			submitDraftdate:'&'
		},
		link: function postLink(scope, iElement, iAttrs) {      
			scope.validationRulesCreategame = {
				league_id	:{required:true},
				season_year	:{required:true},
				min_draft_date	:{required:true},
				min_draft_time	:{required:true},
				max_draft_date	:{required:true},
				max_draft_time	:{required:true},

			};

			scope.validationCreategameMessage    = { 
				league_id	:{required:$rootScope.lang.LEAGUE_ID_REQUIRED},
				season_year	:{required:$rootScope.lang.SEASON_YEAR_REQUIRED},
				min_draft_date	:{required:$rootScope.lang.MIN_DRAFT_DATE_REQUIRED},
				min_draft_time	:{required:$rootScope.lang.MIN_DRAFT_TIME_REQUIRED},
				max_draft_date	:{required:$rootScope.lang.MAX_DRAFT_DATE_REQUIRED},
				max_draft_time	:{required:$rootScope.lang.MAX_DRAFT_TIME_REQUIRED},
			};

			iElement.validate({
				errorElement: "span",
				rules:scope.validationRulesCreategame,
				messages:scope.validationCreategameMessage,
				errorPlacement: function (error, element){
					showFormError(error, element);
				},
				submitHandler: function(form) {         
					scope.submitDraftdate();
				},
				success: function(error, element) {
					hideFormError(error, element);
				}
			});
		}
	};
}]);
// added custom rules to validator
$(document).ready(function(){
	// Add custom function in jquery vallidation to restrict special characters.
		jQuery.validator.addMethod("nospacialchar", function(value, element) {
 			 return this.optional(element) || /^[a-zA-Z0-9\s_-]*$/.test(value);
		}, "Special characters not allowed.");   
});

vfantasy.directive('dateTimePicker', function () {
        return {
                restrict: 'A',
                link: function ($scope, iElm, iAttrs) {
                       var options = {
                                changeMonth: true,
				changeYear: true,
				dateFormat:"yy-mm-dd",
				timeFormat: 'HH:mm'
                        };
                        
                        setTimeout(function () {
                                iElm.datetimepicker(options);
                        }, 10);
                }
        };
});

vfantasy.directive('advertisementForm', ['showFormError', 'hideFormError', function (showFormError, hideFormError) {
	return {
		restrict: 'A',
		scope: {
			submitHandle:'&'
		},
		link: function postLink($scope, iElement, iAttrs) {
			var validationRulesAds = {
										name:{required:true,maxlength: 255},
										target_url:{required:true,url:true},
										position_type:{required:true},
										ads_image:{required:true}
									};
			iElement.validate({
				ignore: ".ignore, .select2-input",
				errorElement: "span",
				rules:validationRulesAds,
				errorPlacement: function (error, element){
					showFormError(error, element);
				},
				submitHandler: function(form) {
					$scope.submitHandle();
				},
				success: function(error, element) {
					hideFormError(error, element);
				}
			});
		}
	};
}]);

vfantasy.directive('leaguenewsForm', ['showFormError', 'hideFormError','$rootScope', function (showFormError, hideFormError, $rootScope) {
	return {
		restrict: 'A',
		scope: {
			submitHandle:'&'
		},
		link: function postLink(scope, iElement, iAttrs) {      
			scope.validationRulesCreateleaguenews = {
				league_type				: {required:true},
				page_type				: {required:true},
				team_abbr				: {required:true},
				players					: {required:true},
				//news_title				: {required:true},
				//news_short_desc			: {required:true},
				//news_desc				: {required:true},
				spanish_news_title		: {required:true},
				spanish_news_short_desc	: {required:true},
				spanish_news_desc		: {required:true}
			};

			scope.validationCreateleaguenewsMessage    = { 
				league_type				:{required: $rootScope.lang.league_type_required},
				page_type				:{required: $rootScope.lang.page_type_required},
				team_abbr				:{required: $rootScope.lang.team_abbr_required},
				players					:{required: $rootScope.lang.players_required},
				//news_title				:{required: $rootScope.lang.news_title_required},
				//news_short_desc			:{required: $rootScope.lang.news_short_desc_required},
				//news_desc				:{required: $rootScope.lang.news_desc_required},
				spanish_news_title		:{required: $rootScope.lang.spanish_news_title_required},
				spanish_news_short_desc	:{required: $rootScope.lang.spanish_news_short_desc_required},
				spanish_news_desc		:{required: $rootScope.lang.spanish_news_desc_required}
			};

			iElement.validate({
				//ignore: ".ignore, .select2-input",
				errorElement: "span",
				rules:scope.validationRulesCreateleaguenews,
				messages:scope.validationCreateleaguenewsMessage,
				
				errorPlacement: function (error, element){
					showFormError(error, element);
				},
				submitHandler: function(form) {         
					scope.submitHandle();
				},
				success: function(error, element) {
					hideFormError(error, element);
				}
			});
		}
	};
}]);

vfantasy.directive('ckEditor', function () {
    return {
        require: '?ngModel',
        link: function (scope, elm, attr, ngModel) {
            var ck = CKEDITOR.replace(elm[0]);
            if (!ngModel) return;
            ck.on('instanceReady', function () {
                ck.setData(ngModel.$viewValue);
            });
            function updateModel() {
            	if (!scope.$$phase && !scope.$root.$$phase) 
            	{
            		scope.$apply(function () {
		                ngModel.$setViewValue(ck.getData());

		            	angular.element('#' + attr.id + '_error').empty();
						angular.element('#' + attr.id + '_error').hide();	
		            });
            	}
        }
        ck.on('change', updateModel);
        ck.on('key', updateModel);
        ck.on('dataReady', updateModel);

        ngModel.$render = function (value) {
            ck.setData(ngModel.$viewValue);
        };
    }
};
});

vfantasy.directive('statsValue', [function(){
	return {
		restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
		require: 'ngModel',
		link: function($scope, iElm, iAttrs, controller) {
			controller.$parsers.push(function (inputValue) {
				// this next if is necessary for when using ng-required on your input. 
				// In such cases, when a letter is typed first, this parser will be called
				// again, and the 2nd time, the value will be undefined
				if (inputValue == undefined) return '' 
				var transformedInput = inputValue.replace(/[^0-9-]/g, ''); 
				if (transformedInput!=inputValue) {
					controller.$setViewValue(transformedInput);
					controller.$render();
				}
				return transformedInput;
			});
		}
	};
}]);


function showMessage(type , msg) {
	if(type ==  'success')
	{
		$.jGrowl(msg, {position: 'center',theme: 'growl-success', header: 'Success!',life: 5000 });
	}
	else if(type ==  'error')
	{
		$.jGrowl(msg, {position: 'center',theme: 'growl-error', header: 'Error!',life: 5000 });
	}
	else if(type == 'warning')
	{
		$.jGrowl(msg, {position: 'center',theme: 'growl-warning', header: 'Warning!',life: 5000 });		
	}
}