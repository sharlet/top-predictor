var vfantasy = angular.module('vfantasy', [
		'ui.router',
		'oc.lazyLoad',
		'ui.bootstrap',
		'ui.bootstrap.pagination',
		'ngSanitize'
	]);

/* Setup global settings */
vfantasy.factory('settings', ['$rootScope', function($rootScope) {
	// supported languages
	var settings = {
		layout: {
			pageSidebarClosed: false, // sidebar menu state
			pageBodyFullWidth: true, // solid body color state
			pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
		}
	};

	$rootScope.settings = settings;

	return settings;
}]);

// Pagination Config
vfantasy.constant('paginationConfig', {
	boundaryLinks: false,
	directionLinks: true,
	maxSize: 3,
	rotate: true
});

vfantasy.config(['$stateProvider', '$locationProvider', '$httpProvider', '$urlRouterProvider', '$ocLazyLoadProvider', function($stateProvider, $locationProvider, $httpProvider, $urlRouterProvider, $ocLazyLoadProvider) {

	$ocLazyLoadProvider.config({
		debug: true
		// global configs go here
	});

	$urlRouterProvider.otherwise(function($injector){
		$injector.invoke(['$state', function($state) {
			$state.go('404', {}, { location: false } );
		}]);
	});

	$stateProvider.state('404',{
		templateUrl: site_url+'template/errors/_404',
		data: {pageTitle:'404', lang:['_404.js']},
		controller:'CommonCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/_404.js'
					]
				});
			}]
		}
	})
	.state('root',{
		url:'/',
		templateUrl: site_url+'template/auth/login',
		data: {pageTitle:'Site Admin Login', lang:['login.js']},
		controller:'LoginCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/login.js',
						'assets/js/controller/loginController.js'
					]
				});
			}]
		}
	})
	.state('login',{
		url:'/auth/login',
		templateUrl: site_url+'template/auth/login',
		data: {pageTitle:'Site Admin Login', lang:['login.js']},
		controller:'LoginCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/login.js',
						'assets/js/controller/loginController.js'
					]
				});
			}]
		}
	})
	.state('dashboard',{
		url:'/dashboard',
		templateUrl:site_url+'template/dashboard/dashboard',
		data: {pageTitle:'Dashboard', routename:'dashboard', lang:['dashboard.js']},
		controller:'DashboardCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/dashboard.js',
						'assets/js/controller/dashboardController.js'
					]
				});
			}]
		}
	})
	.state('roster',{
		url:'/roster',
		templateUrl:site_url+'template/roster/roster',
		data: {pageTitle:'Roster Management', routename:'roster', lang:['roster.js']},
		controller:'RosterCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [						
						'assets/js/plugins/forms/uploader/plupload.full.min.js',
						'assets/js/plugins/forms/uploader/plupload.queue.min.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/forms/uniform.min.js',
						'utility/javascript_var/roster.js',
						'assets/js/controller/rosterController.js'
					],
					serie: true
				});
			}]
		}
	})
	.state('import_player',{
		url:'/import_player_roster/:file_name',
		templateUrl:site_url+'template/roster/import_player_roster',
		data: {pageTitle:'Import Player Roster', routename:'import_player_roster', lang:['roster.js']},
		controller:'RosterCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/roster.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/interface/datatables.min.js',
						'assets/js/plugins/forms/uniform.min.js',
						'assets/js/controller/rosterController.js'
					],
					serie: true
				});
			}]
		}
	})
	.state('user',{
		url:'/user',
		templateUrl:site_url+'template/user/user',
		data: {pageTitle:'Manage User', routename:'user', lang:['user.js']},
		controller:'UserCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/user.js',
						'assets/js/controller/userController.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/interface/datatables.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					],
					serie: true
				});
			}]
		}
	})
	.state('user_detail',{
		url:'/user_detail/:user_id',
		templateUrl:site_url+'template/user/user_detail/',
		data: {pageTitle:'User Detail', routename:'user', lang:['user.js']},
		controller:'UserCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/user.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/forms/uniform.min.js',
						'assets/js/controller/userController.js'
					]
				});
			}]
		}
	})
	.state('withdrawal_list',{
		url:'/withdrawal_list',
		templateUrl:site_url+'template/withdrawal/withdrawal_list',
		data: {pageTitle:'Withdrewal List', routename:'withdrawal_list', lang:['withdrawal.js']},
		controller:'WithdrawalCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/withdrawal.js',
						'assets/js/controller/withdrawalController.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/interface/datatables.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					]
				});
			}]
		}
	})
	.state('transaction_list',{
		url:'/transaction_list',
		templateUrl:site_url+'template/transaction/transaction_list',
		data: {pageTitle:'Transaction List', routename:'transaction_list', lang:['transaction.js']},
		controller:'TransactionCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/transaction.js',
						'assets/js/controller/transactionController.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/interface/datatables.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					]
				});
			}]
		}
	})
	.state('season',{
		url:'/season',
		templateUrl:site_url+'template/season/season_list',
		data: {pageTitle:'Season Schedule', routename:'season', lang:['season.js']},
		controller:'SeasonCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
      //                   'assets/js/plugins/forms/uploader/plupload.full.min.js',
						// 'assets/js/plugins/forms/uploader/plupload.queue.min.js',
						'utility/javascript_var/season.js',
						'assets/js/controller/seasonController.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					]
				});
			}]
		}
	})
	.state('seasonstats',{
		url:'/seasonstats/:game_unique_id/:league_id',
		templateUrl:site_url+'template/season/season_stats',
		data: {pageTitle:'Season Stats', routename:'season_stats', lang:['season.js']},
		controller:'SeasonCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/season.js',
						'assets/js/controller/seasonController.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/interface/datatables.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					]
				});
			}]
		}
	})
	// for NFL (league 5), seasonstats->statslist
    .state('statslist',{
		url:'/statslist/:game_unique_id/:league_id',
		templateUrl:site_url+'template/stats_feed/stats_list',
		data: {pageTitle:'Season Stats', routename:'season_stats', lang:['season.js']},
		controller:'SeasonCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
      //                   'assets/js/plugins/forms/uploader/plupload.full.min.js',
						// 'assets/js/plugins/forms/uploader/plupload.queue.min.js',
						'utility/javascript_var/season.js',
						'assets/js/controller/seasonController.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					]
				});
			}]
		}
	})
	 .state('projectedStatsList',{
		url:'/projectedStatsList/:game_unique_id/:league_id',
		templateUrl:site_url+'template/stats_feed/projected_stats_list',
		data: {pageTitle:'Season Stats', routename:'season_stats', lang:['season.js']},
		controller:'SeasonCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
      //                   'assets/js/plugins/forms/uploader/plupload.full.min.js',
						// 'assets/js/plugins/forms/uploader/plupload.queue.min.js',
						'utility/javascript_var/season.js',
						'assets/js/controller/projectedstatsController.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					]
				});
			}]
		}
	})
	.state('new_contest',{
		url:'/new_contest',
		templateUrl:site_url+'template/contest/new_contest',
		data: {pageTitle:'Create Contest', routename:'new_contest', lang:['contest.js']},
		controller:'ContestCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/contest.js',
						'assets/js/controller/contestController.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/interface/datatables.min.js',
						'assets/js/plugins/interface/timepicker.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					]
				});
			}]
		}
	})
	.state('contest',{
		url:'/contest',
		templateUrl:site_url+'template/contest/contest',
		data: {pageTitle:'Contest List', routename:'contest', lang:['contest.js']},
		controller:'ContestCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/contest.js',
						'assets/js/controller/contestController.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/interface/datatables.min.js',
						'assets/js/plugins/interface/timepicker.min.js',
						'assets/js/plugins/forms/uniform.min.js',						
					]
				});
			}]
		}
	})
	.state('contest_detail',{
		url:'/contest_detail/:game_unique_id',
		templateUrl:site_url+'template/contest/contest_detail',
		data: {pageTitle:'Contest Detail', routename:'contest_detail', lang:['contest.js']},
		controller:'ContestCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/contest.js',
						'assets/js/controller/contestController.js',
						'assets/js/plugins/forms/select2.min.js',						
						'assets/js/plugins/interface/datatables.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					]
				});
			}]
		}
	})
	.state('contest_drafting',{
		url:'/contest_drafting/:game_unique_id',
		templateUrl:site_url+'template/contest/contest_drafting',
		data: {pageTitle:'Contest Detail', routename:'contest_detail', lang:['contest.js']},
		controller:'ContestDraftingCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/contest.js',
						'assets/js/controller/contestDraftingController.js',
						'assets/js/plugins/forms/select2.min.js',						
						'assets/js/plugins/interface/datatables.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					]
				});
			}]
		}
	})
	.state('team_roster',{
		url:'/teamroster',
		templateUrl:site_url+'template/team_roster/team_roster',
		data: {pageTitle:'Team Roster', routename:'team_roster', lang:['teamroster.js']},
		controller:'TeamrosterCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/teamroster.js',
						'assets/js/controller/teamrosterController.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/interface/datatables.min.js',
						'assets/js/plugins/forms/uniform.min.js',						
      //                   'assets/js/plugins/forms/uploader/plupload.full.min.js',
						// 'assets/js/plugins/forms/uploader/plupload.queue.min.js',
					]
				});
			}]
		}
	})
	.state('change_password',{
		url:'/change_password',
		templateUrl:site_url+'template/setting/change_password',
		data: {pageTitle:'Setting', routename:'change_password', lang:['setting.js']},
		controller:'SettingCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/setting.js',
						'assets/js/controller/settingController.js',
					]
				});
			}]
		}
	})
	.state('sales_persons',{
		url:'/sales_person',
		templateUrl:site_url+'template/promo_code/sales_person',
		data: {pageTitle:'Promo Code', routename:'sales_person', lang:['promo_code.js']},
		controller:'PromocodeCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/promo_code.js',
						'assets/js/controller/promocodeController.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/interface/datatables.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					]
				});
			}]
		}
	})
	.state('new_sales_persons',{
		url:'/new_sales_person',
		templateUrl:site_url+'template/promo_code/new_sales_person',
		data: {pageTitle:'Create Sales Person', routename:'new_sales_person', lang:['promo_code.js']},
		controller:'PromocodeCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/promo_code.js',
						'assets/js/controller/promocodeController.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/interface/datatables.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					]
				});
			}]
		}
	})
	.state('edit_sales_person',{
		url:'/edit_sales_person/:sales_person_id',
		templateUrl:site_url+'template/promo_code/edit_sales_person',
		data: {pageTitle:'Edit Sales Person', routename:'edit_sales_person', lang:['promo_code.js']},
		controller:'PromocodeCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/promo_code.js',
						'assets/js/controller/promocodeController.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/interface/datatables.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					]
				});
			}]
		}
	})
	.state('sales_person_detail',{
		url:'/sales_person_detail/:sales_person_id',
		templateUrl:site_url+'template/promo_code/sales_person_detail',
		data: {pageTitle:'Sales Person Detail', routename:'sales_person_detail', lang:['promo_code.js']},
		controller:'PromocodeCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/promo_code.js',
						'assets/js/controller/promocodeController.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/interface/datatables.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					]
				});
			}]
		}
	})
	.state('promo_code',{
		url:'/promo_code',
		templateUrl:site_url+'template/promo_code/promo_code',
		data: {pageTitle:'Promo Code', routename:'promo_code', lang:['promo_code.js']},
		controller:'PromocodeCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/promo_code.js',
						'assets/js/controller/promocodeController.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/interface/datatables.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					]
				});
			}]
		}
	})
	.state('new_promo_codes',{
		url:'/new_promo_code',
		templateUrl:site_url+'template/promo_code/new_promo_code',
		data: {pageTitle:'Create New Promo Code', routename:'new_promo_code', lang:['promo_code.js']},
		controller:'PromocodeCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/promo_code.js',
						'assets/js/controller/promocodeController.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/interface/datatables.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					]
				});
			}]
		}
	})
	.state('promo_code_details',{
		url:'/promo_code_detail/:promo_code',
		templateUrl:site_url+'template/promo_code/promo_code_detail',
		data: {pageTitle:'PromoCode Detail', routename:'promo_code_detail', lang:['promo_code.js']},
		controller:'PromocodeCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/promo_code.js',
						'assets/js/controller/promocodeController.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/interface/datatables.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					]
				});
			}]
		}
	})
	.state('lineup-detail',{
		url:'/lineup-detail/:game_unique_id',
		templateUrl:site_url+'template/contest/lineup_detail',
		data: {pageTitle:'Lineup Detail', routename:'lineup-detail', lang:['contest.js']},
		controller:'LineupCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/contest.js',
						'assets/js/controller/lineupController.js',
						'assets/js/plugins/forms/select2.min.js',						
						'assets/js/plugins/interface/datatables.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					]
				});
			}]
		}
	})
	.state('commish-tool',{
		url:'/commish-tool/:game_unique_id',
		templateUrl:site_url+'template/commishner_tool/action_list',
		data: {pageTitle:'Commishner Tool Kit', routename:'commish-tool', lang:['commish.js']},
		controller:'CommishToolCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/commish.js',
						'assets/js/controller/commishToolController.js',
						'assets/js/plugins/forms/select2.min.js',						
						'assets/js/plugins/interface/datatables.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					]
				});
			}]
		}
	})
	.state('acquisition-budget',{
		url:'/acquisition-budget/:game_unique_id',
		templateUrl:site_url+'template/commishner_tool/acquisition_budget',
		data: {pageTitle:'Edit Roster'},
		controller:'CommishToolCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [												
						'assets/js/controller/commishToolController.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/forms/uniform.min.js',
					],
					serie: true
				});
			}]
		}
	})
	.state('lock-teams',{
		url:'/lock-teams/:game_unique_id',
		templateUrl:site_url+'template/commishner_tool/lock_teams',
		data: {pageTitle:'Commishner Tools:Lock Teams'},
		controller:'CommishToolCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'assets/js/controller/commishToolController.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/forms/uniform.min.js',
					]
				});
			}]
		}
	})
	.state('edit-roster',{
		url:'/edit-roster/:game_unique_id',
		templateUrl:site_url+'template/commishner_tool/edit_roster',
		data: {pageTitle:'Edit Roster'},
		controller:'editRosterCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [												
						'assets/js/controller/edit_roster.js',
						'assets/js/controller/player_card.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					],
					serie: true
				});
			}]
		}
	})
	.state('edit-league-settings',{
		url:'/edit-league-settings/:game_unique_id',
		templateUrl:site_url+'template/commishner_tool/edit_league_settings',
		data: {pageTitle:'Edit League Settings'},
		controller:'editLeagueSettingsCtrl',
		controllerAs: 'els',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [												
						'assets/js/controller/edit_league_settings.js',
						'utility/javascript_var/contest.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/interface/datatables.min.js',
						'assets/js/plugins/interface/timepicker.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					],
					serie: true
				});
			}]
		}
	})
	.state('waiver-priority',{
		url:'/waiver-priority/:game_unique_id',
		templateUrl:site_url+'template/commishner_tool/waiver_priority',
		data: {pageTitle:'Waiver Priority ordering'},
		controller:'waiverPriorityCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [												
						'assets/js/vendor/drag-jquery-ui.js',				
						'assets/js/controller/waiver_priority.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					],
					serie: true
				});
			}]
		}
	})

	.state('change_dates',{
		url:'/change_date',
		templateUrl:site_url+'template/setting/change_date',
		data: {pageTitle:'Setting', routename:'change_date', lang:['setting.js']},
		controller:'SettingCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'assets/js/plugins/forms/validate.min.js',
						'utility/javascript_var/setting.js',
						'assets/js/controller/settingController.js',
					]
				});
			}]
		}
	})
	.state('off_season',{
		url:'/off_season',
		templateUrl:site_url+'template/setting/off_season',
		data: {pageTitle:'Setting', routename:'off_season', lang:['setting.js']},
		controller:'offSeasonSettingCtrl',
		controllerAs: 'os',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'assets/js/plugins/interface/datatables.min.js',
						'assets/js/plugins/interface/timepicker.min.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/forms/uniform.min.js',
						'assets/js/plugins/forms/validate.min.js',
						'utility/javascript_var/setting.js',
						'assets/js/controller/off_season.js',
					]
				});
			}]
		}
	})
	.state('manage_video',{
		url:'/manage_video',
		templateUrl:site_url+'template/setting/manage_video',
		data: {pageTitle:'Setting', routename:'manage_video', lang:['manage_video.js']},
		controller:'manageVideoCtrl',
		controllerAs: 'mvc',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'assets/js/plugins/forms/select2.min.js',
						'utility/javascript_var/setting.js',
						'assets/js/controller/manage_video.js',
					]
				});
			}]
		}
	})
	.state('new_advertisement',{
		url:'/new_advertisement',
		templateUrl:site_url+'template/advertisement/new_advertisement',
		data: {pageTitle:'New Advertisement', routename:'new_advertisement', lang:['advertisement.js']},
		controller:'AdvertisementCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						// 'assets/js/plugins/forms/uploader/plupload.queue.min.js',
						// 'assets/js/plugins/forms/uploader/plupload.full.min.js',
						'assets/js/plugins/forms/validate.min.js',
						'assets/js/plugins/ajaxupload.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/controller/advertisementController.js',
						'assets/js/plugins/forms/uniform.min.js',
						'utility/javascript_var/advertisement.js',
					],
					serie: true
				});
			}]
		}
	})
	.state('advertisement',{
		url:'/advertisement',
		templateUrl:site_url+'template/advertisement/advertisement',
		data: {pageTitle:'Advertisement List', routename:'advertisement', lang:['advertisement.js']},
		controller:'AdvertisementCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/advertisement.js',
						'assets/js/controller/advertisementController.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					]
				});
			}]
		}
	})
	.state('news_list',{
		url:'/news_list',
		templateUrl:site_url+'template/news/news_list',
		data: {pageTitle:'News List', routename:'news_list', lang:['news.js']},
		controller:'leagueNewsCtrl',
		controllerAs: 'LN',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/forms/uniform.min.js',
						'utility/javascript_var/league_news.js',
						'assets/js/controller/leagueNewsController.js'
					]
				});
			}]
		}
	})
	.state('create_news',{
		url:'/create_news',
		templateUrl:site_url+'template/news/league_news',
		data: {pageTitle:'Create News', routename:'create_news', lang:['league_news.js']},
		controller:'leagueNewsCtrl',
		controllerAs: 'LN',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'assets/js/plugins/forms/validate.min.js',
						'assets/js/plugins/ajaxupload.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/forms/uniform.min.js',
						'utility/javascript_var/league_news.js',
						'assets/js/controller/leagueNewsController.js',
						'assets/tinymce/tinymce.min.js',
						'assets/tinymce/tinymce.app.js'
					]
				});
			}]
		}
	})
	.state('news_details',{
		url:'/news_details/:news_id',
		templateUrl:site_url+'template/news/news_details',
		data: {pageTitle:'League News', routename:'news_details', lang:['league_news.js']},
		controller:'leagueNewsCtrl',
		controllerAs: 'LN',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/league_news.js',
						'assets/js/controller/leagueNewsController.js'
					]
				});
			}]
		}
	})
	.state('edit_news',{
		url:'/edit_news/:news_id',
		templateUrl:site_url+'template/news/league_news',
		data: {pageTitle:'League News', routename:'edit_news', lang:['league_news.js']},
		controller:'leagueNewsCtrl',
		controllerAs: 'LN',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'assets/js/plugins/forms/validate.min.js',
						'assets/js/plugins/ajaxupload.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/forms/uniform.min.js',
						'utility/javascript_var/league_news.js',
						'assets/js/controller/leagueNewsController.js'
					]
				});
			}]
		}
	})
	/*.state('commission_payout',{
		url:'/commission_payout',
		templateUrl:site_url+'template/promo_code/commission_payout',
		data: {pageTitle:'Commission Payout', routename:'commission_payout'},
		controller:'PromocodeCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/promo_code.js',
						'assets/js/controller/promocodeController.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/interface/datatables.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					]
				});
			}]
		}
	})
	.state('commission_payout_detail',{
		url:'/commission_payout_detail/:promo_code',
		templateUrl:site_url+'template/promo_code/commission_payout_detail',
		data: {pageTitle:'Commission Payout Detail', routename:'commission_payout_detail'},
		controller:'PromocodeCtrl',
		resolve:{
			deps: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load({
					insertBefore: '#ng_load_plugins_before',
					files: [
						'utility/javascript_var/promo_code.js',
						'assets/js/controller/promocodeController.js',
						'assets/js/plugins/forms/select2.min.js',
						'assets/js/plugins/interface/datatables.min.js',
						'assets/js/plugins/forms/uniform.min.js'
					]
				});
			}]
		}
	})*/

	$httpProvider.interceptors.push(['$q', '$location', '$rootScope', function($q, $location, $rootScope) {
		return {
			request: function(config) {
				
				/*************************** Loader Button Icon Remove *********************/				
				if($rootScope.current_loader!="")
				{
					angular.element($rootScope.current_loader).addClass('disabled');
					angular.element($rootScope.current_loader+' i').addClass('loading-spinner');					
				}				
				/*******************************************************************/
				var key = sessionStorage.getItem(AUTH_KEY);
			
				if(key == null)
				{
					config.headers[AUTH_KEY] = '';
				}
				else 
				{
					config.headers[AUTH_KEY] = key;
				}
				return config;
			},

			response: function(response) {
				/*************************** Loader Button Icon Remove *********************/
				angular.element($rootScope.current_loader).removeClass('disabled');
				angular.element($rootScope.current_loader+' i').removeClass('loading-spinner');
				$rootScope.current_loader = "";
				/*******************************************************************/
				return response || $q.state(response);
			},

			responseError: function(rejection) {
				angular.element($rootScope.current_loader).removeClass('disabled');
				angular.element($rootScope.current_loader+' i').removeClass('loading-spinner');
				$rootScope.current_loader = "";

				if(rejection.status == 401)
				{
					$rootScope.is_logged_in = false;
					sessionStorage.clear();
					$rootScope.$state.go('root');
				}
				return $q.reject(rejection);
			}
		};
	}]);

	$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';

	$locationProvider.html5Mode(true);
}]);

vfantasy.run(['$rootScope', '$location', '$state','$timeout', function ($rootScope, $location, $state, $timeout) {

	$rootScope.alert_success='';
	$rootScope.alert_warning='';
	$rootScope.alert_error='';

	$rootScope.$state = $state; // state to be accessed from view
	$rootScope.routename;

	$rootScope.is_logged_in = true;

	$rootScope.lang = SERVER_GLOBAL;

	$rootScope.notSorted = function(obj){
		if (!obj) {
			return [];
		}
		return Object.keys(obj);
	};

	$rootScope.updateUi = function() {
		$timeout(function(){$.uniform.update();}, 50);
	};
}]);