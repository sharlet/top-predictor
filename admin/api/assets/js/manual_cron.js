/*
# Global scoring for Projection Stats
http://159.203.129.115/api/lfa/LFA_Scoring/global_projection_scoring_calculation/1

# Global scoring for Real Feed Stats
http://159.203.129.115/api/lfa/LFA_Scoring/global_scoring_calculation/1

# Rank calculation for Player on Global score
http://159.203.129.115/api/lfa/LFA_Scoring/global_rank_calculation/1

# Calculation of Lineup player 
http://159.203.129.115/api/lfa/LFA_Scoring/calculate_player_fantasy_score/1

# Cumulative score of whole lineup wrt lineup master id
http://159.203.129.115/api/lfa/LFA_Scoring/lineup_master_standings/1*/


var api_url = {
				"global_scoring_calculation"			:'api/lfa/LFA_Scoring/global_scoring_calculation/',
				"global_projection_scoring_calculation"	:'api/lfa/LFA_Scoring/global_projection_scoring_calculation/',
				"global_rank_calculation"				:'api/lfa/LFA_Scoring/global_rank_calculation/',
				"calculate_player_fantasy_score"		:'api/lfa/LFA_Scoring/calculate_player_fantasy_score/',
				"lineup_master_standings"				:'api/lfa/LFA_Scoring/lineup_master_standings/'
			};

var api_time = {
				"global_scoring_calculation"			:2,
				"global_projection_scoring_calculation"	:8,
				"global_rank_calculation"				:11,
				"calculate_player_fantasy_score"		:3,
				"lineup_master_standings"				:4
			};


var interval = {};

$('document').ready(function(){

	$.each(api_url, function(index, value){
		var url							= site_url + value + season_game_unique_id;
		interval[index]					= new HitUrl(url);
		interval[index].min				= api_time[index];
		interval[index].container_id	= index;

		interval[index].startInterval();
		

	});
});


var HitUrl = function(url)
{
	self_hu = this;
	self_hu.input_url = url;
}

$.extend(HitUrl.prototype,{
	self_hu			:{},
	interval		:{},
	input_url : '',
	container_id	:'',
	min				:1,
	counter : 0,

	url_regex :  /^(ftp|http|https):\/\/[^ "]+$/,

	startInterval:function()
	{
		console.log('Start interval ');
		var input_url	= self_hu.input_url;
		var min			= self_hu.min;

		if(! self_hu.url_regex.test(input_url) )
		{
			self_hu.deleteInterval();
			console.log('url incorrent');
			return false;
		}

		var sec = min * 60 *1000;
		self_hu.interval = setInterval(function(){ 

			$.ajax ({
				url  : input_url,
				data : {},
				type : "GET",
				success: function(response)
				{
					$('#cron_container').append('<BR> URL ->  ' + input_url +'  <BR> <b>Status</b> -  Successfully executed. ');

					self_hu.counter++;
					console.log(self_hu.container_id, response);
				}
			});

		 }, sec);

	},
	deleteInterval : function()
	{
		console.log('deleteInterval');
		clearInterval(self_hu.interval);
	},
	runNow : function()
	{
		var input_url	= $('#url_container .url').val();
		var min			= $('#url_container .time_min').val(); 

		if(! self_hu.url_regex.test(input_url) )
		{
			self_hu.deleteInterval();
			console.log('url incorrent');
			return false;
		}

		var sec = min * 60 *1000;

		$.ajax ({
			url  : input_url,
			data : {},
			type : "GET",
			success: function(response)
			{
				console.log(response);
			}
		});

		
	}
});