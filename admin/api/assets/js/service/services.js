vfantasy.factory('dataSavingHttp', function($http, $location) {
	var wrapper = function(requestConfig) {
		var options = angular.extend({
			url: "",
			method: "POST",
			data : '',
			dataType: "json",
		},requestConfig);

		var httpPromise = $http(options);

		httpPromise.success(function(result, status, headers, config){
			var l = window.location;
			wrapper.lastApiCallConfig = config;
			wrapper.lastApiCallUri = l.protocol + '//' + l.host + '' + config.url + '?' +
				(function(params){
					var pairs = [];
					angular.forEach(params, function(val, key){
						pairs.push(encodeURIComponent(key)+'='+encodeURIComponent(val));
					});
					return pairs.join('&')
				})(config.params);
			wrapper.lastApiCallResult = result;
		})
		return httpPromise;
	};
	return wrapper;
});

vfantasy.factory('showFormError', [function() {
	var bindError = function(error, element){
		var element_id = angular.element(element).attr('id');
		var field = '#'+element_id+'_error';
		angular.element(field).empty().append(error.text());
		angular.element(field).show();
		angular.element(field).removeClass('hide');
		return true;
	};
	return bindError;
}]);

vfantasy.factory('hideFormError', [function() {
	var bindError = function(error, element){
		var element_id = angular.element(element).attr('id');
		var field = '#'+element_id+'_error';
		angular.element(field).empty();
		angular.element(field).hide();
		return true;
	};
	return bindError;
}]);

vfantasy.factory('showServerError', [function() {
	var bindError = function(error){
		var errors = error.error;
		for(index in errors){
			errortext = errors[index];
			var field = '#'+index+'_error';
			angular.element(field).empty().append(errortext).removeClass('hide').show();
		}
		return true;
	};
	return bindError;
}]);

// directive to create salary format
vfantasy.filter('salaryFormat', ['$sce',function($sce) {
	return function (item, withCurrency) {
		if(item || item == 0){
			if(typeof item === "string"){
				var n = item.split('.');
				if(n[1]!=undefined&&n[1]==0){
					item = n[0];
				}
			}
			formattedsalary = item.toString().replace(/(^\d{1,3}|\d{3})(?=(?:\d{3})+(?:$|\.))/g , '$1,');
			// Universal Currency code
			return (withCurrency==true)?$sce.trustAsHtml(formattedsalary):$sce.trustAsHtml(CURRENCY_CODE+formattedsalary);
		}
		return item;
	};
}]);

vfantasy.filter('makeitbold', [function($sce) {
	return function (input, query) {
		var r = RegExp('('+ query + ')', 'g');
		return $sce.trustAsHtml(input.replace(r,'<b>$1</b>'));
	}
}]);

vfantasy.filter('ordinal', [function() {

	return function(number){
		if(isNaN(number) || number < 1){
			return number;
		} else {
			var lastDigit = number % 10;
			//console.log(lastDigit);
			if(lastDigit === 1)
			{
				return number + 'st'
			} else if(lastDigit === 2)
			{
				return number + 'nd'
			} else if (lastDigit === 3)
			{
				return number + 'rd'
			} else if (lastDigit > 3){
				return number + 'th'
			}else
			{
				return number + 'th'
			}
		}
	}
}]);

vfantasy.factory('socket', [function($rootScope, $timeout) {
	var socket  = function(){};
	socket.on   = function(){};
	socket.emit = function(){};
	var socket = io(NodeAddr, {secure: 'https:' == location.protocol} );
	return {
		on: function (eventName, callback) {
			socket.on(eventName, function(){  
				var args = arguments;
				$timeout(function(){
					$rootScope.$apply(function(){
						callback.apply(socket, args);
					});
				});
			});
		},
		emit: function (eventName, data, callback) {
			socket.emit(eventName, data, function(){
				var args = arguments;
				$rootScope.$apply(function(){
					if (callback) {
						callback.apply(socket, args);
					}
				});
			})
		}
	};
}]);

vfantasy.filter('orderObjectBy', [function() {
	return function(items, field, reverse) {
		var filtered = [];
		angular.forEach(items, function(item) {
			filtered.push(item);
		});
		filtered.sort(function (a, b) {
			return (a[field] > b[field] ? 1 : -1);
		});
		if(reverse) filtered.reverse();
		return filtered;
	};
}]);

vfantasy.filter('titleCase', [function() {
	return function(input) {
		input = input || '';
		return input.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	};
}]);

vfantasy.filter('removeUnderScore', [function () {
	return function (text) {
		var str = text.replace(/\_+/g, ' ');
		return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	};
}]);

vfantasy.filter('range', function() {
  return function(input, min, max) {
    min = parseInt(min);
    max = parseInt(max);
    for (var i=min; i<=max; i++)
      input.push(i);
    return input;
  };
});