<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Contest_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->admin_id = $this->session->userdata('admin_id');
	}


	/**
	 * [get_all_master_data_entry description]
	 * @Summary : get_all_master_data_entry Used to get list of all master data entry for size and fee.
	 * @return  [type]
	 */
	function get_all_master_data_entry($config=array())
	{
		$sql = $this->db->select("*")
						->from(MASTER_DATA_ENTRY)
						->get();
		$reslts = $sql->result_array();
		return $reslts;
	}



	/**
	 * [get_all_state_by_country description]
	 * @MethodName get_all_state_by_country
	 * @Summary This function used to get all master state by country id
	 * @return     [type]
	 */
	public function get_all_master_tbl_constant()
	{
		$data = array();
		$sql = $this->db->select("*")
						->from(MASTER_TBL_CONSTANT)
						->where(array('status' =>'ACTIVE','admin'=> '1'))
						->get();
		$reslts = $sql->result_array();
		foreach ($reslts as $key => $value) {
		 	$data[$value['constant_category']][] = $value;
		 }
		return $data;
	}


	/**
	 * [get_all_master_prize_payout description]
	 * @MethodName get_all_master_prize_payout
	 * @Summary This function used to get all game prize payout list
	 * @param
	 * @return     [array]
	 */
	public function get_all_master_prize_payout()
	{
		$sql = $this->db->select("*")
						->from(MASTER_PRIZE_PAYOUT)
						->where('status', 1)
						->get();
		return $sql->result_array();
	}



	/**
	 * [get_league_master_game_roster description]
	 * @MethodName get_league_master_game_roster
	 * @Summary This function used to get all game master game roster by league id.
	 * @param
	 * @return     [array]
	 */
	public function get_league_master_game_roster($league_id)
	{

		$result = $this->db->select('mgr.*, l.max_roster_size')
							->from(	MASTER_GAME_ROSTER." AS mgr" )
							->join(LEAGUE." AS  l", "l.league_id = mgr.league_id", 'inner')
							->where('mgr.league_id', $league_id)
							->order_by('mgr.sequence', 'ASC')
							->get()
							->result_array();

		return $result;
	}

	public function get_season_week($league_id, $year)
	{
		$result =  $this->db->select('*')
							->from(SEASON_WEEK)
							->where('league_id', $league_id)
							->where('year', $year)
							->where('season_week_end_date_time >=', format_date() )
							->get()
							->result_array();
		return $result;
	}

	public function get_week_matches($league_id, $week, $year) {
		// MYSQL_DATE_FORMAT
		$current_date = format_date();
		$result =  $this->db->select("S.league_id, S.season_id, S.season_game_unique_id, S.year, S.type, S.week,
																	DATE_FORMAT(S.season_scheduled_date, '".MYSQL_DATE_FORMAT."') AS  season_scheduled_date,
																	S.home, S.away, S.status,
																	HOME.team_abbr AS home_team_abbr, AWAY.team_abbr as away_team_abbr")
							->from(SEASON . ' AS S' )
							->join(TEAM.' AS HOME', 'HOME.team_id = S.home', 'INNER'	)
							->join(TEAM.' AS AWAY', 'AWAY.team_id = S.away', 'INNER'	)
							->where('S.league_id', $league_id)
							->where('S.week', $week)
							->where('S.year', $year)
							->where('S.season_scheduled_date >= ',$current_date)
							->order_by('S.season_scheduled_date ASC, S.season_id ASC ')
							->get()
							->result_array();
		return $result;
	}


	public function update_contest($updateData, $id) {
    $this->db->where('contest_id', $id);
		$this->db->update(CONTEST, $updateData);
		return true;
	}

	public function validate_season_ids($match_season_id) {
		$result =  $this->db->select('COUNT(season_id) as season_id_count')
												->from(SEASON)
												->where_in('season_id', $match_season_id)
												->group_by('league_id')
												->get()
												->row_array();
		return $result['season_id_count'];
	}

	public function earliest_match_date($match_season_id) {
		$result =  $this->db->select('MIN(season_scheduled_date) as season_scheduled_date')
												->from(SEASON)
												->where('season_scheduled_date >', format_date())
												->where_in('season_id', $match_season_id)
												->group_by('league_id')
												->get()
												->row_array();
		return $result['season_scheduled_date'];
	}

	/**
	 * [get_season_date description]
	 * @MethodName get_season_date
	 * @Summary This function used to get all season date by league id
	 * @param      [int]  $league_id
	 * @return     [array]
	 */
	public function get_season_date($league_id)
	{
		$sql = $this->db->select('scheduled_date')
						->from(SEASON . " AS S")
						->where('S.league_id', $league_id)
						->where('S.season_scheduled_date >', format_date())
						->group_by('scheduled_date')
						->order_by('scheduled_date', 'DESC')
						->get();
		$result = $sql->result_array();

		$season_time = array();

		foreach ($result as $key => $value)
		{
			$seasone_date_time_format = $value['scheduled_date'];
			$timestamp = strtotime($seasone_date_time_format);
			$season_time[] = $seasone_date_time_format;
		}

		return $season_time;
	}

	public function get_master_salary_cap() {
		$result = $this->db->select()
												->from(MASTER_SALARY_CAP)
												->where('status', 1)
												->order_by('sequence', 'ASC')
												->get()
												->result_array();
		return $result;
	}

	/**
	 * [get_all_salary_cap description]
	 * @MethodName get_all_salary_cap
	 * @Summary This functin used to get all salary cap by league id
	 * @param      [int]  $league_id
	 * @return     [array]
	 */
	public function get_all_salary_cap($league_id)
	{
		$sql = $this->db->select('salary_cap, league_salary_cap_id')
						  ->from(LEAGUE_SALARY_CAP . " AS LSC")
						  ->where('LSC.admin_active', ACTIVE)
						  ->where('LSC.league_id', $league_id)
						  ->get();
		$result = $sql->result_array();
		return $result;
	}

	/**
	 * [get_all_available_week description]
	 * @MethodName get_all_available_week
	 * @Summary This function used to get all week by league id
	 * @param      [int]  $league_id
	 * @return     [array]
	 */
	public function get_all_available_week($league_id)
	{
		$sql = $this->db->select('season_week, season_week_start_date_time, season_week_end_date_time')
						  ->from(SEASON_WEEK . " AS SW")
						  ->where('SW.league_id', $league_id)
						  ->where('SW.season_week_close_date_time > ',format_date() )
						  ->order_by('SW.season_week', 'ASC')
						  ->get();
		$result = $sql->result_array();
		$final_result = array();
		foreach ($result as $key => $value)
		{
			$season_week = $value['season_week'];
			$season_week_start_date_time = $value['season_week_start_date_time'];
			$season_week_end_date_time = $value['season_week_end_date_time'];
			$final_result[$season_week] = $season_week . ' ( ' . date('d-M-y', strtotime($season_week_start_date_time)) . ' ' . date('d-M-y', strtotime($season_week_end_date_time)) . ' )';
		}
		return $final_result;
	}

	/**
	 * [get_all_number_of_winner description]
	 * @MethodName get_all_number_of_winner
	 * @Summary This function used for get all prizing list and prize validation
	 * @param [int] $league_id
	 * @return     [array]
	 */
	public function get_all_number_of_winner($league_id)
	{
		$sql = $this->db->select('MNOW.number_of_winner_id,number_of_winner_desc,league_number_of_winner_id,position_or_percentage,places')
						->from(MASTER_NUMBER_OF_WINNER . " AS MNOW")
						->join(LEAGUE_NUMBER_OF_WINNER . " AS LNW", "LNW.number_of_winner_id = MNOW.number_of_winner_id", 'INNER')
						->where('LNW.active', ACTIVE)
						->where('LNW.league_id ', $league_id)
						->where('MNOW.status', '1')
						->order_by('order', 'ASC')
						->get();
		$result = $sql->result_array();

		return array('all_number_of_winner' => array_column($result, 'number_of_winner_desc', 'league_number_of_winner_id'), 'number_of_winner_validation' => $result);
	}




	/**
	 * [get_list_game_date_wise description]
	 * @MethodName get_list_game_date_wise
	 * @Summary This function used to get game list with game detail order by scheduled date
	 * @param      array  game list array
	 * @param      int    league id
	 * @return     array
	 */
	public function get_list_game_date_wise($gamelist ,$league_id)
	{
		$game_ids = array();
		foreach ($gamelist as $key => $value) {
			$game_ids[] = $key;
		}

		$sql = $this->db->select('season_game_unique_id, home, away, season_scheduled_date')
						->from(SEASON)
						->where_in('season_game_unique_id',$game_ids)
						->where('league_id',$league_id)
						->order_by('season_scheduled_date','ASC')
						->get();
		$result = $sql->result_array();
		return $result;
	}

	/**
	 * [get_season_scheduled_date description]
	 * @MethodName get_season_scheduled_date
	 * @Summary This function used to get season date by season game unique id
	 * @param      varchar  season_game_unique_id
	 * @param      int 	league id
	 * @return     datetime
	 */
	public function get_season_scheduled_date($season_game_unique_id, $league_id)
	{
		$sql = $this->db->select('season_scheduled_date')
						->from(SEASON)
						->where('season_game_unique_id',$season_game_unique_id)
						->where('league_id',$league_id)
						->group_by('season_scheduled_date')
						->order_by('season_scheduled_date','ASC')
						->limit(1)
						->get();
		$result = $sql->row_array();
		$season_scheduled_date = '0000-00-00 00:00:00';
		if (isset($result['season_scheduled_date']))
			$season_scheduled_date = $result['season_scheduled_date'];
		return $season_scheduled_date;
	}
	/**
	 * [get_player_salary_master description]
	 * @MethodName get_player_salary_master
	 * @Summary This function used get player salary master by league id
	 * @param      int  league id
	 * @return     array
	 */
	public function get_player_salary_master($league_id)
	{
		$sql = $this->db->select('player_salary_master_id')
						->from(PLAYER_SALARY_MASTER)
						->where('league_id',$league_id)
						->order_by('player_salary_master_id','DESC')
						->get();
		return $sql->row_array();
	}

	/**
	 * [prize_details_by_size_fee_prizing description]
	 * @MethodName prize_details_by_size_fee_prizing
	 * @Summary This function used to calculate prize detail
	 * @param      int   game size
	 * @param      float  entery fees
	 * @param      int   league_number_of_winner_id
	 * @param      string   prize_pool
	 * @param      boolean  desc
	 * @return     prize detail
	 */
	public function prize_details_by_size_fee_prizing($size,$fee,$league_number_of_winner_id,$site_rake,$prize_pool = '0',$desc=FALSE)
	{

		$collected_amount	= $size * $fee;
		$site_rake_amount	= ($collected_amount * $site_rake)/100;
		$prize_pool_amount	= $collected_amount - $site_rake_amount;

		if($prize_pool != '0')
		{
			$prize_pool_amount = $prize_pool;
		}

		$sql = $this->db->select("MNOW.number_of_winner_id,MNOW.number_of_winner_desc,MNOW.places")
						->from(LEAGUE_NUMBER_OF_WINNER." AS LNOW")
						->join(MASTER_NUMBER_OF_WINNER." AS MNOW","MNOW.number_of_winner_id = LNOW.number_of_winner_id","inner")
						->where("LNOW.league_number_of_winner_id",$league_number_of_winner_id)
						->get();

		$result			= $sql->row_array();
		$file			= prize_json();
		$winner_array	= json_decode($file,true);
		$prize_details	= array();

		//for Top 1 Place
		if($result['number_of_winner_id'] == 1)
		{
			$prize_percent		= $winner_array[$result['number_of_winner_desc']];
			$amt				= (($prize_pool_amount*$prize_percent['1'])/100);
			$prize_details[0]	= truncate_number($amt,2);
		}
		//Top 2,3,5,10 Place
		elseif( $result['number_of_winner_id'] == 2 || $result['number_of_winner_id'] == 3
				|| $result['number_of_winner_id'] == 4 || $result['number_of_winner_id'] == 5
				)
		{
			$prize_percent = $winner_array[$result['number_of_winner_desc']];
			for($i=0;$i<count($prize_percent);$i++)
			{
				$amt				= (($prize_pool_amount*$prize_percent[$i+1])/100);
				$prize_details[$i]	= truncate_number($amt,2);
			}
		}
		//for Top 30% 0r 50%
		elseif($result['number_of_winner_id'] == 6 || $result['number_of_winner_id'] == 7)
		{
			$prize_percent = $winner_array[$result['number_of_winner_desc']];

			if($prize_percent['30'] == 100 || $prize_percent['50'] == 100)
			{
				$place = floor(($size*$result['places'])/100);
				$amt = $prize_pool_amount/$place;

				if($desc)
				{
					$prize_details = array($amt.' - TOP '.$place);
				}
				else
				{
					for($i=0;$i<$place;$i++)
					{
						$amt				= $prize_pool_amount/$place;
						$prize_details[$i]	= truncate_number($amt);
					}
				}
			}
		}
		else
		{
			$prize_details = "";
		}
		return  $prize_details;
	}
	/**
	 * [save_contest description]
	 * @MethodName save_contest
	 * @Summary This function used to create new contest
	 * @param      array  data array
	 * @return     int
	 */
	public function save_contest($data , $opt_season_ids)
	{
		$this->db->insert(CONTEST,$data);
		$contest_id = $this->db->insert_id();
		$contest_opt_matches =array();

		foreach($opt_season_ids as $id) {
			$contest_opt_matches[] = array('contest_id' => $contest_id, 'season_id' => $id);
		}
		$this->db->insert_batch(CONTEST_OPT_MATCHES, $contest_opt_matches);
		return true;
	}



	public function get_contest_detail($contest_uid)
	{
		$select = "C.contest_id, C.contest_name, C.owner_id, 	C.entry_fee, L.league_abbr, C.size, C.total_user_joined,
					C.rewards, C.contest_uid, C.league_id, C.is_featured, C.status, DATE_FORMAT(C.created_date, '%d-%b-%Y %H:%i') AS created_date,
					U.user_name,  DATE_FORMAT ( C.start_at , '".MYSQL_DATE_FORMAT."' ) AS start_at, C.site_rake";

		$this->db->select($select);
		$this->db->from(CONTEST . ' AS  C');
		$this->db->join(USER . " AS U", "U.user_id = C.owner_id", 'LEFT');
		$this->db->join(LEAGUE . " AS L", "L.league_id = C.league_id", 'INNER');

		$this->db->where('C.contest_uid', $contest_uid);
		$query = $this->db->get();
		return $query->row_array();
	}

	public function get_master_playoff_week($league_id)
	{
		$data = $this->db->select("*")
						->from(MASTER_PLAYOFF_WEEK)
						->where('league_id', $league_id)
						->get()
						->result_array();
		return $data;
	}

	function getRosterInfoDetails($game_unique_id)
	{
		$select = "grr.size, mgr.position AS display_text ";

		$this->db->select($select);

		$this->db->from(GAME_ROSTER_RULES . ' grr');
		$this->db->join(MASTER_GAME_ROSTER . ' mgr', 'grr.master_game_roster_id = mgr.master_game_roster_id');
		$this->db->where('game_unique_id', $game_unique_id);
		$query = $this->db->get();

		//echo $this->db->last_query();die();
		return $query->result_array();
	}

	function get_scoring_info_details($game_unique_id)
	{
		$select = "gsr.scoring_type,gsr.points,msr.display_label,msr.sequence,msr.status,msr.scoring_category";

		$this->db->select($select);

		$this->db->from(GAME_SCORING_RULES . ' gsr');
		$this->db->join(MASTER_GAME_SCORING . ' msr', 'gsr.master_game_scoring_id = msr.master_game_scoring_id');
		$this->db->where('game_unique_id', $game_unique_id);
		$query = $this->db->get();

		//echo $this->db->last_query();die();
		return $query->result_array();
	}

	public function get_lineup_users_by_game()
	{
		$limit      = 10;
		$page       = 0;

		$post_data = $this->input->post();

		if(isset($post_data['items_perpage']))
		{
			$limit = $post_data['items_perpage'];
		}

		if(isset($post_data['current_page']))
		{
			$page = $post_data['current_page']-1;
		}


		$offset	= $limit * $page;

		$this->db->select("U.user_name,LM.total_score,LM.lineup_master_id")
						->from(LINEUP_MASTER." AS LM")
						->join(USER." AS U", "U.user_id=LM.user_id")
						->where("game_id", $post_data['game_id']);

		$tempdb = clone $this->db;
		$query = $this->db->get();
		$total = $query->num_rows();

		$sql = $tempdb->limit($limit, $offset)
						->get();
		// echo $tempdb->last_query();die;
		$result	= $sql->result_array();

		$result = ($result) ? $result : array();
		return array('result'=>$result, 'total'=>$total);
	}

	/**
	 * [get_lineup_detail description]
	 * @MethodName get_lineup_detail
	 * @Summary This function used get all user lineup detail
	 * @param      [type]  [description]
	 * @return     [type]
	 */
	public function get_lineup_detail($lineup_master_id)
	{
		$sql = $this->db->select("P.full_name,P.position,P.team_abbreviation,P.salary,L.score")
						->from(CONTEST_USER_LINEUP." AS L")
						->join(PLAYER." AS P", "P.player_unique_id=L.player_unique_id")
						->where("L.lineup_master_id", $lineup_master_id)
						->get();
		return $sql->result_array();
	}

	public function get_all_player_last_salaries($player_salary_master_id)
	{
		$sort_field = 'full_name';
		$sort_order = 'DESC';
		$limit      = 10;
		$page       = 0;

		$post_data = $this->input->post();

		if(isset($post_data['items_perpage']))
		{
			$limit = $post_data['items_perpage'];
		}

		if(isset($post_data['current_page']))
		{
			$page = $post_data['current_page']-1;
		}

		if(isset($post_data['sort_field']) && in_array($post_data['sort_field'],array('full_name', 'team_name', 'position', 'salary', 'player_status')))
		{
			$sort_field = $post_data['sort_field'];
		}

		if(($post_data['sort_order']) && in_array($post_data['sort_order'], array('DESC', 'ASC')))
		{
			$sort_order = $post_data['sort_order'];
		}

		$offset	= $limit * $page;

		$this->db->select("P.*")
						->from(PLAYER_SALARY_TRANSACTION." PST")
						->join(PLAYER." AS P", "P.player_unique_id = PST.player_unique_id", "inner")
						->where("PST.player_salary_master_id", $player_salary_master_id);

		$tempdb = clone $this->db;
		$query = $this->db->get();
		$total = $query->num_rows();

		$sql = $tempdb->order_by($sort_field, $sort_order)
						->limit($limit, $offset)
						->get();
		// echo $tempdb->last_query();die;
		$result	= $sql->result_array();

		$result = ($result) ? $result : array();
		return array('result'=>$result, 'total'=>$total);
	}


/* Latest Code Startr */
/* function used to get lobby data from table
	* @param String $post
	* @param String $offset
	*/
	function get_contest_list()
	{
		$limit		= 10;
		$page		= 0;
		$sort_field	= 'created_date';
		$sort_order	= 'DESC';
		$post	= $this->input->post();
		if(isset($post['itemsPerPage']))
		{
			$limit = $post['itemsPerPage'];
		}

		if(isset($post['currentPage']))
		{
			$page = $post['currentPage']-1;
		}
		if(isset($post['orderByField']) && in_array( $post['orderByField'], array('contest_name','size','entry_fee','rewards','is_featured','created_date') ) )
		{
			$sort_field = $post['orderByField'];
		}

		if(isset($post['sortOrder']) && in_array($post['sortOrder'],array('DESC','ASC')))
		{
			$sort_order = $post['sortOrder'];
		}

		$offset	= $limit * $page;

		$select  = "C.contest_id, C.contest_name, C.status, C.owner_id,
								C.entry_fee, C.size, C.total_user_joined, C.rewards, C.contest_uid, C.league_id,
								DATE_FORMAT( C.start_at, '".MYSQL_DATE_FORMAT."' ) AS start_at,	C.is_featured ";

		$this->db->select($select);

		$this->db->from(CONTEST . ' AS C');

		$where = array();

		if(isset($post["status"]) && !empty($post["status"])) {
			$where["C.status"]	= $post["status"];
		} else {
			$where["C.status != "]	= 2;
		}

		if(isset($post["league_id"]) && !empty($post["league_id"])) {
			$where["C.league_id"]	= $post["league_id"];
		}
		
		if(isset($post['keyword']) && $post['keyword'] != "") {
			$this->db->like('contest_name',$post['keyword']);
		}

		if(count($where) > 0) {
			$this->db->where($where);
		}

		$tempdb = clone $this->db;
		$query = $this->db->get();

		$total = $query->num_rows();



		$tempdb->limit($limit,$offset);
		$tempdb->order_by($sort_field, $sort_order);

		$sql	= $tempdb->get();
		$result	= $sql->result_array();

		// echo $tempdb->last_query();die();

		$result = ($result) ? $result : array();
		return array('contest'=>$result,'total' =>$total);
	}

	public function get_all_scoring($league_id)
	{
		$data = $this->db->select()
						->from(MASTER_CONTEST_SCORING)
						->where('status', 1)
						->order_by('sequence', 'ASC')
						->get()

						->result_array();
		return $data;
	}

	public function get_off_season_start_date($year, $league_id = 2)
	{
		$season_end_date =  $this->db->select('MAX(season_week_close_date_time) AS last_date ')
									->from(SEASON_WEEK)
									->where('league_id', $league_id)
									->where('year', $year)
									->get()
									->row_array();
		$off_season_start = date('Y-m-d', strtotime($season_end_date['last_date']. OFF_SEASON_START_DATE));
		return $off_season_start;
	}

	public function get_paid_games($season_year)
	{
		$data = $this->db->select("G.game_unique_id, G.user_id AS commishner_user_id, LM.user_id AS joined_user_id, G.game_name, G.status, G.entry_fee, G.is_premium_game, CONCAT(U.first_name,' ', U.last_name) AS full_name,  IFNULL(GROUP_CONCAT(GUEP.email), U.email) AS user_emails, IFNULL(GP.game_partial_id, 0) AS is_link_sent")
						->from(GAME." G")
						->join(LINEUP_MASTER . ' LM', 'LM.game_unique_id = G.game_unique_id','LEFT')
						->join(GAME_USER_EMAIL_PREFERENCE . ' GUEP', 'GUEP.lineup_master_id = LM.lineup_master_id','LEFT')
						->join(USER." AS U", "LM.user_id = U.user_id", "inner")
						->join(GAME_PARTIAL . ' GP', 'GP.parent_game_unique_id = G.game_unique_id','LEFT')
						->where("G.current_year", $season_year)
						//->where("G.subscription_fee >", 0)
						->group_by('G.game_unique_id')
						->where('(G.status = "COMPLETED" OR G.status = "OFF-SEASON")')
						->get()
						->result_array();
		return $data;
	}
	public function generate_next_game_id($game)
	{
		$game_part = explode('-', $game["game_unique_id"]);
		$new_game  = $game_part[0]. '-'. (intval($game_part[1] + 1));
		return $new_game;
	}

        /**
	 * [get_list_game_date_wise description]
	 * @MethodName get_list_game_date_wise
	 * @Summary This function used to get game list with game detail order by scheduled date
	 * @param      array  game list array
	 * @param      int    league id
	 * @return     array
	 */
	public function get_season_game_detail($season_game_unique_id ,$league_id)
	{
		$sql = $this->db->select('S.season_game_unique_id, S.season_scheduled_date, S.home as home_id, S.away as away_id, T1.team_abbr as home, T2.team_abbr as away')
						->from(SEASON." AS S")
						->join(TEAM.' T1','T1.team_id = S.home AND T1.league_id = S.league_id','LEFT')
						->join(TEAM.' T2','T2.team_id = S.away AND T2.league_id = S.league_id','LEFT')
						->where('S.season_game_unique_id',$season_game_unique_id)
						->where('S.league_id',$league_id)
						->get();
		$result = $sql->row_array();
		return $result;
	}

	/**
	 * [get_all_videos description]
	 * Summary :-
	 * @return [type] [description]
	 */
	public function get_all_videos()
	{
		$sort_field	= 'last_updated';
		$sort_order	= 'DESC';
		$limit		= 10;
		$page		= 0;
		$post_data	= $this->input->post();
		$league_id  = $post_data["league_id"];
		if(isset($post_data['items_perpage']))
		{
			$limit = $post_data['items_perpage'];
		}

		if(isset($post_data['current_page']))
		{
			$page = $post_data['current_page'] - 1;
		}

		if(isset($post_data['sort_field']) && in_array($post_data['sort_field'],array('video_type','video_title_en, last_updated')))
		{
			$sort_field = $post_data['sort_field'];
		}

		if(isset($post_data['sort_order']) && in_array($post_data['sort_order'],array('DESC','ASC')))
		{
			$sort_order = $post_data['sort_order'];
		}

		$offset	= $limit * $page;
		$this->db->select('V.video_id,V.video_title_en,V.video_title_sp, V.video_type, V.league_id, V.video_desc_en, V.video_desc_sp, V.status, V.video_url,V.last_updated', FALSE)
						->from(VIDEOS . " AS V")
						->where("V.league_id", $league_id);

		$tempdb = clone $this->db;
		$query = $this->db->get();

		$total = $query->num_rows();

		$sql = $tempdb->order_by($sort_field, $sort_order)
						->limit($limit,$offset)
						->get();

		$result	= $sql->result_array();

		$result = ($result) ? $result : array();
		return array('result'=>$result,'total'=>$total);
	}

	public function contest_participants($contest_uid) {
		$data =  $this->db->select('CUP.*, U.first_name, U.last_name, U.email')
											->from(CONTEST_USER_PRIME.' AS CUP')
											->join(USER.' AS U', 'U.user_id = CUP.user_id', 'inner')
											->join(CONTEST.' AS C', 'C.contest_id = CUP.contest_id', 'inner')
											->where('C.contest_uid', $contest_uid)
											->order_by('CUP.total_score', 'DESC')
											->get()
											->result_array();
		if(!empty($data)) {
			foreach($data as &$val) {
				switch($val['status']) {
					case 1:
						$val['status_label'] = 'Joined';
					break;
					case 2:
						$val['status_label'] = 'Entry fee refunded';
					break;
					case 3:
						$val['status_label'] = 'Winner';
					break;
				}
			}
		}

		return $data;
	}

}

/* End of file Contest_model.php */
/* Location: ./application/models/Contest_model.php */
