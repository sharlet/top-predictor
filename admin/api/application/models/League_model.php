<?php defined('BASEPATH') OR exit('No direct script access allowed');

class League_model extends MY_Model {

	public function __construct() {
		parent::__construct();
		$this->admin_id = $this->session->userdata('admin_id');
    }
    
    public function get_all_leagues() {
        $sql = $this->db->select('L.league_id, L.league_abbr, L.active as status, L.order')
						->from(LEAGUE . " AS L")
						->join(MASTER_SPORTS . " AS MS", "MS.sports_id = L.sports_id", 'INNER')
						->order_by('L.order', 'ASC')
						->get();
        
        $result  = $sql->result_array();
		return $result;
    }
}
