<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Season_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->admin_id = $this->session->userdata('admin_id');
	}

	
	/**
	 * [get_all_season_schedule description]
	 * @MethodName get_all_season_schedule
	 * @Summary This function used for get all season schedule
	 * @param      boolean  [User List or Return Only Count]
	 * @return     [type]
	 */
	public function get_all_season_schedule($count_only = FALSE, $lang='en')
	{
		$sort_field	= 'season_scheduled_date';
		$sort_order	= 'DESC';

		$post_data = $this->input->post();

		$limit = ($post_data['items_perpage'])? $post_data['items_perpage']: 10;
		$page =  ($post_data['current_page'])? $post_data['current_page']-1 : 0;
		

		if(($post_data['sort_field']) && in_array($post_data['sort_field'],array('year','type','season_scheduled_date','home','away','status','week')))
		{
			$sort_field = $this->input->post('sort_field');
		}

		if(($post_data['sort_order']) && in_array($post_data['sort_order'],array('DESC','ASC')))
		{
			$sort_order = $post_data['sort_order'];
		}

		$offset	= $limit * $page;

		$league_id	= isset($post_data['league_id']) ? $post_data['league_id'] : "";
		$team_id	= isset($post_data['team_id']) ? $post_data['team_id'] : "";
		$week		= isset($post_data['week']) ? $post_data['week'] : "";
		$fromdate	= isset($post_data['fromdate']) ? $post_data['fromdate'] : "";
		$todate		= isset($post_data['todate']) ? $post_data['todate'] : "";

		$this->db->select("S.season_game_unique_id,S.season_id, S.year, S.type, TDH.team_abbr_label as home,TDA.team_abbr_label as away, S.status, S.week, DATE_FORMAT(S.season_scheduled_date,'%d-%b-%Y %H:%i') as season_scheduled_dates")
						->from(SEASON. " AS S")
						->join(TEAM_DETAILS . " AS TDH", "TDH.team_id = S.home AND TDH.league_id = '$league_id' AND TDH.lang = '$lang'", 'LEFT')
						->join(TEAM_DETAILS . " AS TDA", "TDA.team_id = S.away AND TDA.league_id = '$league_id' AND TDA.lang = '$lang'", 'LEFT');

		if($league_id != "") {
			$this->db->where("S.league_id", $league_id);
		}

		if($team_id != "" && $team_id != 'all')	{
			$this->db->where("(S.home = '$team_id' OR  S.away = '$team_id')");
		}

		if($week != "" && $week != 'all') {
			$this->db->where("S.week", $week);
		}

		if($fromdate != "" && $todate != "") {
			$this->db->where("DATE_FORMAT(S.season_scheduled_date,'%Y-%m-%d') >= '".$fromdate."' and DATE_FORMAT(S.season_scheduled_date,'%Y-%m-%d') <= '".$todate."'");
		}
		
		$tempdb = clone $this->db;
		$query = $this->db->get();
		$total = $query->num_rows();

		$sql = $tempdb->order_by($sort_field, $sort_order)
						->limit($limit,$offset)
						->get();
		
		$result	= $sql->result_array();
		//print_qe();
		$result = ($result) ? $result : array();
		return array('result'=>$result,'total'=>$total);
	}
	
	/**
	 * [update_roster description]
	 * @MethodName update_roster
	 * @Summary This function used update multiple player salary and status
	 * @param      [int]  [league_id]
	 * @param      [array]  [data_arr]
	 * @return     [boolean]
	 */
	public function update_withdrawal_request($data_arr)
	{		
		$this->db->update_batch(PAYMENT_WITHDRAW_TRANSACTION, $data_arr, 'payment_withdraw_transaction_id');

		return $this->db->affected_rows();
	}

	/**
	 * [change_withdrawal_status description]
	 * @MethodName change_withdrawal_status
	 * @Summary This function used to withdrawal status
	 * @param      [varchar]  [withdraw_transaction_id]
	 * @param      [int]  [status]
	 * @return     [boolean]
	 */
	public function change_withdrawal_status($withdraw_transaction_id,$status)
	{
		$this->db->where("payment_withdraw_transaction_id",$withdraw_transaction_id);
		$this->db->update(PAYMENT_WITHDRAW_TRANSACTION,array('status'=>$status));
		
		return $this->db->affected_rows();
	}


	/**
	 * [get_all_season_stats description]
	 * @MethodName get_all_season_stats
	 * @Summary This function used for get all season stats
	 * @return     [type]
	 */
	public function get_all_season_stats()
	{
		$post_data = $this->input->post();
		$this->load->config('vconfig');
		
		$season_stats_common_field = $this->config->item('season_stats_common_field');
		$season_stats_field = $this->config->item('season_stats_field')[$post_data['league_id']];
		
		$fields		= array_merge($season_stats_common_field, $season_stats_field);
		$column		= implode(',', $fields);

		$sort_field = '';
		$sort_order = '';
		$limit      = 10;
		$page       = 0;
		$summary 	= '';
		
		if(isset($post_data['items_perpage']))
		{
			$limit = $post_data['items_perpage'];
		}

		if(isset($post_data['current_page']))
		{
			$page = $post_data['current_page']-1;
		}

		if(isset($post_data['sort_field']) && in_array($post_data['sort_field'],$columns))
		{
			$sort_field = $post_data['sort_field'];
		}

		if(isset($post_data['sort_order']) && in_array($post_data['sort_order'],array('DESC','ASC')))
		{
			$sort_order = $post_data['sort_order'];
		}

		$offset	= $limit * $page;		

		switch ($post_data['league_id']) {
			case '1':
				$table_name = GAME_STATISTICS_BASEBALL;
				break;
			case '2':
				$table_name = GAME_STATISTICS_FOOTBALL;
				break;
			case '4';
			case '6';
			case '7';
			case '12';
			case '16':
				$table_name = GAME_STATISTICS_SOCCER;
				break;
			default:
				break;
		}

		$this->db->select("$column")
						->from($table_name)
						->where('season_game_unique_id', $post_data['game_unique_id']);

		$tempdb = clone $this->db;
		$query = $this->db->get();
		$total = $query->num_rows();
		if($sort_field!="" && $sort_order!="")
		{
			$tempdb->order_by($sort_field, $sort_order);
		}
		$tempdb->limit($limit,$offset);
		$sql = $tempdb->get();

		$result	= $sql->result_array();
		if(!empty($result))
		{
			$summary = $result[0]['home']." vs ".$result[0]['away']." - ".date("d-M-Y h:i A",strtotime($result[0]['scheduled_date']));			
		}
		$temp_array = array();
		foreach ($result as $rs)
		{
			$rs['scheduled_date'] = date("d-M-Y h:i A",strtotime($rs['scheduled_date']));
			$temp_array[] = $rs;
 		}
		$result = ($temp_array) ? $temp_array : array();
		return array('fields'=>$fields, 'result'=>$result, 'total'=>$total,'summary'=>$summary);
	}
}
/* End of file Season_model.php */
/* Location: ./application/models/Season_model.php */