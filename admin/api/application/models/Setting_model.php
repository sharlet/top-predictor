<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->admin_id = $this->session->userdata('admin_id');
	}

	public function chnage_password($data_arr)
	{
		$this->db->where('password',md5($data_arr['old_password']))
				->where('admin_id',$this->admin_id)
				->update(ADMIN,array('password'=>md5($data_arr['new_password'])));
		// echo $this->db->last_query();die;
		return $this->db->affected_rows();
	}
}
/* End of file Teamroster_model.php */
/* Location: ./application/models/Teamroster_model.php */