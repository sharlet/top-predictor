<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	/**
	 * [get_all_site_user_detail description]
	 * @MethodName get_all_site_user_detail
	 * @Summary This function used for get all user list and return filter user list 
	 * @param      boolean  [User List or Return Only Count]
	 * @return     [type]
	 */
	public function get_all_user($count_only=FALSE)
	{
		$sort_field	= 'added_date';
		$sort_order	= 'DESC';
		$limit		= 10;
		$page		= 0;
		$post_data = $this->input->post();

		if($post_data['items_perpage'])
		{
			$limit = $post_data['items_perpage'];
		}

		if($post_data['current_page'])
		{
			$page = $post_data['current_page']-1;
		}

		if($post_data['sort_field'] && in_array($post_data['sort_field'],array('added_date','first_name','user_name','email','country','balance','status')))
		{
			$sort_field = $post_data['sort_field'];
		}

		if($post_data['sort_order'] && in_array($post_data['sort_order'],array('DESC','ASC')))
		{
			$sort_order = $post_data['sort_order'];
		}

		$offset	= $limit * $page;

		$sql = $this->db->select("U.user_unique_id,U.user_id,CONCAT_WS(' ',U.first_name,U.last_name, NULL) AS name,U.image,MC.country_name,U.balance,U.email,U.status,DATE_FORMAT(U.added_date,'%d-%b-%Y') AS member_since,U.user_name")
						->from(USER.' AS U')
						->join(MASTER_COUNTRY." AS MC","MC.master_country_id=  U.master_country_id","LEFT")
						->join(MASTER_STATE." AS MS","MS.master_state_id=  U.master_state_id","LEFT")
						->order_by($sort_field, $sort_order);
		if(isset($post_data['country']) && $post_data['country']!="")
		{
			$this->db->where("U.master_country_id",$post_data['country']);
		}
		if(isset($post_data['frombalance']) && isset($post_data['tobalance']))
		{
			$this->db->where("U.balance >= ".$post_data['frombalance']." and U.balance <= ".$post_data['tobalance']."");
		}

		if(isset($post_data['keyword']) && $post_data['keyword'] != "")
		{
			$this->db->group_start();
			$this->db->like('email',$post_data['keyword']);
			$this->db->or_like('user_name',$post_data['keyword']);
			$this->db->or_like('CONCAT(first_name," ",last_name)',$post_data['keyword']);
			$this->db->group_end();
		}
		
		$tempdb = clone $this->db;
		$query = $this->db->get();
		$total = $query->num_rows();

		$sql = $tempdb->order_by($sort_field, $sort_order)
						->limit($limit,$offset)
						->get();
		$result	= $sql->result_array();

		$records	= array();
		foreach($result as $rs)
		{
			if($rs['image'] == "")
			{
				$rs['image'] = base_url()."assets/images/default_user.png";
			}
			else
			{
				$rs['image'] = PATH_URL.PROFILE_THUMB.$rs['image'];
			}
			$records[] = $rs;
		}
		$result=($records)?$records:array();
		return array('result'=>$result,'total'=>$total);
	}
	/**
	 * [get_user_detail_by_id description]
	 * @MethodName get_user_detail_by_id
	 * @Summary This function used for get user Detail
	 * @param      [int]  [User Id]
	 * @return     [array]
	 */
	public function get_user_detail_by_id($user_id)
	{
		$sql = $this->db->select("U.user_unique_id,U.user_id,U.first_name,U.last_name,U.image,MC.country_name,U.balance,U.email,DATE_FORMAT(U.dob,'%d-%b-%Y') as dob,U.phone_no,U.city,U.language,
								U.status,DATE_FORMAT(U.added_date,'%d-%b-%Y') AS member_since,U.user_name,MS.name as state_name",FALSE)
						->from(USER." AS U")
						->join(MASTER_COUNTRY." AS MC","MC.master_country_id = U.master_country_id","left")
						->join(MASTER_STATE." AS MS","MS.master_state_id = U.master_state_id","left")
						->where("U.user_unique_id",$user_id)
						->get();
		$result = $sql->row_array();
		return ($result)?$result:array();
	}

	/**
	 * [user_transaction_history description]
	 * @MethodName user_transaction_history
	 * @Summary This function used for get all transaction history
	 * @return     [array]
	 */
	public function user_transaction_history()
	{
		$sort_field	= 'payment_history_transaction_id';
		$sort_order	= 'DESC';
		$limit		= 10;
		$page		= 0;
		$post_data	= $this->input->post();

		$user_id = $post_data['user_id'];
		
		if(isset($post_data['items_perpage']))
		{
			$limit = $post_data['items_perpage'];
		}

		if(isset($post_data['current_page']))
		{
			$page = $post_data['current_page']-1;
		}

		if(isset($post_data['sort_field']) && in_array($post_data['sort_field'],array('user_name,sort_field,country,balance,added_date,status')))
		{
			$sort_field = $post_data['sort_field'];
		}

		if(isset($post_data['sort_order']) && in_array($post_data['sort_order'],array('DESC','ASC')))
		{
			$sort_order = $post_data['sort_order'];
		}

		$offset	= $limit * $page;

		$this->db->select('PHT.payment_history_transaction_id, PHT.description, PHT.payment_type, PHT.transaction_amount, PHT.user_balance_at_transaction,
						G.game_unique_id, PHT.payment_withdraw_transaction_id,PWT.withdraw_type,PWT.status,DATE_FORMAT(PHT.created_date , "%d-%b-%Y") AS created_date,G.game_name,G.total_user_joined ', FALSE)
						->from(PAYMENT_HISTORY_TRANSACTION . " AS PHT")
						->join(PAYMENT_WITHDRAW_TRANSACTION." AS PWT","PWT.payment_withdraw_transaction_id = PHT.payment_withdraw_transaction_id","left")
						->join(GAME." AS G","G.game_unique_id = PHT.game_unique_id","left")
						->where("PHT.user_id",$user_id);

		$tempdb = clone $this->db;
		$query = $this->db->get();
		$total = $query->num_rows();

		$sql = $tempdb->order_by($sort_field, $sort_order)
						->limit($limit,$offset)
						->get();
		$result	= $sql->result_array();

		$result = ($result) ? $result : array();
		return array('result'=>$result,'total'=>$total);
	}

	/**
	 * [game_history_by_user description]
	 * @MethodName game_history_by_user
	 * @Summary This function used for get all created by user and join game list
	 * @param  [array] [Data Array]
	 * @return     [array]
	 */
	public function game_history_by_user()
	{
		$sort_field = 'season_scheduled_date';
		$sort_order = 'DESC';
		$limit      = 10;
		$page       = 0;
		$user_id = $this->input->post('user_id');
		if(($this->input->post('items_perpage')))
		{
			$limit = $this->input->post('items_perpage');
		}

		if(($this->input->post('current_page')))
		{
			$page = $this->input->post('current_page')-1;
		}

		if(($this->input->post('sort_field')) && in_array($this->input->post('sort_field'),array('')))
		{
			$sort_field = $this->input->post('sort_field');
		}

		if(($this->input->post('sort_order')) && in_array($this->input->post('sort_order'),array('DESC','ASC')))
		{
			$sort_order = $this->input->post('sort_order');
		}

		$offset	= $limit * $page;
		$user_id = $this->input->post('user_id');
		$this->db->select('G.game_unique_id, G.game_name, G.game_type, G.entry_fee, G.prize_pool, G.total_user_joined, G.size,
							G.status,LM.lineup_master_id, DATE_FORMAT(G.game_draft_date, "%d-%b-%Y %H:%i") AS season_scheduled_date', FALSE)
						->from(GAME . " AS G")
						->join(LINEUP_MASTER." AS LM","LM.game_id=G.game_id","left")
						->where("LM.user_id",$user_id);

		$tempdb = clone $this->db;
		$query = $this->db->get();
		$total = $query->num_rows();
		$sql = $tempdb->order_by($sort_field, $sort_order)
						->limit($limit,$offset)
						->get();
		$result	= $sql->result_array();

		$result = ($result) ? $result : array();
		return array('result'=>$result,'total'=>$total);
	}

	/**
	 * [change_user_status description]
	 * @MethodName update_user_detail
	 * @Summary This function used to update user detail
	 * @param      [varchar]  [User Unique ID]
	 * @param      [array]
	 * @return     [boolean]
	 */
	public function update_user_detail($user_unique_id,$data_arr)
	{
		$this->db->where("user_unique_id",$user_unique_id)
				->update(USER,$data_arr);
		return true;
	}

	/**
	 * [make_payment_transaction description]
	 * @MethodName make_payment_transaction
	 * @Summary This function used to debit or credit user balance
	 * @param      array  $data_arr
	 * @return     int
	 */
	public function make_payment_transaction($data_arr)
	{
		$this->db->insert(PAYMENT_HISTORY_TRANSACTION,$data_arr);
		return $this->db->insert_id();
	}

	

	/**
	 * [get_max_user_balance description]
	 * @MethodName get_max_user_balance
	 * @Summary This function used to get user max balance
	 * @return  int
	 */
	public function get_min_max_user_balance()
	{
		$this->db->select_max('balance','max_value');
		$this->db->select_min('balance','min_value');
		$result = $this->db->get(USER)->row_array();
		return $result;
	}
}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */