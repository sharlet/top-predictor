<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Adminauth_model extends MY_Model{

	function __construct()
	{
		parent::__construct();
	}

	/**
	 * [admin_login description]
	 * @MethodName admin_login
	 * @Summary This funciton used for check admin authentication
	 * @param      [array]  [for login email and password]
	 * @return     [array]
	 */
	public function admin_login($email,$password)
	{
		$sql = $this->db->select("AD.admin_id,AD.email,AD.role_id,CONCAT_WS(' ', AD.firstname, AD.lastname) AS full_name,ARR.right_ids",FALSE)
						->from(ADMIN." AS AD")
						->join(ADMIN_ROLES_RIGHTS." AS ARR","ARR.role_id = AD.role_id","inner")
						->where("email",$email)
						->where("password",md5($password))
						->get();
		
		$result = $sql->row_array();
		return ($result)?$result:array();
	}

	/**
	 * [check_user_key description]
	 * @MethodName check_user_key
	 * @Summary This function used for check user key exist
	 * @param      [varchar]  [Login key]
	 * @return     [array]
	 */
	public function check_user_key($key)
	{
		$sql = $this->db->select("*")
						->from(ADMIN_ACTIVE_LOGIN)
						->where("key",$key)
						->get();
		$result = $sql->row_array();
		return ($result)?$result:array();
	}
}