<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Teamroster_model extends MY_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->admin_id = $this->session->userdata('admin_id');
	}	

	/**
	 * [get_all_teamrosters description]
	 * @MethodName get_all_teamrosters
	 * @Summary This function used for get all team list and return filter team list
	 * @return     [type]
	 */
	public function get_all_teamrosters($lang = 'en')
	{
		$sort_field	= 'team_name';
		$sort_order	= 'DESC';
		$post_data	= $this->input->post();
				

		$lang = (isset($post_data['lang'])) ? $post_data['lang'] : $lang;
		$limit = (isset($post_data['items_perpage'])) ? $post_data['items_perpage'] : 10;
		$page = (isset($post_data['current_page'])) ? $post_data['current_page']-1 : 0;
		$league_id	= (isset($post_data['league_id'])) ? $post_data['league_id'] : "";
		
		if(isset($post_data['sort_field']) && in_array($post_data['sort_field'],array('team_name','team_abbr','team_abbr_label','bye_week')))
		{
			$sort_field = $post_data['sort_field'];
		}

		if (isset($post_data['sort_order']) && in_array($post_data['sort_order'],array('DESC','ASC'))) {
			$sort_order = $post_data['sort_order'];
		}
		
		if($post_data['keyword']) {
			$this->db->group_start();
			$this->db->like('team_name', $post_data['keyword']);
			$this->db->or_like('team_abbr', $post_data['keyword']);
			$this->db->group_end();
		}

		$offset	= $limit * $page;

		$this->db->select("T.team_id, team_abbr, team_name, team_abbr_label,bye_week")
				->from(TEAM.' AS T')
				->join(TEAM_DETAILS.' AS TD'," T.team_id = TD.team_id and TD.lang = '".$lang."' AND T.league_id = TD.league_id ");

		if ($league_id != '') 
		{
			$this->db->where('TD.league_id', $league_id);
		}

		$tempdb = clone $this->db;
		$query = $this->db->get();
		
		$total = $query->num_rows();

		$sql = $tempdb->order_by($sort_field, $sort_order)
						->limit($limit,$offset)
						->get();

		$result	= $sql->result_array();

		return array('result'=> $result, 'total'=> $total);
	}

	public function get_team_data($where)
	{
		$team_data = $this->db->select("T.team_id,T.bye_week ,team_abbr, TDE.team_name as team_name_en, TDS.team_name as team_name_sp,TDE.team_abbr_label as team_abbr_label_en, TDS.team_abbr_label as team_abbr_label_sp")
					->from(TEAM.' AS T')
					->join(TEAM_DETAILS.' AS TDE'," T.team_id = TDE.team_id and TDE.lang = 'en' AND T.league_id = TDE.league_id ")
					->join(TEAM_DETAILS.' AS TDS'," T.team_id = TDS.team_id and TDS.lang = 'sp' AND T.league_id = TDS.league_id ")
					->where($where)->get()->row_array();
		return $team_data;
	}

	function get_all_user_team_of_game($game_unique_id = "")
	{
		$current_year = format_date('today','Y');

		$check_year = array($current_year, $current_year-1);

		$data = $this->db->select('LM.*')
						->from(LINEUP_MASTER." AS LM")
						->join(GAME." AS  G", "G.game_unique_id = LM.game_unique_id", "left")
						->where("LM.game_unique_id", $game_unique_id)
						->where_in('G.current_year', $check_year)
						->order_by('LM.draft_order', "ASC")
						->get()
						->result_array();
	    return  $data ;
	}

	function get_positions()
	{
		$sql = $this->db->select('position,allowed_positions,master_game_roster_id,sequence')
					->from(MASTER_GAME_ROSTER)
					->where('league_id', $this->league_id)
					->order_by('sequence', 'ASC')
					->get();
	    $data = $sql->result_array();
	    return (!empty($data)) ? $data : array();
	}

	function get_draft_selected_players_id($game_unique_id = "")
	{
		$current_year = format_date('today','Y');

		$data = $this->db->select("GROUP_CONCAT(L.player_unique_id) as players_id")
						->from(LINEUP_MASTER." AS LM")
						->join(LINEUP." AS L", "L.lineup_master_id = LM.lineup_master_id", 'INNER')
						->join(GAME." AS G", "G.game_unique_id = LM.game_unique_id", 'INNER')
						->where('LM.game_unique_id', $game_unique_id)
						->where('G.current_year', $current_year)
						->get()
						->row_array();	
						
		if(!empty($data))
		{
			return $data = explode(',', $data['players_id']);
		}
		else
		{
			return array();
		}		
	}

	function get_team_roster_by_lineup_id($lineup_master_id = "")
	{

		$data = $this->db->select('LM.team_name,L.lineup_id, LM.team_logo,L.player_unique_id as lineup_player_unique_id, P.player_unique_id,P.rank_number,P.bye_week, 
									SUBSTRING(P.first_name, 1, 1) AS first_char, P.last_name, P.team_market, P.full_name,
									L.master_game_roster_id,U.user_name,MGS.allowed_positions,MGS.position,MGS.sequence,L.lineup_master_id,IF(P.team_abbreviation = S.home,S.away,S.home) as `opponent`')
							->from(LINEUP_MASTER." AS LM")
							->join(LINEUP." AS L", "L.lineup_master_id = LM.lineup_master_id", 'INNER')
							->join(USER." AS U", "U.user_id = LM.user_id", 'LEFT')
							->join(PLAYER." AS P", "P.player_unique_id = L.player_unique_id", 'LEFT')
							->join(MASTER_GAME_ROSTER." AS MGS", "MGS.master_game_roster_id = L.master_game_roster_id", 'LEFT')
							->join(SEASON." AS S","S.home = P.team_abbreviation OR S.away = P.team_abbreviation",'LEFT')
							->where('LM.lineup_master_id', $lineup_master_id)
							->group_by('P.player_unique_id')
							->order_by('MGS.sequence')
							->get()
							->result_array();
						
	    return $data ;
	}

	public function get_game_detail_by_id()
	{
		$sql = "SELECT
					G.game_unique_id ,G.waiver_type,G.last_pick_time,G.site_rake , G.game_name , G.league_id , G.status , G.trade_end_date, 
					G.game_draft_date , G.size, G.entry_fee , G.prize_pool , G.game_draft_time,G.waiver_budget,
					L.max_player_per_team,G.total_user_joined,G.user_id,G.game_type,MS.sports_desc,
					G.current_round_number,G.current_pick_number,G.max_game_played,G.pick_duration,G.is_premium_game,G.current_year,G.trade_reject_time,G.draft_pick_trade,G.game_invite_permission, G.game_forward_status,
					CASE WHEN (G.game_draft='SNAKE_DRAFT') THEN '1' ELSE '0' END AS is_snake_draft
				FROM
					".$this->db->dbprefix( GAME )." AS G
				INNER JOIN
					".$this->db->dbprefix( LEAGUE )." AS L ON L.league_id = G.league_id
				INNER JOIN 
					".$this->db->dbprefix( MASTER_SPORTS )." AS MS ON MS.sports_id = L.sports_id 
				WHERE
						`G`.`game_unique_id` = '" . $this->game_unique_id . "'
					AND
						`L`.`active` 	= '1'
					AND
						`MS`.`active`   = '1'
				";
		return $this->run_query($sql, 'row_array');
	}

	public function calculate_remaining_pick_time($pick_duration_min, $last_pick_time)
	{
		$output = array();
		$cur_date = format_date();
		$final_remaining_time_sec = 0;

		$pick_duration_sec		= $pick_duration_min * 60  ;
		$output['clock_status']	= 'RUNNING';

		$d1			= date_create($cur_date);
		$d2			= date_create($last_pick_time);
		$diff_arry	= date_diff($d1, $d2);

		$used_sec	= $diff_arry->h * 3600 + $diff_arry->i * 60 + $diff_arry->s;

		$final_remaining_time_sec = $pick_duration_sec - $used_sec ;

		$output['time_remaining']	 = sec_to_h_m_s_convert($final_remaining_time_sec);
		$output['sec_remaining']	 = $final_remaining_time_sec;
		$output['js_sec_remaining']	 = $final_remaining_time_sec * 1000;

		if($final_remaining_time_sec <= 0)
		{
			$output['sec_remaining']	 = 0;
			$output['js_sec_remaining']	 = 0;
			$output['clock_status'] = 'STOP';
		}

		/*echo "<pre>";
		print_r($user_info);
		print_r($output);
		exit();*/
		return $output;
	}

	function get_all_player_position($game_unique_id = "", $is_position_str = false)
	{
		$field_list = "MGR.position, MGR.allowed_positions, GR.size, GR.master_game_roster_id ";
		if ($is_position_str) 
		{
			$field_list = "GROUP_CONCAT(QUOTE(MGR.position)) as positions_string";
		}

		$sql = "SELECT 
					$field_list 
				FROM 
					".$this->db->dbprefix( GAME_ROSTER_RULES )." AS GR 
				INNER JOIN 
					".$this->db->dbprefix( MASTER_GAME_ROSTER )." AS MGR ON MGR.master_game_roster_id = GR.master_game_roster_id 
				WHERE 
					GR.game_unique_id = '".$game_unique_id."' 
				ORDER BY sequence ASC";
		$list = $this->db->query($sql);
	    $data = ($is_position_str) ? $list->row_array() : $list->result_array();	    
	    return (!empty($data)) ? $data : array();
	}

	function get_all_player($input_arry = array()) 
	{
		$limit					= $input_arry['limit'];
		$offset					= $input_arry['offset'];
		$order_by				= $input_arry['order_by'];
		$order_sequence			= $input_arry['order_sequence'];
		$search_text			= trim( $this->db->escape_str($input_arry['search_text'] ) );
		$position				= strtolower(trim($input_arry['position']));
		
		$show_drafted_player	= $input_arry['show_drafted_player'];
		$game_unique_id			= $input_arry['game_unique_id'];

		/* Order by   */
		$order_by = 'P.rank_number ';
		switch($input_arry['order_by'])
		{
			case 'position':
				$order_by = ' P.position ';
			break;
			case 'full_name':
				$order_by = ' P.full_name ';
			break;
			case 'match_name':
				$order_by = ' match_name ';
			break;
			case 'rank_number':
				$order_by = 'P.rank_number ';
			break;
			case 'ffpg':
				$order_by = ' ffpg ';
			break;
		}		

		$filter_score_cond = "";

		/* Search Text  */
		$search_condition= '';
		if($search_text != '')
		{
			$search_condition .= "AND ( P.full_name LIKE '".$search_text."%' OR P.last_name LIKE '".$search_text."%' OR P.team_abbreviation LIKE '".$search_text."%' )  ";
		}  

		$and_filter = "";
		/* Position  */
		$position_condition= '';
		if( $position != '' && $position != 'all')
		{
			$all_pos = explode(",", $position);
			$all_pos = implode( ',' , array_map( function( $n ){ return '\''.$n.'\''; } , $all_pos) );
			$position_condition = "AND P.position IN (".$all_pos.") ";
		}

		$show_drafted_player_condition = '';
		if($show_drafted_player)
		{
			$show_drafted_player_condition = ' AND L.lineup_id IS NULL ';
		}

		$today      = format_date('today', 'Y-m-d');
		$sql = "SELECT '0' As fppg,P.img_url,P.player_unique_id,P.rank_number,P.bye_week, SUBSTRING(P.first_name, 1, 1) AS first_char,
						P.last_name, P.team_market, P.full_name, P.position,  P.team_abbreviation,IF(P.weight=0,'-',P.weight) AS weight,
						IF(P.height=0,'-',P.height) AS height,P.league_id,TEAM.season_game_unique_id,P.injury_status,P.player_status, 
						TEAM.home,TEAM.away,DATE_FORMAT(  TEAM.season_scheduled_date, '".MYSQL_DATE_FORMAT."' ) AS season_scheduled_date, L.lineup_id 
				FROM  ".$this->db->dbprefix( PLAYER )." AS P 
				LEFT JOIN 
				(
					SELECT 
						T.team_id, T.team_abbr,S.year, S.week, S.scheduled_date_time,S.season_scheduled_date, S.scheduled_date, S.season_game_unique_id, S.home, S.away
					FROM  	".$this->db->dbprefix( TEAM )." AS T 
					LEFT JOIN 	".$this->db->dbprefix( SEASON )." AS S ON (T.team_abbr = S.home OR T.team_abbr = S.away) 	AND  T.league_id = S.league_id 
					WHERE 
						T.league_id = ".$this->league_id." 
							AND 
						S.scheduled_date > '$today' 
					GROUP BY T.team_abbr
				) AS TEAM  ON TEAM.team_abbr = P.team_abbreviation 
				LEFT JOIN 
				(
					SELECT L.`lineup_id`, L.player_unique_id 
					FROM ".$this->db->dbprefix(LINEUP)." AS L 
					LEFT JOIN ".$this->db->dbprefix(LINEUP_MASTER)." AS LM ON LM.`lineup_master_id` = L.`lineup_master_id`
					WHERE LM.game_unique_id ='".$game_unique_id."'
				) AS L ON L.player_unique_id =  P.`player_unique_id`
				WHERE 								      
			        P.league_id = ".$this->league_id."
				AND
					P.position IN (".$input_arry['positions_string'].") 
			        ".$search_condition." 
					".$position_condition."
					".$show_drafted_player_condition."
			    ORDER BY  ".$order_by."  ".$order_sequence."
    			LIMIT  	".$offset.", ".$limit;

		$list = $this->db->query($sql);
		return $list->result_array();
	}

	/*
	* Method to check if player already drafted for the game
	*/
	public function check_is_player_drafted($game_unique_id, $player_unique_id)
	{
		$master_ids = $this->db->select('lineup_master_id')
									->from('lineup_master')
									->where('game_unique_id', $game_unique_id)
									->get()
									->result_array();

			if (empty($master_ids))
			{
				return array();
			}

			$lineup_master_ids = array();
			foreach ($master_ids as $val)
			{
				$lineup_master_ids[] = $val['lineup_master_id'];
			}


			$sql = "SELECT L.player_unique_id,L.lineup_id 
						FROM " . $this->db->dbprefix(LINEUP) . "  AS L												
						WHERE  
							L.lineup_master_id IN (" . implode(',', $lineup_master_ids) . ") 
						AND 
							L.player_unique_id = '" . $player_unique_id . "' 						
						";
			
		
			$data = array();
			$data = $this->db->query($sql)->row_array();		

			return ($data == NULL)? array(): $data;	
	}

	public function get_waiver_priority_list()
	{
		$result = $this->db->select('LM.team_name, LM.team_logo, LM.lineup_master_id, GWP.current_priority_place')
							->from(	LINEUP_MASTER." AS LM" )
							->join(GAME_WAIVER_PRIORITY." AS  GWP", "LM.lineup_master_id = GWP.lineup_master_id", 'LEFT')
							->where('LM.game_unique_id', $this->game_unique_id)
							->order_by('GWP.current_priority_place', 'ASC')
							->get()
							->result_array();
		return $result;
	}

        public function is_team_abbr_exist($team_abbr_list, $league_id='', $t_id='', $season_year=''){       
		$this->db->select('T.team_abbr', FALSE)
                                ->from(TEAM . " AS T");
                
                if (!empty($team_abbr_list)) {
			$this->db->where_in('T.team_abbr', $team_abbr_list);
		}
                if (!empty($league_id)) {
			$this->db->where('T.league_id', $league_id);
		}
                if (!empty($season_year)) {
			$this->db->where('T.year', $season_year);
		}
                if (!empty($t_id)) {
                        $this->db->where('T.t_id != ', $t_id);
                }
                
                $sql = $this->db->get();
                //echo $this->db->last_query(); die;
		$abbr_array = $sql->result_array();

		return ($abbr_array) ? $abbr_array : array();
	}

}
/* End of file Teamroster_model.php */
/* Location: ./application/models/Teamroster_model.php */