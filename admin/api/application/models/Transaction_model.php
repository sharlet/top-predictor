<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->admin_id = $this->session->userdata('admin_id');
	}

	
	/**
	 * [get_all_transaction description]
	 * @MethodName get_all_transaction
	 * @Summary This function used for get all transaction history
	 * @param      boolean  [transaction history or Return Only Count]
	 * @return     [type]
	 */
	public function get_all_transaction()
	{
		$sort_field	= 'date_added';
		$sort_order	= 'DESC';
		$limit		= 10;
		$page		= 0;
		$post_data = $this->input->post();

		if(($post_data['items_perpage']))
		{
			$limit = $post_data['items_perpage'];
		}

		if(($post_data['current_page']))
		{
			$page = $post_data['current_page']-1;
		}

		if(($post_data['sort_field']) && in_array($post_data['sort_field'],array('user_name','description','transaction_amount','user_balance_at_transaction','payment_type','created_date','is_processed')))
		{
			$sort_field = $post_data['sort_field'];
		}

		if(($post_data['sort_order']) && in_array($post_data['sort_order'],array('DESC','ASC')))
		{
			$sort_order = $post_data['sort_order'];
		}

		$offset	= $limit * $page;

		$this->db->select("PHT.*,U.user_name,DATE_FORMAT(PHT.created_date,'%d-%b-%Y') as transaction_date")
						->from(PAYMENT_HISTORY_TRANSACTION." AS PHT")
						->join(USER." AS U","U.user_id = PHT.user_id","inner");
		if(isset($post_data['payment_type']) && $post_data['payment_type'] != "")
		{
			$this->db->where("payment_type",$post_data['payment_type']);
		}
		
		if(isset($post_data['fromdate']) && $post_data['fromdate']!="" && isset($post_data['todate']) && $post_data['todate']!="")
		{
			$this->db->where("DATE_FORMAT(created_date,'%Y-%m-%d') >= '".$post_data['fromdate']."' and DATE_FORMAT(created_date,'%Y-%m-%d') <= '".$post_data['todate']."'");
		}

		$tempdb = clone $this->db;
		$query = $this->db->get();

		$total = $query->num_rows();

		$sql = $tempdb->order_by($sort_field, $sort_order)
						->limit($limit,$offset)
						->get();
		$result	= $sql->result_array();

		$result = ($result) ? $result : array();
		return array('result'=>$result,'total'=>$total);
	}
}
/* End of file Transaction_model.php */
/* Location: ./application/models/Transaction_model.php */