<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Withdrawal_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->admin_id = $this->session->userdata('admin_id');
	}

	
	/**
	 * [get_all_site_user_detail description]
	 * @MethodName get_all_site_user_detail
	 * @Summary This function used for get all user list and return filter user list 
	 * @param      boolean  [User List or Return Only Count]
	 * @return     [type]
	 */
	public function get_all_withdrawal_request($count_only=FALSE)
	{
		$sort_field	= 'full_name';
		$sort_order	= 'DESC';
		$limit		= 10;
		$page		= 0;
		$post_data	= $this->input->post();

		if(isset($post_data['items_perpage']))
		{
			$limit = $post_data['items_perpage'];
		}

		if(isset($post_data['current_page']))
		{
			$page = $post_data['current_page']-1;
		}

		if(isset($post_data['sort_field']) && in_array($post_data['sort_field'],array('fullname','email','clabe_number','bank_name','status','withdrawal_type','address','amount','created_date','modified_date','address')))
		{
			$sort_field = $this->input->post('sort_field');
		}

		if(isset($post_data['sort_order']) && in_array($post_data['sort_order'],array('DESC','ASC')))
		{
			$sort_order = $post_data['sort_order'];
		}

		$offset	= $limit * $page;
		$status	= isset($post_data['status']) ? $post_data['status'] : "";
		$type	= isset($post_data['type']) ? $post_data['type'] : "";

		$this->db->select("payment_withdraw_transaction_id,transaction_unique_id,withdraw_type,user_id,full_name,email,bank_name,
							clabe_number,amount,address,status,DATE_FORMAT(created_date,'%d-%b-%Y %h:%i %p') as created_date,DATE_FORMAT(modified_date,'%d-%b-%Y %h:%i %p') as modified_date")
						->from(PAYMENT_WITHDRAW_TRANSACTION);
		if($status != "")
		{
			$this->db->where("status","$status");
		}
		if($type != "")
		{
			$this->db->where("withdraw_type","$type");
		}
		$tempdb = clone $this->db;
		$query = $this->db->get();
		
		$total = $query->num_rows();

		$sql = $tempdb->order_by($sort_field, $sort_order)
						->limit($limit,$offset)
						->get();

		$result	= $sql->result_array();

		$result = ($result) ? $result : array();
		return array('result'=>$result,'total'=>$total);
	}
	
	/**
	 * [update_roster description]
	 * @MethodName update_roster
	 * @Summary This function used update multiple player salary and status
	 * @param      [int]  [league_id]
	 * @param      [array]  [data_arr]
	 * @return     [boolean]
	 */
	public function update_withdrawal_request($data_arr)
	{		
		$this->db->update_batch(PAYMENT_WITHDRAW_TRANSACTION, $data_arr, 'payment_withdraw_transaction_id');

		return $this->db->affected_rows();
	}

	/**
	 * [change_withdrawal_status description]
	 * @MethodName change_withdrawal_status
	 * @Summary This function used to withdrawal status
	 * @param      [varchar]  [withdraw_transaction_id]
	 * @param      [int]  [status]
	 * @return     [boolean]
	 */
	public function change_withdrawal_status($date_array)
	{
		$this->db->where("payment_withdraw_transaction_id",$date_array['payment_withdraw_transaction_id']);
		$this->db->update(PAYMENT_WITHDRAW_TRANSACTION,array('status'=>$date_array['status']));
		
		return $this->db->affected_rows();
	}

/**
 * [get_user_deatil_by_withdraw_request description]
 * @Summary :- get withdrwal request with user data by payment_withdraw_transaction_id
 * @param   [type] $user_ids [array or singal variable]
 * @return  [type]           [withdraw request data with user details]
 */
	public function get_user_deatil_by_withdraw_request($user_ids)
	{
		$result =array();
		$this->db->select('u.*,pwt.full_name as paypa_fullname, pwt.email as paypal_email, pwt.amount  ');
		$this->db->from(PAYMENT_WITHDRAW_TRANSACTION.' AS pwt');
		$this->db->join(USER.' AS u','u.user_id = pwt.user_id','left');
		if(is_array($user_ids))
		{
			$this->db->where_in('pwt.payment_withdraw_transaction_id',$user_ids);
			$result = $this->db->get()->result_array();
		}
		else
		{
			$this->db->where('pwt.payment_withdraw_transaction_id',$user_ids);
			$result = $this->db->get()->row();
		}			
		return $result;
	}

/**
 * [update_payment_history_transaction description]
 * @Summary :- Update the update_payment_history_transaction is_process to processed.  
 * @param   [type] $txt_id [var or array contians payment_withdraw_transaction_id[s] ] 
 * @return  [type]         [description]
 */
	public function update_payment_history_transaction($txt_id)
	{
		if(is_array($txt_id))
		{
			$this->db->where_in('payment_withdraw_transaction_id',$txt_id);
		}
		else
		{
			$this->db->where('payment_withdraw_transaction_id',$txt_id);
		}
		$this->db->update(PAYMENT_HISTORY_TRANSACTION ,array('is_processed'=>'1'));
		return $this->db->affected_rows();
	}

	public function update_user_balance($user_id,$balance)
	{
		$this->db->where('user_id',$user_id);
		$this->db->update(USER ,array('balance'=>$balance));
		return $this->db->affected_rows();
	}
}
/* End of file Roster_model.php */
/* Location: ./application/models/Roster_model.php */