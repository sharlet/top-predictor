<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Withdrawal extends MYREST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Withdrawal_model');
		$this->load->model('User_model');
		//Do your magic here
	}

	public function index()
	{
		$this->load->view('layout/layout', $this->data, FALSE);
	}

	public function get_all_withdrawal_request_post()
	{
		$result = $this->Withdrawal_model->get_all_withdrawal_request();
		
		$this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
	}

/*	public function update_withdrawal_request_post()
	{
		$this->form_validation->set_rules('action', 'Action', 'trim|required');
		// $this->form_validation->set_rules('description', 'Description', 'trim|required');
		if (!$this->form_validation->run()) 
		{
			$this->send_validation_errors();
		}

		$withdraw_ids = $this->input->post('withdraw_transaction_id');

		if(empty($withdraw_ids))
		{
			$this->response(array(config_item('rest_status_field_name')=>FALSE, 'message'=>$this->lang->line('select_withdrawal_transaction')) , rest_controller::HTTP_INTERNAL_SERVER_ERROR);
		}
		
		$status = $this->input->post('action');
		$description = $this->input->post('description');
		foreach($withdraw_ids as $key=>$value)
		{
			$withdrawal_status[] = array(
										"payment_withdraw_transaction_id"	=> $value,
										"status"							=> $status,
										// "description"						=> $description
									);
			
		}
		$result = $this->Withdrawal_model->update_withdrawal_request($withdrawal_status);
		if($result)
		{
			$this->update_payment_history_transaction($withdraw_ids);
			$user_data = $this->Withdrawal_model->get_user_deatil_by_withdraw_request($withdraw_ids);

			foreach ($user_data as $user) 
			{
				$msg			='';
				$withdraw_amt	= $user['amount'];					
				if($status == 1){
					$msg = 'Your request for withdraw of amount $'.$withdraw_amt.' approved by admin.';
				}
				if($status == 2){
					$msg = 'Your request for withdraw of amount $'.$withdraw_amt.' rejected by admin.';	
				}
				$this->send_mail_to_user($user,$withdraw_amt,$msg);
			}
			$this->response(array(config_item('rest_status_field_name')=>TRUE, 'message'=>$this->lang->line('update_status_success')) , rest_controller::HTTP_OK);
		}
		else
		{
			$this->response(array(config_item('rest_status_field_name')=>FALSE, 'message'=>$this->lang->line('no_change')) , rest_controller::HTTP_INTERNAL_SERVER_ERROR);
		}
	}*/

	public function change_withdrawal_status_post()
	{
		$this->form_validation->set_rules('payment_withdraw_transaction_id', 'Withdraw transaction id', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');		
		if (!$this->form_validation->run()) 
		{
			$this->send_validation_errors();
		}

		$status = $this->input->post('status');
		$user_data = $this->Withdrawal_model->get_user_deatil_by_withdraw_request($this->input->post('payment_withdraw_transaction_id'));
		if($user_data->balance < $user_data->amount && $status == 1)
		{
			$this->response(array(config_item('rest_status_field_name')=>TRUE, 'message'=>$this->lang->line('insufficent_amt')) , rest_controller::HTTP_INTERNAL_SERVER_ERROR);	
		}

		$result = $this->Withdrawal_model->change_withdrawal_status($this->input->post());
		if($result)
		{
			$this->Withdrawal_model->update_payment_history_transaction($this->input->post('payment_withdraw_transaction_id'));
				
			$msg			='';
			$withdraw_amt	= $user_data->amount;
			$user_bal		= $user_data->balance;				
			$new_user_bal	= $user_bal-$withdraw_amt;

			$raw_data['payment_withdraw_transaction_id'] = $this->input->post('payment_withdraw_transaction_id');
			
			if($status == 1){
				$this->Withdrawal_model->update_user_balance($user_data->user_id,$new_user_bal);
				$msg = str_replace('%s', $withdraw_amt, $this->lang->line('approved_message'));
				//$msg = 'Your request for withdraw of amount $'.$withdraw_amt.' approved by admin.';
			}
			if($status == 2){
				$msg = str_replace('%s', $withdraw_amt, $this->lang->line('rejected_message'));
				//$msg = 'Your request for withdraw of amount $'.$withdraw_amt.' rejected by admin.';	
			}
			$admin_email_log['notification_type_id']	= 21 ; //WITHDRAW_FUND_REQUEST_STATUS
			$admin_email_log['raw_input_data']			=  json_encode($raw_data) ; 

			$this->User_model->admin_log_background_process($admin_email_log);
			// send mail to user
			// $this->send_mail_to_user($user_data,$withdraw_amt,$msg);

			$this->response(array(config_item('rest_status_field_name')=>TRUE, 'message'=>$this->lang->line('update_status_success')) , rest_controller::HTTP_OK);
		}
		else
		{
			$this->response(array(config_item('rest_status_field_name')=>TRUE, 'message'=>$this->lang->line('no_change')) , rest_controller::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function get_filter_data_post()
	{
		$withdrawal_type = array(
								"Paypal"		=> "1",
								"Live Cheque"	=> "2"
							);
		$withdrawal_status = array(
									"Pending"	=> "0",
									"Approved"	=> "1",
									"Rejected"	=> "2"
								);
		$result = array('withdrawal_type'=>$withdrawal_type,'withdrawal_status'=>$withdrawal_status);
		$this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
	}

	public function send_mail_to_user($rows,$withdraw_amt,$msg)
	{
		$this->load->helper('mail_helper');
		$data['username']		= $rows->user_name;
		$data['email']			= $rows->email;
		$data['withdraw_amt']	= $withdraw_amt;
		$data['message']		= $msg;

		$message				= $this->load->view('emailer/withdraw_req_change',$data,true); 
		$to						= $rows->email;
		$subject				= '['.PROJECT_NAME_FORMATED.'] Payment widthdraw request';
		$message				= $message;    
		send_email($to,$subject,$message);
	}	
}
/* End of file Withdrawal.php */
/* Location: ./application/controllers/Withdrawal.php */