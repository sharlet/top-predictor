<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Teamroster extends MYREST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Teamroster_model');
		//Do your magic here
	}

	public function index()
	{
		$this->load->view('layout/layout', $this->data, FALSE);
	}

	public function index_get() {
		$this->index();
	}

	public function get_all_league_post() {
		$result = $this->Teamroster_model->get_all_league();
		$this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
	}

	public function get_all_teamrosters_post() {
		$result = $this->Teamroster_model->get_all_teamrosters();
		$this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
	}

	public function get_team_data_post() {
		$post_data = $this->input->post();
		$where = array('team_abbr'=>$post_data['team_abbr'],'T.league_id'=>$post_data['league_id']);
		$result = $this->Teamroster_model->get_team_data($where);
		$this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
	}
}

/* End of file Roster.php */
/* Location: /admin/application/controllers/Roster.php */