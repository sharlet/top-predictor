<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Utility extends MY_Controller {

	public $data = array();

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	
	public function index()
	{
		redirect();
	}

	public function template($folder="", $view="")
	{
		$this->load->view("template/$folder/$view", $this->data);
	}

	public function javascript_var($lang='')
	{
		$lang = str_ireplace(".js", "", $lang);

		$language = ($this->session->userdata('language'))?$this->session->userdata('language'):$this->config->item('language');
		$this->lang->load('javascript_lang', $language);

		$lang = (isset($this->lang->language[$lang]))?$this->lang->line($lang):array();
		$common = (isset($this->lang->language['common']))?$this->lang->line('common'):array();
		$data['lang'] = array_merge($lang,$common);

		$this->load->view('utility/javascript_var', $data);
	}

	public function layout()
	{
		$this->load->view('layout/layout', $this->data, FALSE);
	}

	public function _404()
	{
		$this->load->view('layout/layout', $this->data, FALSE);
	}

	public function open_manual_cron_tab($league_id, $season_game_unique_id)
	{
		$data['site_url']				= site_url();

		$data['site_url'] = str_replace('admin/', '', $data['site_url']);
		$data['season_game_unique_id']	= $season_game_unique_id;

		$sql =" SELECT S.*, T1.team_abbr AS home , T2.team_abbr AS away
				FROM ".$this->db->dbprefix(SEASON)." AS S
				LEFT JOIN ".$this->db->dbprefix(TEAM)." AS T1 ON T1.team_id = S.home AND T1.league_id = S.league_id
				LEFT JOIN ".$this->db->dbprefix(TEAM)." AS T2 ON T2.team_id = S.away AND T2.league_id = S.league_id
				WHERE 
					S.season_game_unique_id = $season_game_unique_id
						AND 
					S.league_id = $league_id
						AND 
					S.year = ".APP_CURRENT_YEAR;

		$data['season_data'] = $this->db->query($sql)->row_array();


		$this->load->view('manual_cron_tab', $data);
	}
}

/* End of file Utility.php */
/* Location: ./application/controllers/Utility.php */
