<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Contest extends MYREST_Controller {
    public $contest_status = array( '1'=> 'Upcoming', '2'=> 'Live', '3'=> 'Partial', '4'=> 'Cancelled', '5' => 'Completed' );
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Contest_model');
        //Do your magic here
    }
    public function index()
    {
        $this->load->view('layout/layout', $this->data, FALSE);
    }
    public function new_contest_get()
    {
        $this->index();
    }
    public function contest_detail_get()
    {
        $this->index();
    }
    public function index_get()
    {
        $this->index();
    }
    public function get_all_league_post()
    {
        $result = $this->Contest_model->get_all_league();
        $this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
    }
    public function get_all_contest_data_post()
    {
      $leagues           = $this->Contest_model->get_all_league();
      $prize_pool        = $this->Contest_model->get_all_master_prize_payout();
      $master_data_entry = $this->Contest_model->get_all_master_data_entry();
      $salary_cap        = $this->Contest_model->get_master_salary_cap();
      $result = array(
                      "leagues"           => $leagues ,
                      "prize_pool"        => $prize_pool,
                      "master_data_entry" => $master_data_entry,
                      'salary_cap'        => $salary_cap
                      );
        $this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
    }

    /**
     * [prize_calculate_post description]
     * @Summary :- DEPRECATED FUNCTION
     * @return  [type]
     */
    public function prize_calculate_post()
    {
        $result = array();
        $percent = 0;
        $prize = $this->input->post('prize');
        $total = $this->input->post('total');
        foreach ($prize as $key => $value) {
            $count = round($value*100 / $total);
            $percent += $count;
            $result['prize_pool'] = '$'.number_format($total,2,'.','').' Total '. $percent .'%';
            $result['total_percent'] = $percent;
            $result['amount'] = $total;
            $result['prize'][] = array('place'=>$key,'prize_percentage'=>$count,'amount'=>$value);
        }
        $this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
    }
    public function get_all_roster_position_post()
    {
        if(!$this->input->post('league_id'))return;
        $result['roster'] = $this->Contest_model->get_league_master_game_roster($this->input->post('league_id'));
        if(isset($result['roster']) && count($result['roster']) > 0)
        {
        $result['roster_count'] = count($result['roster']);
        $result['roster_limit'] = $result['roster'][0]['max_roster_size'];//ROSTER_LIMIT;
        }
        else
        {
            $result['roster_count'] = 0;
            $result['roster_limit'] = 0;//ROSTER_LIMIT;
        }
        $this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
    }
    public function get_max_game_played_post()
    {
        if(!$this->input->post('league_id'))return;
        $result['max_game_played'] = $this->Contest_model->get_max_game_played($this->input->post('league_id'));
        if(!isset($result['max_game_played']) || is_null($result['max_game_played']))
        {
            $result['max_game_played'] = 0;
            $message = $this->lang->line('no_master_max_game_played');
        }
        else
        {
            $message = 'Data found';
        }
        $this->response(array(config_item('rest_status_field_name')=>TRUE,'message'=>$message, 'data'=>$result) , rest_controller::HTTP_OK);
    }
    public function get_season_week_post() {

      if(!$this->input->post('league_id'))return;

      $year = APP_CURRENT_YEAR;

      $result['season_week'] = $this->Contest_model->get_season_week($this->input->post('league_id'), $year);
      $this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
    }

    public function get_week_matches_post() {
      if(!$this->input->post('league_id'))return;

      $year      = APP_CURRENT_YEAR;
      $week      = $this->input->post('week');
      $league_id = $this->input->post('league_id');

      $result['week_matches'] = $this->Contest_model->get_week_matches($league_id, $week, $year);

      $this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
    }

    public function new_contest_post() {

      if( !$this->input->post() ) {
        $this->response(array(config_item('rest_status_field_name')=>FALSE, 'message'=>$this->lang->line('invalid_parameter')), rest_controller::HTTP_INTERNAL_SERVER_ERROR);
      }
      $this->form_validation->set_rules('leagueId', $this->lang->line('league'), 'trim|required|is_natural_no_zero');
      $this->form_validation->set_rules('contestName', $this->lang->line('contest_name'), 'trim|required');
      $this->form_validation->set_rules('entryFee', $this->lang->line('entry_fee'), 'trim|required');
      $this->form_validation->set_rules('size', $this->lang->line('size'), 'trim|required|is_natural_no_zero');
      $this->form_validation->set_rules('salaryCap', $this->lang->line('salary_cap'), 'trim|required|is_natural_no_zero');

      if($this->input->post('entryFee') > 0) {
        $this->form_validation->set_rules('prizePayoutId', $this->lang->line('prizePayoutId'), 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('siteRake', $this->lang->line('site_rake'), 'trim|required');
      }

      if (!$this->form_validation->run())
      {
          $this->send_validation_errors();
      }

      $opt_season_ids = $this->input->post('optMatches');

      // Validate min number of matches
      $this->min_matches($opt_season_ids);


      $game_data                           = array();
      $game_data['contest_uid']            = random_string('alnum', 9);
      $game_data['contest_name']           = $this->input->post('contestName');
      $game_data['league_id']              = $this->input->post('leagueId');
      $game_data['owner_id']               = 0;
      $game_data['start_at']               = $this->Contest_model->earliest_match_date( $opt_season_ids );
      $game_data['size']                   = $this->input->post('size');
      $game_data['total_user_joined']      = 0;
      $game_data['entry_fee']              = $this->input->post('entryFee');
      $game_data['is_featured']            = $this->input->post('isFeaturedGame') ? 1 : 0;
      $game_data['site_rake']              = $this->input->post('siteRake');
      $game_data['salary_cap']             = $this->input->post('salaryCap');

      $game_data['rewards']                = $this->compute_rewards( $game_data['entry_fee'] , $game_data['size']  ,$game_data['site_rake'] );
      $game_data['master_prize_payout_id'] = $this->input->post('prizePayoutId');

      $game_data['custom_rewards']         =  htmlentities( $this->input->post('customRewards') );

      $game_data['created_date']           = format_date();
      $game_data['modified_date']          = format_date();


      $this->Contest_model->save_contest($game_data , $opt_season_ids);

      $this->response(array(config_item('rest_status_field_name')=>TRUE, 'message'=>$this->lang->line('contest_create_successfully')), rest_controller::HTTP_OK);
    }

    private function min_matches($match_list) {

      if(count($match_list) < 1) {
        $this->response(array( config_item('rest_status_field_name')=>FALSE, 'message'=>'Please select at least two matches '), rest_controller::HTTP_INTERNAL_SERVER_ERROR);
      }

      $count_match_season_id = $this->Contest_model->validate_season_ids($match_list);

      if(count($match_list) != $count_match_season_id) {
        $this->response(array(config_item('rest_status_field_name')=>FALSE, 'message'=>'Invalid selection of matches '), rest_controller::HTTP_INTERNAL_SERVER_ERROR);
      }

      return TRUE;
    }

    private function compute_rewards( $entry_fee, $size, $site_rake ) {
      $gross_rewards = $entry_fee * $size;
      $site_rake_amount = $gross_rewards * ( $site_rake / 100) ;
      return ($gross_rewards - $site_rake_amount);
    }


    // get all filters on contest page
    public function get_contest_filters_post()
    {
        $this->load->config('vconfig');
        $result['leagues']  = $this->Contest_model->get_all_league();
        $result['contest_status'] = $this->contest_status;

        $this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
    }
    // get all games with selected filter
    public function contest_list_post()
    {
        $result = array();
        $result=  $this->Contest_model->get_contest_list();
        $this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
    }
    public function update_contest_name_post()
    {
        $post = $this->input->post();
        $updateData = array("contest_name" => $post["contest_name"]);
        $updated = $this->Contest_model->update_contest($updateData, $post['contest_id']);
        if(!$updated)
        {
            $this->response(array(config_item('rest_status_field_name') => FALSE, 'message'=> $this->lang->line('invalid_parameter')), rest_controller::HTTP_INTERNAL_SERVER_ERROR);
        }
        $this->response(array(config_item('rest_status_field_name') => TRUE, 'message'=> "Contest name has been updated.") , rest_controller::HTTP_OK);
    }

    public function delete_league_post() {
        $contest_id = $this->input->post('contest_id');
        $contest_details =  $this->db->select()->from(CONTEST)->where('contest_id', $contest_id)->get()->row_array();

        $deleted = FALSE;
        if(!empty($contest_details) && $contest_details['total_user_joined'] == 0 ) {
          $deleted = $this->db->delete(CONTEST, array('contest_id' => $contest_id));
        }

        if(!$deleted)
        {
            $this->response(array(config_item('rest_status_field_name') => FALSE, 'message'=> 'Contest delete action failed.'), rest_controller::HTTP_INTERNAL_SERVER_ERROR);
        }
        $this->response(array(config_item('rest_status_field_name') => TRUE, 'message'=> "Contest has been deleted.") , rest_controller::HTTP_OK);
    }


    public function get_contest_detail_post()
    {
        $contest_uid             = $this->input->post('contest_uid');
        $result["contest_detail"]     = $this->Contest_model->get_contest_detail($contest_uid);

        $final = array();

        $this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
    }
    public function get_user_lineup_detail_post()
    {
        $game_unique_id = $this->input->post('game_unique_id');
        $result      = $this->Contest_model->contest_participants($game_unique_id);

        $this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
    }
    public function get_game_lineup_detail_post()
    {
        $result = $this->Contest_model->get_lineup_users_by_game();
        $this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
    }
    public function get_lineup_detail_post()
    {
        $lineup_master_id   = $this->input->post('lineup_master_id');
        $result             = $this->Contest_model->get_lineup_detail($lineup_master_id);
        $this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
    }
    public function get_all_player_last_salaries_post()
    {
        $player_salary_master_id    = $this->input->post('player_salary_master_id');
        $result                     = $this->Contest_model->get_all_player_last_salaries($player_salary_master_id);
        $this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
    }


    public function edit_contest_post() {
      $contest_id   =  $this->input->post('contest_id');
      $entry_fee    = $this->input->post('entryFee');
      $site_rake    = $this->input->post('siteRake');
      $contest_name = $this->input->post('contestName');
      $prize_pool =  $this->compute_rewards( $entry_fee , $this->input->post('size'), $site_rake );

      $where = array('contest_id' => $contest_id);
      $update = array(
        'contest_name' => $contest_name,
        'size'         => $this->input->post('size'),
        'rewards'      => $prize_pool
      );
      $this->db->update(CONTEST,$update, $where );

      $this->response(array(config_item('rest_status_field_name')=>TRUE, 'message'=>'Contest updated successfully'), rest_controller::HTTP_OK);

    }


}
/* End of file Contest.php */
/* Location: ./application/controllers/Contest.php */
