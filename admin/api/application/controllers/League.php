<?php defined('BASEPATH') OR exit('No direct script access allowed');

class League extends MYREST_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('League_model');
    }
    
	public function index_get() {
        echo ' Your are at wrong address';
        exit;
	}

	public function get_all_league_get() {
		$result = $this->League_model->get_all_leagues();
		$this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
	}	
}
