<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
		//Do your magic here
	}

	public function index()
	{
		$this->load->view('layout/layout', $this->data, FALSE);
	}

	public function index_get()
	{
		$this->index();
	}

	public function user_detail_get($key)
	{
		$this->index();
	}

	public function users_post()
	{
		$result = $this->User_model->get_all_user();
		$this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
	}

	public function get_user_detail_post()
	{
		$user_id = $this->post('user_unique_id');
		$result = $this->User_model->get_user_detail_by_id($user_id);
		$this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
	}

	public function user_transaction_history_post()
	{
		$result = $this->User_model->user_transaction_history();
		$this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
	}

	public function user_game_history_post()
	{
		$result = $this->User_model->game_history_by_user();
		
		$this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
	}

	public function change_user_status_post()
	{
		$this->form_validation->set_rules('user_unique_id', 'User Unique Id', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');

		if (!$this->form_validation->run()) 
		{
			$this->send_validation_errors();
		}
		
		$user_unique_id = $this->input->post('user_unique_id');
		$status = $this->input->post("status");
		$reason = $this->input->post("reason")?$this->input->post("reason"):"";
		$data_arr = array(
						"status"		=> $status,
						"status_reason"	=> $reason
					);
		$result = $this->User_model->update_user_detail($user_unique_id,$data_arr);
		if($result)
		{
			$user_id = $this->input->post('user_id');
			$this->db->delete(ACTIVE_LOGIN, array('user_id'=> $user_id));
			$this->response(array(config_item('rest_status_field_name')=>TRUE, 'message'=>$this->lang->line('update_status_success')) , rest_controller::HTTP_OK);
		}
		else
		{
			$this->response(array(config_item('rest_status_field_name')=>TRUE, 'message'=>$this->lang->line('no_change')) , rest_controller::HTTP_INTERNAL_SERVER_ERROR);
		}	
	}

	public function add_user_balance_post()
	{
		if(empty($this->input->post()))
		{
			$this->response(array(config_item('rest_status_field_name')=>TRUE, 'message'=>$this->lang->line('enter_required_field')) , rest_controller::HTTP_INTERNAL_SERVER_ERROR);
		}
		$this->form_validation->set_rules('user_unique_id', 'User Unique Id', 'trim|required');
		$this->form_validation->set_rules('amount', 'Amount', 'trim|required|max_length[7]|numeric');
		$this->form_validation->set_rules('description','Description' ,'trim|required');
		$this->form_validation->set_rules('transaction_type','Transaction Type' ,'trim|required');

		if (!$this->form_validation->run()) 
		{
			$this->send_validation_errors();
		}
		
		$user_unique_id		= $this->input->post('user_unique_id');
		$amount				= $this->input->post("amount");
		$transaction_type	= $this->input->post("transaction_type");
		$description		= $this->input->post("description");

		$user_detail = $this->User_model->get_user_detail_by_id($user_unique_id);
		
		if($user_detail['balance']<$amount && $transaction_type == 'DEBIT')
		{
			$this->response(array(config_item('rest_status_field_name')=>TRUE, 'message'=>$this->lang->line('insufficient_balance')) , rest_controller::HTTP_INTERNAL_SERVER_ERROR);
		}

		if($transaction_type == 'DEBIT')
		{
			$balance = $user_detail['balance'] - $amount;
			$payment_type = 1;
		}
		else
		{
			$balance = $user_detail['balance'] + $amount;
			$payment_type = 2;
		}

		$payment_history = array(
						"user_id"						=> $user_detail['user_id'],
						"description"					=> $description,
						"payment_type"					=> $payment_type,
						"transaction_amount"			=> $amount,
						"user_balance_at_transaction"	=> $user_detail['balance'],
						"created_date"					=> date("Y-m-d H:i:s"),
						"is_processed"					=> 1
					);
		$this->User_model->make_payment_transaction($payment_history);

		$data_arr = array(
					"balance"		=> $balance
				);

		$result = $this->User_model->update_user_detail($user_unique_id, $data_arr);

		if($result)
		{
			$this->response(array(config_item('rest_status_field_name')=>TRUE,'data'=>$data_arr ,'message'=>$this->lang->line('balance_update_success')) , rest_controller::HTTP_OK);
		}
		else
		{
			$this->response(array(config_item('rest_status_field_name')=>TRUE, 'message'=>$this->lang->line('no_change')) , rest_controller::HTTP_INTERNAL_SERVER_ERROR);
		}
	}

	public function get_filter_data_post()
	{
		$result		= $this->User_model->get_all_country();
		$value	= $this->User_model->get_min_max_user_balance();
		
		$result['country']		= $result;
		$result['min_value']	= round($value['min_value']);
		$result['max_value']	= round($value['max_value']);

		$this->response(array(config_item('rest_status_field_name')=>TRUE,'data'=>$result) , rest_controller::HTTP_OK);
	}
}
/* End of file User.php */
/* Location: ./application/controllers/User.php */