<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Season extends MYREST_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Season_model');
        //Do your magic here
    }
    public function index()
    {
        $this->load->view('layout/layout', $this->data, FALSE);
    }
    public function index_get()
    {
        $this->index();
    }
    public function season_stats_get()
    {
        $this->index();
    }
    public function get_all_season_schedule_post()
    {
        $result = $this->Season_model->get_all_season_schedule();
        $this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
    }
    public function get_all_league_post()
    {
        $result = $this->Season_model->get_all_league();
        $this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
    }
    public function get_all_team_post()
    {
        $league_id  = $this->input->post('league_id');
        $result = $this->Season_model->get_all_team($league_id);

        $records[0] = array(
            'team_id' => '',
            'team_abbreviation' => 'all',
            'team_abbr' => 'all',
            'team_name' => 'All Teams'
        );

        $records = array_merge($records, $result);
        $this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$records) , rest_controller::HTTP_OK);
    }
    public function get_all_week_post()
    {
        $league_id  = $this->input->post('league_id');
        $result = $this->Season_model->get_all_week($league_id);
        $records[0]['week'] = "all";
        $records[0]['season_week'] = "All Week";
        $records = array_merge($records,$result);
        $this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$records) , rest_controller::HTTP_OK);
    }
    public function get_season_stats_post()
    {
        $this->form_validation->set_rules('league_id', 'League Id', 'trim|required');
        if (!$this->form_validation->run())
        {
            $this->send_validation_errors();
        }
        $result = $this->Season_model->get_all_season_stats();
        $this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
    }
    // get year list
    public function get_all_year_post()
    {
        $years = array();
        for( $i= (intVal(APP_CURRENT_YEAR) - 1) ; $i <= intVal(APP_CURRENT_YEAR) + 5; $i++ )
        {
            $years[]=$i;
        }
        $data = array(
                        'years'         => $years,
                        'current_year'  =>APP_CURRENT_YEAR
                    );
        $this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$data) , rest_controller::HTTP_OK);
    }
}
/* End of file Season.php */
/* Location: ./application/controllers/Season.php */