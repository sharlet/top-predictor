<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends MYREST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Setting_model');		
		//Do your magic here
	}

	public function index()
	{
		$this->load->view('layout/layout', $this->data, FALSE);
	}

	public function index_get()
	{
		$this->index();
	}

	public function change_password_post()
	{
		$this->form_validation->set_rules('old_password', 'Old Password', 'trim|required');
		$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|matches[confirm_password]|min_length[5]');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|min_length[5]');

		if (!$this->form_validation->run()) 
		{
			$this->send_validation_errors();
		}
		$data_arr = $this->input->post();

		$result = $this->Setting_model->chnage_password($data_arr);
		if($result)
		{
			$this->response(array(config_item('rest_status_field_name')=>TRUE, 'message'=>$this->lang->line('change_password_success')) , rest_controller::HTTP_OK);
		}
		else
		{
			$this->response(array(config_item('rest_status_field_name')=>FALSE, 'message'=>$this->lang->line('change_password_error')) , rest_controller::HTTP_INTERNAL_SERVER_ERROR);	
		}
	}

	public function change_date_time_post()
	{
		$date = '';
		$this->load->helper( 'file' );
		if ($this->input->post("date"))
		{
			$date = date("Y-m-d H:i:s", strtotime($this->input->post("date")));
		}
		$date_time = '';

		if ( $date )
			$date_time = $date;

		$path = ROOT_PATH.'date_time.php';
		$data = '<?php $date_time = "'.$date_time.'";';

		write_file($path, $data);
		$this->response(array(config_item('rest_status_field_name')=>TRUE, 'message'=>"Update date time successfully") , rest_controller::HTTP_OK);
	}
}

/* End of file Setting.php */
/* Location: ./application/controllers/Setting.php */