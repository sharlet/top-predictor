<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MYREST_Controller {

	public function __construct()
	{
		parent::__construct();
		$_POST = $this->post();
		//Do your magic here
		
	}

	public function index()
	{
		$this->load->view('layout/layout', $this->data, FALSE);
	}

	public function index_get()
	{		
		$this->index();
	}

	public function login_post()
	{
		$this->form_validation->set_rules('email', 'Login', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		
		if (!$this->form_validation->run()) 
		{
			$this->send_validation_errors();
		}

		$this->load->model('Adminauth_model');

		$data = $this->Adminauth_model->admin_login($this->input->post('email'), base64_decode($this->input->post('password')));
		if ($data != NULL)
		{
			$key = $this->generate_active_login_key($data['admin_id']); //Generate Active Login Key
			$data[AUTH_KEY] = $key;

			$this->seesion_initialization($data); // Initialize User Session

			$this->response(array(config_item('rest_status_field_name') => TRUE,'Data'=>array(AUTH_KEY=>$key)) , rest_controller::HTTP_OK);
		}
		else
		{
			$this->response(array(config_item('rest_status_field_name') => FALSE, config_item('rest_message_field_name') => array('email'=>'Incorrect credentials!')) , rest_controller::HTTP_BAD_REQUEST);
		}
	}

	public function logout_get()
	{
		$key = $this->input->get_request_header(AUTH_KEY); 
		$this->delete_active_login_key($key);

		$this->admin_id			= "";
		$this->admin_fullname	= "";
		$this->admin_email		= "";
		$this->role_type		= "";
		$this->session->sess_destroy();
		$this->response(array(config_item('rest_status_field_name') => TRUE, 'Data' => array()) , rest_controller::HTTP_OK);
	}

}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */