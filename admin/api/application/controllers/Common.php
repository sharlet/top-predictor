<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends MYREST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Contest_model');
		//Do your magic here
	}

	public function index()
	{
		redirect('','refresh');
	}

	public function index_get()
	{
		$this->index();
	}

	public function get_language_list_post()
	{
		$this->load->config('vconfig');
		$language = $this->config->item('language_list');
		$this->response(array(config_item('rest_status_field_name')=>TRUE,'data'=>array('language_list'=>$language, 'site_language'=>$this->session->userdata('language'))) , rest_controller::HTTP_OK);
	}

	/**
	 * [get_master_filters_post description]
	 * Summary :-
	 * @return [type] [description]
	 */
	public function get_master_filters_post()
	{
		$leagues = $this->Contest_model->get_all_league();
		$result = array('leagues' => $leagues);
		$this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
	}
	/**
	 * [get_video_list_post description]
	 * Summary :-
	 * @return [type] [description]
	 */
	public function get_video_list_post()
	{
		$result = $this->Contest_model->get_all_videos();
		
		$this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
	}

	/**
	 * [add_video_post description]
	 * Summary :-
	 */
	public function add_video_post()
	{
		$this->form_validation->set_rules('league_id', 'League Id', 'trim|required');
		$this->form_validation->set_rules('video_type', 'Video Type', 'trim|required');
		$this->form_validation->set_rules('video_title_en', 'Video title in english', 'trim|required');
		$this->form_validation->set_rules('video_title_sp', 'Video title in spanish', 'trim|required');
		$this->form_validation->set_rules('video_desc_en', 'Video description in englsih', 'trim|required');
		$this->form_validation->set_rules('video_desc_sp', 'Video description in spanish', 'trim|required');
		$this->form_validation->set_rules('video_url', 'Video link', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');

		if (!$this->form_validation->run()) 
		{
			$this->send_validation_errors();
		}

		$post_params = $this->input->post();

		if($post_params["video_type"] == "LOBBY")
		{
			$videos = $this->Contest_model->get_all_table_data('video_id', VIDEOS, array('video_type' => "LOBBY"));
			if(count($videos) == 3)
			{
				$message = "Your LOBBY video limit has finished.";
				$this->response(array(config_item('rest_status_field_name')=>FALSE, 'message'=> $message), rest_controller::HTTP_INTERNAL_SERVER_ERROR);
			}
		}
		// save video in db
		$video_insert					= array();
		$video_insert["league_id"]		= $post_params["league_id"];
		$video_insert["video_type"]		= $post_params["video_type"];
		$video_insert["video_url"]		= $post_params["video_url"];
		$video_insert["video_title_en"]	= $post_params["video_title_en"];
		$video_insert["video_title_sp"]	= $post_params["video_title_sp"];
		$video_insert["video_desc_en"]	= $post_params["video_desc_en"];
		$video_insert["video_desc_sp"]	= $post_params["video_desc_sp"];
		$video_insert["status"]			= $post_params["status"];
		$video_insert["last_updated"]	= format_date();
		$video_insert["video_order"]    = ($post_params["video_type"] == "LOBBY") ? $post_params["video_order"] : 0;
		
		$this->Contest_model->table_name = VIDEOS;

		if($this->Contest_model->insert($video_insert))
		{
			$message = "Video created successfuly.";
			$this->response(array(config_item('rest_status_field_name')=> TRUE, 'message'=> $message) , rest_controller::HTTP_OK);
		}
		else
		{
			$message = "Video creation failed.";
			$this->response(array(config_item('rest_status_field_name')=>FALSE, 'message'=> $message), rest_controller::HTTP_INTERNAL_SERVER_ERROR);
		}
	}
	/**
	 * [delete_video_post description]
	 * Summary :-
	 * @return [type] [description]
	 */
	public function delete_video_post()
	{
		$this->form_validation->set_rules('league_id', 'League Id', 'trim|required');
		$this->form_validation->set_rules('video_id', 'Video Id', 'trim|required');

		if (!$this->form_validation->run()) 
		{
			$this->send_validation_errors();
		}
		$post_params = $this->input->post();

		$this->Contest_model->table_name = VIDEOS;
		
		if($this->Contest_model->delete(array('video_id' => $post_params["video_id"], 'league_id' => $post_params["league_id"])))
		{
			$message = "Video removed successfuly.";
			$this->response(array(config_item('rest_status_field_name')=> TRUE, 'message'=> $message) , rest_controller::HTTP_OK);	
		}
		else
		{
			$message = "Video remove failed.";
			$this->response(array(config_item('rest_status_field_name')=> FALSE, 'message'=> $message, rest_controller::HTTP_INTERNAL_SERVER_ERROR));
		}
	}

	/**
	 * [update_video_post description]
	 * Summary :-
	 * @return [type] [description]
	 */
	public function update_video_post()
	{
		$this->form_validation->set_rules('league_id', 'League Id', 'trim|required');
		$this->form_validation->set_rules('video_type', 'Video Type', 'trim|required');
		$this->form_validation->set_rules('video_title_en', 'Video title in english', 'trim|required');
		$this->form_validation->set_rules('video_title_sp', 'Video title in spanish', 'trim|required');
		$this->form_validation->set_rules('video_desc_en', 'Video description in englsih', 'trim|required');
		$this->form_validation->set_rules('video_desc_sp', 'Video description in spanish', 'trim|required');
		$this->form_validation->set_rules('video_url', 'Video link', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');

		if (!$this->form_validation->run()) 
		{
			$this->send_validation_errors();
		}

		$post_params = $this->input->post();
		$video_id = $post_params["video_id"];
		unset($post_params["video_id"]);
		
		if($post_params["video_type"] == "LOBBY")
		{
			$videos = $this->Contest_model->get_all_table_data('video_id', VIDEOS, array('video_type' => "LOBBY"));
			$ids = array_column($videos, 'video_id');
			if(count($videos) == 3 && !in_array($video_id, $ids))
			{
				$message = "Your LOBBY video limit has finished.";
				$this->response(array(config_item('rest_status_field_name')=>FALSE, 'message'=> $message), rest_controller::HTTP_INTERNAL_SERVER_ERROR);
			}
		}
		$post_params["last_updated"]	= format_date();

		$this->Contest_model->table_name = VIDEOS;
		if($this->Contest_model->update(array("video_id" => $video_id),$post_params))
		{
			$message = "Video updated successfuly.";
			$this->response(array(config_item('rest_status_field_name')=> TRUE, 'message'=> $message) , rest_controller::HTTP_OK);	
		}
		else
		{
			$message = "Video update failed.";
			$this->response(array(config_item('rest_status_field_name')=> FALSE, 'message'=> $message, rest_controller::HTTP_INTERNAL_SERVER_ERROR));
		}
	}
}

/* End of file common.php */
/* Location: ./application/controllers/common.php */