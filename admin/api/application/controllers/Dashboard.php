<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$this->load->view('layout/layout', $this->data, FALSE);
	}

	public function index_get()
	{
		$this->index();
	}
}

/* End of file Dashboard.php */
/* Location: /admin/application/controllers/Dashboard.php */