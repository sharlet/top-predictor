<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_transaction extends MYREST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Transaction_model');
		//Do your magic here
	}

	public function index()
	{
		$this->load->view('layout/layout', $this->data, FALSE);
	}


	public function get_all_transaction_post()
	{
		$result = $this->Transaction_model->get_all_transaction();
		
		$this->response(array(config_item('rest_status_field_name')=>TRUE, 'data'=>$result) , rest_controller::HTTP_OK);
	}
}

/* End of file Payment_transaction.php */
/* Location: ./application/controllers/Payment_transaction.php */