<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Controller extends MYREST_Controller {

	public $admin_id		= "";
	public $admin_fullname	= "";
	public $admin_email		= "";
	public $role_type		= "";	
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	}
}

/* End of file Admin_controller.php */
/* Location: ./application/core/Admin_controller.php */