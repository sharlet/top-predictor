<?php
class MY_Model extends CI_Model {

	function __construct()
	{
		parent::__construct();	
	}

	function insert($data)
	{
		$this->db->insert($this->table_name, $data);
		if ($this->db->affected_rows() >= 1)
		{
			return $this->db->insert_id();
		}
		
		return FALSE;       
	}

	function update($condition,$data)
	{
		if(is_array($condition)){
			$this->db->where($condition);    
		}else{
			$this->db->where($this->primary_key,$condition);
		}

		$this->db->update($this->table_name, $data);
		if ($this->db->affected_rows())
		{
			return TRUE;
		}
		
		return FALSE;       
	}

	/**
	* Replace into Batch statement
	*
	* Generates a replace into string from the supplied data
	*
	* @access    public
	* @param    string    the table name
	* @param    array    the update data
	* @return    string
	*/
	public function replace_into_batch($table, $data)
	{
		$column_name	= array();
		$update_fields	= array();
		$append			= array();
		foreach($data as $i=>$outer)
		{
			$column_name = array_keys($outer);
			$coloumn_data = array();
			foreach ($outer as $key => $val) 
			{

				if($i == 0)
				{
					// $column_name[]   = "`" . $key . "`";
					$update_fields[] = "`" . $key . "`" .'=VALUES(`'.$key.'`)';
				}

				if (is_numeric($val)) 
				{
					$coloumn_data[] = $val;
				} 
				else 
				{
					$coloumn_data[] = "'" . replace_quotes($val) . "'";
				}
			}
			$append[] = " ( ".implode(', ', $coloumn_data). " ) ";
		}

		/*

			INSERT INTO `vi_player_temp` (`player_unique_id`, `salary`, `weightage`) VALUES ('000bc6c6-c9a8-4631-92d6-1cea5aaa1644',0,'')
			ON DUPLICATE KEY UPDATE `player_unique_id`=VALUES(`player_unique_id`), `salary`=VALUES(`salary`), `weightage`=VALUES(`weightage`)
		*/

		// $sql = "REPLACE INTO " . $this->db->dbprefix($table) . " ( " . implode(", ", $column_name) . " ) VALUES " . implode(', ', $append) ;

		$sql = "INSERT INTO " . $this->db->dbprefix($table) . " ( " . implode(", ", $column_name) . " ) VALUES " . implode(', ', $append) . " ON DUPLICATE KEY UPDATE " .implode(', ', $update_fields);
		// $sql = "INSERT INTO ". $this->db->dbprefix($table) ." (".implode(', ', $keys).") VALUES (".implode(', ', $values).") ON DUPLICATE KEY UPDATE ".implode(', ', $update_fields);

		
		$this->db->query($sql);
	}

	/**
	 * [get_all_team description]
	 * @MethodName get_all_team
	 * @Summary This function is used to get all team by league id in database
	 * @param      [int]  [league_id]
	 * @return     [array]
	 */
	public function get_all_team($league_id, $lang = 'en') {
		$sql = $this->db->select('TD.team_name, T.team_id, GROUP_CONCAT(T.team_abbr) AS team_abbreviation, GROUP_CONCAT(T.team_abbr) AS team_abbr')
						->from(TEAM.' AS T')
						->join(TEAM_DETAILS.' AS TD'," T.team_id = TD.team_id AND TD.lang = '".$lang."'  AND TD.league_id = '".$league_id."' ", 'inner')
						->where('T.league_id', $league_id)
						->group_by('T.team_id')
						->order_by('TD.team_name','ASC')
						->get();
		$result = $sql->result_array();
		return $result;
	}

	/**
	 * [get_all_week description]
	 * @Summary : Used to get list of all week
	 * @return  [array]
	 */
	public function get_all_week($league_id)
	{
		$sql = $this->db->select('week as season_week,week')
						->from(SEASON)
						->where('league_id',$league_id)
						->order_by('week', 'ASC')
						->group_by('week')
						->get();
		$result  = $sql->result_array();
		$result = ($result)?$result:array();
		return $result;
	}

	/**
	 * @Summary: This function for return current week from current date.
	 * @access: public
	 * @param:
	 * @return:
	 */
	public function get_current_week($league_id  , $date)
	{
		$sql = $this->db->select('season_week')
						->from(SEASON_WEEK)
						->where('league_id',$league_id)
						->where('season_week_close_date_time > "' . $date . '"')
						->where('year', format_date($date, 'Y'))
						->limit(1)
						->get();
		$res = $sql->row_array();
		return $res['season_week'];
	}

	/**
	 * [get_all_country description]
	 * @MethodName get_all_country
	 * @Summary This function used to get all master country
	 * @return     [type]
	 */
	public function get_all_country()
	{
		$sql = $this->db->select("*")
						->from(MASTER_COUNTRY)
						->order_by("country_name","ASC")
						->get();
		return $sql->result_array();
	}

	/**
	 * [get_all_state_by_country description]
	 * @MethodName get_all_state_by_country
	 * @Summary This function used to get all master state by country id
	 * @return     [type]
	 */
	public function get_all_state_by_country($country)
	{
		$sql = $this->db->select("*")
						->from(MASTER_STATE)
						->where('master_country_id', $country)
						->order_by("name","ASC")
						->get();
		return $sql->result_array();
	}

	/**
	 * [get_all_sports description]
	 * @Summary : Used to get list of all sports
	 * @return  [type]
	 */
	public function get_all_league()
	{
		$sql = $this->db->select('league_id, league_abbr')
						->from(LEAGUE . " AS L")
						->join(MASTER_SPORTS . " AS MS", "MS.sports_id = L.sports_id", 'INNER')
						->where('L.active', 1)
						->where('MS.active', 1)
						->order_by('L.order', 'ASC')
						->get();
		$temp_array  = $sql->result_array();
		foreach ($temp_array as $rc)
		{
			$result[] = $rc;
		}
		$result = ($result)?$result:array();
		return $result;
	}

	/* Method to enter background process data
	* @param Array $argument_array
	* 
	* Expected array param 
	* $argument_array('game_unique_id'=>'','initiator_user_id'=>'','action_type'=>'','notification_type_id'=>'','status'=>'PENDING','raw_input_data'=>'') 
	* return Boolean
	*/
	public function admin_log_background_process($argument_array = array())
	{
		$argument_array['created_date']  = format_date();
		$argument_array['modified_date'] = format_date();
		$argument_array['status']        = 'PENDING';
		$this->table_name = BACKGROUND_PROCESS;
		if($this->insert($argument_array))
		{
			return TRUE;
		}else{
			return FALSE;
		}
	}

	/*common function used to get single row from any table
	* @param String $select
	* @param String $table
	* @param Array/String $where
	*/
	function get_single_row ($select = '*', $table, $where = "") {
		$this->db->select($select);
		$this->db->from($table);
		if ($where != "") {
			$this->db->where( $where );
		}
		$query = $this->db->get();
	    $this->db->last_query();
		return $query->row_array();
	}

	/*common function used to get all data from any table
	* @param String $select
	* @param String $table
	* @param Array/String $where
	*/
	function get_all_table_data ($select = '*', $table, $where = "") {
		$this->db->select($select);
		$this->db->from($table);
		if ($where != "") {
			$this->db->where($where);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function run_query($sql,$result_type = 'result_array'){
		$rs = $this->db->query($sql);
		if ($rs->num_rows() > 0) {
			if ($result_type == 'row_array') {
				return $rs->row_array();
			} else {
				return $rs->result_array();
			}
		} else {
			return false;
		}
	}
	/* Method to enter background process data
	* @param Array $argument_array
	* 
	* Expected array param 
	* $argument_array('game_unique_id'=>'','initiator_user_id'=>'','action_type'=>'','notification_type_id'=>'','status'=>'PENDING','raw_input_data'=>'') 
	* return Boolean
	*/
	public function log_background_process($argument_array = array())
	{
		$argument_array['created_date']  = format_date();
		$argument_array['modified_date'] = format_date();
		$argument_array['status']        = 'PENDING';
		$this->table_name = BACKGROUND_PROCESS;
		if($this->insert($argument_array))
		{
			return TRUE;
		}else{
			return FALSE;
		}
	}

	    /**
	 * [get_all_position description]
	 * @MethodName get_all_position
	 * @Summary This function is used to get all position by league id in database
	 * @param      [int]  [league_id]
	 * @return     [array]
	 */
	public function get_all_feed_position($league_id)
	{
		$result = array();
		$sql = $this->db->select('position')
						->from(FEED_POSITION)
						->where('league_id', $league_id)
						->where('status',1) // to avoid FLEX position
						->order_by('sequence','ASC')
						->get();
		$temp_array = $sql->result_array();
		foreach ($temp_array as $rc)
		{
			$result[] = $rc;
		}
		return ($result) ? $result : array();
	}
	
	function delete($condition)
	{
		if(is_array($condition)){
			$this->db->where($condition);
		}else{
			$this->db->where($this->primary_key,$condition);
		}
		$this->db->delete($this->table_name);
		if ($this->db->affected_rows() == 1)
		{
			return TRUE;
		}	
		return FALSE;        
	} 

	/**
	 * @Summary: This function for use set season week in season_week table  as per our requirement like Thusday-Wednesday
	 * @access: protected
	 * @param: $league_id,$season_type,$season_year
	 * @return: 
	 */
	protected function set_season_week($league_id,$season_type,$season_year)
	{
		
		$sql = $this->db->select("MAX(scheduled_date) AS max_date ,MIN(scheduled_date) AS min_date")
						->from(SEASON)
						->where("league_id",$league_id)
						->where("type",$season_type)
						->where("year",$season_year) 
						->get();	
		$result = $sql->row_array();		
		// echo "<pre>";print_r($result);die;   
		//echo "<pre>";print_r($result['min_date']);
		//echo "<pre>";print_r($result['max_date']);
		//delete week from table
		if(!empty($result))
		{
			$this->db->delete(SEASON_WEEK,array('league_id' => $league_id,'type'=>$season_type));
			
			$start_day = '';
			$end_day = '';
			//get start & end day in week from DB
			$query = $this->db->get_where(MASTER_LEAGUE_WEEK,array('league_id'=>$league_id));
			if($query->num_rows() > 0)
			{
				$res = $query->row_array();
				$start_day = $res['start_week_day'];
				$end_day = $res['end_week_day'];
			}
			
			$predate = strtotime($result['min_date']);
			$nextdate = strtotime($result['max_date']);
			//echo strtoupper(date('l', $predate));die;
			if(strtoupper(date('l', $predate)) != strtoupper($start_day))
				$previous_date = date('Y-m-d', strtotime('previous '.$start_day, strtotime($result['min_date'])));
			else
				$previous_date = $result['min_date'];
			if(strtoupper(date('l', $nextdate)) != strtoupper($end_day))		
				$next_date = date('Y-m-d', strtotime('next '.$end_day, strtotime($result['max_date'])));
			else
				$next_date = $result['max_date'];

			$startdate = strtotime($previous_date);
			$enddate = strtotime($next_date);
			
			$weeks = array();
			
			while ($startdate < $enddate)
			{  
				$weeks[] = date('W', $startdate); 
				$startdate += strtotime('+1 week', 0);
			}
			
			$season_week 					= 1;
			$season_week_start_date_time 	= date('Y-m-d 00:00:00',strtotime($previous_date));
			$season_week_end_date_time 		= date('Y-m-d 23:59:59', strtotime('next '.$end_day, strtotime($previous_date)));
			$season_week_close_date_time	= date('Y-m-d 23:59:59', strtotime('next '.$end_day, strtotime($previous_date)));
			//echo "<pre>";print_r($weeks);die; 
			for($i=1;$i <= count($weeks);$i++)
			{
				//echo "<pre>";
				//echo $season_week.'-'.$season_week_start_date_time.'-'.$season_week_end_date_time.'-'.$season_week_close_date_time;
				$data = array(
								'type'          			    => trim($season_type),
								'season_week'          			=> trim($season_week),
								'season_week_start_date_time' 	=> trim($season_week_start_date_time),
								'season_week_end_date_time'     => trim($season_week_end_date_time),
								'season_week_close_date_time'   => trim($season_week_close_date_time),
								'league_id'         			=> $league_id,
								'year' 							=> $season_year
							);	
				
				//Insert data into database
				$this->db->insert(SEASON_WEEK, $data);
				
				$season_week 					= $season_week+1;
				$season_week_start_date_time	= date('Y-m-d 00:00:00', strtotime('next '.$start_day, 
																		 strtotime($season_week_start_date_time)));
				$season_week_end_date_time 		= date('Y-m-d 23:59:59', strtotime('next '.$end_day, 
																		 strtotime($season_week_end_date_time)));
				$season_week_close_date_time	= date('Y-m-d 23:59:59', strtotime('next '.$end_day, 
																		 strtotime($season_week_close_date_time)));
			}
		}	
	}

	/**
	 * @Summary: This function for use update season week in season table from season_week table  
	 * @access: protected
	 * @param: $league_id,$season_type,$season_year
	 * @return: 
	 */
	protected function update_season_week($league_id,$season_type,$season_year)
	{		
		$sql = $this->db->select("DATE_FORMAT(season_week_start_date_time,'%Y-%m-%d') AS start_date,DATE_FORMAT(season_week_end_date_time,'%Y-%m-%d') AS end_date",FALSE)
						->from(SEASON_WEEK)
						->where("league_id",$league_id)
						->where("type","$season_type")
						->order_by("season_week","ASC")
						->get();

		$result = $sql->result_array();
		//echo "<pre>";print_r($result);	
		if(!empty($result))
		{
			foreach($result as $key=>$value)
			{
				$sql = $this->db->select("season_game_unique_id")
								->from(SEASON)
								->where("league_id",$league_id)
								->where("scheduled_date BETWEEN '".$value['start_date']."' AND '".$value['end_date']."'")
								->where("type","$season_type")
								->where("year","$season_year")
								->get();

				$result2 = $sql->result_array();
				//echo "<pre>";print_r($result2);	
				if(!empty($result2))
				{
					$season_game_unique_id = array();
					for($i=0;$i<count($result2);$i++)
					{
						$season_game_unique_id[] =  $result2[$i]['season_game_unique_id'];
					}
					$i = $key+1;

					$this->db->where("league_id",$league_id)
							->where("type","$season_type")
							->where("year","$season_year")
							->where("season_game_unique_id in (".implode( ',' , array_map( function( $n ){ return '\''.$n.'\''; } , $season_game_unique_id ) ).")")
							->update(SEASON,array("week"=>$i));
					$rs = $this->db->affected_rows();
				}
			}
		}
	}

}
//End of file