<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	
	public $layout         = "<br><br>Please don't forget to set a layout for this page. <br>Layout file must be kept in views/layout folder ";
	public $data           = array();

	public $success_message     = "";
	public $error_message       = "";
	public $warning_message     = "";
	public $information_message = "";

	function __construct()
	{
		parent::__construct();
		$this->layout = 'layout/layout';

		if(!$this->session->userdata('language'))
		{
			$this->session->set_userdata('language', $this->config->item('language'));
		}
	}

	function get_messages()
	{
		$warning_message     = $this->session->flashdata('warning_message');
		$information_message = $this->session->flashdata('information_message');
		$success_message     = $this->session->flashdata('success_message');
		$error_message       = $this->session->flashdata('error_message');

		$this->data['open_login'] = $this->session->flashdata( 'login' );

		if ( $warning_message )		$this->data['warning_message']		= $warning_message;
		if ( $information_message ) $this->data['information_message']	= $information_message;
		if ( $success_message )		$this->data['success_message']		= $success_message;
		if ( $error_message )		$this->data['error_message']		= $error_message;
	}
}

/* End of file MY_Controller.php */
/* Location: application/core/MY_Controller.php */