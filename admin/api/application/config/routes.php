<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']	= 'login';
$route['404_override']			= 'utility/_404';
$route['translate_uri_dashes']	= FALSE;

$route['logs/(:any)'] = ['logs/$1'];

$route['template/(:any)/(:any)'] = 'utility/template/$1/$2';
$route['update_site_language']   = 'common/update_site_language';
$route['get_language_list']      = 'common/get_language_list';

/*Common layout routing*/
$route['auth']							= 'utility/layout';
$route['login']							= 'utility/layout';
$route['dashboard']						= 'utility/layout';
$route['roster']						= 'utility/layout';
$route['import_player_roster/(:any)'] 	= 'utility/layout';
$route['contest']						= 'utility/layout';
$route['new_contest']					= 'utility/layout';
$route['contest_detail/(:any)']			= 'utility/layout';
$route['user']							= 'utility/layout';
$route['user_detail/(:any)']			= 'utility/layout';
$route['teamroster']					= 'utility/layout';
$route['withdrawal_list']				= 'utility/layout';
$route['transaction_list']	 			= 'utility/layout';
$route['sales_person']					= 'utility/layout';
$route['new_sales_person']				= 'utility/layout';
$route['sales_person_detail/(:any)']	= 'utility/layout';
$route['promo_code']					= 'utility/layout';
$route['new_promo_code']				= 'utility/layout';
$route['promo_code_detail/(:any)']		= 'utility/layout';
$route['edit_sales_person/(:any)']		= 'utility/layout';
$route['season']						= 'utility/layout';
$route['seasonstats/(:any)/(:any)']	 	= 'utility/layout';
$route['change_password']				= 'utility/layout';
$route['lineup-detail/(:any)']			= 'utility/layout';

$route['news_list']						= 'utility/layout';
$route['edit_news/(:any)']				= 'utility/layout';
$route['news_details/(:any)']			= 'utility/layout';

/*Common layout routing*/

/*Auth Section*/
$route['login']		= 'auth/login';
$route['logout']	= 'auth/logout';

/*User Section*/
$route['users']				 = 'user/users';
$route['get_user_detail']	 = 'user/get_user_detail';