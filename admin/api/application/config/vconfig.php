<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*Season Stats Columns*/
/*$config['season_stats_common_field'] = array(
		"player_name"		=> "Player Name",
		"position"			=> "Position",
		"team_name"			=> "Team Name",
		"DATE_FORMAT(scheduled_date,'%d-%b-%Y %H:%i') as scheduled_dates"	=> "Schedule",
		"home"				=> "Home",
		"away"				=> "Away"
	);*/
$config['season_stats_common_field'] = array(
		"player_name",
		"position",
		"team_name",
		"scheduled_date",
		"home",
		"away"
	);

$config['season_stats_field'] =  array(
		"1" => array(
			"team_points",
			"at_bats",
			"runs",
		),
		"2" => array(
			"team_points"
		),
		"3" => array(
			"team_points"
		),
		"4" => array(
			"team_points"
		),
		"5" => array(
			"team_goals"
		),
		"6" => array(
			"team_goals"
		),
		"7" => array(
			"team_goals"
		),
		"8" => array(
			"team_goals"
		),
		"9" => array(
			"team_goals"
		),
		"10" => array(
			"team_goals"
		),
		"11" => array(
			"team_goals"
		),
		"12" => array(
			"team_goals"
		),
		"13" => array(
			"team_goals"
		),
		"14" => array(
			"team_goals"
		),
		"16" => array(
			"team_goals"
		)
	);

$config['language_list'] = array(
		'english' => 'English',
		'spanish' => 'Spanish'
	);

$config['game_status_list'] = array('PRE-DRAFT', 'DRAFTING','LIVE','COMPLETED','CANCELLED','DELETED','OFF-SEASON');
$config['game_type'] = array(''=>'ALL', 'SNAKE_DRAFT'=>'Snake', 'MANUAL_DRAFT'=>'Offline');
/* End of file vconfig.php */
/* Location: ./application/config/vconfig.php/ */