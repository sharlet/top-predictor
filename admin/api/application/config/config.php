<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('../../all_config/config.php');
switch (ENVIRONMENT)
{
    case 'production':
        $config['base_url']     = '';
    break;
    case 'testing':
        $config['base_url'] = '';
    break;
    default:
        $config['base_url'] = 'http://localhost/fantasy_dfs/admin/api/';
    break;
}
// Admin language will be in English ALWAYS :-  07-Dec-16 2:45:04 PM
$config['language'] = 'english';