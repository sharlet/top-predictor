<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation
{
	/**
	 * return array of error.
	 * 
	 *
	 * @access public
	 *
	 * @param 
	 * @param 
	 *
	 * @return array
	 */
	public function __construct()
	{
		parent::__construct();
	}

	public function error_array()
    {
		return $this->_error_array;
	}

	public function valid_salary()
	{
		$salaries = $this->CI->input->post('salary');
		foreach($salaries as $sa)
		{
			if(is_numeric($sa))
			{
				continue;
			}
			else
			{
				$this->set_message('valid_salary',$this->CI->lang->line('valid_salary'));
				return FALSE;
			}
		}
		return $salaries;
	}
}