<?php defined('BASEPATH') OR exit('No direct script access allowed');

//positions for rosters in admin For LFA
$lang['positions'][1] = array(
							// array('position'=> "All Positions", "position_value"=> "all"),
							array('position'=> "QB", "position_value"=> "QB"),
							array('position'=> "RB", "position_value"=> "RB"),
							array('position'=> "WR", "position_value"=> "WR"),
							array('position'=> "TE", "position_value"=> "TE"),
							array('position'=> "K", "position_value"=> "K"),
							array('position'=> "DEF", "position_value"=> "DEF"),
						);
//positions for rosters in admin For NFL
$lang['positions'][2] = array(
							// array('position'=> "All Positions", "position_value"=> "all"),
							array('position'=> "QB", "position_value"=> "QB"),
							array('position'=> "RB", "position_value"=> "RB"),
							array('position'=> "WR", "position_value"=> "WR"),
							array('position'=> "TE", "position_value"=> "TE"),
							array('position'=> "K", "position_value"=> "K"),
							array('position'=> "DEF", "position_value"=> "DEF"),
						);
/*Roster Section*/
$lang["update_status_success"]		= "Status updated successfully";
$lang["no_change"]					= "No change";
$lang["insufficent_amt"]			= "Insufficient user balance.";
$lang["player_release_success"]		= "Player release successfully.";
$lang["salary_length_less"]			= "Salary should be less then 7 digit.";
$lang["valid_salary"]				= "Enter only numeric salary";
$lang["roster_successfull_import"]	= "Player roster successfully imported";

/*Withdrawal Section*/
$lang["enter_required_field"]			= "Entered required field.";
$lang["insufficient_balance"]			= "Insufficient balance";
$lang["balance_update_success"]			= "User balance updated successfully";
$lang["select_withdrawal_transaction"]	= "Select withdrawal transaction";

/*New Contest*/
$lang["no_contest_list"]				= "Not found conetst list";
$lang["invalid_parameter"]				= "Invalid Parameter";
$lang["contest_create_successfully"]	= "Contest has been successfully created.";
$lang["player_relase_error"]			= "Please Relase First.";
$lang["no_master_max_game_played"]				= "Not max game played found";

$lang['league']='League';
$lang['contest_name']='Contest Name';
$lang['entry_fee'] = 'Entry Fee';
$lang['size'] = 'Size';
$lang['site_rake'] = 'Site Rake';
$lang['prize_pool'] = 'Prize Pool';
$lang['salary_cap'] = 'Salary Cap';


$lang['prize_payout_error'] = ' Some thing went wrong! Unable to process prize payout';
$lang['roster_limit_error'] = 'Roster positions should not greater then '.ROSTER_LIMIT;
$lang['scoring_rules_req'] = " Please enable scoring rules ";


/*User Section*/
$lang["manage_site_users"]	= "Manage Site Users";

/*Setting Section*/
$lang["change_password_success"]	= "Password has been changed successfully";
$lang["change_password_error"]		= "Not Match Old Password / Old Password and New Password Same";

/*Promo code Section*/
$lang["create_sales_person"]			= "Sales has been created successfully";
$lang["create_promo_code"]				= "Promo code has been created successfully";
$lang["sales_person_updated"]			= "Sales persons has been updated successfully";
$lang["alredy_exists"]					= "Email address is alredy exists";
$lang["sales_person_earning_update"]	= "Sales person earning updated successfully";

$lang['ad_created_success'] 		= "Advertisement successfully created.";
$lang['ad_try_again'] 				= "Please try again.";
$lang['ad_image_invalid_size'] 		= 'Please upload image of size should be {max_width}x{max_height} .';
$lang['ad_image_removed'] 			= "image removed successfully.";
$lang['ad_status_updated']			= "Advertisement Status Updated Successfully.";

$lang['league_id']		= "League id";
$lang['season_year']	= "Season year";
$lang['min_draft_time']	= "Min draft time";
$lang['min_draft_date']	= "Min draft date";
$lang['max_draft_time']	= "Max draft time";
$lang['max_draft_date']	= "Max draft date";



/************************************* Grinta language start *****************************/


/* League News(Common) */
$lang['league_news']['league_type']				= "League";
$lang['league_news']['page_type']				= "Page type";
$lang['league_news']['player_unique_id']		= "player";
$lang['league_news']['news_desc']				= "English Description";
$lang['league_news']['news_image']				= "Image";
$lang['league_news']['news_title']				= "English Title";
$lang['league_news']['news_short_desc']			= "English Title";
$lang['league_news']['all_required']			= "All field is required.";
$lang['league_news']['less_than_2mb']			= "Please upload image of size less than 1000X1024 and less than 2MB.";
$lang['league_news']['less_than_4mb']			= "Please upload image of size greater than 80x80 and less than 4MB.";
$lang['league_news']['invalid_image_size']		= "Image size is greater than permitted size.";
$lang['league_news']['invalid_image_dimension']	= "Please upload image of size should be [img_width]X[img_height]";
$lang['league_news']['league_news_success']		= "News created.";
$lang['league_news']['spanish_news_desc']		= "Spanish Description";
$lang['league_news']['spanish_news_title']		= "Spanish Title";
$lang['league_news']['spanish_news_short_desc']	= "Spanish Title";
$lang['league_news']['please_select_league']	= "Please select league.";
$lang['league_news']['no_change']				= "No change.";
$lang['league_news']['sequence_updated']		= "League news sequence updated successfully.";
$lang['league_news']['league_news_updated_success']		= "News updated successfully.";


/* End of file general.php */
/* Location: ./application/language/english/general.php */
