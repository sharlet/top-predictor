<?php defined("BASEPATH") OR exit("No direct script access allowed");

$select_action		= "Please Select Action";
$filters			= "Filters";
$select_league		= "Select League";
$username			= "Username";
$email				= "Email";
$clear_filters		= "Reset Filters";
$action				= "Action";
$status				= "Status";
$description		= "Description";
$close				= "Close";
$first_name			= "First Name";
$last_name			= "Last Name";
$fees				= "Matrícula";
$league				= "Fees";
$apply_action		= "Apply action";
$teamname			= "Team Name";
$select_actions		= "Select Action";
$date_of_birth		= "Date of Birth";
$address			= "Address";
$country			= "Country";
$state				= "State";
$city				= "City";
$promo_code			= "Promo code";
$entry_fee			= "Entry fee";
$amount_received	= "Amount Received";
/*Home Page variable start*/
$lang["login"]["SIGNUP_EMAIL_ERROR"] = "Please enter email.";
$lang["login"]["EMAIL_REQUIRED"]	 = "Please enter email.";
$lang["login"]["EMAIL_NOT_VALID"]	 = "Please enter valid email.";
$lang["login"]["PASS_REQUIRED"]		 = "Please enter password.";
/*Home Page variable end*/

$lang["dashboard"]["dashboard_label"] = "Dashboard";

/*User Page variable start*/
$lang["user"]["BALANCE_GREATER_FROM_TO"]		= "From balance greater then to balance";
$lang["user"]["manage_site_users"]				= "Manage Site Users";
$lang["user"]["filters"]						= "Filters";
$lang["user"]["country"]						= $country;
$lang["user"]["balance"]						= "Balance";
$lang["user"]["all_country"]					= "All Country";
$lang["user"]["clear_filters"]					= $clear_filters;
$lang["user"]["users"]							= "Users";
$lang["user"]["username"]						= $username;
$lang["user"]["name"]							= "Name";
$lang["user"]["email"]							= $email;
$lang["user"]["member_since"]					= "Member Since";
$lang["user"]["status"]							= $status;
$lang["user"]["action"]							= $action;
$lang["user"]["user_detail"]					= "User Detail";
$lang["user"]["manage_balance"]					= "Manage Balance";
$lang["user"]["ban_user"]						= "Block User";
$lang["user"]["no_record_found"]				= "No Record Found";
$lang["user"]["manage_user_balance"]			= "Manage User Balance";
$lang["user"]["user_name"]						= "User Name";
$lang["user"]["current_balance"]				= "Current Balance";
$lang["user"]["transaction_type"]				= "Transaction Type";
$lang["user"]["credit"]							= "Credit";
$lang["user"]["debit"]							= "Debit";
$lang["user"]["amount"]							= "Amount";
$lang["user"]["description"]					= $description;
$lang["user"]["close"]							= $close;
$lang["user"]["add_balance"]					= "Update Balance";
$lang["user"]["manage_ban_user"]				= "Block User";
$lang["user"]["reason"]							= "Reason";
$lang["user"]["user_ban"]						= "Block";
$lang["user"]["user_deleted_title"]				= "User Deleted";
$lang["user"]["user_banned_title"]				= "User Banned";
$lang["user"]["user_email_not_verified_title"]	= "User Email Not Verified";
$lang["user"]["user_active_title"]				= "User Active";
$lang["user"]["user_activation_pending_title"]	= "User Activation Pending";
$lang["user"]["email_name_username"]			= "Email, Name or Username";
$lang["user"]["user_informations"]				= "User Informations";
$lang["user"]["first_name"]						= $first_name;
$lang["user"]["last_name"]						= $last_name;
$lang["user"]["balance"]						= "Balance";
$lang["user"]["date_of_birth"]					= $date_of_birth;
$lang["user"]["phone"]							= "Phone";
$lang["user"]["city"]							= $city;
$lang["user"]["state"]							= $state;
$lang["user"]["language"]						= "Language";
$lang["user"]["users_transaction"]				= "Users Transaction";
$lang["user"]["withdrawal_status"]				= "Withdrawal Status";
$lang["user"]["transaction_date"]				= "Transaction Date";
$lang["user"]["no_user_transaction"]			= "No User Transaction History";
$lang["user"]["user_games"]						= "User Games";
$lang["user"]["no_user_games"]					= "No User Games";
$lang["user"]["game"]							= "Game";
$lang["user"]["entrants"]						= "Entrants";
$lang["user"]["fees"]							= $fees;
$lang["user"]["prize_pool"]						= "Prize Pool";
$lang["user"]["start_time"]						= "Start Time";
/*User Page variable end*/

/*Roster Page variable start*/
$lang["roster"]["SELECT_ACTION"]		= $select_action;
$lang["roster"]["SELECT_PLAYER"]		= "Please Select Player.";
$lang["roster"]["manage_roster_label"]	= "Manage roster";
$lang["roster"]["filters"]				= "Filters";
$lang["roster"]["league"]				= "League";
$lang["roster"]["team"]					= "Team";
$lang["roster"]["position"]				= "Position";
$lang["roster"]["select_league"]		= $select_league;
$lang["roster"]["all_team"]				= "All Teams";
$lang["roster"]["all_position"]			= "All Positions";
$lang["roster"]["player_name"]			= "Player Name";
$lang["roster"]["clear_filters"]		= $clear_filters;
$lang["roster"]["roster_management"]	= "Roster Management";
$lang["roster"]["export_roster"]		= "Export Roster";
$lang["roster"]["import_roster"]		= "Import Roster";
$lang["roster"]["full_name"]			= "Full Name";
$lang["roster"]["salary"]				= "Salary";
$lang["roster"]["status"]				= $status;
$lang["roster"]["action"]				= $action;
$lang["roster"]["no_player"]			= "No Player";
$lang["roster"]["active"]				= "Active";
$lang["roster"]["inactive"]				= "Inactive";
$lang["roster"]["update"]				= "Update";
$lang["roster"]["release_player"]		= "Release Player";
$lang["roster"]["csv"]					= "CSV";
$lang["roster"]["excel"]				= "Excel";
$lang["roster"]["close"]				= $close;
$lang["roster"]["export"]				= "Export";
$lang["roster"]["apply_action"]			= $apply_action;
$lang["roster"]["select_action"]		= $select_actions;
$lang["roster"]["injury"]				= "Injury";
$lang["roster"]["language"]				= "Language";
/*Roster Page variable end*/

/*Contest Page variable start*/
$lang["contest"]["INVALID_COMBINATION"]			= "Invalid combination of minimum size and prize";
$lang["contest"]["PRIZE_DETAIL_TEXT"]			= "PRIZE POOL WILL VARY ACCORDING TO USER JOINED THIS GAME.";
$lang["contest"]["create_new_contest"]			= "Create New Contest";
$lang["contest"]["contest_name"]				= "Contest Name";
$lang["contest"]["featured"]					= "Featured(Auto cancel disabled)";
$lang["contest"]["uncapped"]					= "Uncapped";
$lang["contest"]["league"]						= "League";
$lang["contest"]["league_type"]						= "League Type";
$lang["contest"]["select_league"]				= $select_league;
$lang["contest"]["duration"]					= "Duration";
$lang["contest"]["select_duration"]				= "Select Duration";
$lang["contest"]["date"]						= "Date";
$lang["contest"]["week"]						= "Week";
$lang["contest"]["select_week"]					= "Select Week";
$lang["contest"]["games"]						= "Games";
$lang["contest"]["drafting_style"]				= "Drafting Style";
$lang["contest"]["select_drafting"]				= "Select Drafting";
$lang["contest"]["salary_cap"]					= "Salary Cap";
$lang["contest"]["select_salary_cap"]			= "Select Salary Cap";
$lang["contest"]["size"]						= "Size";
$lang["contest"]["size_min"]					= "Size(Minimum)";
$lang["contest"]["entry_fee"]					= $entry_fee;
$lang["contest"]["site_rake"]					= "Site Rake (%)";
$lang["contest"]["multiple_lineup"]				= "Multiple Lineup";
$lang["contest"]["contest_type"]				= "Contest Type :";
$lang["contest"]["select_contest_type"]			= "Select Contest Type";
$lang["contest"]["prize_pool"]					= "Prize Pool :";
$lang["contest"]["auto"]						= "Auto(Predefine)";
$lang["contest"]["custom"]						= "Custom(Guaranteed)";
$lang["contest"]["prize_detail"]				= "Prize Detail :";
$lang["contest"]["save_contest"]				= "Save Contest";
$lang["contest"]["update_contest"]				= "Update Contest";
$lang["contest"]["featured_help"]				= "Check to make this contest a feature game. Featured contest can not be canceled";
$lang["contest"]["size_min_help"]				= "Define minmum size of game";
$lang["contest"]["site_rake_help"]				= "Site Rake";
$lang["contest"]["is_multiple_lineup_help"]		= "Allow participants to join this contest multiple time as define";
$lang["contest"]["auto_prize_help"]				= "Prize pool calculated according the contest size. i.e. Size=10, Entry Fee=$5, Site Rake=10% than Prize Pool=$45";
$lang["contest"]["prize_selection_help"]		= "Prize Pool defined manual";
$lang["contest"]["custom_prize_help"]			= "Total prize pool for this contest";
$lang["contest"]["is_uncapped_help"]			= "Uncapped contest have infinite participaints";
$lang["contest"]["contest_list"]				= "Contest List";
$lang["contest"]["contest_status"]				= "Contest Status";
$lang["contest"]["current_contest"]				= "Current Contest";
$lang["contest"]["completed_contest"]			= "Completed Contest";
$lang["contest"]["cancelled_contest"]			= "Cancelled Contest";
$lang["contest"]["contest"]						= "Contest";
$lang["contest"]["no_contest"]					= "No Contest have been made yet";
$lang["contest"]["entrants_participants"]		= "Entrants/Participants";
$lang["contest"]["fees"]						= $fees;
$lang["contest"]["prize_pool"]					= "Prize Pool";
$lang["contest"]["start_time"]					= "Start Time";
$lang["contest"]["feature_contest"]				= "Feature Contest";
$lang["contest"]["action"]						= $action;
$lang["contest"]["no"]							= "No";
$lang["contest"]["yes"]							= "Yes";
$lang["contest"]["contest_detail"]				= "Contest Detail";
$lang["contest"]["select_contest_status"]		= "Select Contest Status";
$lang["contest"]["contest_detail_informations"]	= "Contest Detail Informations";
$lang["contest"]["scheduled_date"]				= "Scheduled Date";
$lang["contest"]["prizing"]						= "Prizing";
$lang["contest"]["duration"]					= "Duration";
$lang["contest"]["created_date"]				= "Created Date";
$lang["contest"]["status"]						= $status;
$lang["contest"]["created_by"]					= "Created By";
$lang["contest"]["admin"]						= "ADMIN";
$lang["contest"]["player_roster"]				= "Player Roster";
$lang["contest"]["full_name"]					= "Full Name";
$lang["contest"]["salary"]						= "Salary";
$lang["contest"]["team"]						= "Team";
$lang["contest"]["position"]					= "Position";
$lang["contest"]["no_player"]					= "No Player";
$lang["contest"]["player_name"]					= "Player Name";
$lang["contest"]["team_name"]					= $teamname;
$lang["contest"]["player_salary"]				= "Player Salary";
$lang["contest"]["score"]						= "Score";
$lang["contest"]["participant_lineup_detail"]	= "Participant Lineup Detail";
$lang["contest"]["user_name"]					= "User Name";
$lang["contest"]["participants"]				= "Participants";
$lang["contest"]["promo_code"]					= $promo_code;
$lang["contest"]["user_name"]					= "User Name";
$lang["contest"]["entry_fee"]					= $entry_fee;

$lang["contest"]["promocode_used_date"]			= "Promocode Used Date";

$lang["contest"]["LEAGUE_REQUIRED"]				= "Please select league.";
$lang["contest"]["LEAGUE_NAME_REQUIRED"]		= "Please enter league name.";
$lang["contest"]["SPECIAL_CHAR_ERROR"]			= "Please enter valid characters.";
$lang["contest"]["LEAGUE_TYPE_REQUIRED"]		= "Please select league type.";
$lang["contest"]["TEAM_SIZE_REQUIRED"]			= "please enter team size.";
$lang["contest"]["INVITE_PERMISSION_REQUIRED"]	= "please select invite permission.";
$lang["contest"]["DRAFT_REQUIRED"]				= "please select game draft.";
$lang["contest"]["DRAFT_DATE_REQUIRED"]			= "please select draft date.";
$lang["contest"]["DRAFT_TIME_REQUIRED"]			= "please select draft time.";
$lang["contest"]["ENTRY_FEE_REQUIRED"]			= "please enter entry fee.";
$lang["contest"]["KEEPER_DATE_REQUIRED"]		= "please select keeper date.";
$lang["contest"]["TRADE_DATE_REQUIRED"]			= "please select trade end date.";



/*Contest Page variable end*/

/*Team Roster Page variable start*/
$lang["teamroster"]["manage_team"]		= "Manage Team";
$lang["teamroster"]["filters"]			= "Filters";
$lang["teamroster"]["league"]			= "League";
$lang["teamroster"]["clear_filters"]	= $clear_filters;
$lang["teamroster"]["team_list"]		= "Team List";
$lang["teamroster"]["team_id"]			= "Team ID";
$lang["teamroster"]["team_name"]		= $teamname;
$lang["teamroster"]["team_abbr"]		= "Team Abbr";
$lang["teamroster"]["no_team"]			= "No Team";

$lang["teamroster"]["LEAGUE_REQUIRED"]			= "Please select league.";
$lang["teamroster"]["TEAM_NAME_REQUIRED"]		= "Please enter team name.";
$lang["teamroster"]["TEAM_ABBR_REQUIRED"]		= "Please enter team abbreviation name.";
$lang["teamroster"]["TEAM_ABBR_LABLE_REQUIRED"]	= "Please enter team abbreviation lable name.";
$lang["teamroster"]["SPECIAL_CHAR_ERROR"]		= "Please enter valid characters.";


/*Team Roster Page variable end*/

/*Common Page variable start*/
$lang["common"]["dashboard"]			= "Dashboard";
$lang["common"]["roster_management"]	= "Roster Management";
$lang["common"]["team_list"]			= "Team List";
$lang["common"]["manage_contest"]		= "Manage Contest";
$lang["common"]["view_contest"]			= "View Contest";
$lang["common"]["new_contest"]			= "New Contest";
$lang["common"]["manage_finance"]		= "Manage Finance";
$lang["common"]["withdrawal_list"]		= "Withdrawal List";
$lang["common"]["transaction_list"]		= "Transaction List";
$lang["common"]["manage_user"]			= "Manage User";
$lang["common"]["season_schedule"]		= "Season Schedule";
$lang["common"]["manage_promo_code"]	= "Manage Promo Code";
$lang["common"]["sales_person"]			= "Sales Person";
$lang["common"]["promo_code"]			= $promo_code;
$lang["common"]["setting"]				= "Setting";
$lang["common"]["logout"]				= "Logout";
$lang["common"]["advertisement"]		= "Manage Advertisement";
$lang["common"]["view_advertisement"]	= "View Advertisement";
$lang["common"]["new_advertisement"]	= "New Advertisement";
/*Common Page variable end*/

/*Withdrawal Page variable start*/
$lang["withdrawal"]["SELECT_ACTION"]				= $select_action;
$lang["withdrawal"]["SELECT_WITHDRAWAL"]			= "Please Select Withdrawal Request.";
$lang["withdrawal"]["withdrawal"]					= "Withdrawal";
$lang["withdrawal"]["filters"]						= "Filters";
$lang["withdrawal"]["withdrawal_type"]				= "Withdrawal Type";
$lang["withdrawal"]["withdrawal_status"]			= "Withdrawal Status";
$lang["withdrawal"]["all_type"]						= "All Type";
$lang["withdrawal"]["all_status"]					= "All Status";
$lang["withdrawal"]["clear_filters"]				= $clear_filters;
$lang["withdrawal"]["withdrawal_request"]			= "Withdrawal Request";
$lang["withdrawal"]["full_name"]					= "Full Name";
$lang["withdrawal"]["email"]						= $email;
$lang["withdrawal"]["address"]						= $address;
$lang["withdrawal"]["amount"]						= "Amount";
$lang["withdrawal"]["withdrawal_type"]				= "Withdrawal Type";
$lang["withdrawal"]["added_date"]					= "Added Date";
$lang["withdrawal"]["modified_date"]				= "Modified Date";
$lang["withdrawal"]["status"]						= $status;
$lang["withdrawal"]["action"]						= $action;
$lang["withdrawal"]["no_withdrawal"]				= "No Withdrawal Request";
$lang["withdrawal"]["paypal"]						= "Paypal";
$lang["withdrawal"]["live_cheque"]					= "Live Cheque";
$lang["withdrawal"]["apply_action"]					= $apply_action;
$lang["withdrawal"]["select_action"]				= $select_actions;
$lang["withdrawal"]["approved"]						= "Approved";
$lang["withdrawal"]["rejected"]						= "Rejected";
$lang["withdrawal"]["update"]						= "Update";
$lang["withdrawal"]["manage_withdrawal_request"]	= "Manage Withdrawal Request";
$lang["withdrawal"]["submit"]						= "Submit";
$lang["withdrawal"]["description"]					= "Description";
$lang["withdrawal"]["close"]						= $close;
/*Withdrawal Page variable end*/

/*Transaction Page variable start*/
$lang["transaction"]["transaction"]					= "Transaction";
$lang["transaction"]["filters"]						= "Filters";
$lang["transaction"]["clear_filters"]				= $clear_filters;
$lang["transaction"]["payment_type"]				= "Payment Type";
$lang["transaction"]["transaction_date"]			= "Transaction Date";
$lang["transaction"]["select_payment_type"]			= "Select Payment Type";
$lang["transaction"]["debit"]						= "DEBIT";
$lang["transaction"]["credit"]						= "CREDIT";
$lang["transaction"]["transaction_history"]			= "Transaction History";
$lang["transaction"]["user_name"]					= "User Name";
$lang["transaction"]["description"]					= $description;
$lang["transaction"]["payment_type"]				= "Payment Type";
$lang["transaction"]["amount"]						= "Amount";
$lang["transaction"]["user_balance_at_transaction"]	= "User Balance At Transaction";
$lang["transaction"]["transaction_date"]			= "Transaction Date";
$lang["transaction"]["status"]						= $status;
$lang["transaction"]["no_transaction_history"]		= "No Transaction History";
$lang["transaction"]["from"]						= "From";
$lang["transaction"]["to"]							= "To";
/*Transaction Page variable end*/


/*Season Page variable start*/
$lang["season"]["seasons"]				= "seasons";
$lang["season"]["filters"]				= "filters";
$lang["season"]["league"]				= "League";
$lang["season"]["team"]					= "Team";
$lang["season"]["season_week"]			= "Season Week";
$lang["season"]["all_team"]				= "All Team";
$lang["season"]["all_week"]				= "All Week";
$lang["season"]["from"]					= "From";
$lang["season"]["to"]					= "to";
$lang["season"]["clear_filters"]		= $clear_filters;
$lang["season"]["season_game_id"]		= "Season Game ID";
$lang["season"]["season_type"]			= "Season Type";
$lang["season"]["home"]					= "Home";
$lang["season"]["away"]					= "Away";
$lang["season"]["season_schedule"]		= "Season Schedule";
$lang["season"]["year"]					= "Year";
$lang["season"]["status"]				= $status;
$lang["season"]["season_week"]			= "Season Week";
$lang["season"]["season_stats"]			= "Season Stats";
$lang["season"]["no_season_schedule"]	= "No Season Schedule";
$lang["season"]["stats_detail"]			= "Stats Detail";
/*Season Page variable end*/

/*Promocode Page variable start*/
$lang["promo_code"]["sales_person_list"]		= "Sales Person List";
$lang["promo_code"]["sales_person"]				= "Sales Person";
$lang["promo_code"]["create_new"]				= "CREATE NEW";
$lang["promo_code"]["no_sales_person"]			= "No Sales Person";
$lang["promo_code"]["first_name"]				= $first_name;
$lang["promo_code"]["last_name"]				= $last_name;
$lang["promo_code"]["email"]					= $email;
$lang["promo_code"]["amount_received"]			= $amount_received;
$lang["promo_code"]["dob"]						= "DOB";
$lang["promo_code"]["added_date"]				= "Added Date";
$lang["promo_code"]["status"]					= $status;
$lang["promo_code"]["action"]					= $action;
$lang["promo_code"]["edit_sales_person"]		= "Edit Sales Person";
$lang["promo_code"]["sales_person_detail"]		= "Sales Person Detail";
$lang["promo_code"]["new_sales_person"]			= "New Sales Person";
$lang["promo_code"]["paypal_email"]				= "Paypal Email";
$lang["promo_code"]["date_of_birth"]			= $date_of_birth;
$lang["promo_code"]["address"]					= $address;
$lang["promo_code"]["zipcode"]					= "ZipCode";
$lang["promo_code"]["city"]						= $city;
$lang["promo_code"]["country"]					= $country;
$lang["promo_code"]["select_country"]			= "Select Country";
$lang["promo_code"]["state"]					= $state;
$lang["promo_code"]["submit"]					= "Submit";
$lang["promo_code"]["cancel"]					= "Cancel";
$lang["promo_code"]["promo_code_list"]			= "Promo Code List";
$lang["promo_code"]["promo_code"]				= $promo_code;
$lang["promo_code"]["discount"]					= "Discount(%)";
$lang["promo_code"]["benifit_cap"]				= "Benifit Cap";
$lang["promo_code"]["commission"]				= "Commission(%)";
$lang["promo_code"]["start_date"]				= "Start Date";
$lang["promo_code"]["end_date"]					= "End Date";
$lang["promo_code"]["detail_view"]				= "Detail View";
$lang["promo_code"]["no_promo_code"]			= "No Promo Code";
$lang["promo_code"]["promo_code_date"]			= "Promocode Date";
$lang["promo_code"]["select_sales_person"]		= "Select Sales Person";
$lang["promo_code"]["create_promo_code"]		= "Create Promo Code";
$lang["promo_code"]["promo_code_detail_list"]	= "Promo Code Detail List";
$lang["promo_code"]["promo_code_detail"]		= "Promo Code Detail";
$lang["promo_code"]["no_promo_code_detail"]		= "No Promo Code Detail";
$lang["promo_code"]["name"]						= "Name";
$lang["promo_code"]["game_name"]				= "Game Name";
$lang["promo_code"]["game_status"]				= "Game Status";
$lang["promo_code"]["entrants_participants"]	= "Entrants/Participants";
$lang["promo_code"]["entry_fee"]				= $entry_fee;
$lang["promo_code"]["amount_received"]			= $amount_received;
$lang["promo_code"]["contest_scheduled_date"]	= "Contest Scheduled Date";
$lang["promo_code"]["promocode_used_date"]		= "Promocode Used Date";
$lang["promo_code"]["promo_code_help"]			= "Please enter seven alphanumeric character for promocode";
$lang["promo_code"]["discount_help"]			= "Discount to be given to user on entry fee";
$lang["promo_code"]["benefit_cap_help"]			= "Maximum dollar value of the discount";
$lang["promo_code"]["promo_code_date_help"]		= "Effective start date of promo code";
/*Promocode Page variable end*/

/*Setting Page variable Start */
$lang["setting"]["OLD_PASS_REQUIRED"]		 = "Please enter old password.";
$lang["setting"]["NEW_PASS_REQUIRED"]		 = "Please enter new password.";
$lang["setting"]["CONFIRM_PASS_REQUIRED"]	 = "Please enter confirm password.";
$lang["setting"]["PASS_MIN_LENGTH"]			 = "Password length must be at least 6 characters.";
$lang["setting"]["PASS_NOT_MATCH"]			 = "Passwords do not match.";
$lang["setting"]["OFF_SEASON_END_DATE_REQUIRED"]= "Please set off season end date";

$lang["setting"]["MIN_DRAFT_DATE_REQUIRED"]	= "Please set min draft date";
$lang["setting"]["SEASON_YEAR_REQUIRED"]	= "Please select season year";
$lang["setting"]["MIN_DRAFT_TIME_REQUIRED"]	= "Please set min draft time";
$lang["setting"]["MAX_DRAFT_DATE_REQUIRED"]	= "Please set max draft date";
$lang["setting"]["MAX_DRAFT_TIME_REQUIRED"]	= "Please set max draft time";
$lang["setting"]["LEAGUE_ID_REQUIRED"]		= "Please select league";

// Manage Video Keys
$lang["setting"]["TYPE_REQUIRED"]			= "Video type is required";
$lang["setting"]["TITLE_EN_REQUIRED"]		= "Video title in english is required";
$lang["setting"]["TITLE_SP_REQUIRED"]		= "Video title in spanish is required";
$lang["setting"]["VIDEO_URL_REQUIRED"]		= "Video link is required";
$lang["setting"]["VIDEO_DESC_EN_REQUIRED"]	= "Video description in english is required";
$lang["setting"]["VIDEO_DESC_SP_REQUIRED"]	= "Video description in spanish is required";

/*Setting Page variable Start */

/* Commishner Tool Kit */
$lang["commish"]["list"]			 = "";

/* Commishner Tool Kit */

/*Advertisement Page variable start*/
$lang["advertisement"]["advertisement"]		= "Manage Advertisement";
$lang["advertisement"]["ads_name"]			= "Name";
$lang["advertisement"]["ads_target_url"]	= "Target Url";
$lang["advertisement"]["ads_position"]		= "Ads Position";
$lang["advertisement"]["ads_size"]			= "Ads Size";
$lang["advertisement"]["ads_view"]			= "View";
$lang["advertisement"]["ads_click"]			= "Click";
$lang["advertisement"]["ads_status"]		= "Status";
$lang["advertisement"]["action"]			= "Action";
$lang["advertisement"]["active"]			= "Active";
$lang["advertisement"]["inactive"]			= "Inactive";
$lang["advertisement"]["no_ads"]			= "No Advertisement found.";
$lang["advertisement"]["select_position_type"]			= "Select Position";
$lang["advertisement"]["ads_image"]			= "Upload Image";
$lang["advertisement"]["select_image"]			= "Select Image";
$lang["advertisement"]["submit"]			= "Save";
$lang["advertisement"]["cancel"]			= "Cancel";
$lang["advertisement"]["invalid_img_type"]	= 'Only JPG, PNG or GIF files are allowed!';
$lang["advertisement"]["position_error"]	= 'Please select position';


$lang["_404"]["Twest"] = "";

/*League News Page variables */
$lang["league_news"]["league_news_title"]				= "League News";
$lang["league_news"]["league_type"]						= "League";
$lang["league_news"]["page_type"]						= "Page Type";
$lang["league_news"]["player_unique_id"]				= "Player";
$lang["league_news"]["all_team"]						= "Team";
$lang["league_news"]["players"]							= "Players";
$lang["league_news"]["news_title"]						= "English Title";
$lang["league_news"]["spanish_news_title"]				= "Spanish Title";
$lang["league_news"]["news_description"]				= "English Description";
$lang["league_news"]["spanish_news_description"]		= "Spanish Description";
$lang["league_news"]["news_short_desc"]					= "English Title";
$lang["league_news"]["spanish_news_short_desc"]			= "Spanish Title";
$lang["league_news"]["save_news"]						= "Save News";
$lang["league_news"]["news_image"]						= "News Image";
$lang["league_news"]["league_type_required"]			= "Please select league type.";
$lang["league_news"]["page_type_required"]				= "Please select page type.";
$lang["league_news"]["team_abbr_required"]				= "Please select team.";
$lang["league_news"]["players_required"]				= "Please select player.";
$lang["league_news"]["news_title_required"]				= "Title is required.";
$lang["league_news"]["news_desc_required"]				= "Description is required.";
$lang["league_news"]["news_image_required"]				= "Image is required.";
$lang["league_news"]["news_short_desc_required"]		= "Short description is required.";
$lang["league_news"]["news_desc_required"]				= "Description is required.";
$lang["league_news"]["spanish_news_title_required"]		= "Title is required.";
$lang["league_news"]["spanish_news_desc_required"]		= "Description is required.";
$lang["league_news"]["spanish_news_short_desc_required"]= "Short description is required.";
$lang["league_news"]["spanish_news_desc_required"]		= "Description is required.";
$lang["league_news"]['active']							=	"Active";
$lang["league_news"]['inactive']						=	"Inactive";
$lang["league_news"]['activate']						=	"Activate";
$lang["league_news"]['deactivate']						=	"Deactivate";
/*League News Page variables */

/* End of file javascript_lang.php */
/* Location: ./application/language/english/javascript_lang.php */