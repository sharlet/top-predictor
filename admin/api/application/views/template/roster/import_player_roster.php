<!--Page content -->
<div class="page-content"  data-ng-init="intilizeRosterList();">
	<!-- Page title -->
	<div class="page-title">
		<h5><i class="fa fa-bars"></i>Manage roster</h5>
	</div>
	<!-- Table with footer -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h6 class="panel-title">Roster Management</h6>
		</div>
		<div class="datatable-header">
			<div class="dataTables_filter" id="DataTables_Table_2_filter">
				<label>
					<span>Filter:</span> 
					<input type="text" aria-controls="DataTables_Table_2" placeholder="Type to filter..." ng-model="search.full_name" >
				</label>
			</div>
			<div id="DataTables_Table_1_length" class="dataTables_length">
				<button class="btn btn-success" ng-click="importRoster();"><i></i>Import</button>
			</div>
		</div>
		<div class="table-responsive pre-scrollable custom-load-div" when-scrolled="load_more_roster()">
			<table class="table table-hover table-striped table-bordered table-check">
				<thead>
					<tr ng-if="tempRosterList.length>0">
						<th width="8%">
							Player Unique ID
						</th>
						<th>
							Player Name
						</th>
						<th>
							Position
						</th>
						<th>
							Team Name
						</th>
						<th>
							Salary
						</th>
						<th>
							League ID
						</th>
						<th>
							League Name
						</th>
					</tr>
					<tr ng-if="tempRosterList.length==0">
						<td align="center" colspan="7">No Player</td>
					</tr>
				</thead>
				<tbody>
					<tr ng-if="tempRosterList.length>0" ng-repeat="(key, roster) in tempRosterList | filter:search.full_name | orderBy:'full_name' | limitTo: roster_limit track by $index">
						<td ng-bind="roster.player_unique_id"></td>
						<td ng-bind="roster.full_name"></td>
						<td ng-bind="roster.position"></td>
						<td ng-bind="roster.team_name"></td>
						<td>
							<input type="text" name="player_salary" ng-model="tempRosterList[key].salary" class="form-control">
						</td>
						<td ng-bind="roster.league_id"></td>
						<td ng-bind="roster.league_abbr"></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- /table with footer -->
</div>
<!-- /Page content-->