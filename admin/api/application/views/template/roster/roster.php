<!--Page content -->
<div class="page-content" data-ng-init="initObject();getAllLeague();">
	<!-- Page title -->
	<div class="page-title">
		<h5><i class="fa fa-bars"></i>Manage Rosters</h5>
	</div>
	<!-- Table elements -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h6 class="panel-title" data-ng-bind="::lang.filters">
			</h6>
		</div>
		<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th data-ng-bind="::lang.league"></th>
						<th data-ng-bind="::lang.team"></th>
						<th data-ng-bind="::lang.position"></th>
						<!-- <th data-ng-bind="::lang.language"></th> -->
						<th > Player Name: </th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-center">
							<select data-placeholder="{{::lang.select_league}}" id="league_id" ng-options="league.league_id as league.league_abbr for league in leagues track by league.league_id" class="select-full" ng-model="rosterParam.league_id" data-ng-change="getAllTeam()" select-two="minimumResultsForSearch:'2',width:'100%'">
								<option value=""></option>
							</select>
						</td>
						
						<td class="text-center">
							<select data-placeholder="{{lang.all_team}}" id="team_abbr" ng-options="team.team_abbreviation as team.team_name+'('+team.team_abbreviation+')' for team in teams track by team.team_abbreviation" class="select-full" id="team_abbr" ng-model="rosterParam.team_abbr" data-ng-change="filterResult()" select-two="minimumResultsForSearch:'2',width:'100%'">
								<option value=""></option>
							</select>
						</td>

						<td class="text-center">
							<select data-placeholder="{{lang.all_position}}" id="position" ng-options="position.position_value as position.position for position in team_postition track by position.position_value" class="select-full" ng-model="rosterParam.position" data-ng-change="filterResult()" select-two="minimumResultsForSearch:'1',width:'100%'">
								<option value=""></option>
							</select>
						</td>
						<!-- <td class="text-center">
							<select data-placeholder="{{lang.language}}" id="language_id" class="select-full" ng-model="rosterParam.lang" data-ng-change="filterResult()" select-two="minimumResultsForSearch:'2',width:'100%'">
								<option value="en">English</option>
								<option value="sp">Spanish</option>
							</select>
						</td> -->
						<td class="text-center">
							<input type="text" placeholder="{{lang.player_name}}" id="player_name" name="player_name" ng-model="rosterParam.player_name" class="form-control" data-ng-change="searchByName()">
						</td>
					</tr>
					<tr>
						<td>
							<a href="javascript:void(0);" ng-click="clearFilter()">
								<span class="label label-info" data-ng-bind="lang.clear_filters"></span>
							</a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- /table elements -->
	<!-- Table with footer -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h6 class="panel-title" data-ng-bind="lang.roster_management"></h6>
			 <!-- <a href="javascript:void(0);" ng-click="showAddRosterPopup()">   <span class="pull-right label label-success ">Add Roster</span> </a> -->
		<!-- 	<a href="javascript:void(0);">
				<span class="pull-right badge" ng-click="showExportPopup()" data-ng-bind="lang.export_roster"></span>
			</a>
			<a href="javascript:void(0);" upload-file="post_url:'roster/do_upload'" callback="getFileRosterDetail(data);" id="uploadRoster" class="pull-right badge" data-ng-bind="lang.import_roster"></a>
		 -->
		</div>
		<div class="table-responsive">
			<table class="table table-hover table-striped table-bordered table-check">
				<thead>
					<tr ng-if="rosterList.length>0">
						<th>
							<input type="checkbox" ng-model="rosterObj.selectall" value="{{rosterObj.selectall}}" ng-checked="rosterObj.selectall" uniform="radioClass:'choice', selectAutoWidth:false" ng-click="togglePlayer($event);">
						</th>
						<th class="pointer" ng-click="sortRosterList('full_name');">
							{{lang.full_name}}
							<i ng-class="(rosterParam.sort_field=='full_name'&&rosterParam.sort_order=='DESC')?'fa-sort-desc':((rosterParam.sort_field=='full_name'&&rosterParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortRosterList('team_name');">
							{{lang.team}}
							<i ng-class="(rosterParam.sort_field=='team_name'&&rosterParam.sort_order=='DESC')?'fa-sort-desc':((rosterParam.sort_field=='team_name'&&rosterParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortRosterList('position');">
							{{lang.position}}
							<i ng-class="(rosterParam.sort_field=='position'&&rosterParam.sort_order=='DESC')?'fa-sort-desc':((rosterParam.sort_field=='position'&&rosterParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortRosterList('salary');">
							{{lang.salary}}
							<i ng-class="(rosterParam.sort_field=='salary'&&rosterParam.sort_order=='DESC')?'fa-sort-desc':((rosterParam.sort_field=='salary'&&rosterParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="" >
							{{lang.injury}}
							<!-- <i ng-class="(rosterParam.sort_field=='nick_name'&&rosterParam.sort_order=='DESC')?'fa-sort-desc':((rosterParam.sort_field=='nick_name'&&rosterParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i> -->
						</th>
						<!-- <th class="pointer" ng-click="sortRosterList('projection_rank');">
							Projected Rank
							<i ng-class="(rosterParam.sort_field=='projection_rank'&&rosterParam.sort_order=='DESC')?'fa-sort-desc':((rosterParam.sort_field=='projection_rank'&&rosterParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th> -->
						<th class="pointer" ng-click="sortRosterList('player_status');">
							{{lang.status}}
							<i ng-class="(rosterParam.sort_field=='player_status'&&rosterParam.sort_order=='DESC')?'fa-sort-desc':((rosterParam.sort_field=='player_status'&&rosterParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th>
							{{lang.action}}
						</th>
					</tr>
					<tr ng-if="rosterList.length==0">
						<td align="center" data-ng-bind="lang.no_player"></td>
					</tr>
				</thead>
				<tbody>
					<tr ng-if="rosterList.length>0" ng-repeat="roster in rosterList">
						<td>
							<input type="checkbox" name="player_unique_id[]" value="{{roster.player_unique_id}}" ng-checked="player_unique_id[$index]" ng-model="player_unique_id[$index]" ng-click="selectPlayer($event)" uniform="radioClass:'choice', selectAutoWidth:false"/>
						</td>
						<td ng-bind="roster.full_name"></td>
						<td>{{roster.team_name}} ({{roster.team_abbreviation}})</td>
						<td ng-bind="roster.position"></td>
						<td class="col-md-2">
							<input type="text"  class="form-control" intiger-only maxlength="7" name="salary[]" ng-model="rosterObj.salary[roster.player_unique_id]" ng-init="rosterObj.salary[roster.player_unique_id]=roster.salary" />
						</td> 
						<td class="col-md-2">
							<input type="text"  class="form-control"  maxlength="200" name="injury[]" ng-model="rosterObj.injury[roster.player_unique_id]" ng-init="rosterObj.injury[roster.player_unique_id]=roster.injury_status" />
						</td>
						<!-- <td ng-bind="roster.projection_rank"></td> -->
						<td>
							<i class="fa fa-ban danger" ng-show="roster.player_status==0" title="In-active"></i>
							<i class="fa fa-check-circle-o success" ng-show="roster.player_status==1" title="Active"></i>
						</td>
						<td>
							<div class="btn-group">
								<button class="btn btn-link btn-icon btn-xs tip" data-toggle="dropdown" data-hover="dropdown">
									<i class="fa fa-cogs"></i>
								</button>
								<button class="btn btn-link btn-icon btn-xs tip" data-toggle="dropdown" data-hover="dropdown">
									<span class="caret caret-split"></span>
								</button>
								<ul class="dropdown-menu dropdown-menu-right">
									<li ><a tabindex="-1" href="#" ng-click="showEditRosterPopup(roster)">Edit</a></li>
									<li ng-show="roster.player_status==0"><a tabindex="-1" href="#" ng-click="changePlayerStatus(roster.player_unique_id, 1, $index);" data-ng-bind="lang.active"></a></li>
									<li ng-show="roster.player_status==1"><a tabindex="-1" href="#" ng-click="changePlayerStatus(roster.player_unique_id, 0, $index);" data-ng-bind="lang.inactive"></a></li>
								</ul>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="table-footer" ng-if="rosterList.length>0">
			<div class="table-actions">
				<label data-ng-bind="lang.apply_action"></label>
				<select id="action" class="select-liquid" data-placeholder="{{lang.select_action}}" select-two="minimumResultsForSearch:'-1'" ng-model="rosterObj.action">
					<option value="" data-ng-bind="lang.select_action"></option>
					<option value="1" data-ng-bind="lang.active"></option>
					<option value="0" data-ng-bind="lang.inactive"></option>
				</select>
				<button type="button" ng-click="updateRoster()" class="btn btn-primary" ng-class="{disabled:rosterObj.player_unique_id.length==0}"><i class=""></i> {{lang.update}}</button>
				<!-- <button type="button" ng-click="playerRelease()" class="btn btn-success"><i class="fa fa-bullhorn"></i> {{lang.release_player}}</button> -->
			</div>
			<pagination ng-if="rosterParam.total_items>10" boundary-links="true" total-items="rosterParam.total_items" ng-model="rosterParam.current_page" ng-change="getPlayerList()" items-per-page="rosterParam.items_perpage" class="pagination" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></pagination>
		</div>
	</div>
	<!-- /table with footer -->
	<!-- /Export Roster Start-->
	<div id="export_roster_modal" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h5 class="modal-title"data-ng-bind="lang.export_roster"></h5>
				</div>

				<form role="form" encrypt="multipart/form-data">
					<div class="modal-body has-padding">
						<div class="form-group">
							<label data-ng-bind="lang.select_roster_type"></label>
							<div class="widget-inner">
								<div class="radio-inline">
									<label for="export_csv">
										<input type="radio" name="roster_export_type" id="export_csv" value="csv" class="styled" ng-checked="roster_export_type=='csv'" ng-model="roster_export_type" uniform="radioClass:'choice', selectAutoWidth:false">
										{{lang.csv}}
									</label>
								</div>
								<div class="radio-inline">
									<label for="export_xlsx">
										<input type="radio" name="roster_export_type" id="export_xlsx" value="xlsx" class="styled" ng-checked="roster_export_type=='xlsx'" ng-model="roster_export_type" uniform="radioClass:'choice', selectAutoWidth:false">
										{{lang.excel}}
									</label>
								</div>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-ng-bind="lang.close" data-dismiss="modal"></button>
						<a ng-if="rosterObj.roster_type!=''" class="btn btn-success" target="_blank" href="javascript:void(0);" ng-click="closeModel()" data-ng-bind="lang.export">
							
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- /End Export Roster-->
</div>
<!-- /Page content-->

<!-- Edit Statistics Popup START -->
<div id="edit_roster_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
                <div class="modal-content">
                        <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h5 class="modal-title">Edit Roster</h5>
                        </div>

                    <form role="form">
                                <div class="modal-body has-padding">
                                        <div class="form-group">					
                                                <div class="row">
                                                <!-- {{edit_roster_data.team_abbr}} -->
                                                        <label class="col-md-3 control-label" for="edit_team_abbr">Team <span class="mandatory">*</span></label>
                                                        <div class="col-md-9">
                                                                <select data-placeholder="Select Team" id="edit_team_abbr" 
                                                                        ng-model="edit_roster_data.team_abbr" class="select-full" select-two="minimumResultsForSearch:'2',width:'100%'">
                                                                        <option value=""></option>
                                                                        <option ng-if="team.team_abbreviation != 'all'" ng-repeat="team in teams" value="{{team.team_abbreviation}}" ng-bind="team.team_name+' ('+team.team_abbreviation+')'"></option>
                                                                </select>
                                                        </div>
                                                </div>
                                        </div>
                                    
                                        <div class="form-group">					
						<div class="row">
							<label class="col-md-3 control-label" for="edit_position">Position<span class="mandatory">*</span></label>
							<div class="col-md-9">
								<select data-placeholder="Select Position" id="edit_position" 
                                                                        ng-model="edit_roster_data.position" class="select-full" select-two="minimumResultsForSearch:'2',width:'100%'">
                                                                        <option value=""></option>
                                                                        <option ng-if="position.position != 'all'" ng-repeat="position in team_postition" value="{{position.position}}" ng-bind="position.position"></option>
                                                                </select>
							</div>
						</div>
					</div>
                                    
                    <div class="form-group">					
						<div class="row">
							<label class="col-md-3 control-label" for="edit_first_name">Full Name <span class="mandatory">*</span></label>
							<div class="col-md-9">
								<input type="text"  maxlength="100" placeholder="Full Name EnglishFull Name English" id="edit_full_name_en" name="full_name_en" ng-model="edit_roster_data.full_name_en" class="form-control">
							</div>
						</div>
					</div>

					<!-- <div class="form-group">					
						<div class="row">
							<label class="col-md-3 control-label" for="edit_full_name_sp">Full Name Spanish<span class="mandatory">*</span></label>
							<div class="col-md-9">
								<input type="text"  maxlength="100" placeholder="Full Name Spanish" id="edit_full_name_sp" name="full_name_sp" ng-model="edit_roster_data.full_name_sp" class="form-control">
							</div>
						</div>
					</div> -->
	<!--                    <div class="form-group">					
						<div class="row">
							<label class="col-md-3 control-label" for="edit_injury_status">Salary</label>
							<div class="col-md-9">
								<input type="text" placeholder="Injury" id="edit_salary" name="salary" ng-model="edit_roster_data.salary" class="form-control">
							</div>
						</div>
					</div> -->

                   <div class="form-group">					
						<div class="row">
							<label class="col-md-3 control-label" for="edit_injury_status">Injury</label>
							<div class="col-md-9">
								<input type="text"  maxlength="100" placeholder="Injury" id="edit_injury_status" name="injury_status" ng-model="edit_roster_data.injury_status" class="form-control">
							</div>
						</div>
					</div>

                   	<div class="form-group">					
						<div class="row">
							<label class="col-md-3 control-label" for="edit_jersey_number">Jersey Number</label>
							<div class="col-md-9">
								<input type="text"  maxlength="3" placeholder="Jersey Number" id="edit_jersey_number" name="jersey_number"  intiger-only ng-model="edit_roster_data.jersey_number" class="form-control">
							</div>
						</div>
					</div>	
                   	<div class="form-group">					
						<div class="row">
							<label class="col-md-3 control-label" for="edit_bye_week">Bye Week</label>
							<div class="col-md-9">
								<input type="text"  maxlength="3" placeholder="Bye Week" id="edit_bye_week" name="bye_week" intiger-only ng-model="edit_roster_data.bye_week" class="form-control">
							</div>
						</div>
					</div>  
                   	<div class="form-group">					
						<div class="row">
							<label class="col-md-3 control-label" for="edit_bye_week">Projected Rank</label>
							<div class="col-md-9">
								<input type="text"  maxlength="7" placeholder="Projection Ranl" id="edit_pro_rank" name="edit_pro_rank" intiger-only ng-model="edit_roster_data.projection_rank" class="form-control">
							
							</div>
						</div>
					</div>					
					<div class="form-group">					
						<div class="row">
	                        <label class="col-md-3 control-label" for="edit_bye_week">Roster Image</label>
	                        	<div class="col-md-9"> 
									<img width="100" height="100" ng-src="{{imgPath+edit_roster_data.img_url}}" alt="" ng-show="edit_roster_data.img_url!=''">
								<br/><br/>	<span upload-file="post_url:'roster/do_image_upload'" callback="UpdateRosterImg(data);"  id="edit_roster_img" class="label label-success">Upload Image</span>
									<!-- <a href="#" data-placement="top" title="{{lang.featured_image_help}}" tool-tip><i class="fa fa-info-circle help"></i></a> -->
									<label for="edit_roster_img" class="error hide" id="roster_img_error"></label>
								</div>
							
						</div>
					</div>	

                </div>

                    <div class="modal-footer">
                            <button type="button" class="btn btn-success" ng-click="UpdateRosterStats();">Save</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                    </div>
            </form>
                </div>
        </div>
</div>
<!-- Edit Statistics Popup END -->

<!-- Add Seasons Popup START -->
<div id="add_roster_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 80%;">
                <div class="modal-content">
                        <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h5 class="modal-title">Add Roster</h5>
                        </div>

                    <form role="form">
                                <div class="modal-body has-padding">
                                        <div style="max-height:350px; overflow:auto;">
                                                <div class="table-responsive">
                                                        <table class="table table-striped table-bordered">
                                                                <thead>
                                                                        <tr>
                                                                                <th width="15%">Team</th>
                                                                                <th width="12%">Position</th>
                                                                                <th width="15%">Full Name</th>
                                                                                <!-- <th width="15%">Full Name In Spanish</th> -->
                                                                                <!-- <th width="10%">Salary</th> -->
                                                                                <th width="15%">Injury</th>
                                                                                <th width="10%">Jersey Number</th>
                                                                                <th width="10%">Bye Week</th>
                                                                                <th width="10%">Projected Rank</th>
                                                                                <th width="3%"></th>
                                                                        </tr>
                                                                </thead>
                                                                <tbody>
                                                                        <tr >
                                                                                
                                                                                <td class="text-center">
                                                                                        <select data-placeholder="Select Team" id="team_abbr" 
                                                                                                ng-model="roster_data.roster.team_abbr" class="select-full"  select-two="minimumResultsForSearch:'2',width:'100%'" ng-change="getRostersByTeamAndPos()">
                                                                                                <option value=""></option>
                                                                                                <option ng-if="team.team_abbreviation != 'all'" ng-repeat="team in teams" value="{{team.team_abbreviation}}" ng-bind="team.team_name+' ('+team.team_abbreviation+')'"></option>
                                                                                        </select>
                                                                                </td>
                                                                                
                                                                                <td class="text-center">
                                                                                        <select data-placeholder="Select Position" id="position" 
                                                                                                ng-model="roster_data.roster.position" class="select-full" select-two="minimumResultsForSearch:'2',width:'100%'" ng-change="getRostersByTeamAndPos()">
                                                                                                <option value=""></option>
                                                                                                <option ng-if="position.position != 'all'" ng-repeat="position in team_postition" value="{{position.position}}" ng-bind="position.position"></option>
                                                                                        </select>
                                                                                </td>

                                                                                <td class="text-center">
                                                                                        <input type="text"  maxlength="100" placeholder="Full Name " id="full_name_en" name="full_name_en" ng-model="roster_data.roster.full_name_en" class="form-control">
                                                                                </td>

                                                                              <!--   <td class="text-center">
                                                                                        <input type="text" maxlength="100" placeholder="Full Name In Spanish" id="full_name_sp" name="full_name_sp" ng-model="roster_data.roster.full_name_sp" class="form-control">
                                                                                        <span></span>
                                                                                </td> -->

                                                                               <!--  <td class="text-center">
                                                                                        <input type="text" placeholder="Salary" id="salary" name="salary" ng-model="roster_data.roster.salary" class="form-control">
                                                                                        <span></span>
                                                                                </td> -->
                                                                                
                                                                                <td class="text-center">
                                                                                        <input type="text"  maxlength="100" placeholder="Injury" id="injury_status" name="injury_status" ng-model="roster_data.roster.injury_status" class="form-control">
                                                                                </td>

                                                                               	<td class="text-center">
                                                                                        <input type="text"  maxlength="3" placeholder="Jersey Number" id="jersey_number" name="jersey_number" intiger-only ng-model="roster_data.roster.jersey_number" class="form-control">
                                                                                </td>
                                                                               	<td class="text-center">
                                                                                        <input type="text"  maxlength="3" placeholder="Bye Week" id="bye_week" name="bye_week" intiger-only ng-model="roster_data.roster.bye_week" class="form-control">
                                                                                </td>
                                                                               	<td class="text-center">
                                                                                        <input type="text"  maxlength="7" placeholder="Projection Ranl" id="pro_rank" name="pro_rank" intiger-only ng-model="roster_data.roster.projection_rank" class="form-control">
                                                                                </td>                                                                                                                                                                  
                                                                                
                                                                                <td class="text-center">
                                                                                  <!--   <a ng-show="roster_data.roster.length > 1" ng-click="RemoveRosterRow($index)">
                                                                                        <i class="fa fa-times" title="Remove"></i>
                                                                                    </a> -->
															                        <div>
															                        		<img width="100" height="100" ng-src="{{roster_img}}" alt="" ng-show="roster_img!=''">
																							<br/><br/>
																						<!-- <div class="col-md-9">   callback="getRoserUploadedImg(data);" -->
																							<span upload-file="post_url:'roster/do_image_upload'" callback="getRoserUploadedImg(data);"  id="roster_img" class="label label-success">Upload Image</span>
																							<!-- <a href="#" data-placement="top" title="{{lang.featured_image_help}}" tool-tip><i class="fa fa-info-circle help"></i></a> -->
																							<label for="roster_img" class="error hide" id="roster_img_error"></label>
																						<!-- </div> -->
																						</br>
																						
																					</div>                                                     
                                                                                </td>
                                                                        </tr>
                                                                </tbody>
                                                        </table>
                                                </div>
                                        </div>
                                        <div class="table-responsive">
                                                <table class="table table-striped table-bordered">
                                                		<thead>
                                                            <tr>
                                                                    <th width="15%">Player Name </th>
                                                                    <!-- <th width="12%">Full Name In Spanish</th> -->
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                                <tr ng-repeat="(key, value) in addedRosterList"> 
                                                                        <td ng-bind="value.full_name_en">
                                                                                
                                                                        </td>
                                                                     <!--    <td ng-bind="value.full_name_sp">
                                                                                
                                                                        </td> -->
                                                                </tr>
                                                        </tbody>
                                                </table>
                                        </div>
                                </div>

                                <div class="modal-footer">
                                        <button type="button" class="btn btn-success" ng-click="SaveRoster();">Save</button>
                                        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                                </div>
                        </form>
                </div>
        </div>
</div>
<!-- Add Seasons Popup END -->
