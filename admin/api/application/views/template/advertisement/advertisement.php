<!-- Page content --> -->
<div class="page-content" ng-init="initAdvertisement()">
	<!-- Page title -->
	<div class="page-title">
		<h5><i class="fa fa-bars"></i>Manage Advertisements</h5>
	</div>
	<!-- /page title -->

	<!-- Promocode Section -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h6 class="panel-title" > Manage Advertisements</h6>
			<h6 class="panel-title pull-right">Total Record Count : <span ng-bind="adsParam.total_items"></span></h6>
		</div>
		<div class="table-responsive">
			<table class="table table-hover table-striped table-bordered">
				<thead>
					<tr>
						<th class="pointer" ng-click="sortAdvertisementList('name');">
							{{lang.ads_name}}
							<i ng-class="(adsParam.sort_field=='name'&&adsParam.sort_order=='DESC')?'fa-sort-desc':((adsParam.sort_field=='name'&&adsParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortAdvertisementList('target_url');">
							{{lang.ads_target_url}}
							<i ng-class="(adsParam.sort_field=='target_url'&&adsParam.sort_order=='DESC')?'fa-sort-desc':((adsParam.sort_field=='target_url'&&adsParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortAdvertisementList('ads_position');">
							{{lang.ads_position}}
							<i ng-class="(adsParam.sort_field=='ads_position'&&adsParam.sort_order=='DESC')?'fa-sort-desc':((adsParam.sort_field=='ads_position'&&adsParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortAdvertisementList('ads_size');">
							{{lang.ads_size}}
							<i ng-class="(adsParam.sort_field=='ads_size'&&adsParam.sort_order=='DESC')?'fa-sort-desc':((adsParam.sort_field=='ads_size'&&adsParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<!-- <th class="pointer" ng-click="sortAdvertisementList('view');">
							{{lang.ads_view}}
							<i ng-class="(adsParam.sort_field=='view'&&adsParam.sort_order=='DESC')?'fa-sort-desc':((adsParam.sort_field=='view'&&adsParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th> -->
						<th class="pointer" ng-click="sortAdvertisementList('click');">
							{{lang.ads_click}}
							<i ng-class="(adsParam.sort_field=='click'&&adsParam.sort_order=='DESC')?'fa-sort-desc':((adsParam.sort_field=='click'&&adsParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						
						<th class="pointer" ng-click="sortAdvertisementList('status');">
							{{lang.ads_status}}
							<i ng-class="(adsParam.sort_field=='status'&&adsParam.sort_order=='DESC')?'fa-sort-desc':((adsParam.sort_field=='status'&&adsParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th ng-bind="lang.action">
						</th>
						
					</tr>
					<tr ng-if="adsList.length==0">
						<td align="center" data-ng-bind="lang.no_ads" colspan="8"></td>
					</tr>
				</thead>
				<tbody>
					<tr ng-if="adsList.length>0" ng-repeat="ads in adsList">
						<td data-ng-bind="::ads.name"></td>
						<td data-ng-bind="::ads.target_url"></td>
						<td data-ng-bind="::ads.ads_position"></td>
						<td data-ng-bind="::ads.ads_size"></td>
						<!-- <td data-ng-bind="::ads.view"></td> -->
						<td data-ng-bind="::ads.click"></td>
						<td>
							<i class="fa fa-check-circle-o success" ng-show="ads.status==1" title="{{lang.active}}"></i>
							<i class="fa fa-ban danger" ng-show="ads.status==0" title="{{lang.inactive}}"></i>
						</td>
						<td>
							<div class="btn-group">
								<button class="btn btn-link btn-icon btn-xs tip" data-toggle="dropdown" data-hover="dropdown">
									<i class="fa fa-cogs"></i>
								</button>
								<button class="btn btn-link btn-icon btn-xs tip" data-toggle="dropdown" data-hover="dropdown">
									<span class="caret caret-split"></span>
								</button>
								<ul class="dropdown-menu dropdown-menu-right">
									<li ng-show="ads.status == 0">
										<a tabindex="-1" href="#" ng-click="update_status(ads.ad_management_id,1, $index);" data-ng-bind="lang.active"></a>
									</li>
									<li ng-show="ads.status == 1">
										<a tabindex="-1" href="#" ng-click="update_status(ads.ad_management_id,0, $index);" data-ng-bind="lang.inactive"></a>
									</li>
									<li>
										<a ng-click="deleteAdv(ads.ad_management_id,$index)">Delete</a>
									</li>
								</ul>
							</div>
						</td>
						
					</tr>
				</tbody>
			</table>
		</div>
		<div class="table-footer" ng-show="adsParam.total_items>10">
			<pagination boundary-links="true" total-items="adsParam.total_items" ng-model="adsParam.current_page" ng-change="getAdvertisements()" items-per-page="adsParam.items_perpage" class="pagination-sm pull-right" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></pagination>
		</div>
	</div>
	<!--Promocode End -->
</div>
<!-- /Page content