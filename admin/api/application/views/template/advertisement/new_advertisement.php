Page content -->
<div class="page-content" ng-init="initAdvertisement()">
	<!-- Page title -->
	<div class="page-title">
		<h5><i class="fa fa-align-justify"></i>{{lang.advertisement}}</h5>
	</div>
	<!-- page title -->

	<!-- New Promocode Section Start -->
	<form class="" role="form" advertisement-form submit-handle="newAdvertisement()" role="form">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h6 class="panel-title" data-ng-bind="lang.new_advertisement"></h6>
			</div>
			<div class="panel-body">
				<div class="form-group">
					<div class="row">
						<label class="col-md-3 control-label" for="promo_code">{{lang.ads_name}}
							<span class="mandatory">*</span>
						</label>
						<div class="col-md-9">
							<input id="name" name="name" type="text" ng-model="adsObj.name" class="form-control" maxlength="7">
							<label for="name" class="error hide" id="name_error"></label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<label class="col-md-3 control-label" for="discount">{{lang.ads_target_url}}
							<span class="mandatory">*</span>
						</label>
						<div class="col-md-9">
							<input id="target_url" name="target_url" type="text" ng-model="adsObj.target_url" class="form-control">
							<label for="discount" class="error hide" id="target_url_error"></label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<label class="col-md-3 control-label" for="commission_sales">{{lang.ads_position}}
							<span class="mandatory">*</span> 
						</label>
						<div class="col-md-9">
							<select id="position_type" name="position_type" ng-change="getPositionInfo();doBlur('position_type');" data-placeholder="{{lang.select_position_type}}" ng-model="adsObj.position_type" class="select-full" select-two="minimumResultsForSearch:'-2',width:'100%'">
									<option value="" ng-bind="lang.select_position_type"></option>
									<option ng-repeat="position in adsPositionList" value="{{position.ad_position_id}}">{{position.type}}</option>
								</select>
							<label for="position_type" class="error hide" id="position_type_error"></label>
						</div>
					</div>
				</div>
<!-- 				<div class="form-group">
					<div class="row">
						<label class="col-md-3 control-label" for="commission_sales">{{lang.ads_image}}
							<span class="mandatory">*</span> 
							<span ng-if="adsObj.size_tip != ''" ng-bind="adsObj.size_tip"></span>
						</label>
						<div class="col-md-9">
						{{adsObj.position_type}}
							<input type="file" name="userfile" id="upload_btn" ng-show="adsObj.uploadbtn == 1">
							<input type="button" class="btn btn-danger" value="Remove" ng-show="adsObj.is_remove == 1" ng-click="removeImage();">
							<input type="hidden" name="ads_image" id="ads_image" value="{{adsObj.ads_image}}">
							
							</br>
							<img width="150" height="150" ng-src="{{imagePath+adsObj.ads_image}}" alt="" ng-if="adsObj.is_preview == 1">

								<br/><br/>	
								<span ng-if="showUpload" img-size='2048000' upload-file="post_url:'advertisements/do_image_upload'" callback="UpdateAdvImg(data);" add-data="position:'{{adsObj.position_type}}'"  id="ads_image" class="label label-success">Upload Image</span>
					
								<label for="ads_image" class="error hide" id="ads_image_error"></label>
						</div>
					</div>
				</div> -->
				<div class="form-group">
					<div class="row">
						<label class="col-md-3 control-label" for="commission_sales">{{lang.ads_image}}
							<span class="mandatory">*</span> 
							<span ng-if="adsObj.size_tip != ''" ng-bind="adsObj.size_tip"></span>
						</label>
						<div class="col-md-9">
						
							<input type="file" name="userfile" id="upload_btn" ng-show="adsObj.uploadbtn == 1">
							<input type="button" class="btn btn-danger" value="Remove" ng-show="adsObj.is_remove == 1" ng-click="removeImage();">
							<input type="hidden" name="ads_image" id="ads_image" value="{{adsObj.ads_image}}">
							<label for="ads_image" class="error hide" id="ads_image_error"></label>
							</br>
							<img ng-src="{{adsObj.ads_image}}" alt="" ng-if="adsObj.is_preview == 1">
						</div>
					</div>
				</div>
				<div class="form-actions text-left">
					<button type="submit" class="btn btn-success">
						<i class=""></i>{{lang.submit}}
					</button>
					<a ng-href="advertisement">
						<button type="button" class="btn btn-warning">
						{{lang.cancel}}
						</button>
					</a>
				</div>
			</div>
		</div>
	</form>
	<!-- New Promocode Section End -->

</div>
<!-- Page content 