<!--Page content -->
<div class="page-content">
    <!-- Page title -->
    <div class="page-title">
        <h5><i class="fa fa-bars"></i>Manage Teams</h5>
    </div>
    <!-- Table elements -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h6 class="panel-title">Filters</h6>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>League</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" data-ng-init="getAllLeague()" width="30%">
                            <select data-placeholder="Select League" id="league_id" ng-options="league.league_id as league.league_abbr for league in leagues track by league.league_id" class="select-full" ng-model="teamrosterParam.league_id" data-ng-change="filterResult();" select-two="minimumResultsForSearch:'-2',width:'100%'">
                                <option value=""></option>
                            </select>
                        </td>
                        <td class="text-center" width="30%">
                            <select data-placeholder="Select Language" id="language_id" class="select-full" ng-model="teamrosterParam.lang" data-ng-change="getTeamList()" select-two="minimumResultsForSearch:'-2',width:'100%'">
                                <option value="en">English</option>
                                <option value="sp">Spanish</option>
                            </select>
                        </td>
                        <!-- <tr>
							<td><a href="javascript:;" ng-click="initObject()"><span class="label label-info">Clear Filters</span></a></td>
						</tr> -->
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /table elements -->
    <!-- Table with footer -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h6 class="panel-title">Team List</h6>
            <a href="javascript:void(0);" ng-click="showAddTeamPopup()">
                <span class="pull-right label label-success ">Add Team</span>
            </a>
            <h6 class="panel-title pull-right">Total Record Count : <span ng-bind="teamrosterParam.total_items"></span></h6>
        </div>
        <div class="table-responsive">
            <table class="table table-hover table-striped table-bordered table-check">
                <thead>
                    <tr ng-if="teamrosterList.length>0">
                        <!-- <th width="7%">
							Team ID
						</th> -->
                        <th class="pointer" ng-click="sortTeamrosterList('team_name');" width="40%">
                            Team Name
                            <i ng-class="(teamrosterParam.sort_field=='team_name'&&teamrosterParam.sort_order=='DESC')?'fa-sort-desc':((teamrosterParam.sort_field=='team_name'&&teamrosterParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
                        </th>
<!--                         <th class="pointer" ng-click="sortTeamrosterList('team_abbr');">
                            Team Abbreviation
                            <i ng-class="(teamrosterParam.sort_field=='team_abbr'&&teamrosterParam.sort_order=='DESC')?'fa-sort-desc':((teamrosterParam.sort_field=='team_abbr'&&teamrosterParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
                        </th> -->

                        <th class="pointer" ng-click="sortTeamrosterList('team_abbr_label');">
                            Team Abbreviation Label
                            <!-- <i ng-class="(teamrosterParam.sort_field=='team_abbr'&&teamrosterParam.sort_order=='DESC')?'fa-sort-desc':((teamrosterParam.sort_field=='team_abbr'&&teamrosterParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i> -->
                        </th>
                        <th class="pointer" ng-click="sortTeamrosterList('bye_week');">
                            Bye Week
                            <!-- <i ng-class="(teamrosterParam.sort_field=='team_abbr'&&teamrosterParam.sort_order=='DESC')?'fa-sort-desc':((teamrosterParam.sort_field=='team_abbr'&&teamrosterParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i> -->
                        </th>                        

                        <th>
                            Action
                        </th>
                    </tr>
                    <tr ng-if="teamrosterList.length==0">
                        <td align="center">No Team</td>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-if="teamrosterList.length>0" ng-repeat="teamroster in teamrosterList">
                        <!-- <td ng-bind="teamroster.team_id"></td> -->
                        <td ng-bind="teamroster.team_name"></td>
                        <!-- <td ng-bind="teamroster.team_abbr"></td> -->
                        <td ng-bind="teamroster.team_abbr_label"></td>
                        <td ng-bind="teamroster.bye_week"></td>
                        <td>
                            <a href="javascript:void(0);" ng-click="showEditTeamPopup(teamroster)">
                                <i class="fa fa-pencil-square" title="Edit Team"></i>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="table-footer" ng-if="teamrosterList.length>0">
            <pagination ng-if="teamrosterParam.total_items>10" boundary-links="true" total-items="teamrosterParam.total_items" ng-model="teamrosterParam.current_page" ng-change="getTeamList()" items-per-page="teamrosterParam.items_perpage" class="pagination" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></pagination>
        </div>
    </div>
    <!-- /table with footer -->
</div>
<!-- /Page content-->
<!-- Edit Statistics Popup START -->
<div id="edit_statistics_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 class="modal-title">Edit Team</h5>
            </div>
            <form role="form">
                <div class="modal-body has-padding">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 control-label" for="team_id">Team Name English <span class="mandatory">*</span></label>
                            <div class="col-md-9">
                                <input type="text" maxlength="100" placeholder="Team Name English" id="team_name" name="team_name" ng-model="edit_team_data.team_name_en" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 control-label" for="team_id">Team Name Spanish<span class="mandatory">*</span></label>
                            <div class="col-md-9">
                                <input type="text" maxlength="100" placeholder="Team Name Spanish" id="team_name" name="team_name" ng-model="edit_team_data.team_name_sp" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 control-label" for="team_abbr_lable_en">English Team Abbreviation Label  <span class="mandatory">*</span></label>
                            <div class="col-md-9">
                                <input type="text" maxlength="100" placeholder="Team Abbreviation English" id="team_abbr_lable_en" name="team_abbr_lable_en" ng-model="edit_team_data.team_abbr_label_en" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 control-label" for="team_abbr_lable_sp">Spanish Team Abbreviation Label<span class="mandatory">*</span></label>
                            <div class="col-md-9">
                                <input type="text"  maxlength="100" placeholder="Team Abbreviation Spanish" id="team_abbr_lable_sp" name="team_abbr_lable_sp" ng-model="edit_team_data.team_abbr_label_sp" class="form-control">
                            </div>
                        </div>
                    </div>                                        
      <!--               <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 control-label" for="team_abbr">Team Abbreviation <span class="mandatory">*</span></label>
                            <div class="col-md-9">
                                <input type="text" maxlength="100"  placeholder="Team Abbreviation" id="team_abbr" readonly="" name="team_abbr" ng-model="edit_team_data.team_abbr" class="form-control">
                            </div>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 control-label" for="bye_week">Bye Week</label>
                            <div class="col-md-9">
                                <input type="text" placeholder="Bye Week" id="bye_week" name="bye_week" maxlength="3" intiger-only ng-model="edit_team_data.bye_week" class="form-control">
                                <!-- <label for="bye_week" class="error hide" id="bye_week_error"></label> -->
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" ng-click="UpdateTeam();">Save</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Edit Statistics Popup END -->
<!-- Add Statistics Popup START -->
<div id="add_statistics_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 class="modal-title">Add Team</h5>
            </div>
            <form role="form" team-form submit-handle="AddTeam()">
                <div class="modal-body has-padding">
                    <div class="form-group" data-ng-init="getAllLeague()">
                        <div class="row">
                            <label class="col-md-4 control-label" for="add_league_id">League <span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <select data-placeholder="Select League" id="add_league_id" ng-change="doBlur('add_league_id')" name="add_league_id" class="select-full" ng-model="teamrosterParam.league_id" data-ng-change="getTeamList()" select-two="minimumResultsForSearch:'-2',width:'100%'">
                                    <option value=""></option>
                                    <option ng-repeat="league in leagues" value="{{league.league_id}}" ng-bind="league.league_abbr"></option>
                                </select>
                                <label for="add_league_id" class="error hide" id="add_league_id_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label" for="team_name_en">Team Name English <span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <input type="text" maxlength="100" placeholder="Team Name English" id="team_name_en" name="team_name_en" ng-model="add_team_data.team_name_en" class="form-control">
                                <label for="team_name_en" class="error hide" id="team_name_en_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label" for="team_name_sp">Team Name Spanish<span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <input type="text" maxlength="100"  placeholder="Team Name Spanish" id="team_name_sp" name="team_name_sp" ng-model="add_team_data.team_name_sp" class="form-control">
                                <label for="team_name_sp" class="error hide" id="team_name_sp_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label" for="team_abbr_lable_en">English Team Abbreviation Label <span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <input type="text" maxlength="100"  placeholder="Team Abbreviation English" id="team_abbr_lable_en" name="team_abbr_lable_en" ng-model="add_team_data.team_abbr_lable_en" class="form-control">
                                <label for="team_abbr_lable_en" class="error hide" id="team_abbr_lable_en_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label" for="team_abbr_lable_sp">Spanish Team Abbreviation Label<span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <input type="text" maxlength="100" placeholder="Team Abbreviation Spanish" id="team_abbr_lable_sp" name="team_abbr_lable_sp" ng-model="add_team_data.team_abbr_lable_sp" class="form-control">
                                <label for="team_abbr_lable_sp" class="error hide" id="team_abbr_lable_sp_error"></label>
                            </div>
                        </div>
                    </div>
             <!--        <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label" for="team_abbr">Team Abbreviation<span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <input type="text" maxlength="100" placeholder="Team Abbreviation" id="team_abbr" name="team_abbr" ng-model="add_team_data.team_abbr" class="form-control">
                                <label for="team_abbr" class="error hide" id="team_abbr_error"></label>
                            </div>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label" for="bye_week">Bye Week</label>
                            <div class="col-md-8">
                                <input type="text" placeholder="Bye Week" id="bye_week" name="bye_week" maxlength="3" intiger-only ng-model="add_team_data.bye_week" class="form-control">
                                <label for="bye_week" class="error hide" id="bye_week_error"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Save</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Edit Statistics Popup END -->
