<!--Page content -->
<div class="page-content" data-ng-init="getAddStatisticsFormData(); getSeasonStatisticsFeedList();">
    <!-- Page title -->
    <div class="page-title">
        <h5><i class="fa fa-bars"></i>Season Stats</h5>
    </div>
    <!-- Table elements -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h6 class="panel-title" data-ng-bind="lang.filters"></h6>
            <!--  <a href="javascript:void(0);" ng-click="downloadStatsTemplate()"><span class="pull-right label label-success">Download Template</span></a>
            <span upload-file="post_url:'stats_feed/do_upload_score'" callback="saveUploadedStatsData(data);" id="uploadRoster" class="pull-right label label-success" style="position: relative; z-index: 1;">Upload Statistics</span>
 -->
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Position</th>
                        <th data-ng-bind="lang.team"></th>
                        <th>Player</th>
                        <th width="40%"></th>
                        <!--                        <th data-ng-bind="lang.season_schedule"></th>-->
                    </tr>
                </thead>
                <tbody>
                    <tr ng-init="initStatsFilterObject()">
                        <td class="text-center">
                            <select data-placeholder="Select Position" id="filter_position" ng-options="position.position as position.position for position in positions track by position.position" ng-model="seasonStatsParam.position" ng-change="seasonStatsParam.player_unique_id = ''; filterStatsResult()" class="select-full" select-two="minimumResultsForSearch:'2',width:'100%'">
                                <option value=""></option>
                            </select>
                        </td>
                        <td class="text-center">
                            <select data-placeholder="Select Team" id="filter_team_id" ng-options="team.team_id as team.team_name+' ('+team.team_abbr+')' for team in teams track by team.team_id" ng-model="seasonStatsParam.team_id" ng-change="seasonStatsParam.player_unique_id = ''; filterStatsResult()" class="select-full" select-two="minimumResultsForSearch:'2',width:'100%'">
                                <option value=""></option>
                            </select>
                        </td>
                        <td class="text-center">
                            <select data-placeholder="Select Player" id="filter_player_id" class="select-full" ng-model="seasonStatsParam.player_unique_id" ng-change="filterStatsResult()" select-two="minimumResultsForSearch:'2',width:'100%'">
                                <option value=""></option>
                                <option ng-repeat="player in players  | filter:{team_id:seasonStatsParam.team_id, position: seasonStatsParam.position}:strict" value="{{player.player_unique_id}}" ng-bind="player.full_name"></option>
                            </select>
                        </td>
                        <td></td>
                        <!--                        <td  width="25%">
                            <div class="col-sm-6">
                                <input type="text" class="from-date form-control" name="from" date-picker-range="to-date" placeholder="{{lang.from}}" ng-model="seasonStatsParam.fromdate" readonly="">
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="to-date form-control" name="to" date-picker-range placeholder="{{lang.to}}" ng-model="seasonStatsParam.todate" data-ng-change="filterStatsResult()" readonly="">
                            </div>
                        </td>-->
                    </tr>
                    <tr>
                        <td colspan="3">
                            <a href="javascript:void(0);" ng-click="clearStatsFilter()"><span class="label label-info" data-ng-bind="lang.clear_filters"></span></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /table elements -->
    <!-- Table with footer -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h6 class="panel-title">Season Stats List <span class="" ng-if="summaries!=''"> ({{summaries}})</span> </h6>
            <a href="javascript:void(0);" ng-click="showAddStatisticsPopup()">
                <span class="pull-right label label-success ">Add Statistics</span>
            </a>

            <a href="utility/open_manual_cron_tab/{{current_league_id}}/{{current_season_game_unique_id}}"  target="_blank">
                <span class="pull-right label label-success ">Manual Cron</span>
            </a>


        </div>
        <div class="table-responsive" style="overflow:auto;">
            <table class="table table-hover table-striped table-bordered">
                <thead>
                    <tr ng-if="season_stats.length>0">
                        <th>
                            Player Name
                        </th>
                        <th>
                            Position
                        </th>
                        <th>
                            Team
                        </th>
                        <th>
                            Scoring Type
                        </th>
                        <th>
                            Event
                        </th>
                        <th>
                            Score
                        </th>
                        <!--  <th>
                            Minute
                        </th> -->
                        <th>
                            Action
                        </th>
                    </tr>
                    <tr ng-if="season_stats.length==0">
                        <td align="center" colspan="7">No Season Stats Found.</td>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="stats in season_stats">
                        <td ng-bind="::stats.player_name"> </td>
                        <td ng-bind="::stats.player_position"> </td>
                        <td ng-bind="::stats.player_team_name"> </td>
                        <td ng-bind="::stats.scoring_category_name"> </td>
                        <td ng-bind="::stats.stats_key"> </td>
                        <td ng-bind="::stats.stats_value"> </td>
                        <!-- <td ng-bind="::stats.minute"> </td> -->
                        <td>
                            <a href="javascript:void(0);" ng-click="showEditStatisticsPopup(stats)">
                                <i class="fa fa-pencil-square" title="Edit Statistics"></i>
                            </a>&nbsp;
                            <a href="javascript:void(0);" ng-click="showConfirmPopUp(stats)">
                                <i class="fa fa-trash" title="Delete Statistics"></i>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="table-footer" ng-if="seasonStatsParam.total_items>10">
            <pagination boundary-links="true" total-items="seasonStatsParam.total_items" ng-model="seasonStatsParam.current_page" ng-change="getSeasonStatisticsFeedList()" items-per-page="seasonStatsParam.items_perpage" class="pagination" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></pagination>
        </div>
    </div>
    <!-- /table with footer -->
</div>
<!-- /Page content-->
<!-- Delete Confirmation modal END -->
<div id="delete_confirm_model" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 class="modal-title">Delete Statistics</h5>
            </div>
            <div class="modal-body has-padding">
                <div class="form-group">
                    <h4>Are you sure to delete this statistics data?</h4>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary" ng-click="deleteStatistics();"><i class=""></i>Yes</button>
            </div>
        </div>
    </div>
</div>
<!-- Delete Confirmation modal END -->
<!-- Add Statistics Popup START -->
<div id="add_statistics_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 class="modal-title">Add Statistics</h5>
            </div>
            <form role="form">
                <div class="modal-body has-padding">
                    <div style="max-height:350px; overflow:auto;">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th width="15%">Team Name</th>
                                        <th width="20%">Player Name</th>
                                        <th width="20%">Scoring Type</th>
                                        <th width="20%">Event</th>
                                        <th width="10%">Score</th>
                                        <!-- <th width="10%">Minutes</th> -->
                                        <th width="5%"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="statistic in statistics_data.statistics track by $index">
                                        <td class="text-center">
                                            <select data-placeholder="Select Team" id="team_id_{{$index}}" ng-options="team.team_id as team.team_name+' ('+team.team_abbr+')' for team in teams track by team.team_id" ng-model="statistic.team_id" ng-change="onChangeTeam($index)" class="select-full" select-two="minimumResultsForSearch:'2',width:'100%'">
                                                <option value=""></option>
                                            </select>
                                            <!--                                                                                <label for="team_id_{{$index}}" class="error hide" id="team_id_{{$index}}_error"></label>-->
                                        </td>
                                        <td class="text-center">
                                            <!--ng-options="player.player_unique_id as player.full_name for player in players track by player.player_unique_id  | filter:{team_id: statistic.team_id}" -->
                                            <!-- <select data-placeholder="Select Player" id="player_id_{{$index}}" class="select-full" ng-options="player.player_unique_id as player.full_name+' ('+ player.position+')' for player in players  | filter:{team_id:statistic.team_id}:strict " ng-model="statistic.player_unique_id" select-two="minimumResultsForSearch:'2',width:'100%',vasl:'{{$index}}'" ng-change="selectPosition(player.position)" > -->
                                            <select data-placeholder="Select Player" id="player_id_{{$index}}" class="select-full" ng-model="statistic.player_unique_id" select-two="minimumResultsForSearch:'2',width:'100%',vasl:'{{$index}}'" ng-change="selectPosition($index)">
                                                <option value=""></option>
                                                <option ng-repeat="player in players  | filter:{team_id:statistic.team_id}:strict" value="{{player.player_unique_id}}" data-val="{{player.position}}" ng-bind="player.full_name+' ('+ player.position+')'"></option>
                                            </select>
                                            <label for="player_id_{{$index}}" class="error hide" id="player_id_{{$index}}_error"></label>
                                        </td>
                                        <td class="text-center">
                                            <select data-placeholder="Select Scoring Type" id="scoring_category_{{$index}}" ng-options="scKey as scategory  for (scKey,scategory) in scoring_category track by scKey" class="select-full" ng-model="statistic.scoring_type" ng-change="onChangeScoringCategory($index,statistic.scoring_type)" select-two="minimumResultsForSearch:'2',width:'100%'">
                                                <option value=""></option>
                                            </select>
                                            <label for="scoring_category_{{$index}}" class="error hide" id="scoring_category_{{$index}}_error"></label>
                                        </td>
                                        <td class="text-center">
                                            <!-- ng-options="srule.stats_key as srule.stats_key for srule in scoring_rule track by srule.stats_key" -->
                                            <select data-placeholder="Select Event" id="scoring_rule_{{$index}}" class="select-full" ng-model="statistic.stats_key" select-two="minimumResultsForSearch:'2',width:'100%'">
                                                <option value=""></option>
                                                <option ng-repeat="(stats_key,stats_val) in rules[$index] " value="{{stats_key}}" ng-bind="stats_val"></option>
                                                <!-- <option ng-repeat="srule in scoring_rule  | filter:{master_scoring_category_id:statistic.master_scoring_category_id}:strict | unique:'stats_key'" value="{{srule.stats_key}}" ng-bind="srule.stats_key"></option> -->
                                            </select>
                                            <label for="scoring_rule_{{$index}}" class="error hide" id="scoring_rule_{{$index}}_error"></label>
                                        </td>
                                        <td class="text-center">
                                            <input type="text" placeholder="Score" id="score_{{$index}}" name="score" stats-value maxlength="4" ng-model="statistic.stats_value" class="form-control">
                                            <label for="score_{{$index}}" class="error hide" id="score_{{$index}}_error"></label>
                                        </td>
                                        <!--   <td class="text-center">
                                            <input type="text" placeholder="Minute" id="minute_{{$index}}" name="minute" ng-model="statistic.minute" class="form-control">
                                            <label for="minute_{{$index}}" class="error hide" id="minute_{{$index}}_error"></label>
                                        </td> -->
                                        <td class="text-center">
                                            <a ng-show="statistics_data.statistics.length > 1" ng-click="RemoveStatisticRow($index)">
                                                <i class="fa fa-times" title="Remove"></i>
                                            </a>
                                            <input type="hidden" id="row_{{$index}}" name="row" ng-model="statistic.row" ng-init="statistic.row = $index;">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <tbody>
                                <tr>
                                    <td>
                                        <a href="javascript:void(0);" ng-click="AddStatisticRow()">
                                            <span class="label label-info">Add More</span>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" ng-click="SaveStatistics();">Save</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Add Statistics Popup END -->
<!-- Edit Statistics Popup START -->
<div id="edit_statistics_modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 class="modal-title">Edit Statistics</h5>
            </div>
            <form role="form">
                <div class="modal-body has-padding">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 control-label" for="team_id">Team Name <span class="mandatory">*</span></label>
                            <div class="col-md-9">
                                <select data-placeholder="Select Team" id="team_id" ng-model="edit_stats_data.team_id" class="select-full" select-two="minimumResultsForSearch:'2',width:'100%'">
                                    <option ng-if="edit_stats_data.team_id" value="{{edit_stats_data.team_id}}" ng-bind="edit_stats_data.player_team_name+' ('+edit_stats_data.player_team_abbr+')'"></option>
                                </select>
                                <label for="team_id" class="error hide" id="team_id_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 control-label" for="player_id">Player Name <span class="mandatory">*</span></label>
                            <div class="col-md-9">
                                <select data-placeholder="Select Player" id="player_id" class="select-full" ng-model="edit_stats_data.player_unique_id" select-two="minimumResultsForSearch:'2',width:'100%',vasl:'{{$index}}'">
                                    <option ng-if="edit_stats_data.player_unique_id" value="{{edit_stats_data.player_unique_id}}" ng-bind="edit_stats_data.player_name+' ('+ edit_stats_data.player_position+')'"></option>
                                </select>
                                <label for="player_id" class="error hide" id="player_id_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 control-label" for="scoring_category">Scoring Type <span class="mandatory">*</span></label>
                            <div class="col-md-9">
                                <select data-placeholder="Select Scoring Type" id="scoring_category" class="select-full" ng-model="edit_stats_data.scoring_type" select-two="minimumResultsForSearch:'2',width:'100%'">
                                    <option ng-if="edit_stats_data.scoring_type" value="{{edit_stats_data.scoring_type}}" ng-bind="edit_stats_data.scoring_type "></option>
                                </select>
                                <label for="scoring_category" class="error hide" id="scoring_category_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 control-label" for="scoring_rule">Event <span class="mandatory">*</span></label>
                            <div class="col-md-9">
                                <select data-placeholder="Select Event" id="scoring_rule" class="select-full" ng-model="edit_stats_data.stats_key" select-two="minimumResultsForSearch:'2',width:'100%'">
                                    <option ng-if="edit_stats_data.stats_key" value="{{edit_stats_data.stats_key}}" ng-bind="edit_stats_data.stats_key"></option>
                                </select>
                                <label for="scoring_rule" class="error hide" id="scoring_rule_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 control-label" for="score">Score <span class="mandatory">*</span></label>
                            <div class="col-md-9">
                                <input type="text" placeholder="Score" id="score" name="score" stats-value  maxlength="4" ng-model="edit_stats_data.stats_value" class="form-control">
                                <label for="score" class="error hide" id="score_error"></label>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <div class="row">
                            <label class="col-md-3 control-label" for="minute">Minute </label>
                            <div class="col-md-9">
                                <input type="text" placeholder="Minute" id="minute" name="minute" ng-model="edit_stats_data.minute" class="form-control">
                                <label for="minute" class="error hide" id="minute_error"></label>
                            </div>
                        </div>
                    </div> -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" ng-click="UpdateStatistics();">Save</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Edit Statistics Popup END -->
