<div class="page-content">
	<!-- Game Detail Section Start -->
	<div class="col-md-12">
		<!-- Page title -->
		<div class="page-title">
			<h5><i class="fa fa-bars"></i>Commishner Tool Kit</h5>
		</div>
		<button class="btn btn-success" ng-click="els.goToContest()"><i class=""></i>Back</button>
		<button class="btn btn-success" ng-click="els.goToLineup()"><i class=""></i>Lineup Details</button>
		<button class="btn btn-success" ng-click="els.goToCommishnerTool()" ><i class=""></i>Commisher Tools</button> 
		<!-- /page title -->
		
		<form class="form-horizontal" role="form" name="leaguesettings"  leaguesettings-form submit-leaguesettings="els.updateLeague()">
			<div class="panel panel-default" ng-init="els.get_game_details()">
				<div class="panel-heading">
					<h6 class="panel-title">League Name - {{els.gameDetail.game_name}} </h6>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<div class="col-md-6">
							<div class="row">
								<label class="col-md-4 control-label" for="date">Draft Date :<span class="mandatory">*</span> </label>
								<div class="col-md-8" ng-if="els.draft_settings">
									<input id="game_date" name="game_date" ng-model="els.contestParam.game_date" date-picker="minDate:'{{els.draft_settings.min_draft_date}}',maxDate: '{{els.draft_settings.max_draft_date}}'" class="form-control" on-select="els.setDate(date,'game_date')" readonly="true">
									<label for="game_date" class="error hide" id="game_date_error" ></label>
								</div>
							</div>
						</div>
						<div class="col-md-6" ng-show="els.contestParam.game_date">
							<div class="row">
								<label class="col-md-4 control-label" for="time" >Draft Time :<span class="mandatory">*</span></label>
								<div class="col-md-8">
									<input id="gametime" name="gametime" ng-change="els.updateTimePicker()" ng-model="els.contestParam.gametime" type="text" value="" class="form-control" init-time-picker />
									<label for="gametime" class="error hide" id="gametime_error"></label>
								</div>
							</div>
						</div>
					</div>
					<!-- <div class="form-group">
						<div class="col-md-6" ng-show="els.gameDetail.keeper_end_date !== null">
							<div class="row">
								<label class="col-md-4 control-label" for="date">Keeper End Date :<span class="mandatory">*</span> </label>
								<div class="col-md-8" ng-if="els.keeper_end_date">
									<input id="keeper_end_date" name="keeper_end_date" ng-model="els.contestParam.keeper_end_date" date-picker="minDate:'{{els.season_start_date}}',maxDate: '{{els.keeper_end_date}}'" class="form-control" on-select="els.setDate(date,'keeper_end_date')" readonly="true">
									<label for="keeper_end_date" class="error hide" id="keeper_end_date_error" ></label>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
								<label class="col-md-4 control-label" for="date">Trade Deadline Date :<span class="mandatory">*</span> </label>
								<div class="col-md-8" ng-if="els.trade_end_date">
									<input id="trade_end_date_picker" name="trade_end_date_picker" ng-model="els.contestParam.trade_end_date" date-picker="minDate:'{{els.trade_start_date}}',maxDate: '{{els.trade_end_date}}'" class="form-control" on-select="els.setDate(date,'trade_end_date')" readonly="true">
									<label for="trade_end_date_picker" class="error hide" id="trade_end_date_error" ></label>
								</div>
							</div>
						</div>
					</div> -->
					<div class="form-actions text-center">
						<div class="form-group">
							<button type="submit" class="btn btn-success"><i class=""></i>{{lang.update_contest}}</button>
						</div>
					</div>
				</div>
			</div>
		</form>		
	</div>
</div>		