
<div class="page-content">
	<!-- Game Detail Section Start -->
	<div class="col-md-12">
		<!-- Page title -->
		<div class="page-title">
			<h5><i class="fa fa-bars"></i>Commishner Tool Kit</h5>
		</div>
		<button class="btn btn-success" ng-click="goToContest()"><i class=""></i>Back</button>
		<button class="btn btn-success" ng-click="goToLineup()"><i class=""></i>Lineup Details</button>
		<button class="btn btn-success" ng-click="goToCommishnerTool()" ><i class=""></i>Commisher Tools</button>
		<!-- /page title -->
		
		<div class="panel panel-default" ng-init="getContestDetail()">
			<div class="panel-heading">
				<h6 class="panel-title">League Name - {{gameDetail.game_name}} </h6>
			</div>
			<div class="panel-body">
				<table class="table tool-table clear-border">
					<thead>
						<tr>
							<th>TOOL</th>
							<th>DESCRIPTION</th>
							<th>AVAILABLITY</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="pos col-cell-xs-3" data-title="TOOL">                                
								<a  ng-if="gameDetail.status =='LIVE'" href="edit-roster/{{game_unique_id}}">EDIT ROSTER </a> 
								<span ng-if="(gameDetail.status !='LIVE')">EDIT ROSTER </span> 
							</td>
							<td class="players col-cell-xs-6" data-title="DESCRIPTION">
								Edit roster for any team for any week.
							</td>
							<td class="opponent col-cell-xs-3" data-title="AVAILABLITY">
								Post Draft
							</td>                            
						</tr>
						<tr>
							<td class="pos col-cell-xs-3" data-title="TOOL">
								<a ng-if="gameDetail.status =='LIVE'" href="waiver-priority/{{game_unique_id}}">EDIT WAIVER PRIORITY</a> 
								<span ng-if="gameDetail.status!='LIVE'">EDIT WAIVER PRIORITY </span> 
							</td>
							<td class="players col-cell-xs-6" data-title="DESCRIPTION">
								Edit the waiver priority for any team.
							</td>
							<td class="opponent col-cell-xs-3" data-title="AVAILABLITY">
								Post Draft
							</td>                            
						</tr>
						<tr>
							<td class="pos col-cell-xs-3" data-title="TOOL">
								<a ng-if="gameDetail.status !='CANCELLED'" href="lock-teams/{{game_unique_id}}">LOCK TEAMS</a> 
								<span ng-if="gameDetail.status =='CANCELLED'">LOCK TEAMS </span> 
							</td>
							<td class="players col-cell-xs-6" data-title="DESCRIPTION">
							   Prevents any team from making any actions. 
							</td>
							<td class="opponent col-cell-xs-3" data-title="AVAILABLITY">
								All Season
							</td>                            
						</tr>
						 <tr ng-if="gameDetail.game_waiver_type == 'WAIVER_BUDGET'" >
                            <td class="pos col-cell-xs-3" data-title="TOOL">
                                <a  ng-if="gameDetail.status=='PRE-DRAFT'" href="acquisition-budget/{{game_unique_id}}">EDIT WAIVER BUDGET </a> 
                                <span ng-if="gameDetail.status!='PRE-DRAFT'">EDIT WAIVER BUDGET </span> 
                            </td>
                            <td class="players col-cell-xs-6" data-title="DESCRIPTION">
                                -------
                            </td>
                            <td class="opponent col-cell-xs-3" data-title="AVAILABLITY">
                                Pre-Draft
                            </td>                            
                        </tr>
                        <tr>
                            <td class="pos col-cell-xs-3" data-title="TOOL">
                                <a ng-if="gameDetail.status =='PRE-DRAFT'" href="edit-league-settings/{{game_unique_id}}">EDIT LEAGUE SETTINGS</a> 
                                <span ng-if="gameDetail.status!='PRE-DRAFT'">EDIT LEAGUE SETTINGS </span> 
                            </td>
                            <td class="players col-cell-xs-6" data-title="DESCRIPTION">
                               Change draft date settings for a league.
                            </td>
                            <td class="opponent col-cell-xs-3" data-title="AVAILABLITY">
                                Pre Draft
                            </td>                            
                        </tr> 
					</tbody>
				</table>
			</div>
		</div>		
	</div>
</div>