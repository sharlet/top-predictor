<div class="page-content">
    <!-- Game Detail Section Start -->
    <div class="col-md-12">
        <!-- Page title -->
        <div class="page-title">
            <h5><i class="fa fa-bars"></i>Commishner Tool Kit</h5>
        </div>
        <button class="btn btn-success" ng-click="goToContest()"><i class=""></i>Back</button>
        <button class="btn btn-success" ng-click="goToLineup()"><i class=""></i>Lineup Details</button>
        <button class="btn btn-success" ng-click="goToCommishnerTool()" ><i class=""></i>Commisher Tools</button> 
     </div>
</div> 
<div class="container-fluid short-description short-description-sm">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-heading">WAIVER PRIORITY: <span ng-if="gameDetail.game_name" ng-bind="gameDetail.game_name"></span></h3>
            </div>
        </div>
    </div>
</div>
<section class="col-wrapper">
    <div class="container">
        <div class="panel-container">
            <div class="inner-wrapper drafting-order-wrapper commissioner-wrapper">                        
                <div class="panel">
                    <div class="drafting-order">
                        <ul class="head-ul">
                            <li>
                                PRIORITY#
                            </li>
                            <li>
                                TEAM NAME
                            </li>
                        </ul>
                        <ul id="sortable">
                            <li ng-repeat="priority in priority_list">
                                <div class="num_span" data-id="{{$index + 1}}">{{$index + 1}}</div>
                                <div class="profile_span">
                                    <figure class="profile-pic-xs">
                                        <img class="img-circle" src="../{{priority.team_logo}}" alt="">
                                    </figure> {{priority.team_name}}
                                </div>
                                <div class="drag_span">
                                    <svg class="icon-drag-pointer" viewBox="0 0 16 16">
                                        <use xmlns:xlink="https://www.w3.org/1999/xlink" xlink:href="../assets/img/svg-sprit.svg#drag_pointer"></use>
                                    </svg>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="btn-wrapper clearfix"> 
                        <p></p>
                        <div class="col-xs-12 form-group">
                            <a href="#" class="btn btn-success" ng-click="updateDraftOrder();">UPDATE PRIORITY</a>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
