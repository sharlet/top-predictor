<div class="page-content">
    <!-- Game Detail Section Start -->
    <div class="col-md-12">
        <!-- Page title -->
        <div class="page-title">
            <h5><i class="fa fa-bars"></i>Commishner Tool Kit</h5>
        </div>
        <button class="btn btn-success" ng-click="goToContest()"><i class=""></i>Back</button>
        <button class="btn btn-success" ng-click="goToLineup()"><i class=""></i>Lineup Details</button>
        <button class="btn btn-success" ng-click="goToCommishnerTool()" ><i class=""></i>Commisher Tools</button> 
     </div>
</div>       
<div class="container-fluid short-description short-description-sm">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-heading">Edit Roster: <span ng-if="gameDetail.game_name" ng-bind="gameDetail.game_name"></span></h3>
            </div>
        </div>
    </div>
</div>
<!-- Secondry Navigation End -->
<div class="container league-invite top-gutter">
    <div class="details-card">
            <div class="detailscard-header display-block">
                <div class="row"><!--data-toggle="buttons"-->
                     <div class="team-option-wrapper">
                        <div class="col-sm-3" data-ng-repeat="teams in teamList" data-ng-click="getUserTeamRoster(teams);">
                            <div class="profile-pic-name-wrap team-option-block">
                                <input type="radio" class="custom-team-radio" name="team" id="active_{{teams.lineup_master_id}}">
                                <div class="team-option-bar">
                                    <div class="cell-profile-pic">
                                        <figure class="profile-pic-sm">
                                             <img ng-src="../{{teams.team_logo}}" class="img-circle">
                                        </figure>
                                    </div>
                                    <div class="cell-profile-name">
                                        <span data-ng-bind="::teams.team_name"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        
    <div class="row flex-layout sm xs" ng-controller="playerCardCtrl as pc" >                
        <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12 left-block" >
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="panel-body-pd-zero">
                        <div class=" mobileContent active" id="players">
                            <div class="details-card">
                                <div class="detailscard-subheader">
                                    <div class="left-nav convertDropdown" ng-init="newText = 'All'">
                                        <a href="javascript:void(0);" class="hidden-lg">Filter By <span>{{newText}}</span> <svg class="icon-downarrow icon-dark"><use xlink:href="../assets/img/svg-sprit.svg#downarrow"></use></svg></a>
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li ng-class="('all'==activeTab)?'active':''" ng-click="searchbyposition();">
                                                <a href="javascript:void(0);" aria-controls="all" role="tab" data-toggle="tab" ng-click="newText ='All';searchbyposition();">All</a>
                                            </li>
                                            <li data-ng-repeat="positions in tabPosition" ng-class="(positions.position==activeTab)?'active':''">
                                                <a href="javascript:void(0);" aria-controls="{{positions.position}}" role="tab" ng-bind="positions.position" ng-click="newText ='{{positions.position}}';searchbyposition(positions);"></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="right-nav">
                                       <!--  <a href="javascript:void(0);" class="hidden-xs hidden-sm" data-ng-click="downloadReports();">
                                            <svg class="icon-csv icon-dark">
                                                <use xlink:href="assets/img/svg-sprit.svg#csv"></use>
                                            </svg> Download Players List</a> -->
                                        <a href="#searchFieldHeader" data-toggle="collapse" class="visible-xs btn-in-xs">
                                            <svg class="icon-svg" viewBox="0 0 92 91">
                                                <use xlink:href="../assets/img/svg-sprit.svg#search"></use>
                                            </svg>
                                        </a>
                                        <!-- <a href="#mobiChat" data-toggle="collapse" class="visible-xs btn-in-xs">
                                            <svg class="icon-svg" viewBox="0 0 80 77">
                                                <use xlink:href="assets/img/svg-sprit.svg#chat"></use>
                                            </svg>
                                        </a> -->
                                        <div class="form-group search-input col-xs-6 " id="searchFieldHeader">
                                            <input type="text" class="form-control search-control" placeholder="Player Search" ng-model="rosterParam.search_text" ng-keyup="searchByName();" />
                                            <span class="form-control-feedback">
                                            <svg class="icon-svg-xs icon-search"  viewBox="0 0 92 91">
                                            <use xlink:href="../assets/img/svg-sprit.svg#search"></use>
                                        </svg>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-content" >
                                <!-- <div class="loader" style="background:rgba(0,0,0,0.4); width:100%; border-radius:0; z-index:9; height:100%;position:absolute; text-align: center; left:0; right:0; margin:0 auto;"><img style="position:absolute; top:49%;" src="assets/img/select2-spinner.gif" /></div> -->
                                    <!-- All player list begins here-->
                                    <div role="tabpanel" class="tab-pane active" id="all" >
                                        <div class="fixed-table" ng-init="pc.playerDetailPage = 'edit-roster';" >
                                            <div class="player-list-table"  when-scrolled="getPlayerRoster()">
                                                <table class="table table-fixed-header table-secondary">
                                                    <thead class="header">
                                                        <tr>                                                                    
                                                            <th>RANK</th>
                                                            <th>POS</th>
                                                            <th>PLAYER</th>
                                                            <th>BYE</th>
                                                            <th class="hidden-xs">FPPG</th>
                                                            <th class="hidden-xs">DATE & TIME</th>
                                                            <th class="hidden-xs">GAME</th>
                                                            <th>&nbsp;</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr ng-repeat="list in playerList">
                                                            
                                                            <td>{{list.rank_number}}</td>
                                                            <td>{{list.position}}</td>
                                                            <td>
                                                                <figure class="profile-pic-xs hidden-sm hidden-xs">
                                                                     <img ng-if="list.img_url" ng-src="{{list.img_url}}" class="img-circle" alt="">
                                                                    <img ng-if="!list.img_url" ng-src="../assets/img/default-player.jpg" class="img-circle" alt="">
                                                                </figure>
                                                                <span><a href="javaScript:void(0)" data-toggle="modal" data-ng-bind="::list.full_name" ng-click="pc.playerDetailPopup(list.player_unique_id)"></a> 
                                                               <span class="indicator" ng-show="{{list.injured}}">
                                                    <svg class="icon-plusxs icon-white" ng-click="addPlayer();">
                                                        <use xlink:href="../assets/img/svg-sprit.svg#plusXs"></use>
                                                    </svg>
                                                </span></span>
                                                            </td>
                                                            <td>{{list.bye_week}}</td>
                                                            <td class="hidden-xs">{{list.fppg}}</td>
                                                            <td class="hidden-xs">{{list.season_scheduled_date}}</td>
                                                             <td class="hidden-xs">
                                                <span ng-class="{'primary-color':list.team_abbreviation == list.away}">{{::list.away}}</span> @ 
                                                <span ng-class="{'primary-color':list.team_abbreviation == list.home}">{{::list.home}}</span>
                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0);" class="btn btn-circle btn-sm-icon">
                                                                    <svg class="icon-plusSm" ng-click="addPlayer(list);">
                                                                        <use xlink:href="../assets/img/svg-sprit.svg#plusSm"></use>
                                                                    </svg>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        <tr ng-if="!playerList.length"><td colspan="8" style="text-align:center;">No result found.</td></tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- All player list ends here -->    
                                                                         
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12 right-block">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="panel-body-pd-zero ">
                        <div class="league-member-list">
                            <div class="detailscard-subheader border-bottom">
                                <div class="h6" data-ng-bind="teamDetail.team_name"></div>                                       
                                
                            </div>
                            <div class="fixed-table">
                                <div class="player-list-table small-height">
                                    <table id="#sortable" class="table table-fixed-header  table-secondary">
                                        <thead class="header">
                                            <tr>
                                                <th style="width: 70px;">POS</th>
                                                <th style="width: 266px;">PLAYER</th>
                                                <th style="width: 69px;">BYE</th>
                                                <th class="hidden-xs" style="width: 83px;">FPPG</th>
                                                <th style="width: 70px;">&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody >
                                            <tr ng-repeat="(key, value) in myLineupDetails">
                                                <td>
                                                     {{value.display_position}}
                                                </td>
                                                <td>
                                                   <span>
                                                       <a href="javaScript:void(0)" data-toggle="modal" data-ng-bind="::value[value.display_position].full_name" ng-click="pc.playerDetailPopup(value[value.display_position].player_unique_id)"></a> 
                                                   </span>
                                                </td>
                                                <td class="ng-binding" data-ng-bind="::value[value.display_position].bye_week"></td>
                                                <td class="hidden-xs" data-ng-bind="::value[value.display_position].fppg"></td>
                                                <td>
                                                    <a href="javascript:void(0);" ng-if="value[value.display_position].player_unique_id" class="btn btn-circle btn-sm-icon btn-sm-icon-minus" ng-click="removeTeamRoster(value);">
                                                        <svg class="icon-svg icon-minusSm">
                                                            <use xmlns:xlink="https://www.w3.org/1999/xlink" xlink:href="../assets/img/svg-sprit.svg#minusSm"></use>
                                                        </svg>
                                                    </a>
                                                </td>
                                            </tr>
                                            
                                            
                                        </tbody>
                                    </table>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="clearfix p">&nbsp;</div>
    <!-- <div class="row">
        <div class="col-xs-12 ">
            <a href="javascript:void(0);" class="btn btn-default">UPDATE</a>
            <a href="league-summary/{{game_unique_id}}" class="btn btn-primary">CANCEL</a>
        </div>
    </div> -->
</div>