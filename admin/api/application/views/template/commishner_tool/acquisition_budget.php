<div class="page-content">
	<!-- Game Detail Section Start -->
	<div class="col-md-12">
		<!-- Page title -->
		<div class="page-title">
			<h5><i class="fa fa-bars"></i>Commishner Tool Kit</h5>
		</div>
		<button class="btn btn-success" ng-click="goToContest()"><i class=""></i>Back</button>
		<button class="btn btn-success" ng-click="goToLineup()"><i class=""></i>Lineup Details</button>
		 <button class="btn btn-success" ng-click="goToCommishnerTool()" ><i class=""></i>Commisher Tools</button> 
		<!-- /page title -->
		
		<div class="panel panel-default" ng-init="getGameAcquisitionBudget()">
			<div class="panel-heading">
				<h6 class="panel-title">League Name - {{gameDetail.game_name}} </h6>
			</div>
			<div class="panel-body">
				<div class="row">
                    <div class="col-sm-5 col-sm-offset-4">
                        <h3 class="h3 lead-heading">
                            Current Budget - <b class="primary-color"> ${{gameBudget}}</b> 
                            <!-- <a href="#" class="btn btn-link">Update</a> -->
                        </h3>
                        <div class="clearfix h1">&nbsp;</div>
                        <form class="form-inline">
                          <div class="form-group">
                            <label for="exampleInputName2">Enter New Budget &nbsp;&nbsp;&nbsp; $</label>
                             <select id="exampleInputName2" class="form-control" ng-model="acqBudget" select-two="minimumResultsForSearch:'-2'" ng-model="acqBudget" >
                                <option value="">Select Budget</option>
                                <option value="1000">1000</option>
                                <option value="2000">2000</option>
                                <option value="3000">3000</option>
                            </select> 
                          </div>
                        </form>
                        <div class="clearfix h1">&nbsp;</div>
                        <div class=" mBZero">
                            <div class="row">
                                <div class="type-list-primary lock-list-primary">
                                    <div class="col-xs-12 ">
                                        <a href="javascript:void(0);" class="btn btn-default" ng-click="updateAcqBudget();" ng-disabled="!acqBudget">SUBMIT</a>
                                        <a  href="league-summary/{{game_unique_id}}" class="btn btn-primary">CANCEL</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>		
	</div>
</div>