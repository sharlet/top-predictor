<div class="page-content">
	<!-- Game Detail Section Start -->
	<div class="col-md-12">
		<!-- Page title -->
		<div class="page-title">
			<h5><i class="fa fa-bars"></i>Commishner Tool Kit</h5>
		</div>
		<button class="btn btn-success" ng-click="goToContest()"><i class=""></i>Back</button>
		<button class="btn btn-success" ng-click="goToLineup()"><i class=""></i>Lineup Details</button>
		 <button class="btn btn-success" ng-click="goToCommishnerTool()" ><i class=""></i>Commisher Tools</button> 
		<!-- /page title -->
		
		<div class="panel panel-default" ng-init="getGameTeam()">
			<div class="panel-heading">
				<h6 class="panel-title">League Name - {{gameDetail.game_name}} </h6>
			</div>
			<div class="panel-body">
				 <div class="card-head">
									<h1 class="lead-heading pull-left">Lock Teams</h1>
								</div>
								<div class=" mBZero">
									<div class="row">
									<div class="type-list-primary lock-list-primary">
										<table class="table tool-table clear-border">
											<thead>
												<tr>
													<th>TEAM</th>
													<th>MESSAGE <br />BOARDS/POSTING</th>
													<th>TRADES</th>
													<th>ADD/DROP<br />PLAYERS</th>
													<th>ROSTER <br />CHANGES</th>
												   <!--  <th>TEAM <br />NAME</th>
													<th>EMAIL LEAGUE</th> -->
												</tr>
											</thead>
											<tbody>
												<tr data-ng-repeat="list in teamList">
													<td class="pos col-cell-xs-12" data-title="TEAM">
														<div class="use-img">
															<figure class="profile-pic-xs">
																<img ng-src="../{{::list.team_logo}}" class="img-circle">
															</figure>
															<span data-ng-bind="list.team_name"></span>
														</div>
													</td>
													<td class="pos col-cell-xs-4" data-title="MESSAGE BOARDS/POSTING">
														<label class="checkbox-inline inverse">
															<input type="checkbox" value="{{list.recent_message}}" class="chk-bx recent_message_{{list.lineup_master_id}}" rel="{{list.lineup_master_id}}" ng-click="updateTeamSetting($index,'recent_message');" name="recent_message"/>
															<span></span>
														</label>
													</td>
													<td class="pos col-cell-xs-4" data-title="TRADES">
														<label class="checkbox-inline inverse">
															<input type="checkbox" value="{{list.trade}}" class="chk-bx trade_{{list.lineup_master_id}}" rel="{{list.lineup_master_id}}" ng-click="updateTeamSetting($index,'trade');" name="trade"/>
															<span></span>
														</label>
													</td>
													<td class="pos col-cell-xs-4 " data-title="ADD/DROP PLAYERS">
														<label class="checkbox-inline inverse">
															<input type="checkbox" value="{{list.transaction}}" class="chk-bx transaction_{{list.lineup_master_id}}" rel="{{list.lineup_master_id}}" ng-click="updateTeamSetting($index,'transaction');" name="transaction" />
															<span></span>
														</label>
													</td>
													<td class="pos col-cell-xs-4 " data-title="ROSTER/CHANGES">
														<label class="checkbox-inline inverse">
															<input type="checkbox" value="{{list.roster_change}}" class="chk-bx roster_change_{{list.lineup_master_id}}" rel="{{list.lineup_master_id}}" ng-click="updateTeamSetting($index,'roster_change');" name="roster_change"/>
															<span></span>
														</label>
													</td>
													<!-- <td class="pos col-cell-xs-4" data-title="TEAM NAME">
														<label class="checkbox-inline inverse">
															<input type="checkbox">
															<span></span>
														</label>
													</td>
													<td class="pos col-cell-xs-4" data-title="TRADES">
														<label class="checkbox-inline inverse">
															<input type="checkbox">
															<span></span>
														</label>
													</td>
													<td class="pos col-cell-xs-4" data-title="EMAIL LEAGUE">
														<label class="checkbox-inline inverse">
															<input type="checkbox">
															<span></span>
														</label>
													</td> -->
												</tr>
												
											</tbody>
										</table>
										</div>
										<p>&nbsp;</p>
										<div class="col-xs-12 form-group">
											<a href="javascript:void(0);" class="btn btn-default" ng-click="updateTeamLock();">SUBMIT</a>
											<a href="javascript:void(0);" ng-click="goToCommishnerTool()" class="btn btn-primary">CANCEL</a>
										</div>
									</div>
								</div>
			</div>
		</div>		
	</div>
</div>