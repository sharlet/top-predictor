<!--Page content -->
<div class="page-content">
	<!-- Page title -->
	<div class="page-title">
		<h5><i class="fa fa-bars"></i>Season Stats</h5>
	</div>	
	<!-- Table with footer -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h6 class="panel-title">Season Stats List <span class="" ng-if="summaries!=''"> ({{summaries}})</span> </h6>
		</div>
		<div class="table-responsive" ng-init="getSeasonStatsList()">
			<table class="table table-hover table-striped table-bordered table-check">
				<thead>
					<tr ng-if="season_stats.length>0">
						<th ng-repeat="stats in fields" ng-click="sortSeasonStatsList(key);">
						{{stats | removeUnderScore}}
						<i ng-class="(seasonStatsParam.sort_field==stats&&seasonStatsParam.sort_order=='DESC')?'fa-sort-desc':((seasonStatsParam.sort_field==stats&&seasonStatsParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
					</tr>
					<tr ng-if="season_stats.length==0">
						<td align="center">No Season Schedule</td>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="stats in season_stats">
						<td ng-repeat="fi in fields">{{stats[fi]}}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="table-footer" ng-if="seasonStatsParam.total_items>10">
			<pagination  boundary-links="true" total-items="seasonStatsParam.total_items" ng-model="seasonStatsParam.current_page" ng-change="getSeasonStatsList()" items-per-page="seasonStatsParam.items_perpage" class="pagination" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></pagination>
		</div>
	</div>
	<!-- /table with footer -->
</div>
<!-- /Page content-->
