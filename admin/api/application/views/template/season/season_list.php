<style>
    .ui-datepicker {
        z-index: 9999 !important;
    }
</style>
<!--Page content -->
<div class="page-content">
	<!-- Page title -->
	<div class="page-title">
		<h5><i class="fa fa-bars"></i>Manage Schedules</h5>
	</div>
	<!-- Table elements -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h6 class="panel-title">Filters</h6>
		</div>
		<div class="table-responsive">
			<table class="table table-striped table-bordered" ng-init="getFilterData()">
				<thead>
					<tr>
						<th>League</th>
						<th>Team</th>
						<th>Week</th>
						<th>Season Schedule</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-center" >	
							<select id="league_id" data-placeholder="Select league" ng-options="league.league_id as league.league_abbr for league in leagues track by league.league_id"  class="select-full" ng-model="seasonParam.league_id" data-ng-change="getAllTeam()" select-two="minimumResultsForSearch:'-2',width:'100%'">
								<option value=""></option>
							</select>
						</td>
						<td class="text-center">
							<select id="team_id" data-placeholder="All Teams" ng-options="team.team_id as team.team_name for team in teams track by team.team_id" class="select-full" ng-model="seasonParam.team_id" ng-change="getSeasonList()" select-two="minimumResultsForSearch:'-2',width:'100%'">
								<option value=""></option>
							</select>
						</td>
						<td class="text-center">
							<select id="week" data-placeholder="All Week" ng-options="week.week as week.season_week for week in weeks track by week.week" class="select-full" ng-model="seasonParam.week" data-ng-change="getSeasonList()" select-two="minimumResultsForSearch:'-2',width:'100%'">
								<option value=""></option>
							</select>
						</td>

				<!-- 		<td class="text-center">
							<select id="year" data-placeholder="All Year"  class="select-full" ng-model="seasonParam.year" data-ng-change="getSeasonList()" select-two="minimumResultsForSearch:'-2',width:'100%'">
								<option ng-repeat=" year in years" value="{{year}}" ng-bind="year"></option>
							</select>
						</td> -->

						<td  width="25%">
							<div class="col-sm-6">
								<input type="text" class="from-date form-control" name="from" date-picker-range="to-date" placeholder="From" ng-model="seasonParam.fromdate" readonly="">
							</div>
							<div class="col-sm-6">
								<input type="text" class="to-date form-control" name="to" date-picker-range placeholder="To" ng-model="seasonParam.todate" data-ng-change="getSeasonList()" readonly="">
							</div>
						</td>
					</tr>
					<tr>
						<td><a href="javascript:;" ng-click="initObject() "><span class="label label-info">Clear Filters</span></a></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- /table elements -->
	<!-- Table with footer -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h6 class="panel-title">Season Schedule</h6>
			                        <a href="javascript:void(0);" ng-click="showAddSeasonPopup()">
                            <span class="pull-right label label-success ">Add Game</span>
                        </a>
		</div>
		<div class="table-responsive" ng-init="initObject()">
			<table class="table table-hover table-striped table-bordered table-check">
				<thead>
					<tr ng-if="seasonList.length>0">
						<!-- <th width="15%">
							Season Game ID
						</th> -->
						<th class="pointer" ng-click="sortSeasonList('type');">
							Game Type
							<i ng-class="(seasonParam.sort_field=='type'&&seasonParam.sort_order=='DESC')?'fa-sort-desc':((seasonParam.sort_field=='type'&&seasonParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortSeasonList('home');">
							Home
							<i ng-class="(seasonParam.sort_field=='home'&&seasonParam.sort_order=='DESC')?'fa-sort-desc':((seasonParam.sort_field=='home'&&seasonParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortSeasonList('away');">
							Away
							<i ng-class="(seasonParam.sort_field=='away'&&seasonParam.sort_order=='DESC')?'fa-sort-desc':((seasonParam.sort_field=='away'&&seasonParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortSeasonList('season_scheduled_date');">
							Gameday
							<i ng-class="(seasonParam.sort_field=='season_scheduled_date'&&seasonParam.sort_order=='DESC')?'fa-sort-desc':((seasonParam.sort_field=='season_scheduled_date'&&seasonParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortSeasonList('year');">
							Season
							<i ng-class="(seasonParam.sort_field=='year'&&seasonParam.sort_order=='DESC')?'fa-sort-desc':((seasonParam.sort_field=='year'&&seasonParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortSeasonList('status');">
							Status
							<i ng-class="(seasonParam.sort_field=='status'&&seasonParam.sort_order=='DESC')?'fa-sort-desc':((seasonParam.sort_field=='status'&&seasonParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortSeasonList('week');">
							Week
							<i ng-class="(seasonParam.sort_field=='week'&&seasonParam.sort_order=='DESC')?'fa-sort-desc':((seasonParam.sort_field=='week'&&seasonParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th>
							Game Stats
						</th>
					</tr>
					<tr ng-if="seasonList.length==0">
						<td align="center">No Season Schedule</td>
					</tr>
				</thead>
				<tbody>
					<tr ng-if="seasonList.length>0" ng-repeat="season in seasonList">
						<!-- <td ng-bind="season.season_game_unique_id" align="left"></td> -->
						<td ng-bind="season.type"></td>
						<td ng-bind="season.home"></td>
						<td ng-bind="season.away"></td>
						<td ng-bind="season.season_scheduled_dates"></td>
						<td ng-bind="season.year"></td>
						<td ng-bind="season.status"></td>
						<td ng-bind="season.week"></td>
						<td>
							<!-- <a href="seasonstats/{{season.season_game_unique_id}}/{{seasonParam.league_id}}"> -->
							<div class="btn-group">
								<button class="btn btn-link btn-icon btn-xs tip" data-toggle="dropdown" data-hover="dropdown">
									<i class="fa fa-cogs"></i>
								</button>
								<button class="btn btn-link btn-icon btn-xs tip" data-toggle="dropdown" data-hover="dropdown">
									<span class="caret caret-split"></span>
								</button>
						<ul class="dropdown-menu dropdown-menu-right">
							<li>							
								<a href="statslist/{{season.season_game_unique_id}}/{{seasonParam.league_id}}">
									<span >Stats Detail</span>
								</a>
							</li>
							<li>
								<a href="projectedStatsList/{{season.season_game_unique_id}}/{{seasonParam.league_id}}">
									<span >Projected Stats Detail</span>
								</a>
							</li>
				
						</ul>
						</div>
						<div class="btn-group" > </div>
						 <a href="" class="btn btn-default btn-icon btn-xs tip pull-right " ng-click="showSeasonDelConfirmPopUp(season)"><i class="fa fa-trash-o"></i></a>
						
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="table-footer" ng-if="seasonParam.total_items>10">
			<pagination  boundary-links="true" total-items="seasonParam.total_items" ng-model="seasonParam.current_page" ng-change="getSeasonList()" items-per-page="seasonParam.items_perpage" class="pagination" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></pagination>
		</div>
	</div>
	<!-- /table with footer -->

	<!-- Create and Update week -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h6 class="panel-title">Update Season Week ( After inserting all the seasons, Please update season week ) </h6>
		</div>
		<div class="table-responsive">
			<table class="table table-striped table-bordered" ng-init="getFilterData()">
				<thead>
					<tr>
						<th>League</th>
						<th>Year</th>
						<th>Type</th>
						<th>Update Season Week </th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-center" >	
							<select id="league_id" data-placeholder="Select league"   class="select-full" ng-model="seasonWeek.league_id"  select-two="minimumResultsForSearch:'-2',width:'100%'">
								<option value=""></option>
								<option ng-repeat=" league in leagues" value="{{league.league_id}}" ng-bind="league.league_abbr"></option>
							</select>
						</td>
						<td class="text-center">
							<select id="year" data-placeholder="Select Year"  class="select-full" ng-model="seasonWeek.year" select-two="minimumResultsForSearch:'-2',width:'100%'">
								<option value=""></option>
								<option ng-repeat=" year in years" value="{{year}}" ng-bind="year"></option>
							</select>
						</td> 

	                    <td class="text-center">
	                            <select data-placeholder="Select Type" id="type" class="select-full" 
	                                    ng-model="seasonWeek.type" select-two="minimumResultsForSearch:'2',width:'100%'">
	                                    <option value=""></option>
	                                    <option ng-repeat="stype in season_type" value="{{stype}}"  ng-bind="stype"></option>
	                            </select>
	                    </td>
						<td  width="25%">
							<button type="button" class="btn btn-primary" ng-click="create_update_week()"> Update</button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>	
	<!-- End Create and Update week -->
</div>
<!-- /Page content-->

<!-- Delete Confirmation modal END -->
<div id="delete_confirm_model" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
                <div class="modal-content">
                        <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h5 class="modal-title">Delete Season</h5>
                        </div>
                        
                        <div class="modal-body has-padding">

                                <div class="form-group">
                                        <h4 >Are you sure to delete this season record?</h4>
                                </div>
                        </div>
                        <div class="modal-footer">
                                <button type="button" class="btn btn-warning" data-dismiss="modal" >No</button>
                                <button type="submit" class="btn btn-primary" ng-click="deleteSeason();"><i class=""></i>Yes</button>
                        </div>

                </div>
        </div>
</div>
<!-- Delete Confirmation modal END -->

<!-- Add Seasons Popup START -->
<div id="add_season_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 80%;">
                <div class="modal-content">
                        <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h5 class="modal-title">Add Seasons Schedules</h5>
                        </div>

                    <form role="form">
                                <div class="modal-body has-padding">
                                        <div style="max-height:350px; overflow:auto;">
                                                <div class="table-responsive">
													<table class="table ">
														<tr>
					                                        <td>Select League</td>
					                                        <td>
																	<select id="form_league_id" data-placeholder="Select league"   class="select-full" ng-model="seasonFormLeagueId" ng-change="getAddSeasonFormData()" ng-disabled="formleague" select-two="minimumResultsForSearch:'-2',width:'100%'">
																		<option value=""></option>
																		<option ng-repeat=" league in leagues" value="{{league.league_id}}" ng-bind="league.league_abbr"></option>
																	</select>
															</td>															
														</tr>
													</table>
                                                        <table class="table table-striped table-bordered">
                                                                <thead>
                                                                        <tr>
                                                                                <th width="10%">Season Year</th>
                                                                                <!-- <th width="10%">League</th> -->
                                                                                <th width="20%">Home Team</th>
                                                                                <th width="20%">Away Team</th>
                                                                                <th width="20%">Type</th>
                                                                                <th width="25%">Schedule Date</th>
                                                                                <th width="5%"></th>
                                                                        </tr>
                                                                </thead>
                                                                <tbody>
                                                                        <tr ng-repeat="statistic in season_data.seasons track by $index">
																				<td class="text-center">
																					<select id="year_{{$index}}" data-placeholder="Select Year" data-placeholder="All Year"  class="select-full" ng-model="statistic.year"  select-two="minimumResultsForSearch:'-2',width:'100%'">
																						<option value=""></option>
																						<option ng-repeat=" year in years" value="{{year}}" ng-bind="year"></option>
																					</select>
																				</td>  
																				
<!-- 																				<td class="text-center" >	
																					<select id="league_id" data-placeholder="Select league"   class="select-full" ng-model="statistic.league_id" ng-change="getAddSeasonFormData()" select-two="minimumResultsForSearch:'-2',width:'100%'">
																						<option value=""></option>
																						<option ng-repeat=" league in leagues" value="{{league.league_id}}" ng-bind="league.league_abbr"></option>
																					</select>
																				</td> -->

                                                                                <td class="text-center">
                                                                                        <select data-placeholder="Select Team" id="hteam_id_{{$index}}" ng-options="hteam.team_id as hteam.team_name+' ('+hteam.team_abbr+')' for hteam in home_teams track by hteam.team_id" 
                                                                                                ng-model="statistic.home_id" ng-change="onChangeHomeTeam($index);"  class="select-full home_teams" select-two="minimumResultsForSearch:'2',width:'100%'">
                                                                                                <option value=""></option>
                                                                                        </select>
                                                                                </td>

                                                                                <td class="text-center">
                                                                                    <!--ng-options="player.player_unique_id as player.full_name for player in players track by player.player_unique_id  | filter:{team_id: statistic.team_id}" -->
                                                                                        <select data-placeholder="Select Team" id="ateam_id_{{$index}}"  
                                                                                                ng-model="statistic.away_id" class="select-full away_teams" select-two="minimumResultsForSearch:'2',width:'100%'">
                                                                                                <option value=""></option>
                                                                                                <option ng-if="statistic.home_id != ateam.team_id" ng-repeat="ateam in away_teams" value="{{ateam.team_id}}"  ng-bind="ateam.team_name+' ('+ateam.team_abbr+')'"></option>
                                                                                        </select>
                                                                                </td>

                                                                                <td class="text-center">
                                                                                        <select data-placeholder="Select Type" id="type_{{$index}}" class="select-full" 
                                                                                                ng-model="statistic.type" select-two="minimumResultsForSearch:'2',width:'100%'">
                                                                                                <option value=""></option>
                                                                                                <option ng-repeat="stype in season_type" value="{{stype}}"  ng-bind="stype"></option>
                                                                                        </select>
                                                                                </td>
																				


                                                                                <td class="text-center">
                                                                                        <input type="text" placeholder="Season Schedule Date" id="scheduled_date_{{$index}}" name="scheduled_date[]" date-time-picker ng-model="statistic.scheduled_date" name="scheduled_date" class="form-control" readonly="">
                                                                                </td>

                                                                                <td class="text-center">
                                                                                    <a ng-show="season_data.seasons.length > 1" ng-click="RemoveSeasonRow($index)">
                                                                                        <i class="fa fa-times" title="Remove"></i>
                                                                                    </a>
                                                                                    <input type="hidden" id="row_{{$index}}" name="row" ng-model="statistic.row" ng-init="statistic.row = $index;">
                                                                                </td>
                                                                        </tr>
                                                                </tbody>
                                                        </table>
                                                </div>
                                        </div>
                                        <div class="table-responsive">
                                                <table class="table table-striped table-bordered">
                                                        <tbody>
                                                                <tr>
                                                                        <td >
                                                                                <a href="javascript:void(0);" ng-click="AddSeasonRow()">
                                                                                    <span class="label label-info">Add More</span>
                                                                                </a>
                                                                        </td>
                                                                </tr>
                                                        </tbody>
                                                </table>
                                        </div>
                                </div>

                                <div class="modal-footer">
                                        <button type="button" class="btn btn-success" ng-click="SaveSeason();">Save</button>
                                        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                                </div>
                        </form>
                </div>
        </div>
</div>
<!-- Add Seasons Popup END -->
