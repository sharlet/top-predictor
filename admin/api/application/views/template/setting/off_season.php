<!--Page content -->
<div class="page-content" ng-init="os.get_games()">
<div class="content">
	<!-- Page title -->
	<div class="page-title">
		<h5><i class="fa fa-bars"></i>Settings</h5>
	</div>
	<div class="row">
		<div class="col-md-12">
			<!-- <div class="col-md-6">
				<div class="table-responsive">
					<table class="table table-hover table-striped table-bordered" ng-show="os.gameListLength == 0">
						<thead>
							<tr>
								<td align="center">No Games</td>
							</tr>	
						</thead>
					</table>
					<table class="table table-hover table-striped table-bordered" ng-show="os.gameListLength > 0">
						<thead>
							<tr>
								<th class="pointer">
									Game Name
								</th>
								<th class="pointer">
									Game Status
								</th>
								<th class="pointer">
									User Name 
								</th>
								<th class="pointer">
									Entry Fee
								</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="game in os.gameList">
								<td data-ng-bind="game.game_name"></td>
								<td data-ng-bind="game.status"></td>
								<td data-ng-bind="game.full_name"></td>
								<td data-ng-bind="game.entry_fee"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div> -->
			<div class="col-md-6">
				<h6 ng-if="!os.off_season.show_off_season">Off Season not started till now.</h6>
				<form class="form-horizontal" role="form" ng-if="os.off_season.show_off_season" name="offseason"  offseason-form submit-offseason="os.sendNotifications()">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h6 class="panel-title">Off Season Settings</h6>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<div class="col-md-6">
									<label class="col-md-6 control-label" for="">Year :<span class="mandatory">*</span> </label>
									<div class="col-md-6">
										<select ng-model="os.contestParam.season_year" name="off_season_year" id="off_season_year" data-placeholder="Select season year" select-two="minimumResultsForSearch:'-2',width:'100%'" data-placeholder="{{lang.select_season_year}}" ng-change="os.OffSeasonChange()">
											<option></option>
											<option ng-repeat="year in [] | range:2017:2030" value="{{year}}" ng-bind="year"></option>
										</select>
										<label for="off_season_year" class="error hide" id="off_season_year_error" ></label>
									</div>
								</div>
								<div class="col-md-6" ng-if="os.contestParam.season_year" ng-cloak>
									<label class="col-md-6 control-label" for="">Off Season End Date :<span class="mandatory">*</span> </label>
									<div class="col-md-6">
										<input id="off_season_end_date" name="off_season_end_date" ng-model="os.contestParam.off_season_end_date" date-picker="minDate:'{{os.off_season.off_season_start_date}}',maxYear: '{{os.contestParam.season_year}}'" class="form-control" on-select="os.setDate(date,'off_season_end_date')" readonly="true">
										<label for="off_season_end_date" class="error hide" id="off_season_end_date_error" ></label>
									</div>
								</div>
								<!-- <div class="col-md-6">
									<label class="col-md-6 control-label" for="off_season_time">Off Season End Time :<span class="mandatory">*</span> </label>
									<div class="col-md-6">
										<input id="off_season_time" name="off_season_time" ng-model="os.contestParam.off_season_time" type="text" value="" class="form-control" init-time-picker />
										<label for="off_season_time" class="error hide" id="off_season_time_error"></label>
									</div>
								</div> -->
							</div>
							<div class="form-group">
								<div class="col-md-6">
									<label class="col-md-6 control-label" for="">Is current year :</label>
									<input type="checkbox" ng-model="os.contestParam.is_current_year" ng-checked="os.contestParam.is_current_year" name="is_current_year">
								</div>

								<div class="col-md-2">
									<div class="form-actions">
										<button type="submit" class="btn btn-success" ng-click="os.contestParam.action = 'save'">Save</button>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-actions">
										<button type="submit" class="btn btn-success" ng-click="os.contestParam.action = 'send'">Send Notifications to All</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- DEFAULT DRAFT MIN AND MAX per year setting for league-->
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">
				<form class="form-horizontal" role="form" ng-if="os.off_season.show_off_season" name="draft_settings"  draftdate-form submit-draftdate="os.saveDraftDate()">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h6 class="panel-title">Draft date Settings</h6>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<div class="col-md-6">
									<label class="col-md-6 control-label" for="">League :<span class="mandatory">*</span> </label>
									<div class="col-md-6">
										<select ng-model="os.draftParam.league_id" name="league_id" id="league_id" data-placeholder="Select League" select-two="minimumResultsForSearch:'-2',width:'100%'" data-placeholder="{{lang.select_league}}" ng-change="os.OnLeagueChange()">
											<option></option>
											<option ng-repeat="lg in os.leagues" value="{{lg.league_id}}" ng-bind="lg.league_abbr"></option>
										</select>
										<label for="league_id" class="error hide" id="league_id_error" ></label>
									</div>
								</div>
								<div class="col-md-6">
									<label class="col-md-4 control-label" for="">Year :<span class="mandatory">*</span> </label>
									<div class="col-md-8">
										<select ng-model="os.draftParam.season_year" name="season_year" id="season_year" data-placeholder="Select season year" select-two="minimumResultsForSearch:'-2',width:'100%'" data-placeholder="{{lang.select_season_year}}" ng-change="os.OnYearChange()">
											<option></option>
											<option ng-repeat="year in [] | range:2017:2030" value="{{year}}" ng-bind="year"></option>
										</select>
										<label for="season_year" class="error hide" id="season_year_error" ></label>
									</div>
								</div>
							</div>
							<div class="form-group" ng-if="os.draftParam.season_year" ng-cloak>
								<div class="col-md-6">
									<label class="col-md-6 control-label" for="">Min Draft Date :<span class="mandatory">*</span> </label>
									<div class="col-md-6">
										<input id="min_draft_date" name="min_draft_date" ng-model="os.draftParam.min_draft_date" date-picker="minDate:'{{os.min_draft_date}}',maxYear: '{{os.draftParam.season_year}}'" class="form-control" on-select="os.setDate(date,'min_draft_date')" readonly="true">
										<label for="min_draft_date" class="error hide" id="min_draft_date_error" ></label>
									</div>
								</div>
								<div class="col-md-6">
									<label class="col-md-6 control-label" for="off_season_time">Min Draft Time :<span class="mandatory">*</span> </label>
									<div class="col-md-6">
										<input id="min_draft_time" name="min_draft_time" ng-model="os.draftParam.min_draft_time" type="text" value="" class="form-control" init-time-picker />
										<label for="min_draft_time" class="error hide" id="min_draft_time_error"></label>
									</div>
								</div>
							</div>
							<div class="form-group" ng-if="os.draftParam.min_draft_date" ng-cloak>
								<div class="col-md-6">
									<label class="col-md-6 control-label" for="">Max Draft Date :<span class="mandatory">*</span> </label>
									<div class="col-md-6">
										<input id="max_draft_date" name="max_draft_date" ng-model="os.draftParam.max_draft_date" date-picker="minDate:'{{os.draftParam.max_draft_date}}',maxYear: '{{os.draftParam.season_year}}'" class="form-control" on-select="os.setDate(date,'max_draft_date')" readonly="true">
										<label for="max_draft_date" class="error hide" id="max_draft_date_error" ></label>
									</div>
								</div>
								<div class="col-md-6">
									<label class="col-md-6 control-label" for="off_season_time">Max Draft Time :<span class="mandatory">*</span> </label>
									<div class="col-md-6">
										<input id="max_draft_time" name="max_draft_time" ng-model="os.draftParam.max_draft_time" type="text" value="" class="form-control" init-time-picker />
										<label for="max_draft_time" class="error hide" id="max_draft_time_error"></label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-4">
									<div class="form-actions">
										<button type="submit" class="btn btn-success">Save Draft Date</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- DRAFT SETTING END-->
</div>
</div>
<!-- /Page content-->
