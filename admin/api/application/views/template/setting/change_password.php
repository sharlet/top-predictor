<!--Page content -->
<div class="page-content">
	<!-- Page title -->
	<div class="page-title">
		<h5><i class="fa fa-bars"></i>Settings</h5>
	</div>
	<!-- Change Password -->
	<form class="form-horizontal" role="form" changepassword-form submit-handle="changePassword()">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h6 class="panel-title">Change Password</h6>
			</div>
			<div class="panel-body">
				<div class="form-group">
					<label class="col-md-2 control-label" for="old_password">Old Password :<span class="mandatory">*</span> </label>
					<div class="col-md-5">
						<input type="password" id="old_password" name="old_password" ng-model="settingObj.old_password" class="form-control">
						<label for="old_password" class="error hide" id="old_password_error"></label>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label" for="new_password">New Password :<span class="mandatory">*</span> </label>
					<div class="col-md-5">
						<input type="password" id="new_password" name="new_password" ng-model="settingObj.new_password" class="form-control">
						<label for="new_password" class="error hide" id="new_password_error"></label>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label" for="confirm_password">Confirm Password :<span class="mandatory">*</span> </label>
					<div class="col-md-5">
						<input type="password" id="confirm_password" name="confirm_password" ng-model="settingObj.confirm_password" class="form-control">
						<label for="confirm_password" class="error hide" id="confirm_password_error"></label>
					</div>
				</div>
				<div class="form-actions text-left">
					<button type="submit" class="btn btn-success"><i class=""></i>Save</button>
				</div>
			</div>
		</div>
	</form>
	<!-- Change Password -->
</div>
<!-- /Page content-->
