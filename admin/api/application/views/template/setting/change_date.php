<!--Page content -->
<div class="page-content">
<div class="content">
	<!-- Page title -->
	<div class="page-title">
		<h5><i class="fa fa-bars"></i>Settings</h5>
	</div>
	<!-- Change Password -->
	<form class="form-horizontal" role="form">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h6 class="panel-title">Change Date</h6>
			</div>
			<div class="panel-body">
				<div class="form-group">
					<label class="col-md-2 control-label" for="date">Current Date :<span class="mandatory">*</span> </label>
					<div class="col-md-5">
						<label  class="form-control">
						<?php echo format_date();?>
						</label>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label" for="date">Date :<span class="mandatory">*</span> </label>
					<div class="col-md-5">
						<input type="datetime" id="date" name="date" ng-model="settingObj.date" class="form-control">						
					</div>
				</div>
				
				<div class="form-actions text-left">
					<button type="submit" class="btn btn-success" ng-click="changeDateTime()"><i class=""></i>Change Date</button>
				</div>
			</div>
		</div>
	</form>
	<!-- Change Password -->
</div>
</div>
<!-- /Page content-->
