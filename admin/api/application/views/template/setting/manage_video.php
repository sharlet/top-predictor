<!--Page content -->
<div class="page-content">
    <!-- Page title -->
    <div class="page-title">
        <h5><i class="fa fa-bars"></i>Manage Videos</h5>
    </div>
    <!-- Table elements -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h6 class="panel-title" data-ng-bind="'Filters'">
			</h6>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th data-ng-bind="'League'"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" ng-if="mvc.leagues">
                            <select data-placeholder="select league" id="league_id" ng-options="league.league_id as league.league_abbr for league in mvc.leagues track by league.league_id" class="select-full" ng-model="mvc.videoParam.league_id" select-two="minimumResultsForSearch:'2',width:'100%'" data-ng-change="mvc.get_league_video_list()">
                                <option value=""></option>
                            </select>
                        </td>
                    </tr>
                    <!-- <tr>
                        <td>
                            <a href="javascript:void(0);" ng-click="mvc.clearFilter()">
                                <span class="label label-info" data-ng-bind="'Reset Filters'"></span>
                            </a>
                        </td>
                    </tr> -->
                </tbody>
            </table>
        </div>
    </div>
    <!-- /table elements -->
    <!-- Table with footer -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h6 class="panel-title" data-ng-bind="'Video Management'"></h6>
            <a href="javascript:void(0);" ng-click="mvc.show_add_popup()">
                <span class="pull-right label label-success ">Add Video</span>
            </a>
        </div>
        <div class="table-responsive">
            <table class="table table-hover table-striped table-bordered">
                <thead>
                    <tr ng-iff="mvc.video_list.length>0">
                        <th class="pointer" ng-click="mvc.sortRosterList('video_type');">
                            Video Type
                            <i ng-class="(mvc.videoParam.sort_field=='video_type' && mvc.videoParam.sort_order=='DESC')?'fa-sort-desc':((mvc.videoParam.sort_field=='video_type'&&mvc.videoParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
                        </th>
                        <th class="pointer" ng-click="mvc.sortRosterList('video_title');">
                            Title
                            <i ng-class="(mvc.videoParam.sort_field=='video_title'&&mvc.videoParam.sort_order=='DESC')?'fa-sort-desc':((mvc.videoParam.sort_field=='video_title'&&mvc.videoParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
                        </th>
                        <th class="pointer" ng-clickk="mvc.sortRosterList('video_url');">
                            Link
                            <i ng-class="(mvc.videoParam.sort_field=='video_url'&&mvc.videoParam.sort_order=='DESC')?'fa-sort-desc':((mvc.videoParam.sort_field=='video_url'&&mvc.videoParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
                        </th>
                        <th class="pointer">
                            Status
                        </th>
                        <th class="pointer" ng-click="mvc.sortRosterList('last_updated');">
                           Updated Date
                            <i ng-class="(mvc.videoParam.sort_field=='last_updated'&&mvc.videoParam.sort_order=='DESC')?'fa-sort-desc':((mvc.videoParam.sort_field=='last_updated'&&mvc.videoParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
                        </th>
                        <th style="min-width: 85px">
                           Action
                        </th>
                    </tr>
                    <tr ng-if="mvc.video_list.length == 0">
                        <td align="center" data-ng-bind="'no video found'" colspan="5"></td>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-if="mvc.video_list.length>0" ng-repeat="video in mvc.video_list">
                        <td ng-bind="video.video_type"></td>
                        <td ng-bind="video.video_title_en"></td>
                        <td ng-bind="video.video_url"></td>
                        <td ng-bind="video.status == '1' ? 'ACTIVE': 'INACTIVE'"></td>
                        <td ng-bind="video.last_updated"></td>
                        <td>
                            <a href="" class="btn btn-default btn-icon btn-xs tip" ng-click="mvc.edit_vedio_popup(video)"><i class="fa fa-pencil-square"></i></a>
                            <a href="" class="btn btn-default btn-icon btn-xs tip" ng-click="mvc.remove_vedio_confirm(video)"><i class="fa fa-trash-o"></i></a>
                         </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="table-footer" ng-if="mvc.video_list.length>0">
            <pagination ng-if="mvc.videoParam.total_items>10" boundary-links="true" total-items="mvc.videoParam.total_items" ng-model="mvc.videoParam.current_page" ng-change="getPlayerList()" items-per-page="mvc.videoParam.items_perpage" class="pagination" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></pagination>
        </div>
    </div>
    <!-- /table with footer -->
</div>
<!-- /Page content-->

<!-- Add video Popup START -->
<div id="add_video_modal" class="modal fade" tabindex="-1" role="dialog" ng-if="mvc.add_video_case">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 class="modal-title">Add Video</h5>
            </div>
            <form role="form" addvideo-form submit-handle="mvc.add_video()">
                <div class="modal-body has-padding">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label" for="add_league_id">League <span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <select placeholder="Select League" id="add_league_id" name="add_league_id" class="select-full" ng-options="league.league_id as league.league_abbr for league in mvc.leagues track by league.league_id" ng-model="mvc.add_video_param.league_id" select-two="minimumResultsForSearch:'-2',width:'100%'">
                                    <option value=""></option>
                                </select>
                                <label for="add_league_id" class="error hide" id="add_league_id_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label" for="video_type">Video Type <span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <select data-placeholder="Select Type" id="video_type" name="video_type" class="select-full" ng-model="mvc.add_video_param.video_type" select-two="minimumResultsForSearch:'-2',width:'100%'">
                                    <option value=""></option>
                                    <option ng-repeat="type in mvc.video_types" value="{{type}}" ng-bind="type"></option>
                                </select>
                                <label for="video_type" class="error hide" id="video_type_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" ng-show="mvc.add_video_param.video_type == 'LOBBY'">
                        <div class="row">
                            <label class="col-md-4 control-label" for="video_order">Video Order <span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <select data-placeholder="Select Order" id="video_order" name="video_order" class="select-full" ng-model="mvc.add_video_param.video_order" data-ng-change="mvc.change_video_order()" select-two="minimumResultsForSearch:'-2',width:'100%'">
                                    <option value=""></option>
                                    <option ng-repeat="order in mvc.video_orders" value="{{order}}" ng-bind="order"></option>
                                </select>
                                <label for="video_order" class="error hide" id="video_order_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label" for="video_title_en">Title English <span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <input type="text" placeholder="Title English" id="video_title_en" name="video_title_en" ng-model="mvc.add_video_param.video_title_en" class="form-control">
                                <label for="video_title_en" class="error hide" id="video_title_en_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label" for="video_title_sp">Title Spanish<span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <input type="text" placeholder="Title Spanish" id="video_title_sp" name="video_title_sp" ng-model="mvc.add_video_param.video_title_sp" class="form-control">
                                <label for="video_title_sp" class="error hide" id="video_title_sp_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label" for="team_abbr_lable_en">Desc English <span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <textarea placeholder="Desc English" id="video_desc_en" name="video_desc_en" ng-model="mvc.add_video_param.video_desc_en" class="form-control" rows="3"></textarea>
                                <label for="video_desc_en" class="error hide" id="video_desc_en_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label" for="video_desc_sp">Desc Spanish<span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <textarea placeholder="Desc Spanish" id="video_desc_sp" name="video_desc_sp" ng-model="mvc.add_video_param.video_desc_sp" class="form-control" rows="3"></textarea> 
                                <label for="video_desc_sp" class="error hide" id="video_desc_sp_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label" for="video_url">Video Link<span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <input type="text" placeholder="Video Link" id="video_url" name="video_url" ng-model="mvc.add_video_param.video_url" class="form-control">
                                <label for="video_url" class="error hide" id="video_url_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label" for="status">Status<span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <select data-placeholder="Select status" id="status" name="status" class="select-full" ng-model="mvc.add_video_param.status" select-two="minimumResultsForSearch:'-2',width:'100%'">
                                    <option value="1">ACTIVE</option>
                                    <option value="0">INACTIVE</option>
                                </select>
                                <label for="status" class="error hide" id="status_error"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Save</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Add video Popup END -->
<!-- Edit video Popup START -->
<div id="edit_video_modal" class="modal fade" tabindex="-1" role="dialog" ng-if="mvc.edit_video_case">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 class="modal-title">Edit Video</h5>
            </div>
            <form role="form" addvideo-form submit-handle="mvc.edit_video()">
                <div class="modal-body has-padding">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label" for="edit_league_id">League <span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <select placeholder="Select League" id="edit_league_id" name="edit_league_id" class="select-full" ng-options="league.league_id as league.league_abbr for league in mvc.leagues track by league.league_id" ng-model="mvc.edit_video_param.league_id" select-two="minimumResultsForSearch:'-2',width:'100%'">
                                    <option value=""></option>
                                </select>
                                <label for="edit_league_id" class="error hide" id="edit_league_id_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label" for="video_type">Video Type <span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <select data-placeholder="Select Type" id="video_type" name="video_type" class="select-full" ng-model="mvc.edit_video_param.video_type" select-two="minimumResultsForSearch:'-2',width:'100%'">
                                    <option value=""></option>
                                    <option ng-repeat="type in mvc.video_types" value="{{type}}" ng-bind="type"></option>
                                </select>
                                <label for="video_type" class="error hide" id="video_type_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" ng-show="mvc.edit_video_param.video_type == 'LOBBY'">
                        <div class="row">
                            <label class="col-md-4 control-label" for="video_order">Video Order <span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <select data-placeholder="Select Order" id="video_order" name="video_order" class="select-full" ng-model="mvc.edit_video_param.video_order" select-two="minimumResultsForSearch:'-2',width:'100%'">
                                    <option value=""></option>
                                    <option ng-repeat="order in mvc.video_orders" value="{{order}}" ng-bind="order"></option>
                                </select>
                                <label for="video_order" class="error hide" id="video_order_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label" for="video_title_en">Title English <span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <input type="text" placeholder="Title English" id="video_title_en" name="video_title_en" ng-model="mvc.edit_video_param.video_title_en" class="form-control">
                                <label for="video_title_en" class="error hide" id="video_title_en_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label" for="video_title_sp">Title Spanish<span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <input type="text" placeholder="Title Spanish" id="video_title_sp" name="video_title_sp" ng-model="mvc.edit_video_param.video_title_sp" class="form-control">
                                <label for="video_title_sp" class="error hide" id="video_title_sp_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label" for="team_abbr_lable_en">Desc English <span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <textarea placeholder="Desc English" id="video_desc_en" name="video_desc_en" ng-model="mvc.edit_video_param.video_desc_en" class="form-control" rows="3"></textarea>
                                <label for="video_desc_en" class="error hide" id="video_desc_en_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label" for="video_desc_sp">Desc Spanish<span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <textarea placeholder="Desc Spanish" id="video_desc_sp" name="video_desc_sp" ng-model="mvc.edit_video_param.video_desc_sp" class="form-control" rows="3"></textarea> 
                                <label for="video_desc_sp" class="error hide" id="video_desc_sp_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label" for="video_url">Video Link<span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <input type="text" placeholder="Video Link" id="video_url" name="video_url" ng-model="mvc.edit_video_param.video_url" class="form-control">
                                <label for="video_url" class="error hide" id="video_url_error"></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-4 control-label" for="status">Status<span class="mandatory">*</span></label>
                            <div class="col-md-8">
                                <select data-placeholder="Select status" id="status" name="status" class="select-full" ng-model="mvc.edit_video_param.status" select-two="minimumResultsForSearch:'-2',width:'100%'">
                                    <option value="1">ACTIVE</option>
                                    <option value="0">INACTIVE</option>
                                </select>
                                <label for="status" class="error hide" id="status_error"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Save</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Edit video Popup END -->
<!-- Confirm Model for delete video -->
<div id="delete_video_confirm_modal" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title">Confirm</h5>
            </div>

            <div class="modal-body has-padding">
                <p>Are you sure, you want to remove this video?</p>
            </div>

            <div class="modal-footer">
                <button class="btn btn-warning" data-dismiss="modal">No</button>
                <button class="btn btn-primary" ng-click="mvc.delete_video()" >Yes</button>
            </div>
        </div>
    </div>
</div>
<!-- Confirm Model for delete video End-->
