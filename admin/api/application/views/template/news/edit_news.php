<!--Page content -->
<div class="page-content" ng-init="LN.intializeImageLoad();LN.get_master_leagues();">
<div class="content">
	<!-- Page title -->
	<div class="page-title">
		<h5><i class="fa fa-bars"></i>{{lang['league_news_title']}}</h5>
	</div>
	<!-- Change Password -->
	<form class="form-horizontal" role="form" leaguenews-form submit-handle="LN.create_league_news()">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h6 class="panel-title" ng-bind="lang['league_news_title']">League News</h6>
			</div>
			<div class="panel-body">
				<div class="lft">
					<div class="form-group">
						<div class="col-md-12">
							<div class="row">
								<label class="col-md-2 control-label" for="league_type">{{lang['league_type']}} :<span class="mandatory">*</span></label>
								<div class="col-md-6">
									<select id="league_type" name="league_type" data-placeholder="{{lang['league_type']}}" ng-model="LN.leagueNewsObj.league_type" select-two="minimumResultsForSearch:'-2',width:'100%'">
										<option></option>
										<option ng-repeat="type in LN.leagueType" value="{{type.league_id}}" ng-bind="type.league_abbr"></option>
									</select>
								</div>
								<label for="league" class="error hide" id="league_type_error"></label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<div class="row">
								<label class="col-md-2 control-label" for="news_title">{{lang['news_title']}} :<span class="mandatory">*</span> </label>
								<div class="col-md-6">
									<input type="text" id="news_title" name="news_title"  ng-model="LN.leagueNewsObj.news_title" class="form-control">
								</div>
								<label for="news_title" class="error hide" id="news_title_error"></label>						
							</div>
						</div>		
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<div class="row">
								<label class="col-md-2 control-label" for="news_desc">{{lang['news_description']}} :<span class="mandatory">*</span> </label>
								<div class="col-md-6">
									<textarea ck-editor name="news_desc" id="news_desc"  ng-model="LN.leagueNewsObj.news_desc"></textarea>
									<label for="news_desc" class="error hide" id="news_desc_error"></label>						
								</div>
								
							</div>
						</div>		
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<div class="row">
								<label class="col-md-2 control-label" for="news_short_desc">{{lang['news_short_desc']}} :<span class="mandatory">*</span> </label>
								<div class="col-md-6">
									<textarea class="form-control" rows="5" cols="5" name="news_short_desc" id="news_short_desc"  ng-model="LN.leagueNewsObj.news_short_desc"></textarea>
								</div>
								<label for="news_short_desc" class="error hide" id="news_short_desc_error"></label>						
							</div>
						</div>		
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<div class="row">
								<label class="col-md-2 control-label" for="news_image">{{lang['news_image']}} :<span class="mandatory">*</span> </label>
								<div class="col-md-6">
									<input type="file" id="news_image" name="news_image" ng-model="LN.leagueNewsObj.news_image" class="form-control">
									<label id="news_image_text" ng-show="LN.display_file_name"></label>
									<figure class="profile-pic-lg">
	                                    <img src="assets/img/default-user.jpg" alt="" class="news_image img-circle " ng-show="LN.is_show_figure">
	                                </figure>
								</div>
								<label for="news_image" class="error hide" id="news_image_error"></label>						
							</div>
						</div>		
					</div>
					<div class="form-actions text-left">
						<button type="submit" class="btn btn-success" ng-bind="lang['save_news']"></button>
					</div>
				</div>
				<div class="rgt">
					<div class="form-group margin-top-5"></div>
					<div class="form-group">
						<div class="col-md-12">
							<div class="row">
								<label class="col-md-2 control-label" for="arabic_news_title">{{lang['arabic_news_title']}} :<span class="mandatory">*</span> </label>
								<div class="col-md-6">
									<input type="text" id="arabic_news_title" name="arabic_news_title" style="direction:RTL;" ng-model="LN.leagueNewsObj.arabic_news_title" class="form-control">
								</div>
								<label for="arabic_news_title" class="error hide" id="arabic_news_title_error"></label>						
							</div>
						</div>		
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<div class="row">
								<label class="col-md-2 control-label" for="arabic_news_desc">{{lang['arabic_news_description']}} :<span class="mandatory">*</span> </label>
								<div class="col-md-6">
									<textarea ck-editor-arabic name="arabic_news_desc" id="arabic_news_desc" style="direction:RTL;" ng-model="LN.leagueNewsObj.arabic_news_desc"></textarea>
									<label for="arabic_news_desc" class="error hide" id="arabic_news_desc_error"></label>						
								</div>
							</div>
						</div>		
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<div class="row">
								<label class="col-md-2 control-label" for="arabic_news_short_desc">{{lang['arabic_news_short_desc']}} :<span class="mandatory">*</span> </label>
								<div class="col-md-6">
									<textarea class="form-control" rows="5" cols="5" name="arabic_news_short_desc" style="direction:RTL;" id="arabic_news_short_desc"  ng-model="LN.leagueNewsObj.arabic_news_short_desc"></textarea>
								</div>
								<label for="arabic_news_short_desc" class="error hide" id="arabic_news_short_desc_error"></label>						
							</div>
						</div>		
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
</div>
<!-- /Page content-->