<!--Page content -->
<div class="page-content" data-ng-init="initObject();getAllLeague();getAllNews();">
	<!-- Page title -->
	<div class="page-title">
		<h5><i class="fa fa-bars"></i>Manage League News</h5>
	</div>
	<!-- Table with footer -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h6 class="panel-title">League News List</h6>
		</div>
		<div class="table-responsive">
			<table class="table table-hover table-striped table-bordered">
				<thead>
					<tr ng-if="newsList.length>0">
						<th class="pointer" ng-click="sortNewsList('title');">
							Title
							<i ng-class="(newsParam.sort_field=='title'&&newsParam.sort_order=='DESC')?'fa-sort-desc':((newsParam.sort_field=='title'&&newsParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortNewsList('short_desc');">
							Description
							<i ng-class="(newsParam.sort_field=='description'&&newsParam.description=='DESC')?'fa-sort-desc':((newsParam.sort_field=='description'&&newsParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortNewsList('created_date');">
							Created Date
							<i ng-class="(newsParam.sort_field=='created_date'&&newsParam.sort_order=='DESC')?'fa-sort-desc':((newsParam.sort_field=='created_date'&&newsParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortNewsList('status');">
							Status
							<i ng-class="(newsParam.sort_field=='status'&&newsParam.sort_order=='DESC')?'fa-sort-desc':((newsParam.sort_field=='status'&&newsParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer">
							Action
						</th>
					</tr>
					<tr ng-if="newsList.length==0">
						<td align="center">League news not found</td>
					</tr>
				</thead>
				<tbody>
					<tr ng-if="newsList.length>0" ng-repeat="news in newsList">
						<!-- <td ng-bind="$index+1"></td> -->
						<td  ng-bind-html="news.title|limitTo:100" style="text-align: left;"></td>
						<td ng-bind-html="news.description"></td>
						<td ng-bind="news.created_date"></td>
						<td>
							<i class="fa fa-unlock success" ng-show="news.status==1" title="{{lang.active}}"></i>
							<i class="fa fa-lock danger" ng-show="news.status==0" title="{{lang.inactive}}"></i>
						</td>
						<td>
							<!-- for activate -->
							<a href="javascript:void(0);" ng-show="news.status == 0" ng-click="changeNewsStatus(news.player_news_id,1,$index)">
								<i class="fa fa-check success" title="{{lang.activate}}"></i>
							</a>
							<!-- for inactive -->
							<a href="javascript:void(0);" ng-show="news.status == 1" ng-click="changeNewsStatus(news.player_news_id,0,$index)" >
								<i class="fa  fa-ban danger" title="{{lang.deactivate}}"></i>
							</a>
							<a href="news_details/{{news.player_news_id}}" title="View"><i class="fa fa-eye"></i></a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="table-footer" ng-if="newsList.length>0">
			<pagination ng-if="newsParam.total_items>10" boundary-links="true" total-items="newsParam.total_items" ng-model="newsParam.current_page" ng-change="getAllNews()" items-per-page="newsParam.items_perpage" class="pagination" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></pagination>
		</div>
	</div>
	<!-- /table with footer -->
</div>
<!-- /Page content-->
