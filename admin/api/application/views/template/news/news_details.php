<!-- Page content --> 
<style type="text/css">
	.news_label { font-weight: bold; font-size: 25px }
	.news_section span {clear:right; width: 100%; display: block; font-size: 13px;}
	.news_section { width: 49%; float: left; }
</style>
<div class="page-content" ng-init="get_new_details();">
<div class="content">
	<!-- Page title -->
	<div class="page-title">
		<h5><i class="fa fa-bars"></i> News Details</h5>
	</div>
	<div class="panel panel-default">
		
		<div class="panel-body news_section">
			<label class="news_label" >Title</label>
			<span ng-bind-html="newDetails.title"></span>

			
			
			<label class="news_label"> Description </label>
			<span ng-bind-html="newDetails.description"></span>

		</div>

		<div class="panel-body news_section">
			
			<span> 
				<img src="{{newDetails.news_image}}" style="max-width: 100%">
			</span>

			
		</div>
	</div>
	<!-- Page -->
</div>
</div>
<!-- /Page content