<!--Page content -->
<div class="page-content" ng-init="LN.intializeImageLoad();LN.get_master_leagues();">
<div class="content">
	<!-- Page title -->
	<div class="page-title">
		<h5><i class="fa fa-bars"></i>{{lang['league_news_title']}}</h5>
	</div>
	<!-- Change Password -->
	<form class="form-horizontal" role="form" leaguenews-form submit-handle="LN.create_league_news()">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h6 class="panel-title" ng-bind="lang['league_news_title']">League News</h6>
			</div>
			<div class="panel-body">
				<div class="lft">
					<div class="form-group">
						<div class="col-md-12">
							<div class="row">
								<label class="col-md-2 control-label" for="league_type">{{lang['league_type']}} :<span class="mandatory">*</span></label>
								<div class="col-md-6">
									<select id="league_type" name="league_type" data-placeholder="{{lang['league_type']}}" ng-model="LN.leagueNewsObj.league_type" data-ng-change="getAllTeam();getAllRoster();doBlur('league_type');" select-two="minimumResultsForSearch:'-2',width:'100%'" ng-disabled="LN.isEditMode">
										<option></option>
										<option ng-repeat="type in LN.leagueType" value="{{type.league_id}}" ng-bind="type.league_abbr"></option>
									</select>
								</div>
								<label for="league" class="error hide" id="league_type_error"></label>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-12">
							<div class="row">
								<label class="col-md-2 control-label" for="page_type">{{lang['page_type']}} :<span class="mandatory">*</span></label>
								<div class="col-md-6">
									<select id="page_type" name="page_type" data-placeholder="{{lang['page_type']}}"  ng-model="LN.leagueNewsObj.page_type" data-ng-change="doBlur('page_type');" select-two="minimumResultsForSearch:'-2',width:'100%'" ng-disabled="LN.isEditMode">
										<option></option>
										<option ng-value="1">Player Card</option>
										<option ng-value="2">News Page</option>
									</select>
								</div>
								<label for="league" class="error hide" id="page_type_error"></label>
							</div>
						</div>
					</div>

					<div class="form-group" ng-show="LN.leagueNewsObj.page_type == 1">
						<div class="col-md-12">
							<div class="row">
								<label class="col-md-2 control-label" for="all_team">{{lang['all_team']}} :<span class="mandatory">*</span></label>
								<div class="col-md-6">
									<select id="team_abbr" name="team_abbr" data-placeholder="{{lang.all_team}}"  class="select-full ignore"  ng-model="LN.leagueNewsObj.team_abbr" data-ng-change="getRoster(true);doBlur('team_abbr');" select-two="minimumResultsForSearch:'2',width:'100%'">
										<option value=""></option>
										<option ng-repeat="team in teams" value="{{team.team_id}}" ng-bind="team.team_name+'('+team.team_abbreviation+')'"></option>
									</select>
								</div>
								<label for="league" class="error hide" id="team_abbr_error"></label>
							</div>
						</div>
					</div>

					<div class="form-group" ng-show="LN.leagueNewsObj.page_type == 1">
						<div class="col-md-12">
							<div class="row">
								<label class="col-md-2 control-label" for="players">{{lang['players']}} :<span class="mandatory">*</span></label>
								<div class="col-md-6">
									<select id="players" name="players" data-placeholder="{{lang.players}}" class="select-full ignore"  ng-model="LN.leagueNewsObj.player_unique_id" data-ng-change="doBlur('players');"  select-two="minimumResultsForSearch:'2',width:'100%'">
										<option value=""></option>
										<option ng-repeat="player in players" value="{{player.player_unique_id}}" ng-bind="player.full_name"></option>
									</select>
								</div>
								<label for="league" class="error hide" id="players_error"></label>
							</div>
						</div>
					</div>

					

					<div class="form-group" ng-show="LN.leagueNewsObj.page_type == 2">
						<div class="col-md-12">
							<div class="row">
								<label class="col-md-2 control-label" for="news_title">{{lang['news_title']}} :<span class="mandatory">*</span> </label>
								<div class="col-md-6">
									<input type="text" maxlength="100" id="news_title" name="news_title"  ng-model="LN.leagueNewsObj.news_title" class="form-control">
								</div>
								<label for="news_title" class="error hide" id="news_title_error"></label>						
							</div>
						</div>		
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<div class="row">
								<label class="col-md-2 control-label" for="news_desc">{{lang['news_description']}} :<span class="mandatory">*</span> </label>
								<div class="col-md-6">
									<textarea maxlength="1000" class="form-control tinymce_txt" rows="5" cols="5"  name="news_desc" id="news_desc"  ng-model="LN.leagueNewsObj.news_desc"></textarea>
									<label for="news_desc" class="error hide" id="news_desc_error"></label>	
									<input name="image" type="file" id="upload" style = "display: none;" onchange="">									
								</div>
								
							</div>
						</div>		
					</div>
					<div class="form-group" ng-show="LN.leagueNewsObj.page_type == 1">
						<div class="col-md-12">
							<div class="row">
								<label class="col-md-2 control-label" for="news_short_desc">{{lang['news_short_desc']}} :<span class="mandatory">*</span> </label>
								<div class="col-md-6">
									<textarea maxlength="200" class="form-control" rows="5" cols="5" name="news_short_desc" id="news_short_desc"  ng-model="LN.leagueNewsObj.news_short_desc"></textarea>
								</div>
								<label for="news_short_desc" class="error hide" id="news_short_desc_error"></label>						
							</div>
						</div>		
					</div>
					<div class="form-group" ng-show="LN.leagueNewsObj.page_type == 2">
						<div class="col-md-12">
							<div class="row">
								<label class="col-md-2 control-label" for="news_image">{{lang['news_image']}} :<span class="mandatory">*</span> </label>
								<div class="col-md-6">
									<input type="file" id="news_image" name="news_image" ng-model="LN.leagueNewsObj.news_image" class="form-control">
									<label id="news_image_text" ng-show="LN.display_file_name"></label>
									<figure class="profile-pic-lg">
	                                    <img src="assets/img/default-user.jpg" alt="" class="news_image img-thumbnail" ng-show="LN.is_show_figure">
	                                </figure>
	                                <label>Allowed dimension between 320X180 and 1920X1020.</label>
								</div>
								<label for="news_image" class="error hide" id="news_image_error"></label>						
							</div>
						</div>		
					</div>
					
				</div>
				<div class="rgt">
					<div class="form-group margin-top-5"></div>
					<div class="form-group" ng-show="LN.leagueNewsObj.page_type == 2">
						<div class="col-md-12">
							<div class="row">
								<label class="col-md-2 control-label" for="spanish_news_title">{{lang['spanish_news_title']}} :<span class="mandatory">*</span> </label>
								<div class="col-md-6">
									<input type="text" maxlength="100" id="spanish_news_title" name="spanish_news_title"  ng-model="LN.leagueNewsObj.spanish_news_title" class="form-control">
								</div>
								<label for="spanish_news_title" class="error hide" id="spanish_news_title_error"></label>						
							</div>
						</div>		
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<div class="row">
								<label class="col-md-2 control-label" for="spanish_news_desc">{{lang['spanish_news_description']}} :<span class="mandatory">*</span> </label>
								<div class="col-md-6">
									<textarea maxlength="1000" class="form-control tinymce_txt" rows="5" cols="5" name="spanish_news_desc" id="spanish_news_desc" ng-model="LN.leagueNewsObj.spanish_news_desc"></textarea>
									<label for="spanish_news_desc" class="error hide" id="spanish_news_desc_error"></label>						
									<input name="image" type="file" id="upload" style = "display: none;" onchange="">				
								</div>
							</div>
						</div>		
					</div>
					<div class="form-group" ng-show="LN.leagueNewsObj.page_type == 1">
						<div class="col-md-12">
							<div class="row">
								<label class="col-md-2 control-label" for="spanish_news_short_desc">{{lang['spanish_news_short_desc']}} :<span class="mandatory">*</span> </label>
								<div class="col-md-6">
									<textarea maxlength="200" class="form-control" rows="5" cols="5" name="spanish_news_short_desc" id="spanish_news_short_desc"  ng-model="LN.leagueNewsObj.spanish_news_short_desc"></textarea>
								</div>
								<label for="spanish_news_short_desc" class="error hide" id="spanish_news_short_desc_error"></label>						
							</div>
						</div>		
					</div>
				</div>
				<div class="form-actions text-left">
					<button type="submit" class="btn btn-success" ng-bind="lang['save_news']"></button>
				</div>
			</div>
		</div>
	</form>
</div>
</div>

<script type="text/javascript">
	tinymce.init({
		selector: '.tinymce_txt',
		height: 500,
		theme: 'modern',
		plugins: 'searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
		toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | link image',
		image_advtab: true,
		file_picker_callback: function(callback, value, meta) {
			if (meta.filetype == 'image') {
				$('#upload').trigger('click');
				$('#upload').on('change', function() {
					var file = this.files[0];
					var reader = new FileReader();
					reader.onload = function(e) {
						callback(e.target.result, {
							alt: ''
						});
					};
					reader.readAsDataURL(file);
				});
			}
		},
		templates: [
		{ title: 'Test template 1', content: 'Test 1' },
		{ title: 'Test template 2', content: 'Test 2' }
		],
		content_css: [
		'//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
		'//www.tinymce.com/css/codepen.min.css'
		]
	});
</script>
<!-- /Page content-->