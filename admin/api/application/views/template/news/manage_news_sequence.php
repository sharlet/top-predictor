<!--Page content -->
<div class="page-content" data-ng-init="getAllLeagueForSequence();">
	<!-- Page title -->
	<div class="page-title">
		<h5><i class="fa fa-bars"></i>Manage League News Sequence</h5>
	</div>
	<!-- Table elements -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h6 class="panel-title">Filters</h6>
		</div>
		<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Leagues</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-center">
							<select data-placeholder="Select League" id="league_id" ng-options="league.league_id as league.league_abbr for league in Leagues track by league.league_id" class="select-full" ng-model="newsSequenceParam.league_id" data-ng-change="changeLeagueForSequence()" select-two="minimumResultsForSearch:'-1',width:'100%'">
								<option value=""></option>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- /table elements -->
	<!-- Table with footer -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h6 class="panel-title">League News List</h6>
			<div class="pull-right" ng-if="newsSequenceList.length>0">
				<button type="button" class="btn btn-success" ng-click="updateNewsSequence();">Update News Sequence</button>
			</div>
		</div>
		<div class="table-responsive">
			<table class="table table-hover table-striped table-bordered" >
				<thead>
					<tr ng-if="newsSequenceList.length>0">
						<th class="pointer">Title</th>
						<th class="pointer">Short Description</th>
						<th class="pointer">Created Date</th>
					</tr>
					<tr ng-if="newsSequenceList.length==0">
						<td align="center">League news not found</td>
					</tr>
				</thead>
				<tbody id="newsSequence" ng-model="newsSequenceList">
					<tr ng-if="newsSequenceList.length>0" ng-repeat="news in newsSequenceList" drag-item class="dragehandle">
						<td ng-bind="news.en_title|limitTo:100" style="text-align: left;"></td>
						<td ng-bind="news.en_short_description|limitTo:300"></td>
						<td ng-bind="news.created_date"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="table-footer" ng-if="newsSequenceList.length>0">
			<div class="pull-right">
				<button type="button" class="btn btn-success" ng-click="updateNewsSequence();">Update News Sequence</button>
			</div>
		</div>
	</div>
	<!-- /table with footer -->
</div>
<!-- /Page content-->
