<!-- Page content -->
<div class="page-content" ng-controller="LoginCtrl">	
	<!-- Login wrapper -->
	<div class="login-wrapper">
		<form role="form" autocomplete="off" login-form submit-handle="doLogin()">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h6 class="panel-title">
						<i class="fa fa-user"></i> User login
					</h6>
				</div>
				<div class="panel-body">
					<div class="form-group has-feedback">
						<label>Email</label>
						<input type="text" name="email" id="email" class="form-control" placeholder="Email" autocomplete="off" data-ng-model="formData.email">
						<i class="fa fa-user form-control-feedback"></i>
						<label for="email" class="error hide" id="email_error"></label>
					</div>
					<div class="form-group has-feedback">
						<label>Password</label>
						<input type="password" name="password" id="password" class="form-control" placeholder="Password" autocomplete="off" data-ng-model="formData.password">
						<i class="fa fa-lock form-control-feedback"></i>
						<label for="password" class="error hide" id="password_error"></label>
					</div>					
					<div class="row form-actions">
						<div class="col-xs-6"></div>
						<div class="col-xs-6">
							<button type="submit" class="btn btn-warning pull-right">
								<i class="fa fa-bars"></i> Sign in
							</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>  
	<!-- /login wrapper -->
</div>
<!-- /page content -->