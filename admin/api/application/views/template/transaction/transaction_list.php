<!-- Page content -->
<div class="page-content">
<div class="content" data-ng-init="getTransactionList();">
	<!-- Page title -->
	<div class="page-title">
		<h5><i class="fa fa-bars"></i>Transaction List</h5>
	</div>
	<!-- Table elements -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h6 class="panel-title" data-ng-bind="lang.filters"></h6>
		</div>
		<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th data-ng-bind="lang.payment_type"></th>
						<th data-ng-bind="lang.transaction_date"></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-center">
							<select id="payment_type" class="select-full" data-placeholder="{{lang.select_payment_type}}" ng-model="transactionParam.payment_type" data-ng-change="getTransactionList()" select-two="minimumResultsForSearch:'-2',width:'100%'">
								<option value="" data-ng-bind="lang.select_payment_type"></option>
								<option value="DEBIT" data-ng-bind="lang.debit"></option>
								<option value="CREDIT" data-ng-bind="lang.credit"></option>
							</select>
						</td>
						<td width="50%">
							<div class="col-sm-6">
								<input type="text" class="from-date form-control" name="from" date-picker-range="to-date" placeholder="{{lang.from}}" ng-model="transactionParam.fromdate" readonly="" data-ng-change="getTransactionList()" >
							</div>
							<div class="col-sm-6">
								<input type="text" class="to-date form-control" name="to" date-picker-range=""  placeholder="{{lang.to}}" ng-model="transactionParam.todate" data-ng-change="getTransactionList()" readonly="">
							</div>
                        </td>
					</tr>
					<tr>
						<td><a href="javascript:void(0);" ng-click="clearFilter()"><span class="label label-info" data-ng-bind="lang.clear_filters"></span></a></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- /table elements -->
	<!-- Table with footer -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h6 class="panel-title" data-ng-bind="lang.transaction_history"></h6>
		</div>
		<div class="table-responsive">
			<table class="table table-hover table-striped table-bordered table-check">
				<thead>
					<tr ng-if="transactionList.length>0">
						<th class="pointer" ng-click="sortTransactionList('user_name');" width="10%">
							{{lang.user_name}}
							<i ng-class="(transactionParam.sort_field=='user_name'&&transactionParam.sort_order=='DESC')?'fa-sort-desc':((transactionParam.sort_field=='user_name'&&transactionParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" class="numeric" ng-click="sortTransactionList('description');">
							{{lang.description}}
							<i ng-class="(transactionParam.sort_field=='description'&&transactionParam.sort_order=='DESC')?'fa-sort-desc':((transactionParam.sort_field=='description'&&transactionParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortTransactionList('payment_type');">
							{{lang.payment_type}}
							<i ng-class="(transactionParam.sort_field=='payment_type'&&transactionParam.sort_order=='DESC')?'fa-sort-desc':((transactionParam.sort_field=='payment_type'&&transactionParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortTransactionList('transaction_amount');">
							{{lang.amount}}
							<i ng-class="(transactionParam.sort_field=='transaction_amount'&&transactionParam.sort_order=='DESC')?'fa-sort-desc':((transactionParam.sort_field=='transaction_amount'&&transactionParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortTransactionList('user_balance_at_transaction');">
							{{lang.user_balance_at_transaction}}
							<i ng-class="(transactionParam.sort_field=='user_balance_at_transaction'&&transactionParam.sort_order=='DESC')?'fa-sort-desc':((transactionParam.sort_field=='user_balance_at_transaction'&&transactionParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortTransactionList('created_date');">
							{{lang.transaction_date}}
							<i ng-class="(transactionParam.sort_field=='created_date'&&transactionParam.sort_order=='DESC')?'fa-sort-desc':((transactionParam.sort_field=='created_date'&&transactionParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortTransactionList('is_processed');">
							{{lang.status}}
							<i ng-class="(transactionParam.sort_field=='is_processed'&&transactionParam.sort_order=='DESC')?'fa-sort-desc':((transactionParam.sort_field=='is_processed'&&transactionParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
					</tr>
					<tr ng-if="transactionList.length==0">
						<td align="center" data-ng-bind="lang.no_transaction_history"></td>
					</tr>
				</thead>
				<tbody>
					<tr ng-if="transactionList.length>0" ng-repeat="transaction in transactionList">
						<td ng-bind="::transaction.user_name"></td>
						<td ng-bind="::transaction.description"></td>
						<td>
							<span ng-show="transaction.payment_type==1">DEBIT</span>
							<span ng-show="transaction.payment_type==2">CREDIT</span>
						</td>
						<td ng-bind-html="transaction.transaction_amount | salaryFormat"></td>
						<td ng-bind-html="transaction.user_balance_at_transaction | salaryFormat"></td>
						<td ng-bind="::transaction.transaction_date"></td>
						<td>
							<i class="fa fa-check-circle-o success" ng-show="transaction.is_processed==0" title="Not yet"></i>
							<i class="fa fa-ban danger" ng-show="transaction.is_processed==1" title="Payment Processed Done"></i>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="table-footer" ng-if="transactionParam.total_items>10">
			<pagination boundary-links="true" total-items="transactionParam.total_items" ng-model="transactionParam.current_page" ng-change="getTransactionList()" items-per-page="transactionParam.items_perpage" class="pagination" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></pagination>
		</div>
	</div>
	<!-- /table with footer -->
</div>
</div>
<!-- /Page content-->