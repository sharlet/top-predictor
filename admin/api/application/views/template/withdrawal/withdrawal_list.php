<!--Page content -->
<div class="page-content">
<div class="content" data-ng-init="getFilterData();initObject();">
	
	<!-- Page title -->
	<div class="page-title">
		<h5><i class="fa fa-bars"></i>Withdrawal List</h5>
	</div>
	<!-- Table elements -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h6 class="panel-title" data-ng-bind="lang.filters"></h6>
		</div>
		<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
					<!-- 	<th data-ng-bind="lang.withdrawal_type"></th> -->
						<th data-ng-bind="lang.withdrawal_status"></th>
					</tr>
				</thead>
				<tbody>
					<tr>
				<!-- 		<td class="text-center">
							<select name="withdrawal_type" id="withdrawal_type" data-placeholder="{{lang.all_type}}" ng-options="value as key for (key,value) in withdrawalType track by value" class="select-full" ng-model="withdrawalParam.type" data-ng-change="getWithdrawalList()" select-two="minimumResultsForSearch:'-2',width:'100%'">
								<option value="" data-ng-bind="lang.all_type"></option>
							</select>
						</td> -->
						<td class="text-center">
							<select name="withdrawal_status" id="withdrawal_status" data-placeholder="{{lang.all_status}}" ng-options="value as key for (key,value) in withdrawalStatus" class="select-full" ng-model="withdrawalParam.status" data-ng-change="getWithdrawalList()" select-two="minimumResultsForSearch:'-2',width:'100%'">
								<option value="" data-ng-bind="lang.all_status"></option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2"><a href="javascript:void(0);" ng-click="clearFilter()"><span class="label label-info" data-ng-bind="lang.clear_filters"></span></a></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- /table elements -->
	<!-- Table with footer -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h6 class="panel-title" data-ng-bind="lang.withdrawal_request"></h6>
		</div>
		<div class="table-responsive">
			<table class="table table-hover table-striped table-bordered ">
				<thead>
					<tr ng-if="withdrawalList.length>0">
					<!-- 	<th>
							<input type="checkbox" ng-model="withdrawalObj.selectall" value="{{::withdrawalObj.selectall}}" ng-checked="withdrawalObj.selectall" uniform="radioClass:'choice', selectAutoWidth:false" ng-click="toggleWithdrawal($event);">
						</th> -->
						<th class="pointer"ng-click="sortWithdrawalList('full_name');">
							{{lang.full_name}}
							<i ng-class="(withdrawalParam.sort_field=='full_name'&&withdrawalParam.sort_order=='DESC')?'fa-sort-desc':((withdrawalParam.sort_field=='full_name'&&withdrawalParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" class="numeric" ng-click="sortWithdrawalList('email');">
							{{lang.email}}
							<i ng-class="(withdrawalParam.sort_field=='email'&&withdrawalParam.sort_order=='DESC')?'fa-sort-desc':((withdrawalParam.sort_field=='email'&&withdrawalParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortWithdrawalList('address');">
							{{lang.address}}
							<i ng-class="(withdrawalParam.sort_field=='address'&&withdrawalParam.sort_order=='DESC')?'fa-sort-desc':((withdrawalParam.sort_field=='address'&&withdrawalParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortWithdrawalList('amount');">
							{{lang.amount}}
							<i ng-class="(withdrawalParam.sort_field=='amount'&&withdrawalParam.sort_order=='DESC')?'fa-sort-desc':((withdrawalParam.sort_field=='amount'&&withdrawalParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortWithdrawalList('withdraw_type');">
							{{lang.withdrawal_type}}
							<i ng-class="(withdrawalParam.sort_field=='withdraw_type'&&withdrawalParam.sort_order=='DESC')?'fa-sort-desc':((withdrawalParam.sort_field=='withdraw_type'&&withdrawalParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortWithdrawalList('created_date');">
							{{lang.added_date}}
							<i ng-class="(withdrawalParam.sort_field=='created_date'&&withdrawalParam.sort_order=='DESC')?'fa-sort-desc':((withdrawalParam.sort_field=='created_date'&&withdrawalParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortWithdrawalList('modified_date');">
							{{lang.modified_date}}
							<i ng-class="(withdrawalParam.sort_field=='modified_date'&&withdrawalParam.sort_order=='DESC')?'fa-sort-desc':((withdrawalParam.sort_field=='modified_date'&&withdrawalParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortWithdrawalList('status');">
							{{lang.status}}
							<i ng-class="(withdrawalParam.sort_field=='status'&&withdrawalParam.sort_order=='DESC')?'fa-sort-desc':((withdrawalParam.sort_field=='status'&&withdrawalParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th>
							{{lang.action}}
						</th>
					</tr>
					<tr ng-if="withdrawalList.length==0">
						<td align="center" data-ng-bind="lang.no_withdrawal"></td>
					</tr>
				</thead>
				<tbody>
					<tr ng-if="withdrawalList.length>0" ng-repeat="withdrawal in withdrawalList">
						<!-- <td>
							<input type="checkbox" name="withdraw_transaction_id[]" value="{{::withdrawal.payment_withdraw_transaction_id}}" ng-checked="withdraw_transaction_id[$index]" ng-model="withdraw_transaction_id[$index]" ng-click="selectWithdrawal($event)" uniform="radioClass:'choice', selectAutoWidth:false"/>
						</td> -->
						<td ng-bind="::withdrawal.full_name"></td>
						<td ng-bind="::withdrawal.email"></td>
						<td ng-bind="::withdrawal.address"></td>
						<td data-ng-bind-html="withdrawal.amount | salaryFormat"></td>
						<td>
							<span ng-show="withdrawal.withdraw_type==1" data-ng-bind="lang.paypal"></span>
							<span  ng-show="withdrawal.withdraw_type==2" data-ng-bind="lang.live_cheque"></span>
						</td>
						<td ng-bind="::withdrawal.created_date"></td>
						<td ng-bind="::withdrawal.modified_date"></td>
						<td>
							<i class="fa fa-clock-o danger" ng-show="withdrawal.status==0" title="Pending"></i>
							<i class="fa fa-check-circle-o success" ng-show="withdrawal.status==1" title="Approved"></i>
							<i class="fa fa-ban danger" ng-show="withdrawal.status==2" title="Rejected"></i>
						</td>
						<td>
							<div class="btn-group" ng-show="withdrawal.status==0">
								<button class="btn btn-link btn-icon btn-xs tip" data-toggle="dropdown" data-hover="dropdown">
									<i class="fa fa-cogs"></i>
								</button>
								<button class="btn btn-link btn-icon btn-xs tip" data-toggle="dropdown" data-hover="dropdown">
									<span class="caret caret-split"></span>
								</button>
								<ul class="dropdown-menu dropdown-menu-right">
									<li><a tabindex="-1" href="#" ng-click="showPopUp(withdrawal.payment_withdraw_transaction_id,1, $index);" data-ng-bind="lang.approved"></a></li>
									<li><a tabindex="-1" href="#" ng-click="showPopUp(withdrawal.payment_withdraw_transaction_id,2, $index);" data-ng-bind="lang.rejected"></a></li>
								</ul>
							</div>
							<span ng-show="withdrawal.status!=0">--</span>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="table-footer" ng-if="withdrawalList.length>0">
<!-- 			<div class="table-actions">
				<label data-ng-bind="lang.apply_action"></label>
				<select data-placeholder="{{lang.select_action}}" class="select-liquid" select-two="minimumResultsForSearch:'-1'" ng-model="withdrawalObj.action">
					<option value="" data-ng-bind="lang.select_action"></option>
					<option value="1" data-ng-bind="lang.approved"></option>
					<option value="2" data-ng-bind="lang.rejected"></option>
				</select>
				<button type="button" ng-click="showPopUpAllWithdrawal()" class="btn btn-primary" ng-class="{disabled:withdrawalObj.withdraw_transaction_id.length==0}"><i class=""></i>{{lang.update}}</button>
			</div> -->
			<pagination ng-if="withdrawalParam.totalItems >10" boundary-links="true" total-items="withdrawalParam.totalItems" ng-model="withdrawalParam.current_page" ng-change="getWithdrawalList()" items-per-page="withdrawalParam.items_perpage" class="pagination" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></pagination>
		</div>
	</div>
	<!-- /table with footer -->

	<!-- Add Decription modal -->
	<div id="withdrawal_status_model" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h5 class="modal-title" data-ng-bind="lang.manage_withdrawal_request"></h5>
				</div>
				<!-- Ban User modal -->
				<form  role="form" withdrawal-request-form submit-handle="changeWithdrawalRequest()">
					<div class="modal-body has-padding">
						
						<div class="form-group">
							<label data-ng-bind="lang.description"></label>
							<textarea class="form-control" id="description" name="description" ng-model="withdrawalObj.description"></textarea>
							<label for="description" class="error hide" id="description_error"></label>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-dismiss="modal" ng-click="withdrawalObj={}" data-ng-bind="lang.close"></button>
						<button type="submit" class="btn btn-primary"><i class=""></i>{{lang.submit}}</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /Add User Balance modal -->

	<!-- Add Decription modal -->
	<div id="all_withdrawal_status_model" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h5 class="modal-title" data-ng-bind="lang.manage_withdrawal_request"></h5>
				</div>
				<!-- Ban User modal -->
				<form  role="form" withdrawal-request-form submit-handle="updateWithdrawalRequest()">
					<div class="modal-body has-padding">
						
						<div class="form-group">
							<label data-ng-bind="lang.description"></label>
							<textarea class="form-control" id="description" name="description" ng-model="withdrawalObj.description"></textarea>
							<label for="description" class="error hide" id="description_error"></label>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-dismiss="modal" ng-click="withdrawalObj={};withdrawalObj.withdraw_transaction_id=[];deselectWithdrawal();" data-ng-bind="lang.close"></button>
						<button type="submit" class="btn btn-primary"><i class=""></i>{{lang.submit}}</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /Add User Balance modal -->
</div>
</div>
<!-- /Page content-->
