<!-- Page content -->
<div class="page-content" data-ng-init="initObject('');getFilterData();">
	<!-- Page title -->
	<div class="page-title">
		<h5><i class="fa fa-bars"></i>{{lang.manage_site_users}}</h5>
	</div>
	<!-- /page title -->
	<!-- Filter Section -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h6 class="panel-title" data-ng-bind="lang.filters"></h6>
		</div>
		<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th data-ng-bind="lang.country"></th>
						<th data-ng-bind="lang.balance"></th>
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-center">
							<select name="country" id="country" data-placeholder="{{lang.all_country}}" class="select-full" ng-model="userParam.country" data-ng-change="getUserList()" select-two="minimumResultsForSearch:'2',width:'100%'">
								<option value="" data-ng-bind="lang.all_country"></option>
								<option ng-repeat="country in country_list" value="{{country.master_country_id}}">{{country.country_name}}</option>
							</select>
						</td>
						<td class="text-center" width="30%">
							<label>${{userParam.frombalance}} - ${{userParam.tobalance}}</label>
							<div id="range-slider" callback="filterBalance(event, ui);" range-slider="max_value:'{{userParam.tobalance}}',min_value:'{{userParam.frombalance}}',min:'{{min_balance_value}}',max:'{{max_balance_value}}'"></div>
						</td>
						<td class="text-center">
							<input type="text" placeholder="{{lang.email_name_username}}" id="keyword" name="keyword" ng-model="userParam.keyword" class="form-control" data-ng-change="searchByEmailOrName()">
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<a href="javascript:void(0);" ng-click="initObject('clear')"><span class="label label-info" data-ng-bind="lang.clear_filters"></span></a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- /Filter Section -->
	<!-- User Listing -->
	<div class="panel panel-default">
		<div class="panel-heading"><h6 class="panel-title" data-ng-bind="lang.users"></h6></div>
		<div class="table-responsive">
			<table class="table table-hover table-striped table-bordered">
				<thead>
					<tr>
						<th>
							<input type="checkbox" ng-model="userObj.selectall" value="{{userObj.selectall}}" ng-checked="userObj.selectall" uniform="radioClass:'choice', selectAutoWidth:false" ng-click="toggleUser($event);">
						</th>
						<th>
							{{lang.username}}
						</th>
						<th class="pointer" ng-click="sortUesrList('first_name');">
							{{lang.name}}
							<i ng-class="(userParam.sort_field=='first_name'&&userParam.sort_order=='DESC')?'fa-sort-desc':((userParam.sort_field=='first_name'&&userParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortUesrList('email');">
							{{lang.email}}
							<i ng-class="(userParam.sort_field=='email'&&userParam.sort_order=='DESC')?'fa-sort-desc':((userParam.sort_field=='email'&&userParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortUesrList('MC.country_name');">
							{{lang.country}}
							<i ng-class="(userParam.sort_field=='MC.country_name'&&userParam.sort_order=='DESC')?'fa-sort-desc':((userParam.sort_field=='MC.country_name'&&userParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" class="numeric" ng-click="sortUesrList('balance');">
							{{lang.balance}}
							<i ng-class="(userParam.sort_field=='balance'&&userParam.sort_order=='DESC')?'fa-sort-desc':((userParam.sort_field=='balance'&&userParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortUesrList('added_date');">
							{{lang.member_since}}
							<i ng-class="(userParam.sort_field=='added_date'&&userParam.sort_order=='DESC')?'fa-sort-desc':((userParam.sort_field=='added_date'&&userParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortUesrList('status');">
							{{lang.status}}
							<i ng-class="(userParam.sort_field=='status'&&userParam.sort_order=='DESC')?'fa-sort-desc':((userParam.sort_field=='status'&&userParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th>
							{{lang.action}}
						</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-if="userList.length > 0" ng-repeat="user in userList">
						<td><input type="checkbox" name="user_unique_id[]" value="{{user.user_unique_id}}" ng-checked="user_unique_id[$index]" ng-model="user_unique_id[$index]" ng-click="selectUser($event)" uniform="radioClass:'choice', selectAutoWidth:false"/></td>
						<td><img ng-src="{{user.image}}" class="user-avatar">{{user.user_name}}</td>
						<td data-ng-bind="user.name"></td>
						<td data-ng-bind="user.email"></td>
						<td data-ng-bind="user.country_name"></td>
						<td data-ng-bind-html="user.balance | salaryFormat"></td>
						<td data-ng-bind="user.member_since"></td>
						<td>
							<i class="fa fa-trash-o danger" ng-show="user.status==4" title="{{lang.user_deleted_title}}"></i>
							<i class="fa fa-ban danger" ng-show="user.status==3" title="{{lang.user_banned_title}}"></i>
							<i class="fa fa-warning danger" ng-show="user.status==2" title="{{lang.user_email_not_verified_title}}"></i>
							<i class="fa fa-unlock success" ng-show="user.status==1" title="{{lang.user_active_title}}"></i>
							<i class="fa fa-lock danger" ng-show="user.status==0" title="{{lang.user_activation_pending_title}}"></i>
						</td>
						<td>							
							<div class="btn-group">
								<button class="btn btn-link btn-icon btn-xs tip" data-toggle="dropdown" data-hover="dropdown">
									<i class="fa fa-cogs"></i>
								</button>
								<button class="btn btn-link btn-icon btn-xs tip" data-toggle="dropdown" data-hover="dropdown">
									<span class="caret caret-split"></span>
								</button>
									<ul class="dropdown-menu dropdown-menu-right">
										<li><a tabindex="-1" href="user_detail/{{user.user_unique_id}}" data-ng-bind="lang.user_detail"></a></li>
										<li><a ng-click="getUserDetail(user.user_unique_id,$index)" href="javascript:void(0);" data-ng-bind="lang.manage_balance"></a></li>
										<li><a ng-click="showBanUserModel(user.user_unique_id,$index)" ng-if="user.status!=3" data-ng-bind="lang.ban_user"></a></li>
										<li><a ng-click="showActivateUserConfirm(user.user_unique_id,$index)" ng-if="user.status!=1" >Active User</a></li>
									</ul>
							</div>
						</td>
					</tr>
					<tr ng-if="userList.length == 0">
						<td colspan="9" align="center" data-ng-bind="lang.no_record_found"></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="table-footer" ng-if="userParam.total_items>10">
			<pagination boundary-links="true" total-items="userParam.total_items" ng-model="userParam.current_page" ng-change="getUserList()" items-per-page="userParam.items_perpage" class="pagination-sm pull-right" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></pagination>
		</div>
	</div>
	<!-- /User Listing -->
	<!-- Add User Balance modal -->
	<div id="add_balance_modal" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h5 class="modal-title" data-ng-bind="lang.manage_user_balance"></h5>
				</div>
				<!-- Add User Balance modal -->
				<form role="form" addbalance-form submit-handle="addUserBalance()">
					<div class="modal-body has-padding">
						<div class="form-group">
							<label data-ng-bind="lang.user_name"></label>
							<span disabled="disabled" class="form-control"  data-ng-bind="userDetail.user_name"></span>
						</div>
						<div class="form-group">
							<label data-ng-bind="lang.current_balance"></label>
							<span disabled="disabled" class="form-control"  data-ng-bind-html="userDetail.balance | salaryFormat"></span>
						</div>
						<div class="form-group">
							<label data-ng-bind="lang.transaction_type"> </label>
							<div>
								<label class="radio-inline" for="credit">
									<input type="radio" name="transaction_type" id="credit" value="CREDIT" uniform="radioClass:'choice', selectAutoWidth:false" checked="checked" ng-model="userBalance.transaction_type">
									{{lang.credit}}
								</label>
								<label class="radio-inline" for="debit">
									<input type="radio" name="transaction_type" id="debit" value="DEBIT" uniform="radioClass:'choice', selectAutoWidth:false"  ng-model="userBalance.transaction_type">
									{{lang.debit}}
								</label>
							</div>
						</div>

						<div class="form-group">
							<label data-ng-bind="lang.amount"></label>
							<input type="text" name="amount" id="amount" class="form-control" ng-model="userBalance.amount" maxlength="7" intiger-only>
							<label for="amount" class="error hide" id="amount_error"></label>
						</div>
						<div class="form-group">
							<label data-ng-bind="lang.description"></label>
							<textarea class="form-control" name="description" id="description" ng-model="userBalance.description"></textarea>
							<label for="description" class="error hide" id="description_error"></label>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-dismiss="modal" ng-click="userbalance={}" data-ng-bind="lang.close"></button>
						<button type="submit" class="btn btn-primary"><i class=""></i>{{lang.add_balance}}</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /Add User Balance modal -->
	<!-- Ban User modal -->
	<div id="ban_user_modal" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h5 class="modal-title" data-ng-bind="lang.manage_ban_user"></h5>
				</div>

				<!-- Ban User modal -->
				<form  role="form" banuser-form submit-handle="openBlockConfirm()">
					<div class="modal-body has-padding">
						
						<div class="form-group">
							<label data-ng-bind="lang.reason"></label>
							<textarea class="form-control" id="reason" name="reason" ng-model="userObj.reason"></textarea>
							<label for="reason" class="error hide" id="reason_error"></label>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-dismiss="modal" ng-click="userObj={}" data-ng-bind="lang.close"></button>
						<button type="submit" class="btn btn-primary"><i class=""></i>{{lang.user_ban}}</button>
					</div>

				</form>
			</div>
		</div>
	</div>
	<!-- /Add User Balance modal -->
	<!-- Confirm Model for block user -->
	<div id="confirm_modal" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
	    <div class="modal-dialog modal-sm">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                <h5 class="modal-title">Confirm</h5>
	            </div>

	            <div class="modal-body has-padding">
	                <p>Are you sure, you want to block this user?</p>
	            </div>

	            <div class="modal-footer">
	                <button class="btn btn-warning" data-dismiss="modal">No</button>
	                <button class="btn btn-primary" ng-click="banUser()" >Yes</button>
	            </div>
	        </div>
	    </div>
	</div>
	<!-- Confirm Model for block user End-->

	<!-- Confirm Model for block user -->
	<div id="activate_user_confirm_modal" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
	    <div class="modal-dialog modal-sm">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	                <h5 class="modal-title">Confirm</h5>
	            </div>

	            <div class="modal-body has-padding">
	                <p>Are you sure, you want to activate this user?</p>
	            </div>

	            <div class="modal-footer">
	                <button class="btn btn-warning" data-dismiss="modal">No</button>
	                <button class="btn btn-primary" ng-click="activateUser()" >Yes</button>
	            </div>
	        </div>
	    </div>
	</div>
	<!-- Confirm Model for block user End-->


</div>
<!-- /Page content -->