<!-- Page content -->
<div class="page-content">
	<!-- Page title -->
	<div class="page-title">
		<h5><i class="fa fa-bars"></i>User Information</h5>
	</div>
	<button class="btn btn-success" ng-click="goToUserList()"><i class=""></i>Back</button>
	<!-- /page title -->
	<!-- User Detail Section Start -->
	<form class="form-horizontal" action="#" role="form" >
		<div class="panel panel-default" ng-init="getUserDetailByID()">
			<div class="panel-heading"><h6 class="panel-title">User Detail</h6></div>
			<div class="panel-body">
				<div class="form-group">					
					<div class="col-md-6">
						<div class="row">
							<label class="col-sm-6 control-label text-right">First Name : </label>
							<div class="col-sm-6 control-label">
								<label class="" ng-bind="userDetail.first_name"></label>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<label class="col-sm-6 control-label text-right">Last Name : </label>
							<div class="col-sm-6 control-label">
								<label class="" ng-bind="userDetail.last_name"></label>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-6">
						<div class="row">
							<label class="col-sm-6 control-label text-right">User Name : </label>
							<div class="col-sm-6 control-label">
								<label class="" ng-bind="userDetail.user_name"></label>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="row">
							<label class="col-sm-6 control-label text-right">Email : </label>
							<div class="col-sm-6 control-label">
								<label class="" ng-bind="userDetail.email"></label>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-6">
						<div class="row">
							<label class="col-sm-6 control-label text-right">Balance : </label>
							<div class="col-sm-6 control-label">
								<label class="" ng-bind="userDetail.balance | salaryFormat"></label>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<label class="col-sm-6 control-label text-right">Date of Birth : </label>
							<div class="col-sm-6 control-label">
								<label class="" ng-bind="userDetail.dob"></label>
							</div>
						</div>
					</div>
				</div>				
				<div class="form-group">
					<div class="col-md-6">
						<div class="row">
							<label class="col-sm-6 control-label text-right">Phone : </label>
							<div class="col-sm-6 control-label">
								<label class="" ng-bind="userDetail.phone_no"></label>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<label class="col-sm-6 control-label text-right">City : </label>
							<div class="col-sm-6 control-label">
								<label class="" ng-bind="userDetail.city"></label>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-6">
						<div class="row">
							<label class="col-sm-6 control-label text-right">State : </label>
							<div class="col-sm-6 control-label">
								<label ng-bind="userDetail.state_name"></label>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<label class="col-sm-6 control-label text-right">Country : </label>
							<div class="col-sm-6 control-label">
								<label  ng-bind="userDetail.country_name"></label>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-6">
						<div class="row">
							<label class="col-sm-6 control-label text-right">Language : </label>
							<div class="col-sm-6 control-label">
								<label ng-bind="userDetail.language"></label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
	<!-- User Detail Section End -->
	<!-- User Transaction History Section Start -->
	<div class="panel panel-default">
		<div class="panel-heading"><h6 class="panel-title">Users Transaction</h6></div>
		<div class="table-responsive">			
			<table class="table table-hover table-striped table-bordered" >
				<thead>
					<tr ng-show="userTransactions.length>0">
						<th class="pointer" ng-click="sortUserTransaction('description');">
							Description
							<i ng-class="(transParam.sort_field=='description'&&transParam.sort_order=='DESC')?'fa-sort-desc':((transParam.sort_field=='description'&&transParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortUserTransaction('status');">
							Withdrawal Status
							<i ng-class="(transParam.sort_field=='status'&&transParam.sort_order=='DESC')?'fa-sort-desc':((transParam.sort_field=='status'&&transParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortUserTransaction('transaction_amount');">
							Credit
							<i ng-class="(transParam.sort_field=='transaction_amount'&&transParam.sort_order=='DESC')?'fa-sort-desc':((transParam.sort_field=='transaction_amount'&&transParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" class="numeric" ng-click="sortUserTransaction('transaction_amount');">
							Debit
							<i ng-class="(transParam.sort_field=='transaction_amount'&&transParam.sort_order=='DESC')?'fa-sort-desc':((transParam.sort_field=='transaction_amount'&&transParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortUserTransaction('created_date');">
							Transaction Date
							<i ng-class="(transParam.sort_field=='created_date'&&transParam.sort_order=='DESC')?'fa-sort-desc':((transParam.sort_field=='created_date'&&transParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
					</tr>
					<tr ng-show="userTransactions.length==0">
						<td align="center">No User Transaction History</td>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="transaction in userTransactions">
						<td data-ng-bind="transaction.description"></td>
						<td>
							<span ng-if="transaction.status==0">Pending</span>
							<span ng-if="transaction.status==1">Approved</span>
							<span ng-if="transaction.status==2">Rejected</span>
						</td>
						<td>
							<span ng-if="transaction.payment_type=='CREDIT'" data-ng-bind-html="transaction.transaction_amount | salaryFormat"></span>
						</td>
						<td>
							<span ng-if="transaction.payment_type=='DEBIT'" data-ng-bind-html="transaction.transaction_amount | salaryFormat"></span>
						</td>						
						<td data-ng-bind="transaction.created_date"></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="table-footer" ng-show="transParam.total_items.length>10">
			<pagination boundary-links="true" total-items="transParam.total_items" ng-model="transParam.current_page" ng-change="usertransactionhistory()" items-per-page="transParam.items_perpage" class="pagination-sm pull-right" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></pagination>
		</div>
	</div>
	<!-- User Transaction History End -->

	<!-- User Transaction History Section Start -->
	<div class="panel panel-default">
		<div class="panel-heading"><h6 class="panel-title">User Games</h6></div>
		<div class="table-responsive">
			<table class="table table-hover table-striped table-bordered" ng-show="gameList.length==0">
			<thead>
					<tr>
						<td align="center">No User Games</td>
					</tr>	
			</thead>
			</table>
			<table class="table table-hover table-striped table-bordered" ng-show="gameList.length>0">
				<thead>
					<tr>
						<th class="pointer" ng-click="sortUserTransaction('game_name');">
							Game
							<i ng-class="(gameParam.sort_field=='game_name'&&gameParam.sort_order=='DESC')?'fa-sort-desc':((gameParam.sort_field=='game_name'&&gameParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortUserTransaction('size');">
							Entrants
							<i ng-class="(gameParam.sort_field=='size'&&gameParam.sort_order=='DESC')?'fa-sort-desc':((gameParam.sort_field=='size'&&gameParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortUserTransaction('entry_fee');">
							Fees
							<i ng-class="(gameParam.sort_field=='entry_fee'&&gameParam.sort_order=='DESC')?'fa-sort-desc':((gameParam.sort_field=='entry_fee'&&gameParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortUserTransaction('prize_pool');">
							Prize Pool
							<i ng-class="(gameParam.sort_field=='prize_pool'&&gameParam.sort_order=='DESC')?'fa-sort-desc':((gameParam.sort_field=='prize_pool'&&gameParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortUserTransaction('season_scheduled_date');">
							Start Time
							<i ng-class="(gameParam.sort_field=='season_scheduled_date'&&gameParam.sort_order=='DESC')?'fa-sort-desc':((gameParam.sort_field=='season_scheduled_date'&&gameParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="game in gameList">
						<td data-ng-bind="game.game_name"></td>
						<td>{{game.total_user_joined}}/{{game.size}}</td>
						<td data-ng-bind-html="game.entry_fee | salaryFormat"></td>
						<td data-ng-bind-html="game.prize_pool | salaryFormat"></td>						
						<td data-ng-bind="game.season_scheduled_date"></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="table-footer" ng-show="gameParam.total_items.length>10">
			<pagination boundary-links="true" total-items="gameParam.total_items" ng-model="gameParam.current_page" ng-change="userTransactionHistory()" items-per-page="gameParam.items_perpage" class="pagination-sm pull-right" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></pagination>
		</div>
	</div>
	<!-- User Transaction History End -->
</div>
<!-- /Page content -->