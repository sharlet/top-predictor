	<!-- Page content  -->
	<div class="page-content">
		<!-- Game Detail Section Start -->
		<div class="col-md-12">
			<!-- Page title -->
			<div class="page-title">
				<h5><i class="fa fa-bars"></i>Contest Information</h5>
			</div>
			
			<button class="btn btn-success" ng-click="goToContest()"><i class=""></i>Back</button>
			<button class="btn btn-success" ng-click="goToLineup()"><i class=""></i>Lineup Details</button> 

			<!-- /page title -->
			
				<div class="panel panel-default">
					<div class="panel-heading"><h6 class="panel-title">Contest Detail</h6></div>
					
					<div class="panel-body">
						<div class="form-group">
							
							<div class="col-sm-6">
								<div class="row">
									<label class="col-sm-6 control-label text-right">Contest Name :</label>
									<div class="col-sm-6">
										<label class="control-label" ng-bind="contestDetail.contest_name"></label>
									</div>
								</div>
							</div>
							
							<div class="col-sm-6">
								<div class="row">
									<label class="col-sm-6 control-label text-right">League :</label>
									<div class="col-sm-6">
										<label class="control-label" ng-bind="contestDetail.league_abbr"></label>
									</div>
								</div>
							</div>

							<div class="col-sm-6 ">
								<div class="row">
									<label class="col-sm-6 control-label text-right">Entrants/Participants :</label>
									<div class="col-sm-6"> {{contestDetail.total_user_joined}}/
										<label class="control-label" ng-if="contestDetail.size!=-1">{{contestDetail.size}}</label>
										<label class="control-label" ng-if="contestDetail.size==-1">&#8734</label>
									</div>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="row">
									<label class="col-sm-6 control-label text-right">Entry fee :</label>
									<div class="col-sm-6">
										<label class="control-label" ng-bind="contestDetail.entry_fee | salaryFormat"></label>
									</div>
								</div>
							</div>						
							
							<div class="col-sm-6">
								<div class="row">
									<label class="col-sm-6 control-label text-right">Service Fee (%) :</label>
									<div class="col-sm-6">
										<label class="control-label" ng-bind="contestDetail.site_rake"></label>
									</div>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="row">
									<label class="col-sm-6 control-label text-right">Prizing :</label>
									<div class="col-sm-6">
										<label class="control-label" ng-bind="contestDetail.rewards | salaryFormat"></label>
									</div>
								</div>
							</div> 

							<div class="col-sm-6">
								<div class="row">
									<label class="col-sm-6 control-label text-right">Created Date :</label>
									<div class="col-sm-6">
										<label class="control-label" ng-bind="contestDetail.created_date"></label>
									</div>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="row">
									<label class="col-sm-6 control-label text-right">Status :</label>
									<div class="col-sm-6">
										<label class="control-label" ng-bind="contestDetail.status | titleCase"></label>
									</div>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="row">
									<label class="col-sm-6 control-label text-right">Created By :</label>
									<div class="col-sm-6">
										<label class="control-label" ng-if="contestDetail.user_name!=null" ng-bind="contestDetail.user_name"></label>
										<label class="control-label" ng-if="contestDetail.user_name===null">ADMIN</label>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="panel-body">
						<form class="form-horizontal" role="form" contest-form submit-handle="editContest()">
							<div class="form-group">
								<div class="col-sm-12">
									<div class="row">
										<label class="col-sm-12 control-label text-left">Name :</label>
										<div class="col-sm-6">
											<label class="control-label" ></label>
											<input type="text" name="contestName" id="contestName" ng-model="contestParam.contestName" class="form-control" maxlength="100">
	                    <label for="contestName" class="error hide" id="contestNameError"></label>
										</div>
									</div>
								</div>

								<div class="col-sm-12">
									<div class="row">
										<label class="col-sm-12 control-label text-left">Size :</label>
										<div class="col-sm-6">
											<label class="control-label" ></label>
											<input type="text" name="size" class="form-control" id="size"  ng-blur="calculateAbsolutePrizePool()" intiger-only ng-model="contestParam.size"  >
	                        <label for="size" class="error hide" id="sizeError"></label>
										</div>
									</div>
								</div>

								<div class="col-sm-12">
									<div class="row">
										<label class="col-sm-12 control-label text-left">Site Rake :</label>
										<div class="col-sm-6">
											<label class="control-label" ></label>
											<input type="text" name="siteRake" class="form-control" id="siteRake"  intiger-only ng-model="contestParam.siteRake" ng-blur="calculateAbsolutePrizePool()" max="100" min="0">
	                        <label for="siteRake" class="error hide" id="siteRakeError"></label>
										</div>
									</div>
								</div>

						
               <div class="col-sm-12">
                  <div class="row">
                     <label class="col-sm-12 control-label">Prize Pool :<span class="mandatory">*</span> </label>
                     <div class="col-sm-6">
                        <label class="control-label" ng-bind="'$' + contestParam.prizePool"></label>
                     </div>
                  </div>
               </div>
            

							</div>
							<div class="form-actions text-center">
				         <div class="form-group">
				            <button type="submit" class="btn btn-success"><i class=""></i>{{lang.save_contest}}</button>
				         </div>
				      </div>
			      </form>

					</div>

				</div>
			
		</div>
	</div>
	<!-- /Page content