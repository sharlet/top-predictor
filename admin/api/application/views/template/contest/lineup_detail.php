<div class="page-content">
	<!-- Game Detail Section Start -->
	<div class="col-md-12">
		<!-- Page title -->
		<div class="page-title">
			<h5><i class="fa fa-bars"></i>Contest Participants Information</h5>
		</div>
		<button class="btn btn-success" ng-click="goToContestDetails()"><i class=""></i>Back</button>
		<!-- /page title -->
		<div class="col-md-12">
			<form class="form-horizontal">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h6 class="panel-title">Participant Detail</h6>
					</div>
					<div class="table-responsive" >
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>User Name</th>
									<th>Email</th>
									<th>Team Name</th>
									<th>Total Score</th>									
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-show="LineupTeams.length == 0"><td colspan="5"><center>No Participant found</center></td></tr>
								<tr ng-repeat="lineup in LineupTeams">
									<td data-ng-bind="lineup.first_name + ' ' + lineup.last_name"></td>
									<td data-ng-bind="lineup.email"></td>
									<td data-ng-bind="lineup.team_name"></td>
									<td data-ng-bind="lineup.total_score"></td>
									<td data-ng-bind="lineup.status_label"></td>
									
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</form>
		</div>
	</div>	
</div>	