<!-- Page content -->
<div class="page-content">
	<!-- Page title -->
	<div class="page-title">
		<h5><i class="fa fa-bars"></i>League List</h5>
	</div>
	<!-- /page title -->

	<!-- Filter Section -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<h6 class="panel-title">Filters</h6>
		</div>
		<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>League</th>
						<th>Game Status</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-center">
							<select data-placeholder="Leagues" id="league_id" name="league_id" ng-model=" contestListParam.league_id" class="select-full" data-ng-change="filterContestList()" select-two="minimumResultsForSearch:'-2',width:'100%'">
								<option value="">Select League</option>
								<option value="{{league.league_id}}" ng-repeat="league in leagues">{{league.league_abbr}}</option>
							</select>
						</td>
						<td class="text-center">
							<select data-placeholder="All Game" id="game_status" ng-model=" contestListParam.status" class="select-full"  data-ng-change="filterContestList()" select-two="minimumResultsForSearch:'-2',width:'100%'">
								<option value="">Select Game Status</option>
								<option value="{{key}}" ng-repeat="(key,st) in contestStatus">{{st}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<td><a href="javascript:;" ng-click="clearContestFilter()"><span class="label label-info">Clear Filters</span></a></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- /Filter Section  -->
	<!-- Game List Section -->
	<div class="panel panel-default" ng-init="getContestList()">
		<div class="panel-heading"><h6 class="panel-title">Contests</h6></div>
		<div class="table-responsive">
			<table class="table table-hover table-striped table-bordered" ng-show="contestList.length==0">
			<thead>
					<tr>
						<td align="center">No Contest available, create new !</td>
					</tr>
			</thead>
			</table>
			<table class="table table-hover table-striped table-bordered" ng-show="contestList.length>0">
				<thead>
					<tr>
						<th class="pointer" ng-click="sortGameList('contest_name');">
							Contest
							<i ng-class="( contestListParam.orderByField=='contest_name' && contestListParam.sort_order=='DESC') ? 'fa-sort-desc':(( contestListParam.orderByField=='contest_name' && contestListParam.sort_order=='ASC') ? 'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortGameList('size');">
							Entrants/Participants
							<i ng-class="( contestListParam.orderByField=='size' && contestListParam.sort_order=='DESC')? 'fa-sort-desc':(( contestListParam.orderByField=='size' && contestListParam.sort_order=='ASC') ? 'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortGameList('entry_fee');">
							Fees
							<i ng-class="( contestListParam.orderByField=='entry_fee'&& contestListParam.sort_order=='DESC')?'fa-sort-desc':(( contestListParam.orderByField=='entry_fee'&& contestListParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortGameList('rewards');">
							Rewards
							<i ng-class="( contestListParam.orderByField=='rewards'&& contestListParam.sort_order=='DESC')?'fa-sort-desc':(( contestListParam.orderByField=='rewards'&& contestListParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortGameList('start_at');">
							Start Time
							<i ng-class="( contestListParam.orderByField=='start_at'&& contestListParam.sort_order=='DESC')?'fa-sort-desc':(( contestListParam.orderByField=='start_at'&& contestListParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th class="pointer" ng-click="sortGameList('is_featured');">
							Featured Contest
							<i ng-class="( contestListParam.orderByField=='is_featured'&& contestListParam.sort_order=='DESC')?'fa-sort-desc':(( contestListParam.orderByField=='is_featured'&& contestListParam.sort_order=='ASC')?'fa-sort-asc':'')" class="fa"></i>
						</th>
						<th ng-show="contestList[0].status !== 4 ">
							Action
						</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="contest in contestList">
						<td data-ng-bind="contest.contest_name" ng-hide="editObj.showEditBtn == contest.contest_id"></td>
						<td class="col-md-2" ng-show="editObj.showEditBtn == contest.contest_id">
							<input type="text"  class="form-control" name="edit_game_name[$index]" ng-model="editObj.edit_game_name[$index]" value="{{contest.contest_name}}" ng-init="editObj.edit_game_name[$index] = contest.contest_name" />
						</td>
						<td>
							{{contest.total_user_joined}}/
							<label ng-if="contest.size!=-1">{{contest.size}}</label>
							<label ng-if="contest.size==-1">&#8734;</label>
						</td>
						<td>
							<label  data-ng-bind-html="contest.entry_fee | salaryFormat"  ></label>
						</td>
						<td data-ng-bind-html="contest.rewards | salaryFormat"></td>
						<td data-ng-bind-html="contest.start_at"></td>
						<td>
							<label ng-if="contest.is_featured ==0">No</label>
							<label ng-if="contest.is_featured ==1">Yes</label>
						</td>
						<td ng-show="contest.status !== 4">
							<a href="contest_detail/{{contest.contest_uid}}">
								<span class="label label-primary">Detail</span>
							</a>
							<a href="javascript:void(0)"  class="btn btn-default btn-icon btn-xs tip" ng-click="editObj.showEditBtn = contest.contest_id" ng-hide="editObj.showEditBtn == contest.contest_id" data-original-title="Edit entry"><i class="fa fa-pencil"></i></a>
							<a href="javascript:void(0)"  class="btn btn-default btn-icon btn-xs tip" ng-click="updateLeagueName($index)" ng-show="editObj.showEditBtn == contest.contest_id" data-original-title="save entry"><i class="fa fa-save"></i></a>
							<a href="javascript:void(0)" 	class="btn btn-default btn-icon btn-xs tip" ng-click="openConfirmModal(contest)" 	ng-show="contest.total_user_joined == 0" data-original-title="delete entry"><i class="fa fa-trash-o"></i></a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="table-footer" ng-show="contestList.length > 0 &&  contestListParam.totalItems > 10">
			<pagination boundary-links="true" total-items=" contestListParam.totalItems" ng-model=" contestListParam.currentPage"
				ng-change="getContestList()" items-per-page=" contestListParam.itemsPerPage" class="pagination-sm pull-right"
				previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></pagination>
		</div>
	</div>
	<!-- User Transaction History End -->
</div>
<!-- /Page content -->

<!-- Confirm Model -->
<div id="confirm_modal" class="modal fade model-margin" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title">Confirm</h5>
            </div>

            <div class="modal-body has-padding">
                <p>Are you sure, you want to delete this contest?</p>
            </div>

            <div class="modal-footer">
                <button class="btn btn-warning" data-dismiss="modal">No</button>
                <button class="btn btn-primary" ng-click="deleteLeague()" >Yes</button>
            </div>
        </div>
    </div>
</div>
<!-- Confirm Model End-->
