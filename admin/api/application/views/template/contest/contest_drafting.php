<!-- Page content -->
<div class="page-content">
	<!-- Game Detail Section Start -->
	<div class="col-md-12">
		<!-- Page title -->
		<div class="page-title">
			<h5><i class="fa fa-bars"></i>Game Drafting</h5>
		</div>
		<!-- /page title -->
		
			<div class="panel panel-default">
				<div class="panel-heading"><h6 class="panel-title">Game Details</h6></div>
				<div class="panel-body">

					<div class="form-group">
						<div class="col-md-4">
							<div class="row">
								<label class="col-sm-4 control-label text-right">Game Name :</label>
								<div class="col-sm-8">
									<label class="control-label" ng-bind="gameDetail.game_name"></label>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-4">
							<div class="row">
								<label class="col-sm-4 control-label text-right">League :</label>
								<div class="col-sm-8">
									<label class="control-label" ng-bind="gameDetail.league_abbr"></label>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row">
								<label class="col-sm-4 control-label text-right">Pick Duration :</label>
								<div class="col-sm-8">
									<label class="control-label" ng-bind="gameDetail.pick_duration + ' Mins'"></label>
								</div>
							</div>
						</div>
						
						<div class="col-md-4">
							<div class="row">
								<label class="col-sm-4 control-label text-right">Drafting Style :</label>
								<div class="col-sm-8">
									<label class="control-label" ng-bind="gameDetail.game_draft"></label>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-4">
							<div class="row">
								<label class="col-sm-4 control-label text-right">Game Type :</label>
								<div class="col-sm-8">
									<label class="control-label" ng-bind="gameDetail.game_type"></label>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row">
								<label class="col-sm-4 control-label text-right">Entrants/Participants :</label>
								<div class="col-sm-8">
									<label class="control-label">
										{{gameDetail.total_user_joined}}/
										<label ng-if="gameDetail.size!=-1">{{gameDetail.size}}</label>
										<label ng-if="gameDetail.size==-1">&#8734</label>
									</label>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row">
								<label class="col-sm-4 control-label text-right">Entry fee :</label>
								<div class="col-sm-8">
									<label class="control-label" ng-bind="gameDetail.entry_fee | salaryFormat"></label>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">						
						<div class="col-md-4">
							<div class="row">
								<label class="col-sm-4 control-label text-right">Site Rake (%) :</label>
								<div class="col-sm-8">
									<label class="control-label" ng-bind="gameDetail.site_rake"></label>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row">
								<label class="col-sm-4 control-label text-right">Prizing :</label>
								<div class="col-sm-8">
									<label class="control-label" ng-bind="gameDetail.prize_pool | salaryFormat"></label>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row">
								<label class="col-sm-4 control-label text-right">Draft Start Date :</label>
								<div class="col-sm-8">
									<label class="control-label" ng-bind="gameDetail.draft_time"></label>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-4">
							<div class="row">
								<label class="col-sm-4 control-label text-right">Created Date :</label>
								<div class="col-sm-8">
									<label class="control-label" ng-bind="gameDetail.created_date"></label>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row">
								<label class="col-sm-4 control-label text-right">Status :</label>
								<div class="col-sm-8">
									<label class="control-label" ng-bind="gameDetail.status | titleCase"></label>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row">
								<label class="col-sm-4 control-label text-right">Created By :</label>
								<div class="col-sm-8">
									<label class="control-label" ng-if="gameDetail.user_name!=null" ng-bind="gameDetail.user_name"></label>
									<label class="control-label" ng-if="gameDetail.user_name===null">ADMIN</label>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-4">
							<div class="row">
								<label class="col-sm-4 control-label text-right">Scoring Type :</label>
								<div class="col-sm-8">
									<label class="control-label" ng-bind="gameDetail.scoring_type"></label>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row">
								<label class="col-sm-4 control-label text-right">Is Premuim Game :</label>
								<div class="col-sm-8">
									<label class="control-label" ng-if="gameDetail.is_premium_game !== '0'"> Yes</label>
									<label class="control-label" ng-if="gameDetail.is_premium_game == '0'">No</label>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row">
								<label class="col-sm-4 control-label text-right">Invite Permission :</label>
								<div class="col-sm-8">
									<label class="control-label" ng-bind="gameDetail.game_invite_permission"></label>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-4">
							<div class="row">
								<label class="col-sm-4 control-label text-right">Current Round Number :</label>
								<div class="col-sm-8">
									<label class="control-label" ng-if="gameDetail.current_round_number !== null" ng-bind="gameDetail.current_round_number"></label>
									<label class="control-label" ng-if="gameDetail.current_round_number == null">--</label>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="row">
								<label class="col-sm-4 control-label text-right">Current Pick Number :</label>
								<div class="col-sm-8">
									<label class="control-label" ng-if="gameDetail.current_pick_number !== null" ng-bind="gameDetail.current_pick_number"></label>
									<label class="control-label" ng-if="gameDetail.current_pick_number == null">--</label>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group" ng-if="gameDetail.draft_start_now">
						<div class="col-md-4">
							<div class="row">
								
								<div class="col-sm-8">
									<a href="javascript:void(0)" >									
										<span class="label label-info" ng-if="lineup.lineup_master_id==currentLineupMasterID">Start Now</span>
									</a>									
								</div>
							</div>
						</div>						
					</div>
								
				</div>
			</div>
		
	</div>
	
	<div class="col-md-12">
		
	</div>
	<!-- Game Detail Section End -->
	
</div>
<!-- /Page content -->