<!--Page content -->
<div class="page-content" ng-init="getAllContestData()">
   <!-- Page title -->
   <div class="page-title">
      <h5><i class="fa fa-align-justify"></i>Create New Contest</h5>
   </div>
   <!-- page title -->

   <!-- User Detail Section Start -->
   <form class="form-horizontal" role="form" contest-form submit-handle="createContest()">
      <div class="panel panel-default">
         <div class="panel-heading">
            <h6 class="panel-title" >Contest - <span ng-bind="contestParam.contestName"></span></h6>
         </div>
         <!-- Start panel body -->
         <div class="panel-body">
            <div class="form-group">
               <div class="col-md-6">
                  <div class="row">
                     <label class="col-md-4 control-label" for="league">League :<span class="mandatory">*</span> </label>
                     <div class="col-md-8">
                        <select ng-model="contestParam.leagueId" name="leagueId" id="leagueId" data-placeholder="Select League" select-two="minimumResultsForSearch:'-2',width:'100%'" data-placeholder="{{lang.select_league}}" ng-change="OnLeagueChange()">
                           <option></option>
                           <option ng-repeat="lg in leagues" value="{{lg.league_id}}" ng-bind="lg.league_abbr"></option>
                        </select>
                        <label for="leagueId" class="error hide" id="leagueIdError"></label>
                     </div>
                  </div>
               </div>
            </div>

            <div class="form-group">
               <div class="col-md-6">
                  <div class="row" >
                     <label class="col-md-4 control-label" for="contestName">Contest Name :</label>
                     <div class="col-md-8">
                        <input type="text" name="contestName" id="contestName" ng-model="contestParam.contestName" class="form-control" maxlength="100">
                        <label for="contestName" class="error hide" id="contestNameError"></label>
                     </div>
                  </div>
               </div>
            </div>

            <div class="form-group">
               <div class="col-md-6">
                  <div class="row" >
                     <label class="col-md-4 control-label" for="contestName">Contest Week :</label>
                     <div class="col-md-8">
                        <select ng-model="contestParam.week" name="week" id="contestWeek" data-placeholder="Select Week" 
                              select-two="minimumResultsForSearch:'-2',width:'100%'" data-placeholder="{{lang.week}}" ng-change="getWeekMatch()" >
                           <option></option>
                           <option ng-repeat="week in master_season_week" value="{{week.season_week}}" >{{week.round_type}} Week  {{week.season_week}}</option>
                        </select>
                        <label for="contestWeek" class="error hide" id="contestWeekError"></label>
                     </div>
                  </div>
               </div>
            </div>

            <div class="form-group">
               <div class="col-md-12">
                  <div class="row" >
                     <label class="col-md-2 control-label" for="contestName">Match List :</label>
                     <div class="col-md-10" ng-if="weekMatches.length === 0"> Match list will come here on selection of week </div>
                     <div class="col-md-10" ng-if="weekMatches.length > 0"> 
                        <ul class="week-matches">
                           <li class="match-block">
                              <a href="javascript:void(0)" ng-class="(contestParam.optMatches.length == weekMatches.length) ? 'active':'' " ng-click="optAllMatches()">
                                 <span class="opt-all-matches"> Select All </span>
                              </a>
                           </li>
                           <li ng-repeat="match in  weekMatches" class="match-block">
                              <a href="javascript:void(0)" ng-class="contestParam.optMatches.indexOf(match.season_id) > -1 ? 'active':'' "  ng-click="toggleOptMatch(match.season_id)">
                                 <span class="team-abbr" ng-bind=" match.home_team_abbr + ' VS '+ match.away_team_abbr "></span>
                                 <span class="match-datetime" ng-bind=" match.season_scheduled_date"></span>
                              </a>
                           </li>                           
                        </ul>
                     </div>
                  </div>
               </div>
            </div>

              <div class="form-group">
               <div class="col-md-6">
                  <div class="row" >
                     <label class="col-md-4 control-label" for="salaryCap">Salary Cap :</label>
                     <div class="col-md-8">
                        <select ng-model="contestParam.salaryCap" name="salaryCap" id="salaryCap" data-placeholder="Select Salary Cap" 
                              select-two="minimumResultsForSearch:'-2',width:'100%'" data-placeholder="{{lang.salaryCap}}"  >
                           <option ng-repeat="cap in salaryCap" value="{{cap.salary_cap}}" >{{cap.salary_cap}}</option>
                        </select>
                        <label for="salaryCap" class="error hide" id="salaryCapError"></label>
                     </div>
                  </div>
               </div>
            </div>

            <div class="form-group" >
               <div class="col-md-6">
                  <div class="row">
                     <label class="col-md-4 control-label"  for="entry">Entry Fee :<span class="mandatory">*</span> </label>
                     <div class="col-md-8">
                        <input type="text" class="form-control" name="entryFee"  id="entryFee" numbers-only  ng-blur="calculateAbsolutePrizePool()" numbers-only ng-model="contestParam.entryFee"   >
                        <label class="text-muted">Entry fee ranges from  {{entryFee.min}} to {{entryFee.max}} </label>
                        <label for="entryFee" class="error hide" id="entryFeeError"></label>
                     </div>
                  </div>
               </div> 
            </div>

            <div class="form-group" >
               <div class="col-md-6">
                  <div class="row">
                     <label class="col-md-4 control-label">Size :<span class="mandatory">*</span> </label>
                     <div class="col-md-8">
                        <input type="text" name="size" class="form-control" id="size"  ng-blur="calculateAbsolutePrizePool()" intiger-only ng-model="contestParam.size"  >
                        <label for="size" class="error hide" id="sizeError"></label>
                     </div>
                  </div>
               </div>
            </div>

            <div class="form-group" >
               <div class="col-md-6">
                  <div class="row">
                     <label class="col-md-4 control-label">Site Rake :<span class="mandatory">*</span> </label>
                     <div class="col-md-8">
                        <input type="text" name="siteRake" class="form-control" id="siteRake"  intiger-only ng-model="contestParam.siteRake" ng-blur="calculateAbsolutePrizePool()" max="100" min="0">
                        <label for="siteRake" class="error hide" id="siteRakeError"></label>
                     </div>
                  </div>
               </div>
            </div>

            <div class="form-group" >
               <div class="col-md-6">
                  <div class="row">
                     <label class="col-md-4 control-label">Prize Pool :<span class="mandatory">*</span> </label>
                     <div class="col-md-8">
                        <label class="control-label" ng-bind="'$' + contestParam.prizePool"></label>
                     </div>
                  </div>
               </div>
            </div>

            <div class="form-group">
               <div class="col-md-6">
                  <div class="row" >
                     <label class="col-md-4 control-label" for="contestName">Prize Payout :</label>
                     <div class="col-md-8">
                        <select ng-model="contestParam.prizePayoutId" name="prizePayout" id="prizePayout" data-placeholder="Select payout structure" select-two="minimumResultsForSearch:'-2',width:'100%'" ng-change="calculateAbsolutePrizePool()"  >
                           <option></option>
                           <option ng-repeat= " struct in prizePayoutStructure " value="{{struct.master_prize_payout_id}}"> {{struct.name}}</option>
                        </select>
                        <label for="prizePayout" class="error hide" id="prizePayoutError"></label>
                     </div>
                  </div>
               </div>
            </div>

            <div class="form-group">
               <div class="col-md-6">
                  <div class="row" >
                     <label class="col-md-4 control-label" for="contestName">Payout Structure :</label>
                     <div class="col-md-8">
                        <ul>
                           <li ng-repeat = " amountPerPlace in prizeComposition " class="payout-structure-desc" >
                              <span ng-bind="amountPerPlace"></span>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>

            <div class="form-group" >
               <div class="col-md-6">
                  <div class="row">
                     <label class="col-md-4 control-label">Extra Rewards :<span class="mandatory">*</span> </label>
                     <div class="col-md-8">
                        <textarea   name="custom_rewards" class="form-control" id="custom_rewards"   ng-model="contestParam.customRewards"  maxlength="2000"></textarea>
                        <label for="custom_rewards" class="error hide" id="custom_rewardsError"></label>
                     </div>
                  </div>
               </div>
            </div>


            
 
            <div class="form-group">
               <div class="col-md-6">
                  <div class="row">
                     <label class="col-md-4 control-label" for="">Is Featured Game : </label>
                     <div class="col-md-6">
                        <div class="input-group">
                           <div class="checker">
                              <input type="checkbox"  class="styled" uniform="radioClass:'choice', selectAutoWidth:false" ng-checked = "{{contestParam.isFeaturedGame == true}}"  ng-model="contestParam.isFeaturedGame">
                           </div>                           
                        </div>
                     </div>
                  </div>
               </div>          
            </div>
         </div>  
         <!-- End of panel body -->
      </div>
      <div class="form-actions text-center">
         <div class="form-group">
            <button type="submit" class="btn btn-success"><i class=""></i>{{lang.save_contest}}</button>
         </div>
      </div>
   </form>
</div>