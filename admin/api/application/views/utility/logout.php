<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Logging you out ....</title>
	<script type="text/javascript">
		sessionStorage.clear();
		window.location.href = '<?php echo site_url(); ?>';
	</script>
</head>
<body>
	<p>Please wait you will be redirect shortly.</p>
</body>
</html>