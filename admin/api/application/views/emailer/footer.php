<tr>
      <td style="padding:0 30px 10px 30px;background-color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:14px;">Questions?<br/> 
Please send us an email at <a href="mailto:<?php echo SUPPORT_EMAIL; ?>" style="color:#247CDC; text-decoration:none; cursor:pointer;"><?php echo SUPPORT_EMAIL; ?></a> and we will revert as soon as possible.</td>
    </tr> 
    <tr>
      <td style="padding:0 30px 10px 30px; background-color:#ffffff;">&nbsp;</td>
    </tr>
    <tr>
      <td style="background-color:#ffffff;">&nbsp;</td>
    </tr>
    <tr>
     <td style="padding:0 30px 10px 30px; background-color:#ffffff; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
      Thanks,<br/>
      <?php echo PROJECT_NAME_SUPPORT; ?> Team
      </td>    
      </tr>
      <tr>
      <td style="background-color:#ffffff;">&nbsp;</td>
    </tr>
     <tr>
      <td valign="middle" style="background-color:#247CDC; height:40px; text-align:center; color:#ffffff;">
      &copy; <?php echo date( 'Y' ); ?> <?php echo PROJECT_NAME_SUPPORT; ?>. ALL RIGHTS RESERVED.
      </td>
     </tr> 
  </table>
    </td>  
    </tr>   
</table>
</body>
</html>