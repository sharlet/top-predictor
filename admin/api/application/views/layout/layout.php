<!DOCTYPE html>
<html lang="en" ng-app="vfantasy">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title ng-bind="$state.current.data.pageTitle+' | Fantasy DFS'"></title>
<base href="<?php echo base_url();?>">

<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="assets/css/brain-theme.css">
<link rel="stylesheet" type="text/css" href="assets/css/styles.css">
<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Cuprum">
<link rel="stylesheet" type="text/css" href="assets/css/custom.css">
<!-- <link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"> -->
<link rel="shortcut icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon">

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/vendor/md5.min.js"></script>
<?php $this->load->view('include/global_script_var');?>

<link id="ng_load_plugins_before">
</head>
<body ng-class="{'full-width':!settings.layout.pageBodyFullWidth}" ng-controller="CommonCtrl">
	<div class="body-loader page-on-load"></div>
	<!-- BEGIN PAGE SPINNER -->
	<div ng-spinner-bar="" class="page-spinner-bar">
		<div class="bounce1"></div>
		<div class="bounce2"></div>
		<div class="bounce3"></div>
	</div>
	<!-- END PAGE SPINNER -->

	<!-- Navbar -->
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<div class="hidden-lg pull-right">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar">
						<span class="sr-only">Toggle sidebar</span>
						<i class="fa fa-bars"></i>
					</button>
				</div>
				<ul class="nav navbar-nav navbar-left-custom" ng-class="{hide:!is_logged_in}">
					<li class="user dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown">
							<!-- <img src="http://placehold.it/500" alt=""> -->
							<span>Admin</span>
							<i class="caret"></i>
						</a>
						<ul class="dropdown-menu">
							<li><a href="change_password"><i class="fa fa-cog"></i> Settings</a></li>
							<li><a ng-click="logout()"><i class="fa fa-mail-forward"></i> Logout</a></li>
						</ul>
					</li>
					<li><a class="nav-icon sidebar-toggle" ng-click="settings.layout.pageBodyFullWidth = !settings.layout.pageBodyFullWidth;"><i class="fa fa-bars"></i></a></li>
				</ul>
			</div>
			<ul ng-if="is_logged_in" class="nav navbar-nav navbar-right ng-hide">
				<li class="user dropdown" ng-init="getLanguage()">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<span>Language</span>
						<i class="caret"></i>
					</a>
					<ul class="dropdown-menu">
						<li ng-repeat="(key, value) in language_list">
							<a href="#" ng-class="{active:site_language===key}"><i class="fa fa-language"></i> {{value}}</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<!-- /navbar -->

	<!-- Page header -->
	<div class="container-fluid">
		<div class="page-header">
			<div class="logo">
				<a href="./"><img src="assets/images/logo.png" alt="<?php echo PROJECT_NAME_SUPPORT; ?>"> <label style="font-size: 31px; margin-left: 10px; color: rgb(60, 154, 201); font-weight: bold; "><?php echo PROJECT_NAME_SUPPORT; ?></label></a>

			</div>
		</div>
	</div>
	<!-- /page header -->

	<!-- Page container -->
	<div class="page-container container-fluid">
		<!-- Sidebar -->
		<div class="sidebar collapse" ng-if="settings.layout.pageBodyFullWidth">
			<ul class="navigation">
				<!-- <li class="dashboard" ng-class="{active:routename=='dashboard'}">
					<a href="dashboard"><i class="fa fa-bars"></i>Dashboard</a>
				</li> -->
				<li class="finance" ng-class="{active:(['advertisement','new_advertisement'].indexOf(routename)!==-1)}">
					<a href="javascript:void(0);" class="expand" ng-class="(['advertisement','new_advertisement'].indexOf(routename)!==-1)?'level-opened':'level-closed'">
						<i class="fa fa-bars"></i>Manage Advertisements
					</a>
					<ul sub-menu="{{['advertisement','new_advertisement'].indexOf(routename)!==-1}}">
						<li ng-class="{active:routename=='advertisement'}"><a href="advertisement">View Advertisements</a></li>
						<li ng-class="{active:routename=='new_advertisement'}"><a href="new_advertisement">{{lang.new_advertisement}}</a></li>
					</ul>
				</li> 

				<?php if(ENVIRONMENT != 'production') { ?>
				<li ng-class="{active:routename=='change_date'}">
					<a href="change_date"><i class="fa fa-bars"></i>Manage Date</a>
				</li>
				<?php } ?>

				<li class="finance" ng-class="{active:(['withdrawal_list','transaction_list'].indexOf(routename)!==-1)}">
					<a href="javascript:void(0);" class="expand" ng-class="(['withdrawal_list','transaction_list'].indexOf(routename)!==-1)?'level-opened':'level-closed'">
						<i class="fa fa-bars"></i>Manage Finances
					</a>
					<ul sub-menu="{{['withdrawal_list','transaction_list'].indexOf(routename)!==-1}}">
						<li ng-class="{active:routename=='withdrawal_list'}"><a href="withdrawal_list"> Withdrawal List</a></li>
						<li ng-class="{active:routename=='transaction_list'}"><a href="transaction_list"> Transaction List</a></li>
					</ul>
				</li>
				 <li class="contest" ng-class="{active:(['contest','new_contest','contest_detail'].indexOf(routename)!==-1)}">
					<a href="javascript:void(0);" class="expand" ng-class="(['contest','new_contest','contest_detail'].indexOf(routename)!==-1)?'level-opened':'level-closed'">
						<i class="fa fa-bars"></i>Manage Contest
					</a>
					<ul sub-menu="{{['contest','new_contest','contest_detail'].indexOf(routename)!==-1}}">
						<li ng-class="{active:routename=='contest'}"><a href="contest">View Contest</a></li>
						<li ng-class="{active:routename=='new_contest'}"><a href="new_contest">New Contest</a></li>
					</ul>
				</li>
				<li class="contest" ng-class="{active:(['news_list','create_news','news_detail','edit_news','manage_news_sequence'].indexOf(routename)!==-1)}">
					<a href="javascript:void(0);" class="expand" ng-class="(['news_list','create_news','news_detail','edit_news','manage_news_sequence'].indexOf(routename)!==-1)?'level-opened':'level-closed'">
						<i class="fa fa-bars"></i>Manage News
					</a>
					<ul sub-menu="{{['news_list','create_news','news_detail','edit_news','manage_news_sequence'].indexOf(routename)!==-1}}">
						<li ng-class="{active:(['news_list','edit_news'].indexOf(routename)!==-1)}"><a href="news_list"> View News</a></li>
						<li ng-class="{active:routename=='create_news'}"><a href="create_news">Create News</a></li>
						<!-- <li ng-class="{active:routename=='manage_news_sequence'}"><a href="manage_news_sequence">Manage News Sequence</a></li> -->
					</ul>
				</li>


				<li class="roster" ng-class="{active:routename=='roster'}">
					<a href="roster"><i class="fa fa-bars"></i>Manage Rosters</a>
				</li>
				<li ng-class="{active:routename=='season'}">
					<a href="season" ng-click="redirectUrl('season')" ><i class="fa fa-bars"></i>Manage Schedules</a>
				</li>

				<li class="teamroster" ng-class="{active:routename=='team_roster'}">
					<a href="teamroster"><i class="fa fa-bars"></i>Manage Teams</a>
				</li>


				<li ng-class="{active:routename=='user'}">
					<a href="user"><i class="fa fa-bars"></i>Manage Users</a>
				</li>
							
				<li ng-class="{active:routename=='manage_video'}">
					<a href="manage_video"><i class="fa fa-bars"></i>Manage Videos</a>
				</li>	
			</ul>
		</div>
		<!-- /sidebar -->

		<!-- Main contain load here -->
		<ui-view></ui-view>
		<!-- /Main contain load here -->
	</div>
	<!-- /Page container -->
	<!-- Error Message Observer Section -->
	<span show-message="{{alert_success}}"></span>
	<span show-message="{{alert_error}}"></span>
	<span show-message="{{alert_warning}}"></span>
	<!-- End-->
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/interface/collapsible.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/interface/jgrowl.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/interface/colorpicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/interface/jquery-ui-timepicker-addon.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>assets/js/vendor/angular.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/vendor/angular-ui-router.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/vendor/ocLazyLoad.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/forms/validate.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins/angular/ui-bootstrap-tpls.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/application_blank.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/vendor/owl.carousel.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/vendor/svg4everybody.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/static/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/service/services.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/directive/directive.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/controller/commonController.js"></script>

	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular-sanitize.js"></script>
</body>
</html>