<script type="text/javascript">
	Object.defineProperty(window, 'site_url', {value:'<?php echo site_url()?>', writable:false});
	Object.defineProperty(window, 'base_url', {value:'<?php echo base_url()?>', writable:false});
	Object.defineProperty(window, 'AUTH_KEY', {value:'<?php echo AUTH_KEY?>', writable:false});
	Object.defineProperty(window, 'CURRENCY_CODE', {value:'<?php echo CURRENCY_CODE ;?>', writable:false});
	Object.defineProperty(window, 'SERVER_GLOBAL', {value:{}, writable:true});

	<?php if($this->session->userdata(AUTH_KEY)){ ?>
		sessionStorage.setItem('<?php echo AUTH_KEY; ?>', '<?php echo $this->session->userdata(AUTH_KEY); ?>');
	<?php } ?>
</script>