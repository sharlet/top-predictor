<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function format_date( $date = 'today' , $format = 'Y-m-d H:i:s' )
{
	if( $date=="today" )
	{
		if ( IS_LOCAL_TIME === TRUE )
		{
			$back_time = strtotime( BACK_YEAR );
			$dt = date( $format , $back_time );
		}
		else
		{
			$dt = date( $format );
		}
	}
	else
	{
		if( is_numeric ( $date ) )
		{
			$dt = date( $format , $date );
		}
		else
		{
			if( $date != null )
			{
				$dt = date( $format , strtotime ( $date ) );
			}
			else
			{
				$dt="--";
			}
		}
	}
	if(ENVIRONMENT == 'production')
	{
		return $dt;
	}
	else
	{
		$path = ROOT_PATH.'date_time.php';
		if(file_exists($path))
		{
			include($path);
		}
		
		if ( isset( $date_time ) && $date_time  )
		{
			$dt = date( $format , strtotime( $date_time ) );
		}
		return $dt;
	}	
}

function prize_json()
{
    $json = '{  "Top 1":{"1":100},
    			"Top 2":{"1":60,"2":40},
    			"Top 3":{"1":50,"2":30,"3":20},
    			"Top 5":{"1":30,"2":25,"3":20,"4":15,"5":10},
    			"Top 10":{"1":19,"2":17,"3":15,"4":13,"5":11,"6":9,"7":7,"8":5,"9":3,"10":1},
    			"Top 30%":{"30":100},
    			"Top 50%":{"50":100}
    		 }
    	    ';
    return $json;
}

function format_num_callback($n)
{
    return floatval( str_replace(',', '', $n) );
}

function truncate_number( $number = 0 , $decimals = 2 )
{
	$point_index = strrpos( $number , '.' );
	if($point_index===FALSE) return $number;
	return substr( $number , 0 , $point_index + $decimals + 1 );
}

function debug( $msg , $die=FALSE )
{
	echo ( "<style> pre{background-color:chocolate;font-weight:bolder;} .debug{color: black;text-align:center;background-color:yellow;font-weight:bolder;padding:10px;font-size:14px;}</style>" );

	echo ("\n<p class='debug'>\n");

	echo ("MSG".time().": ");

	if ( is_array ( $msg ) )
	{
		echo ( "\n<pre>\n" );
		print_r ( $msg );
		echo ( "\n</pre>\n" );
	}
	elseif ( is_object ( $msg ) )
	{
		echo ( "\n<pre>\n" );
		var_dump ( $msg );
		echo ( "\n</pre>\n" );
	}
	else
	{
		echo ( $msg );
	}

	echo ( "\n</p>\n" );

	if ( $die )
	{
		die;
	}
}
function array_pluck($key, $array)
{
    if (is_array($key) || !is_array($array)) return array();
    $funct = create_function('$e', 'return is_array($e) && array_key_exists("'.$key.'",$e) ? $e["'. $key .'"] : null;');
    return array_map($funct, $array);
}


function get_api_data($url)
{
	$curl_handle=curl_init();
	curl_setopt($curl_handle,CURLOPT_URL,$url);
	curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
	curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
	$buffer = curl_exec($curl_handle);
	curl_close($curl_handle);
	if (empty($buffer)){
		return  "Nothing returned from url.<p>";
	}else{
		return  $buffer;
	}
}

//Function to replace null value to blank
function replacer(& $item, $key){
    if ($item === null) 
    {
        $item = '';
    }
}

function remove_null_values($input_arry = array())
{
	if(empty($input_arry))
		return $input_arry;

	array_walk_recursive($input_arry, 'replacer');
	return $input_arry;
}

function sec_to_h_m_s_convert($sec) {

    if ($sec <= 0) {
        return '00:00:00';
    }

    $h          = $sec / 3600;
    $remain_sec = $sec % 3600;
    $m          = $remain_sec / 60;
    $s          = $remain_sec % 60;

    return floor($h) . ':' . floor($m) . ":" . floor($s);
}

function print_q()
{
	$CI = &get_instance();
	echo $CI->db->last_query();
}

function print_qe()
{
	$CI = &get_instance();
	echo $CI->db->last_query();
	exit();
}

function replace_quotes($string)
{
	return preg_replace(array("/`/", "/'/", "/&acute;/"), "",$string);
}