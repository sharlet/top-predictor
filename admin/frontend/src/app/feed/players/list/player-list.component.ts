import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { FeedService } from '../../../services/feed.service';
import { AlertService } from '../../../services/alert.service';
import { range, hideModal } from '../../../services/utils.service';

@Component({
  selector: 'app-feed-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.scss']
})
export class PlayerListComponent implements OnInit {

    public params = {
        items_perpage: 20,
        current_page: 1,
        sort_field: 'name',
        sort_order: 'ASC',
        league_id: '',
        keyword: '',
        team_abbr: '',
        position: ''
    };
    public players = [];
    public teams = [];
    public leagues = [];
    public playerPositions = [];
    public playerStatus = {
        0: 'Inactive',
        1: 'Active',
        2: 'Deleted'
    };

    public totalPlayers = 0;
    public totalPaginationShow = [];
    public totalPages = 0;
    public editPlayer = { };
    public updatePlayerParams = {
        league_id: this.params.league_id,
        status: '',
        salary: 0,
        injury: '',
        player_unique_id: ''
    };

    public updatePlayerError = '';

    constructor(
        private feedService: FeedService,
        private alertService: AlertService
    ) { }

    ngOnInit() {
        this.getActiveLeagues();
    }

    private getActiveLeagues() {
        this.feedService.getActiveLeagues({}).pipe(first())
            .subscribe( (leagueData: [] ) => {
                this.leagues =  leagueData['data'];
                this.params.league_id = this.leagues[0].league_id;

                this.getPlayerTeams();
                this.getPlayerPosition();
                this.getPlayers();

            }, (err: object) => {
                console.log('Error in active leagues', err);
            }
        );
    }

    public getPlayerTeams() {
        const league_id = this.params.league_id;
        this.feedService.getAllTeams({ league_id }).pipe(first())
            .subscribe( (teamData: [] ) => {
                this.teams = teamData['data'];
                this.params.team_abbr = this.teams[0].team_abbr;

            }, (err: object) => {
                console.log('match List component error ', err);
            }
        );
    }

    public getPlayerPosition() {
        const league_id = this.params.league_id;
        this.feedService.getPlayerPositions({ league_id }).pipe(first())
            .subscribe( (positionData: [] ) => {
                this.playerPositions = positionData['data'];
                this.params.position = this.playerPositions[0].position_value;

            }, (err: object) => {
                console.log('match List component error ', err);
            }
        );
    }

    public getPlayers() {
        this.feedService.getPlayers(this.params).pipe(first())
            .subscribe( (playerData: [] ) => {
                this.players = playerData['data'].result;
                this.totalPlayers = playerData['data'].total;
                this.createPaginationItem(this.totalPlayers);
            }, (err: object) => {
                console.log('player List component error ', err);
            }
        );
    }


    private createPaginationItem(totalPlayers: number) {
        this.totalPlayers = totalPlayers;
        const maxPages: number = Math.ceil(totalPlayers / this.params.items_perpage);
        const end = (this.params.current_page + 5) < maxPages ?  this.params.current_page + 5 : maxPages;
        const start = (this.params.current_page - 5) > 1 ?  this.params.current_page - 5 : 1;
        this.totalPages = maxPages;
        this.totalPaginationShow = range(end, start);
    }

    public paginateList(newPage: number) {
        if (this.params.current_page === newPage) { return false; }
        this.params.current_page = newPage;
        this.getPlayers();
    }

    public nextOrPreviousPage(deviation: number) {
        this.params.current_page = this.params.current_page + deviation ;
        this.getPlayers();
    }

    public applyFilter() {
        this.params.current_page = 1;
        this.getPlayers();
    }

    public showStatusModal(player: object) {
        this.editPlayer = { full_name: player['full_name'], player_status: player['player_status'] };

        this.updatePlayerParams.league_id = this.params.league_id,
        this.updatePlayerParams.player_unique_id = player['player_unique_id'];
        this.updatePlayerParams.salary = player['salary'];
        this.updatePlayerParams.injury = player['injury_status'];
        this.updatePlayerParams.status = player['player_status'];
    }
    public updatePlayerStatus() {
        this.updatePlayerError = '';
        if (this.updatePlayerParams.status == '') {
            this.updatePlayerError = 'Please select status';
            return false;
        } else if (this.updatePlayerParams.salary < 0) {
            this.updatePlayerError = 'Salary can not be less than zero';
            return false;
        }

        this.feedService.updatePlayerStatus(this.updatePlayerParams).pipe(first())
            .subscribe( (response: [] ) => {
                hideModal('playerStatusModal');
                this.alertService.success('Player details updated successfully');
                this.getPlayers();
            }, (err: object) => {
                this.updatePlayerError = err['message'];
                console.log('player List component error ', err);
            }
        );
    }





}
