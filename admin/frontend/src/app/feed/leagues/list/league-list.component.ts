import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { FeedService } from '../../../services/feed.service';

import { range } from '../../../services/utils.service';

@Component({
  selector: 'app-feed-league-list',
  templateUrl: './league-list.component.html',
  styleUrls: ['./league-list.component.scss']
})
export class LeagueListComponent implements OnInit {

    public leagueList = [];
    public leagueStatus = {
        '0': 'Inactive',
        '1': 'Active'
    };

    constructor(
        private feedService: FeedService
    ) { }

    ngOnInit() {
        this.getLeagueList();
    }

    public  getLeagueList() {
        this.feedService.getLeagues().pipe(first())
        .subscribe( (leagues: [] ) => {
            this.leagueList = leagues['data'];
        }, (err: object) => {
                console.log('League List component error ', err);
            }
        );
    }

}
