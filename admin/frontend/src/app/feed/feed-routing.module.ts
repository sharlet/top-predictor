import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LeagueListComponent } from './leagues/list/league-list.component';
import { TeamListComponent } from './teams/list/team-list.component';
import { MatchListComponent } from './matches/list/match-list.component';
import { PlayerListComponent } from './players/list/player-list.component';

const routes: Routes = [
    { path: 'leagues', component: LeagueListComponent },
    { path: 'teams', component: TeamListComponent },
    { path: 'fixtures', component: MatchListComponent },
    { path: 'players', component: PlayerListComponent },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class FeedRoutingModule {}

export const routedComponents: Array<any> = [
    LeagueListComponent,
    TeamListComponent,
    MatchListComponent,
    PlayerListComponent,
];
