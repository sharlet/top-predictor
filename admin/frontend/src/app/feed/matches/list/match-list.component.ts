import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { FeedService } from '../../../services/feed.service';
import { range } from '../../../services/utils.service';

@Component({
  selector: 'app-feed-match-list',
  templateUrl: './match-list.component.html',
  styleUrls: ['./match-list.component.scss']
})
export class MatchListComponent implements OnInit {

    public params = {
        items_perpage: 20,
        current_page: 1,
        sort_field: 'name',
        sort_order: 'ASC',
        keyword: '',
        league_id: '',
        team_id: ''
    };
    public LeagueList = [];
    public teamList = [];
    public matchList = [];
    public totalMatches = 0;
    public totalPaginationShow = [];
    public totalPages = 0;


    constructor(
        private feedService: FeedService
    ) { }

    ngOnInit() {
        this.getActiveLeagues();
    }

    private getActiveLeagues() {
        this.feedService.getActiveLeagues({}).pipe(first())
            .subscribe( (leagues: [] ) => {
                this.LeagueList =  leagues['data'];
                this.params.league_id = this.LeagueList[0].league_id;

                this.getMatchTeams();
                this.getMatchList();

            }, (err: object) => {
                console.log('Error in active leagues', err);
            }
        );
    }

    public getMatchTeams() {
        const league_id = this.params.league_id;
        this.feedService.getAllTeams({ league_id }).pipe(first())
            .subscribe( (matchTeams: [] ) => {
                console.log('matchTeams', matchTeams);
                this.teamList = matchTeams['data'];
                this.params.team_id = this.teamList[0].team_id;

            }, (err: object) => {
                console.log('match List component error ', err);
            }
        );
    }

    public  getMatchList() {
        this.feedService.getMatches(this.params).pipe(first())
            .subscribe( (matches: [] ) => {
                console.log('Match list ', matches);
                this.matchList = matches['data'].result;
                this.totalMatches = matches['data'].total;
                this.createPaginationItem(this.totalMatches);
            }, (err: object) => {
                console.log('match List component error ', err);
            }
        );
    }

    private createPaginationItem(totalMatch: number) {
        this.totalMatches = totalMatch;
        const maxPages: number = Math.ceil(totalMatch / this.params.items_perpage);
        const end = (this.params.current_page + 5) < maxPages ?  this.params.current_page + 5 : maxPages;
        const start = (this.params.current_page - 5) > 1 ?  this.params.current_page - 5 : 1;
        this.totalPages = maxPages;
        this.totalPaginationShow = range(end, start);
    }

    public paginateList(newPage: number) {
        if (this.params.current_page === newPage) { return false; }
        this.params.current_page = newPage;
        this.getMatchList();
    }

    public nextOrPreviousPage(deviation: number) {
        this.params.current_page = this.params.current_page + deviation ;
        this.getMatchList();
    }

    public applyFilter() {
        this.params.current_page = 1;
        this.getMatchList();
    }





}
