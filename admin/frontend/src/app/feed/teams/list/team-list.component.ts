import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { FeedService } from '../../../services/feed.service';

import { range } from '../../../services/utils.service';

@Component({
  selector: 'app-feed-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.scss']
})
export class TeamListComponent implements OnInit {

    public params = {
        items_perpage: 10,
        current_page: 1,
        sort_field: 'name',
        sort_order: 'ASC',
        league_id: 0,
        keyword: ''
    };
    public TeamList = [];
    public LeagueList = [];
    public totalTeams = 0;
    public totalPaginationShow = [];
    public totalPages = 0;

    constructor(
        private feedService: FeedService
    ) { }

    ngOnInit() {
        this.getActiveLeagues();
    }

    private getActiveLeagues() {
        this.feedService.getActiveLeagues({}).pipe(first())
            .subscribe( (leagues: [] ) => {
                this.LeagueList =  leagues['data'];
                this.params.league_id = this.LeagueList[0].league_id;

                this.getTeamList();

            }, (err: object) => {
                    console.log('Error in active leagues', err);
                }
            );
    }

    public  getTeamList() {
        this.feedService.getTeams(this.params).pipe(first())
            .subscribe( (team: [] ) => {
                console.log(team);
                this.TeamList = team['data'].result;
                this.createPaginationItem(team['data'].total);
            }, (err: object) => {
                console.log('Team List component error ', err);
            }
        );
    }


    private createPaginationItem(totalTeams: number) {
        this.totalTeams = totalTeams;
        const maxPages: number = Math.ceil(totalTeams / this.params.items_perpage);
        const end = (this.params.current_page + 5) < maxPages ?  this.params.current_page + 5 : maxPages;
        const start = (this.params.current_page - 5) > 1 ?  this.params.current_page - 5 : 1;
        this.totalPages = maxPages;
        this.totalPaginationShow = range(end, start);
    }

    public paginateList(newPage: number) {
        if (this.params.current_page === newPage) { return false; }
        this.params.current_page = newPage;
        this.getTeamList();
    }

    public nextOrPreviousPage(deviation: number) {
        this.params.current_page = this.params.current_page + deviation ;
        this.getTeamList();
    }

    public applyFilter() {
        this.params.current_page = 1;
        this.getTeamList();
    }

}
