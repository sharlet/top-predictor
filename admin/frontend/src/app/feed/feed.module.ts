import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { FeedRoutingModule, routedComponents } from './feed-routing.module';


@NgModule({
    imports: [
        FeedRoutingModule,
        CommonModule,
        FormsModule
    ],
    declarations: [
        ...routedComponents
    ],
    providers: [
    ]
})
export class FeedModule { }
