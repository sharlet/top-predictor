import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { API_URL } from './utils.service';


@Injectable({ providedIn: 'root' })
export class UserService {

    constructor(
        private http: HttpClient
    ) {  }

    getUsers(params: object) {
        return  this.http.post(`${API_URL}/user/users`, params);
    }
}
