import * as jQuery from 'jquery';

export const API_URL = 'http://localhost/fantasy_dfs/admin/api';

export function encryptPassword (password: string): string {
    return btoa(password);
}

export function range(size: number, startAt = 1) {
    const ans = [];
    for (let i = startAt; i <= size; i++) {
        ans.push(i);
    }
    return ans;
}


export function hideModal(id: string) {
    jQuery(`#${id}`).removeClass('show');
    jQuery('.modal-backdrop').remove();
}
