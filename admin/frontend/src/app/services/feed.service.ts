import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


import { API_URL } from './utils.service';


@Injectable({ providedIn: 'root' })
export class FeedService {

    constructor(
        private http: HttpClient
    ) {  }

    getLeagues() {
        return this.http.get(`${API_URL}/league/get_all_league`);
    }

    getTeams(params: object) {
        return this.http.post(`${API_URL}/teamroster/get_all_teamrosters`, params);
    }

    getActiveLeagues(params: object) {
        return this.http.post(`${API_URL}/teamroster/get_all_league`, params);
    }

    getAllTeams(params: object) {
        return this.http.post(`${API_URL}/season/get_all_team`, params);
    }

    getMatches(params: object) {
        return this.http.post(`${API_URL}/season/get_all_season_schedule`, params);
    }

    getPlayers(params: object) {
        return this.http.post(`${API_URL}/roster/get_all_roster`, params);
    }

    getPlayerPositions(params: object) {
        return this.http.post(`${API_URL}/roster/get_all_position`, params);
    }

    updatePlayerStatus(params: object) {
        return this.http.post(`${API_URL}/roster/update_roster`, params);
    }
}
