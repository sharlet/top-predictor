import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { API_URL } from './utils.service';


@Injectable({ providedIn: 'root' })
export class AuthService {
    private currentUserTokenSubject: BehaviorSubject<string>;
    public currentUserToken: Observable<string>;

    private isAuthenticatedSubject: BehaviorSubject<boolean>;
    public isAuthenticated: Observable<boolean>;

    constructor(
        private http: HttpClient
    ) {
        this.currentUserTokenSubject = new BehaviorSubject<string>(this.getToken());
        this.currentUserToken = this.currentUserTokenSubject.asObservable();

        const isLoggedIn = this.getToken().length > 0 ? true : false;
        this.isAuthenticatedSubject = new BehaviorSubject<boolean>(isLoggedIn);
        this.isAuthenticated = this.isAuthenticatedSubject.asObservable();
    }

    public login (data: object) {
        return this.http.post(`${API_URL}/login`, data)
                    .pipe( map( response => {
                        this.setAuthToken(response['Data'].session_key.toString());
                        return response;
                    }));

    }

    public logout() {
        return  this.http.get(`${API_URL}/logout`)
                    .pipe( map( response => {
                        this.removeAuthToken();
                        return response;
                    }));
    }

    public get isUserAuthenticated(): boolean {
        return this.isAuthenticatedSubject.value;
    }

    public get authenticatedToken(): string {
        return this.currentUserTokenSubject.value;
    }

    private setAuthToken(token: string) {
        localStorage.setItem('AuthToken', token);
        this.currentUserTokenSubject.next(token);
        this.isAuthenticatedSubject.next(true);
    }

    private getToken(): string {
        return localStorage.getItem('AuthToken') || '';
    }

    private removeAuthToken() {
        localStorage.clear();
        this.currentUserTokenSubject.next(null);
        this.isAuthenticatedSubject.next(false);
    }



}
