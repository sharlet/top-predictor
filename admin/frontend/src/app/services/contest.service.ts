import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { API_URL } from './utils.service';


@Injectable({ providedIn: 'root' })
export class ContestService {

    constructor(
        private http: HttpClient
    ) {  }

    getFilters() {
        return  this.http.post(`${API_URL}/contest/get_contest_filters`, {});
    }

    getContests(params: object) {
        return  this.http.post(`${API_URL}/contest/contest_list`, params);
    }

    getContestSettings() {
        return  this.http.post(`${API_URL}/contest/get_all_contest_data`, {});
    }

    getSeasonWeek(league_id: number) {
        return  this.http.post(`${API_URL}/contest/get_season_week`, {league_id});
    }

    getWeekMatchList(league_id: number, week: number) {
        return  this.http.post(`${API_URL}/contest/get_week_matches`, {league_id, week});
    }

}
