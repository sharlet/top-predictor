import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { PostAuthenticationModule } from './shared/layouts/post-authentication/post-authentication.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AlertComponent } from './alert/alert.component';

import { AuthorizationHeaderInterceptor, ErrorInterceptor } from './interceptors';

import { FantasyPipeModule } from './pipes/pipes.module';

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    LoginComponent,
    NotFoundComponent
],
  imports: [
    AppRoutingModule,
    BrowserModule,
    CommonModule,
    FantasyPipeModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthorizationHeaderInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
