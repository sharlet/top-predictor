import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
import { encryptPassword } from '../services/utils.service';
import { AlertService } from '../services/alert.service';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    submitted = false;

    constructor(
        private authService: AuthService,
        private router: Router,
        private formBuilder: FormBuilder,
        private alertService: AlertService
        ) { }

    ngOnInit() {
        if (this.authService.isUserAuthenticated) {
            this.router.navigate(['/dashboard']);
        } else {
            this.router.navigate(['/login']);
        }

        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]]
        });
    }

    // convenience getter for easy access to form fields
    get f() {
        return this.loginForm.controls;
    }

    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        const data = {
            email: this.f.email.value,
            password: encryptPassword(this.f.password.value)
        };
        this.authService.login(data).pipe(first())
            .subscribe( () => {
                    // this.router.navigate(['/dashboard']);
                    window.location.href = '/dashboard';
                }, err => {
                    this.alertService.error(err.error.email);
                    console.log('login component error ', err);
                }
            );
    }

}
