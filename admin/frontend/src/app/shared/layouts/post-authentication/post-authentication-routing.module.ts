import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from '../../../dashboard/dashboard.component';
import { PostAuthenticationComponent } from './post-authentication.component';
import { AuthGuard } from '../../../auth-guard/auth.guard';

const routes: Routes = [
    {
        path: '',
        component: PostAuthenticationComponent,
        canActivate: [AuthGuard],
        children: [
            { path: 'dashboard', component: DashboardComponent },
            { path: 'user', loadChildren: '../../../user/user.module#UserModule'},
            { path: 'contest', loadChildren: '../../../contest/contest.module#ContestModule'},
            { path: 'feed', loadChildren: '../../../feed/feed.module#FeedModule'}
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class PostAuthenticationRoutingModule {}

export const routedComponents: Array<any> = [
    PostAuthenticationComponent,
    DashboardComponent
];
