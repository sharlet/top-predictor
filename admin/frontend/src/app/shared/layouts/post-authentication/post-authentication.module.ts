import { NgModule } from '@angular/core';

import { PostAuthenticationRoutingModule, routedComponents } from './post-authentication-routing.module';
import { HeaderComponent } from '../../header/header.component';
import { SidebarComponent } from '../../sidebar/sidebar.component';
import { FooterComponent } from '../../footer/footer.component';

@NgModule({
    imports: [
        PostAuthenticationRoutingModule
    ],
    declarations: [
        ...routedComponents,
        HeaderComponent,
        SidebarComponent,
        FooterComponent
    ],
    providers: [
    ]
})
export class PostAuthenticationModule { }
