import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { ContestService } from '../../services/contest.service';

@Component({
  selector: 'app-contest-new',
  templateUrl: './contest-new.component.html',
  styleUrls: ['./contest-new.component.scss']
})
export class ContestNewComponent implements OnInit {

    public params = {
        league_id : 0,
        week: 0,
        salaryCap: 0
    };
    public leagues = [];
    public entryFeeRange = {};
    public sizeRange = [];
    public prizePool = [];
    public salaryCap = [];
    public seasonWeeks = [];
    public matchList = [];
    public selectedMatches = [];

    constructor(
        private consestService: ContestService
    ) { }

    ngOnInit() {
        this.getContestSettings();
    }

    getContestSettings() {
        this.consestService.getContestSettings().pipe(first())
        .subscribe( (response) => {
                this.initConstant(response['data']);
            }, err => {
                console.log('constest get contest setting error ', err);
            }
        );
    }

    private initConstant(data) {
        this.leagues = data.leagues;
        this.entryFeeRange = data.master_data_entry[0];
        this.sizeRange = data.master_data_entry[1];
        this.prizePool = data.prize_pool;
        this.salaryCap = data.salary_cap;
    }

    onLeagueChanges() {
        this.consestService.getSeasonWeek(this.params.league_id).pipe(first())
        .subscribe( (response) => {
                this.seasonWeeks = response['data'].season_week;
            }, err => {
                console.log('constest onLeagueChanges error ', err);
            }
        );
    }

    onWeekChanges() {
        this.consestService.getWeekMatchList(this.params.league_id, this.params.week).pipe(first())
        .subscribe( (response) => {
                this.matchList = response['data'].week_matches;
            }, err => {
                console.log('constest onWeekChanges error ', err);
            }
        );
    }

    toggleMatchSelection(matchId: number | string) {
        console.log('Match selected', matchId);
        if (matchId == 'all') {
            this.toggleSelectAllMatches();
        } else {
            this.toggleIndivisualMatches(matchId);
        }
    }

    private toggleSelectAllMatches() {
        if (this.selectedMatches.length ===  this.matchList.length ) {
            this.selectedMatches = [];
        } else {
            this.selectedMatches = this.matchList.map( match => match.season_game_unique_id );
        }
    }

    private toggleIndivisualMatches(matchId: number | string) {
        const index = this.selectedMatches.indexOf(matchId);
        if (index > -1) {
            this.selectedMatches.splice(index, 1);
        } else {
            this.selectedMatches.push(matchId);
        }
        console.log(matchId, this.selectedMatches);
    }
}
