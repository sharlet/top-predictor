import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { range } from '../../services/utils.service';
import { ContestService } from '../../services/contest.service';



@Component({
  selector: 'app-contest-list',
  templateUrl: './contest-list.component.html',
  styleUrls: ['./contest-list.component.scss']
})
export class ContestListComponent implements OnInit {

    public params = {
        itemsPerPage: 10,
        currentPage: 1,
        orderByField: 'contest_name',
        sortOrder: 'ASC',
        keyword: ''
    };
    public contestList = [];
    public totalContest = 0;
    public totalPaginationShow = [];
    public totalPages = 0;
    public contestStatus = {};
    public leagues = {};

    constructor(
        private contestService: ContestService
    ) { }

    ngOnInit() {
        this.getContestFilters();
    }

    private getContestFilters() {
        this.contestService.getFilters().pipe(first())
        .subscribe( (filters: [] ) => {
                this.contestStatus = filters['data'].contest_status;
                this.leagues = this.leagueRemapping(filters['data'].leagues);

                this.getContestList();
        }, (err: object) => {
                console.log('Contest List component error ', err);
            }
        );
    }

    public getContestList() {
        this.contestService.getContests(this.params).pipe(first())
        .subscribe( (contests: [] ) => {
                console.log(contests);
                this.contestList = contests['data'].contest;
                this.createPaginationItem(contests['data'].total);
        }, (err: object) => {
                console.log('Contest List component error ', err);
            }
        );
    }

    private createPaginationItem(totalContest: number) {
        this.totalContest = totalContest;
        const maxPages: number = Math.ceil(totalContest / this.params.itemsPerPage) ;
        const end = (this.params.currentPage + 5) < maxPages ?  this.params.currentPage + 5 : maxPages;
        const start = (this.params.currentPage - 5) > 1 ?  this.params.currentPage - 5 : 1;
        this.totalPages = maxPages;
        this.totalPaginationShow = range(end, start);
    }

    public paginateList(newPage: number) {
        if (this.params.currentPage === newPage) { return false; }
        this.params.currentPage = newPage;
        this.getContestList();
    }

    public nextOrPreviousPage(deviation: number) {
        this.params.currentPage = this.params.currentPage + deviation ;
        this.getContestList();
    }

    public searchFilter() {
        this.params.currentPage = 1;
        this.getContestList();
    }

    private leagueRemapping(leagues: []): object {
        return leagues.reduce((obj, item) => {
                    obj[item['league_id']] = item['league_abbr'];
                    return obj;
                }, {});
    }





}
