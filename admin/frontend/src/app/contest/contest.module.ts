import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ContestRoutingModule, routedComponents } from './contest-routing.module';


@NgModule({
    imports: [
        ContestRoutingModule,
        CommonModule,
        FormsModule
    ],
    declarations: [
        ...routedComponents
    ],
    providers: [
    ]
})
export class ContestModule { }
