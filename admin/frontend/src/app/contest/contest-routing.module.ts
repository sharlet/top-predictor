import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContestListComponent } from './list/contest-list.component';
import { ContestNewComponent } from './new/contest-new.component';

const routes: Routes = [
    { path: 'list', component: ContestListComponent },
    { path: 'new', component: ContestNewComponent },
    { path: '**', redirectTo: '/contest/list' }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ContestRoutingModule {}

export const routedComponents: Array<any> = [
    ContestListComponent,
    ContestNewComponent
];
