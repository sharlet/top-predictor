import { NgModule } from '@angular/core';
import { SafePipe } from './safe.pipe';

@NgModule({
    declarations: [
        SafePipe
    ],
    imports: [],
    exports: [
        SafePipe
    ],
    providers: [
        SafePipe
    ]
})
export class FantasyPipeModule {}
