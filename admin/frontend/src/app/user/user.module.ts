import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { UserRoutingModule, routedComponents } from './user-routing.module';


@NgModule({
    imports: [
        UserRoutingModule,
        CommonModule,
        FormsModule
    ],
    declarations: [
        ...routedComponents
    ],
    providers: [
    ]
})
export class UserModule { }
