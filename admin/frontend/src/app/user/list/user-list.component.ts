import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { UserService } from '../../services/user.service';

import { range } from '../../services/utils.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

    public params = {
        items_perpage: 10,
        current_page: 1,
        sort_field: 'name',
        sort_order: 'ASC',
        country: '',
        keyword: ''
    };
    public userList = [];
    public totalUsers = 0;
    public totalPaginationShow = [];
    public totalPages = 0;
    public userStatus = {
        '0': 'Inactive',
        '1': 'Active',
        '2': 'Not verfied email',
        '3': 'Banned',
        '4': 'Deleted'
    };

    constructor(
        private userService: UserService
    ) { }

    ngOnInit() {
        this.getUserList();
    }

    public  getUserList() {
        this.userService.getUsers(this.params).pipe(first())
        .subscribe( (user: [] ) => {
                this.userList = user['data'].result;
                this.createPaginationItem(user['data'].total);
        }, (err: object) => {
                console.log('User List component error ', err);
            }
        );
    }

    private createPaginationItem(totalUSer: number) {
        this.totalUsers = totalUSer;
        const maxPages: number = Math.ceil(totalUSer / this.params.items_perpage);
        const end = (this.params.current_page + 5) < maxPages ?  this.params.current_page + 5 : maxPages;
        const start = (this.params.current_page - 5) > 1 ?  this.params.current_page - 5 : 1;
        this.totalPages = maxPages;
        this.totalPaginationShow = range(end, start);
    }

    public paginateList(newPage: number) {
        if (this.params.current_page === newPage) { return false; }
        this.params.current_page = newPage;
        this.getUserList();
    }

    public nextOrPreviousPage(deviation: number) {
        this.params.current_page = this.params.current_page + deviation ;
        this.getUserList();
    }

    public searchFilter() {
        this.params.current_page = 1;
        this.getUserList();
    }





}
